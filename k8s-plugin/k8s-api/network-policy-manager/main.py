import asyncio
import logging
from kubernetes import client, config, watch
from  ..k8s.k8s_network_policy import K8sNetworkPolicy
from  ..k8s.k8s_pod import K8sPod
from  ..k8s.k8s_configmap import K8sConfigmap
from  ..k8s.k8s_namespace import K8sNamespace
import os

from ..commons.helpers import get_list_of_urls_in_string, process_urls_info, get_kubernetes_api_client, get_namespace

KUBERNETES_HOST = os.getenv('KUBERNETES_HOST')
KUBERNETES_TOKEN = os.getenv('KUBERNETES_TOKEN')




logging.basicConfig(level=logging.INFO, format='%(name)s - %(levelname)s - %(message)s')


POLICY_ANNOTATION = "ibm.policy/class"

namespace = get_namespace()
logging.info(" Namespace value from get_namespace() function is  : %s " % (namespace))
k8s_connection_client_map = get_kubernetes_api_client()
k8s_network_client =  k8s_connection_client_map["NetworkingV1Api"]
k8s_client = k8s_connection_client_map["CoreV1Api"]
logging.info(" Initialize kubernetes api client with success ")


k8sPod = K8sPod(k8s_client, namespace)
k8sConfigmap = K8sConfigmap(k8s_client, namespace)
k8sNetworkPolicy = K8sNetworkPolicy(k8s_network_client, namespace)
k8sNamespace = K8sNamespace(k8s_client)

namespaces = k8sNamespace.get_namespaces()
logging.info(" Number of namespaces in the kubernetes cluster  : %s " % (len(namespaces)))
pods = k8sPod.get_pods()
logging.info(" Number of pods in  %s namespace in the kubernetes cluster  : %s " % (namespace, len(pods.items)))



def get_annotated_pod_with_configmap(k8sConfigmap, namespaces, annotated_pods):
    pods_with_configmaps = []
    for annotated_pod in annotated_pods:
        configmap = k8sConfigmap.get_configmap_by_name(configmap_name = annotated_pod['annotated_value'])
        application_yaml_value = k8sConfigmap.get_configmap_application_yaml_data(configmap)
        raw_urls_data_infos = get_list_of_urls_in_string(application_yaml_value)
        process_urls_data_infos = process_urls_info(raw_urls_data_infos, namespaces)

        pods_with_configmaps.append({
            "pod": annotated_pod['pod'],
            "configmap": configmap,
            "application_yaml_value": application_yaml_value,
            "raw_urls_data_info" : raw_urls_data_infos,
            "process_urls_data_infos" : process_urls_data_infos
        })
    return pods_with_configmaps


def init_policy():
    annotated_pods = k8sPod.get_pods_for_annotations(pods_list = pods, annotation = POLICY_ANNOTATION)
    logging.info(" Number of pods in  %s namespace in the kubernetes cluster  with annotation %s : %s " % (namespace, POLICY_ANNOTATION, len(annotated_pods)))
    pods_with_configmaps = get_annotated_pod_with_configmap(k8sConfigmap, namespaces, annotated_pods)
    for pods_with_configmaps_item in pods_with_configmaps :
        logging.info(" pods_with_configmaps_item %s " % (pods_with_configmaps_item))
        k8sNetworkPolicy.create_network_policy(pods_with_configmaps_item['pod'], pods_with_configmaps_item['process_urls_data_infos'])



def event_pod(pod):
    annotated_pods = k8sPod.get_pods_for_annotations(pods_list =  [pod], annotation = POLICY_ANNOTATION)
    pods_with_configmaps = get_annotated_pod_with_configmap(k8sConfigmap, k8sNamespace.get_namespaces(), annotated_pods)
    for pods_with_configmaps_item in pods_with_configmaps :
        k8sNetworkPolicy.update_network_policy(pods_with_configmaps_item['pod'], pods_with_configmaps_item['process_urls_data_infos'])


#https://engineering.bitnami.com/articles/kubernetes-async-watches.html   https://medium.com/@sebgoa/kubernets-async-watches-b8fa8a7ebfd4
async def listen_pods():
    w = watch.Watch()
    for event in w.stream(func=k8s_client.list_namespaced_pod, namespace=namespace):
        pod = event['object']
        event_type = event['type'] # event.type: ADDED, MODIFIED, DELETED
        #logging.info("Event: %s %s %s" % (event_type,pod.kind, pod.metadata.name))
        if event_type == "ADDED" and pod.metadata.namespace == namespace and POLICY_ANNOTATION in pod.metadata.annotations:
            pods.items.append(pod)
            event_pod(pod)
        await asyncio.sleep(0)


async def listen_namespaces():
    logging.info(" watch 1 ")
    w = watch.Watch()
    logging.info(" watch 2 ")
    for event in w.stream(k8s_client.list_namespace):
        logging.info("Event: %s %s %s" % (event['type'], event['object'].kind, event['object'].metadata.name))
        namespace_object = event['object']
        event_type = event['type'] # event.type: ADDED, MODIFIED, DELETED
        if event_type == "ADDED" :
            namespaces.append(namespace_object)
        await asyncio.sleep(0)


if __name__ == '__main__':
    try:
        init_policy()
        ioloop = asyncio.get_event_loop()
        ioloop.create_task(listen_pods())
        ioloop.run_forever()
    except KeyboardInterrupt:
        tasks = asyncio.gather(*asyncio.Task.all_tasks(loop=ioloop), loop=ioloop, return_exceptions=True)
        tasks.add_done_callback(lambda t: ioloop.stop())
        tasks.cancel()

