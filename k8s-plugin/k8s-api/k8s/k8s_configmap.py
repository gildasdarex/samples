from kubernetes import client, config, watch
import logging

DEFAULT_TOKEN_PATH = "/run/secrets/kubernetes.io/serviceaccount/token"
DEFAULT_KUBERNETES_HOST = "https://kubernetes"
KUBERNETES_API_AUTHORIZATION_HEADER = "authorization"
CURRENT_NAMESPACE_FILE = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
POLICY_ANNOTATION = "kubernetes.io/policy.class"
APPLICATION_YAML = "application.yaml"

logging.basicConfig(level=logging.INFO, format='%(name)s - %(levelname)s - %(message)s')

class K8sConfigmap:
    def __init__(self, k8s_client = None, namespace = None):
        if k8s_client == None and namespace == None:
            config.load_incluster_config()
            self.api = client.CoreV1Api()
            self.init_namespace()
        else:
            self.api = k8s_client
            self.namespace = namespace



    def init_namespace(self):
        self.authorization = None
        with open(CURRENT_NAMESPACE_FILE, 'r') as namespace_file:
            self.namespace = namespace_file.read().replace('\n', '')

    def get_configmaps(self):
        configmaps_list = self.api.list_namespaced_config_map(self.namespace)
        return configmaps_list.items


    def get_configmap_by_name(self, configmaps = None, configmap_name = None):
        logging.info(" Get configmap associated with name : %s  " % (configmap_name))
        if configmaps is None:
            configmaps_list = self.api.list_namespaced_config_map(self.namespace)
            configmaps = configmaps_list.items
        for configmap in configmaps:
            if configmap.metadata.name == configmap_name :
                return configmap
        return None

    def get_configmap_application_yaml_data(self, configmap):
        return configmap.data[APPLICATION_YAML]

    def update_configmap_application_yaml_data(self, configmap, application_yaml_value):
         configmap.data[APPLICATION_YAML] = application_yaml_value
         return configmap

