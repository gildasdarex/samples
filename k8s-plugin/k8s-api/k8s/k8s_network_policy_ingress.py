from kubernetes import client, config, watch
from  commons.helpers import get_pods_urls_info, get_clean_pod_name



DEFAULT_TOKEN_PATH = "/run/secrets/kubernetes.io/serviceaccount/token"
DEFAULT_KUBERNETES_HOST = "https://kubernetes"
KUBERNETES_API_AUTHORIZATION_HEADER = "authorization"
CURRENT_NAMESPACE_FILE = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
POLICY_ANNOTATION = "kubernetes.io/policy.class"
APP_LABEL = "k8s-app"

#https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/NetworkingV1Api.md
class K8sNetworkPolicyIngress:
    def __init__(self, k8s_client = None, namespace = None):
        if k8s_client == None and namespace == None:
            config.load_incluster_config()
            self.api = client.CoreV1Api()
            self.init_namespace()
        else:
            self.api = k8s_client
            self.namespace = namespace



    def init_namespace(self):
        self.authorization = None
        with open(CURRENT_NAMESPACE_FILE, 'r') as namespace_file:
            self.namespace = namespace_file.read().replace('\n', '')


    # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1LabelSelector.md
    def add_pod_selector_rule(self, ingress_data_info):
        v1LabelSelector = client.V1LabelSelector()
        v1LabelSelector.match_labels = {
            APP_LABEL: ingress_data_info["pod_name"],
        }
        return v1LabelSelector


    def add_pod_selector_rule_with_pod_name(self, pod_to_give_access_name):
        v1LabelSelector = client.V1LabelSelector()
        v1LabelSelector.match_labels = {
            APP_LABEL: get_clean_pod_name(pod_to_give_access_name)
        }
        return v1LabelSelector


    # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1IPBlock.md
    def add_cidr_rule(self, ingress_data_info):
        v1IPBlock = client.V1IPBlock(ingress_data_info["cidr"])
        return v1IPBlock

    # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1NetworkPolicyPeer.md
    def create_network_policy_peer(self, ingress_data_info):
        v1NetworkPolicyPeer = client.V1NetworkPolicyPeer()
        if  "is_pod" in ingress_data_info:
            v1NetworkPolicyPeer.pod_selector  = self.add_pod_selector_rule(ingress_data_info)
        else:
            v1NetworkPolicyPeer.ip_block = self.add_cidr_rule(ingress_data_info)


    def create_default_network_policy_ingress_rules(self):
        return []


    def create_new_ingress_rule(self,  v1NetworkPolicyIngressRules, pod_to_give_access_name):
        v1NetworkPolicyIngressRule = self.check_if_rules_exists(v1NetworkPolicyIngressRules, pod_to_give_access_name)
        if v1NetworkPolicyIngressRule is not None:
            return None
        v1NetworkPolicyPeer = client.V1NetworkPolicyPeer()
        v1NetworkPolicyPeer.pod_selector = self.add_pod_selector_rule_with_pod_name(pod_to_give_access_name)
        v1NetworkPolicyPeers = [v1NetworkPolicyPeer]
        v1NetworkPolicyIngressRule = client.V1NetworkPolicyIngressRule()
        v1NetworkPolicyIngressRule._from = v1NetworkPolicyPeers
        return v1NetworkPolicyIngressRule


    def check_if_rules_exists(self,  v1NetworkPolicyIngressRules, pod_to_give_access_name):
        pod_clean_name = get_clean_pod_name(pod_to_give_access_name)
        for v1NetworkPolicyIngressRule in v1NetworkPolicyIngressRules:
            v1NetworkPolicyPeer = v1NetworkPolicyIngressRule._from[0]
            pod_selector = v1NetworkPolicyPeer.pod_selector
            if pod_selector is None or pod_selector.match_labels is None or APP_LABEL not in pod_selector.match_labels:
                continue
            if pod_selector.match_labels[APP_LABEL] == pod_clean_name:
                return v1NetworkPolicyIngressRule
        return None





    def create_network_policy_ingress_rules(self, ingress_data_infos):
        v1NetworkPolicyIngressRules = []
        for ingress_data_info in ingress_data_infos:
            v1NetworkPolicyPeer = self.create_network_policy_peer(ingress_data_info)
            v1NetworkPolicyPeers = [v1NetworkPolicyPeer]
            v1NetworkPolicyIngressRule = client.V1NetworkPolicyIngressRule()
            v1NetworkPolicyIngressRule._from = v1NetworkPolicyPeers
            v1NetworkPolicyIngressRules.append(v1NetworkPolicyIngressRule)

        return v1NetworkPolicyIngressRules
