from kubernetes import client, config, watch


DEFAULT_TOKEN_PATH = "/run/secrets/kubernetes.io/serviceaccount/token"
DEFAULT_KUBERNETES_HOST = "https://kubernetes"
KUBERNETES_API_AUTHORIZATION_HEADER = "authorization"
CURRENT_NAMESPACE_FILE = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
POLICY_ANNOTATION = "kubernetes.io/policy.class"


class K8sNamespace:
    def __init__(self, k8s_client = None):
        if k8s_client == None:
            config.load_incluster_config()
            self.api = client.CoreV1Api()
        else:
            self.api = k8s_client


    def get_namespaces(self):
        namespace_list = self.api.list_namespace()
        return namespace_list.items

    def get_namespace_by_name(self, name):
        namespaces = self.get_namespaces()
        for namespace in namespaces:
            if namespace.metadata.name == name :
                return True
        return False
