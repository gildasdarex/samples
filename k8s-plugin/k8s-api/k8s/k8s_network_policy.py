from kubernetes import client, config, watch
from  .k8s_network_policy_egress import K8sNetworkPolicyEgress
from  .k8s_network_policy_ingress import K8sNetworkPolicyIngress

import sys
sys.path.append("..")
from  commons.helpers import get_pods_urls_info, get_clean_pod_name




DEFAULT_TOKEN_PATH = "/run/secrets/kubernetes.io/serviceaccount/token"
DEFAULT_KUBERNETES_HOST = "https://kubernetes"
KUBERNETES_API_AUTHORIZATION_HEADER = "authorization"
CURRENT_NAMESPACE_FILE = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
POLICY_ANNOTATION = "kubernetes.io/policy.class"
NETWORK_POLICY_SUFFIX = "-network-policy"
APP_LABEL = "k8s-app"


#https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/NetworkingV1Api.md
class K8sNetworkPolicy:
    def __init__(self, k8s_client = None, namespace = None):
        if k8s_client == None and namespace == None:
            config.load_incluster_config()
            self.api = client.CoreV1Api()
            self.init_namespace()
        else:
            self.api = k8s_client
            self.namespace = namespace

    def init_namespace(self):
        self.authorization = None
        with open(CURRENT_NAMESPACE_FILE, 'r') as namespace_file:
            self.namespace = namespace_file.read().replace('\n', '')

    def get_network_policies(self):
        network_policies_list = self.api.list_namespaced_network_policy(self.namespace)
        return network_policies_list.items


    def get_network_policy_by_name(self, network_policy_name):
        network_policies = self.get_network_policies()
        for network_policy in network_policies:
            if network_policy.metadata.name == network_policy_name :
                return network_policy
        return None



    #https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1NetworkPolicySpec.md
    def create_default_network_policy(self, pod_name, pod_clean_name):
        v1NetworkPolicy = client.V1NetworkPolicy()
        # TODO - ADD COMMENT
        k8sNetworkPolicyEgress = K8sNetworkPolicyEgress(self.api, self.namespace)
        network_policy_egress_rules = k8sNetworkPolicyEgress.create_default_network_policy_egress_rules()
        # TODO - ADD COMMENT
        k8sNetworkPolicyIngress = K8sNetworkPolicyIngress(self.api, self.namespace)
        network_policy_ingress_rules = k8sNetworkPolicyIngress.create_default_network_policy_ingress_rules()
        # TODO - ADD COMMENT
        v1LabelSelector = client.V1LabelSelector()
        v1LabelSelector.match_labels = {
            APP_LABEL: pod_clean_name,
        }

        network_policy_spec = client.V1NetworkPolicySpec( pod_selector = v1LabelSelector)
        network_policy_spec.egress = network_policy_egress_rules
        network_policy_spec.ingress = network_policy_ingress_rules
        v1NetworkPolicy.spec = network_policy_spec

        v1ObjectMeta = client.V1ObjectMeta()
        v1ObjectMeta.name = "{}{}".format(pod_clean_name, NETWORK_POLICY_SUFFIX)
        v1ObjectMeta.namespace = self.namespace

        v1NetworkPolicy.metadata = v1ObjectMeta
        return v1NetworkPolicy


    def update_ingress_policy_of_pod_urls_info(self, network_policy, pod_to_add_in_ingress):
        v1NetworkPolicyIngressRules = network_policy.spec.ingress if network_policy.spec.ingress is not None else []
        k8sNetworkPolicyIngress = K8sNetworkPolicyIngress(self.api, self.namespace)
        network_policy_ingress_rule = k8sNetworkPolicyIngress.create_new_ingress_rule(v1NetworkPolicyIngressRules, pod_to_add_in_ingress)
        if network_policy_ingress_rule is not None:
            v1NetworkPolicyIngressRules.append(network_policy_ingress_rule)

        network_policy.spec.ingress=v1NetworkPolicyIngressRules
        return network_policy


    def update_ingress_policy_of_pod_urls_infos(self, url_data_infos, pod_to_add_in_ingress):
        pods_url_infos = get_pods_urls_info(url_data_infos)
        for pods_url_info in pods_url_infos:
            policy_name = "{}{}".format(pods_url_info["pod_name"], NETWORK_POLICY_SUFFIX)
            network_policy = self.get_network_policy_by_name(policy_name)
            if network_policy is None:
                network_policy = self.create_default_network_policy(pods_url_info["pod_name"] , pods_url_info["pod_name"])
                network_policy = self.api.create_namespaced_network_policy(namespace = self.namespace, body = network_policy)

            network_policy = self.update_ingress_policy_of_pod_urls_info(network_policy, pod_to_add_in_ingress)
            self.api.patch_namespaced_network_policy(name = network_policy.metadata.name, namespace = self.namespace, body = network_policy)



    def create_network_policy(self, pod, url_data_infos):
            pod_name = pod.metadata.name
            pod_clean_name = get_clean_pod_name(pod)
            v1NetworkPolicy = self.create_default_network_policy(pod_name , pod_clean_name)
            v1NetworkPolicy = self.api.create_namespaced_network_policy(namespace = self.namespace, body = v1NetworkPolicy)

            v1NetworkPolicyEgressRules = v1NetworkPolicy.spec.egress
            k8sNetworkPolicyEgress = K8sNetworkPolicyEgress(self.api, self.namespace)
            network_policy_egress_rules = k8sNetworkPolicyEgress.update_egress_rule_for_network_policy(v1NetworkPolicyEgressRules, url_data_infos)
            v1NetworkPolicy.spec.egress = network_policy_egress_rules
            v1NetworkPolicy = self.api.patch_namespaced_network_policy(name = v1NetworkPolicy.metadata.name, namespace = self.namespace, body = v1NetworkPolicy)


            self.update_ingress_policy_of_pod_urls_infos(url_data_infos, pod)
            return v1NetworkPolicy


    def update_network_policy(self, pod, url_data_infos):
        pod_name = pod.metadata.name
        pod_clean_name = get_clean_pod_name(pod)

        policy_name = "{}{}".format(pod_clean_name, NETWORK_POLICY_SUFFIX)
        v1NetworkPolicy = self.get_network_policy_by_name(policy_name)

        v1NetworkPolicyEgressRules = v1NetworkPolicy.spec.egress
        k8sNetworkPolicyEgress = K8sNetworkPolicyEgress(self.api, self.namespace)
        network_policy_egress_rules = k8sNetworkPolicyEgress.update_egress_rule_for_network_policy(v1NetworkPolicyEgressRules, url_data_infos)
        v1NetworkPolicy.spec.egress = network_policy_egress_rules
        v1NetworkPolicy = self.api.patch_namespaced_network_policy(name = v1NetworkPolicy.metadata.name, namespace = self.namespace, body = v1NetworkPolicy)

        self.update_ingress_policy_of_pod_urls_infos(url_data_infos, pod)
        return v1NetworkPolicy





