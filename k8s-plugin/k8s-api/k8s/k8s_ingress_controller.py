from kubernetes import client, config, watch


DEFAULT_TOKEN_PATH = "/run/secrets/kubernetes.io/serviceaccount/token"
DEFAULT_KUBERNETES_HOST = "https://kubernetes"
KUBERNETES_API_AUTHORIZATION_HEADER = "authorization"
CURRENT_NAMESPACE_FILE = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
POLICY_ANNOTATION = "kubernetes.io/policy.class"
APP_LABEL = "k8s-app"
DEFAULT_DNS = "10.0.179.163/32" # TODO UPDATE DNS INFO

#https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/NetworkingV1Api.md
class K8sIngressController:
    def __init__(self, k8s_client = None, namespace = None):
        if k8s_client == None and namespace == None:
            config.load_incluster_config()
            self.api = client.ExtensionsV1beta1Api()
            self.init_namespace()
        else:
            self.api = k8s_client
            self.namespace = namespace



    def init_namespace(self):
        self.authorization = None
        with open(CURRENT_NAMESPACE_FILE, 'r') as namespace_file:
            self.namespace = namespace_file.read().replace('\n', '')


    def get_ingress_controller(self):
        api_response = self.api.list_ingress_for_all_namespaces()
        return api_response.items


    def get_list_of_path_and_associated_pod(self, ingress_controllers = None):
        if ingress_controllers is None:
            ingress_controllers = self.get_ingress_controller()
        ingress_path_pods = []
        for ingress in ingress_controllers:
            extensionsV1beta1IngressSpec = ingress.spec
            extensionsV1beta1IngressRules = extensionsV1beta1IngressSpec.rules
            for extensionsV1beta1IngressRule in extensionsV1beta1IngressRules:
                host = extensionsV1beta1IngressRule.host
                extensionsV1beta1HTTPIngressRuleValue = extensionsV1beta1IngressRule.http
                extensionsV1beta1HTTPIngressPathList = extensionsV1beta1HTTPIngressRuleValue.paths
                for extensionsV1beta1HTTPIngressPath in extensionsV1beta1HTTPIngressPathList:
                    extensionsV1beta1IngressBackend = extensionsV1beta1HTTPIngressPath.backend
                    path = extensionsV1beta1HTTPIngressPath.path
                    service_name = extensionsV1beta1IngressBackend.service_name
                    service_port = extensionsV1beta1IngressBackend.service_port

                    ingress_path_pods.append({
                        path: {
                            "path": path,
                            "service_name": service_name,
                            "service_port" : service_port,
                            "host" : host,
                            "k8s_api" : "http://{}.{}:{}".format(service_name, self.namespace, service_port)
                        }
                    })
        return ingress_path_pods




