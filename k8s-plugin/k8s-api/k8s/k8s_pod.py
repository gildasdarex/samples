from kubernetes import client, config, watch
import logging

DEFAULT_TOKEN_PATH = "/run/secrets/kubernetes.io/serviceaccount/token"
DEFAULT_KUBERNETES_HOST = "https://kubernetes"
KUBERNETES_API_AUTHORIZATION_HEADER = "authorization"
CURRENT_NAMESPACE_FILE = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
POLICY_ANNOTATION = "kubernetes.io/policy.class"

logging.basicConfig(level=logging.INFO, format='%(name)s - %(levelname)s - %(message)s')


class K8sPod:
    def __init__(self, k8s_client = None, namespace = None):
        if k8s_client == None and namespace == None:
            config.load_incluster_config()
            self.api = client.CoreV1Api()
            self.init_namespace()
        else:
            self.api = k8s_client
            self.namespace = namespace



    def init_namespace(self):
        self.authorization = None
        with open(CURRENT_NAMESPACE_FILE, 'r') as namespace_file:
            self.namespace = namespace_file.read().replace('\n', '')

    def get_pods(self):
        pods = self.api.list_namespaced_pod(self.namespace)
        return pods

    def get_pods_for_annotations(self, pods_list = None , annotation = POLICY_ANNOTATION):
        logging.info(" Get pods with annotation  %s in namespace in the kubernetes cluster  : %s " % (annotation, self.namespace))
        if pods_list is None:
            pods_list = self.api.list_namespaced_pod(self.namespace)
        annotated_pods = []
        pods = pods_list.items if hasattr(pods_list, 'items') else pods_list
        #for pod in pods_list.items:
        for pod in pods:
            logging.info(" Check if pod %s has annotation %s . Annotation list is %s " % (pod.metadata.name, annotation, pod.metadata.annotations ))

            if annotation in pod.metadata.annotations:
                annotated_pods.append({
                    "pod": pod,
                    "annotated_value": pod.metadata.annotations[annotation]
                })
        return annotated_pods
                # print(pod.metadata.annotations[POLICY_ANNOTATION])

