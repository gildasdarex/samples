from kubernetes import client, config, watch


DEFAULT_TOKEN_PATH = "/run/secrets/kubernetes.io/serviceaccount/token"
DEFAULT_KUBERNETES_HOST = "https://kubernetes"
KUBERNETES_API_AUTHORIZATION_HEADER = "authorization"
CURRENT_NAMESPACE_FILE = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
POLICY_ANNOTATION = "kubernetes.io/policy.class"
APP_LABEL = "k8s-app"
DEFAULT_DNS = "10.0.179.163/32" # TODO UPDATE DNS INFO

#https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/NetworkingV1Api.md
class K8sNetworkPolicyEgress:
    def __init__(self, k8s_client = None, namespace = None):
        if k8s_client == None and namespace == None:
            config.load_incluster_config()
            self.api = client.CoreV1Api()
            self.init_namespace()
        else:
            self.api = k8s_client
            self.namespace = namespace



    def init_namespace(self):
        self.authorization = None
        with open(CURRENT_NAMESPACE_FILE, 'r') as namespace_file:
            self.namespace = namespace_file.read().replace('\n', '')


    # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1LabelSelector.md
    def add_pod_selector_rule(self, egress_data_info):
        v1LabelSelector = client.V1LabelSelector()
        v1LabelSelector.match_labels = {
            APP_LABEL: egress_data_info["pod_name"],
        }
        return v1LabelSelector

    # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1IPBlock.md
    def add_cidr_rule(self, egress_data_info):
        v1IPBlock = client.V1IPBlock(egress_data_info["cidr"])
        return v1IPBlock

    # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1NetworkPolicyPeer.md
    def create_network_policy_peer(self, egress_data_info):
        v1NetworkPolicyPeer = client.V1NetworkPolicyPeer()
        if  "is_pod" in egress_data_info:
            v1NetworkPolicyPeer.pod_selector  = self.add_pod_selector_rule(egress_data_info)
        else:
            v1NetworkPolicyPeer.ip_block = self.add_cidr_rule(egress_data_info)
        return v1NetworkPolicyPeer



    def create_default_network_policy_egress_rules(self):
        v1IPBlock = client.V1IPBlock( cidr = DEFAULT_DNS)
        v1NetworkPolicyPeer = client.V1NetworkPolicyPeer()
        v1NetworkPolicyPeer.ip_block = v1IPBlock
        v1NetworkPolicyPeers = [v1NetworkPolicyPeer]
        v1NetworkPolicyEgressRule = client.V1NetworkPolicyEgressRule()
        v1NetworkPolicyEgressRule.to = v1NetworkPolicyPeers
        return [v1NetworkPolicyEgressRule]



    def create_network_policy_egress_rules(self, egress_data_infos):
        v1NetworkPolicyEgressRules = []
        for egress_data_info in egress_data_infos:
            v1NetworkPolicyPeer = self.create_network_policy_peer(egress_data_info)
            v1NetworkPolicyPeers = [v1NetworkPolicyPeer]
            v1NetworkPolicyEgressRule = client.V1NetworkPolicyEgressRule()
            v1NetworkPolicyEgressRule.to = v1NetworkPolicyPeers
            v1NetworkPolicyEgressRules.append(v1NetworkPolicyEgressRule)
        return v1NetworkPolicyEgressRules

    def add_network_policy_egress_rules(self, v1NetworkPolicyEgressRules, egress_data_info):
        v1NetworkPolicyPeer = self.create_network_policy_peer(egress_data_info)
        v1NetworkPolicyPeers = [v1NetworkPolicyPeer]
        v1NetworkPolicyEgressRule = client.V1NetworkPolicyEgressRule()
        v1NetworkPolicyEgressRule.to = v1NetworkPolicyPeers
        v1NetworkPolicyEgressRules.append(v1NetworkPolicyEgressRule)
        return v1NetworkPolicyEgressRules


    def clean_network_policy_egress_rules(self, v1NetworkPolicyEgressRule, egress_data_infos):
        pass



    def check_if_rule_exists(self, v1NetworkPolicyEgressRule, egress_data_info):
        v1NetworkPolicyPeer = v1NetworkPolicyEgressRule.to[0]
        if  "is_pod" in egress_data_info  and v1NetworkPolicyPeer.pod_selector is not None:
            pod_name = v1NetworkPolicyPeer.pod_selector.match_labels[APP_LABEL]
            if pod_name == egress_data_info["pod_name"]:
                return v1NetworkPolicyEgressRule
        else:
            cidr = v1NetworkPolicyPeer.ip_block.cidr
            if "cidr" in egress_data_info and cidr == egress_data_info["cidr"]:
                return v1NetworkPolicyEgressRule
        return None


    def check_if_rule_exists_in_all(self, v1NetworkPolicyEgressRules, egress_data_info):
        selectedV1NetworkPolicyEgressRule = None
        for v1NetworkPolicyEgressRule in v1NetworkPolicyEgressRules:
            selectedV1NetworkPolicyEgressRule = self.check_if_rule_exists(v1NetworkPolicyEgressRule, egress_data_info)
            if selectedV1NetworkPolicyEgressRule is not None:
                return selectedV1NetworkPolicyEgressRule
        return selectedV1NetworkPolicyEgressRule


    def check_if_policy_is_deleted(self, v1NetworkPolicyEgressRule, egress_data_infos):
        v1NetworkPolicyPeer = v1NetworkPolicyEgressRule.to[0]
        if v1NetworkPolicyPeer.ip_block is not None:
            for egress_data_info in egress_data_infos:
                if  "is_pod" not in  egress_data_info:
                    if v1NetworkPolicyPeer.ip_block.cidr == egress_data_info["cidr"]:
                        return True
        else:
            for egress_data_info in egress_data_infos:
                if   "is_pod" in egress_data_info:
                    if v1NetworkPolicyPeer.pod_selector.match_labels[APP_LABEL] == egress_data_info["pod_name"]:
                        return True
        return False



    def update_egress_rule_for_network_policy(self, v1NetworkPolicyEgressRules, egress_data_infos):
        newV1NetworkPolicyEgressRules = []
        if v1NetworkPolicyEgressRules is None:
            v1NetworkPolicyEgressRules  = []
        for v1NetworkPolicyEgressRule in v1NetworkPolicyEgressRules:
            if not self.check_if_policy_is_deleted(v1NetworkPolicyEgressRule, egress_data_infos):
                newV1NetworkPolicyEgressRules.append(v1NetworkPolicyEgressRule)
        v1NetworkPolicyEgressRules = newV1NetworkPolicyEgressRules
        missingRules = []
        for egress_data_info in egress_data_infos:
            selectedV1NetworkPolicyEgressRule = self.check_if_rule_exists_in_all(v1NetworkPolicyEgressRules, egress_data_info)
            if selectedV1NetworkPolicyEgressRule is None:
                missingRules.append(egress_data_info)
        for missingRule in missingRules:
            v1NetworkPolicyEgressRules = self.add_network_policy_egress_rules(v1NetworkPolicyEgressRules, missingRule)
        return newV1NetworkPolicyEgressRules

