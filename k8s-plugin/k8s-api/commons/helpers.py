from urlextract import URLExtract
from urllib.parse import urljoin, urlparse, urlsplit
import socket
from kubernetes import client, config, watch
import os
import logging


CURRENT_NAMESPACE_FILE = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"

logging.basicConfig(level=logging.INFO, format='%(name)s - %(levelname)s - %(message)s')


def get_host_name_ip(hostanme):
    logging.info( " Get ip if hostname : %s " % (hostanme))
    try:
        ip = socket.gethostbyname(hostanme)
        return ip
    except:
        logging.error( " Failed to get ip if hostname : %s " % (hostanme))
        return "0.0.0.0"

def get_list_of_urls_in_string(str):
    extractor = URLExtract()
    urls = extractor.find_urls(str, only_unique=True)
    data = []
    for url in urls:
        #new_url = urljoin(url, urlparse(url).path)  # 'http://example.com/'
        url_tokens = urlsplit(url) # ParseResult(scheme='https', netloc='esb-qua.cma-cgm.com', path='/XIAxisAdapter/MessageServlet', params='', query='', fragment='')  ParseResult(scheme='https', netloc='auth-int.cma-cgm.com:9031', path='/ext/jwks', params='', query='', fragment='')
        data.append(url_tokens)
    return data

def get_url_info_hostname(url_info):
    hostname = url_info.hostname
    if hostname is not None:
        return hostname
    path = url_info.path
    separator = path.find(':')
    return path[slice(separator)]

def get_url_info_port(url_info, hostname):
    port = url_info.port
    if port is not None:
        return port
    path = url_info.path
    hostname = hostname + ":"
    return path.replace(hostname, '')


def process_urls_info(urls_infos, namespaces):
    data = []
    print(urls_infos)
    for url_info in urls_infos:
        logging.info( " Process url data  : %s %s %s " % (url_info.hostname, url_info.port, url_info.netloc))
        data_dict = {}
        data_dict["hostname"] = get_url_info_hostname(url_info)
        data_dict["port"] = get_url_info_port(url_info, data_dict["hostname"])
        logging.info( " info  : %s %s " % (data_dict["hostname"], data_dict["port"]))

        for namespace in namespaces:
            name = namespace.metadata.name
            domain = ".{}".format(name)
            if domain in data_dict["hostname"]:
                data_dict["namespace"] = name
                data_dict["is_pod"] = True
                data_dict["pod_name"] = data_dict["hostname"].replace(domain,'')
                break
        if "is_pod" not in data_dict:
            ip = get_host_name_ip(url_info.hostname)
            if ip == "0.0.0.0":
                continue
            data_dict["ip"] = ip
            data_dict["cidr"] = "{}/32".format(data_dict["ip"])
        data.append(data_dict)
    return data


def get_pods_urls_info(urls_infos):
    data = []
    for url_info in urls_infos:
        if "is_pod" in url_info:
            data.append(url_info)
    return data

def get_default_kubernetes_client():
    config.load_incluster_config()
    api = client.CoreV1Api()
    networkApi = client.NetworkingV1Api()
    return {
        "CoreV1Api": client.CoreV1Api(),
        "NetworkingV1Api": client.NetworkingV1Api(),
        "ExtensionsV1beta1Api": client.ExtensionsV1beta1Api()
    }

def get_namespace():
    logging.info(" Inside get namespace function ")
    KUBERNETES_NAMESPACE = os.getenv('KUBERNETES_NAMESPACE')
    logging.info( " KUBERNETES_NAMESPACE env variable value is : %s " % (KUBERNETES_NAMESPACE))
    if KUBERNETES_NAMESPACE is  not None:
        return KUBERNETES_NAMESPACE
    else:
        logging.info(" Read namespace from /var/run/secrets/kubernetes.io/serviceaccount/namespace ")
        namespace = "default"
        with open(CURRENT_NAMESPACE_FILE, 'r') as namespace_file:
            namespace = namespace_file.read().replace('\n', '')
        return namespace



#https://github.com/kubernetes-client/python/blob/master/examples/remote_cluster.py
def get_kubernetes_client(kubernetes_host, kubernetes_token):
    configuration = client.Configuration()
    configuration.host = kubernetes_host
    configuration.verify_ssl = False
    configuration.api_key = {"authorization": "Bearer " + kubernetes_token}

    aApiClient = client.ApiClient(configuration)
    return {
        "CoreV1Api": client.CoreV1Api(aApiClient),
        "NetworkingV1Api": client.NetworkingV1Api(aApiClient),
        "ExtensionsV1beta1Api": client.ExtensionsV1beta1Api(aApiClient)
    }



def get_kubernetes_api_client():
    logging.info(" Inside get_kubernetes_api_client function ")
    KUBERNETES_HOST = os.getenv('KUBERNETES_HOST')
    KUBERNETES_TOKEN = os.getenv('KUBERNETES_TOKEN')
    logging.info( " KUBERNETES_HOST , KUBERNETES_TOKEN env variables value are : %s %s " % (KUBERNETES_HOST, KUBERNETES_TOKEN))
    if KUBERNETES_HOST is None or KUBERNETES_TOKEN is None:
        return get_default_kubernetes_client()
    else:
        return get_kubernetes_client(KUBERNETES_HOST, KUBERNETES_TOKEN)



def get_clean_pod_name(pod):
    pod_name = pod.metadata.name
    pod_generated_name = pod.metadata.generate_name
    pod_template_hash = pod.metadata.labels["pod-template-hash"]
    remove_string = "-"+pod_template_hash+"-"
    pod_clean_name = pod_generated_name.replace(remove_string, "")
    return pod_clean_name


