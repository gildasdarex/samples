import asyncio
import logging
from kubernetes import client, config, watch
from  ..k8s.k8s_configmap import K8sConfigmap
from  ..k8s.k8s_namespace import K8sNamespace
from  ..k8s.k8s_ingress_controller import K8sIngressController
from urllib.parse import urlunsplit
import os

from ..commons.helpers import get_list_of_urls_in_string, process_urls_info, get_kubernetes_api_client, get_namespace

KUBERNETES_HOST = os.getenv('KUBERNETES_HOST')
KUBERNETES_TOKEN = os.getenv('KUBERNETES_TOKEN')




logging.basicConfig(level=logging.INFO, format='%(name)s - %(levelname)s - %(message)s')


POLICY_ANNOTATION = "ibm.policy/class"

namespace = get_namespace()
logging.info(" Namespace value from get_namespace() function is  : %s " % (namespace))


k8s_connection_client_map = get_kubernetes_api_client()
k8s_network_client =  k8s_connection_client_map["NetworkingV1Api"]
k8s_client = k8s_connection_client_map["CoreV1Api"]
k8s_extensions_v1_beta1_api = k8s_connection_client_map["ExtensionsV1beta1Api"]

logging.info(" Initialize kubernetes api client with success ")


k8sIngressController = K8sIngressController(k8s_extensions_v1_beta1_api, namespace)
k8sConfigmap = K8sConfigmap(k8s_client, namespace)
k8sNamespace = K8sNamespace(k8s_client)

namespaces = k8sNamespace.get_namespaces()
logging.info(" Number of namespaces in the kubernetes cluster  : %s " % (len(namespaces)))
k8sIngressControllerList = k8sIngressController.get_ingress_controller()
logging.info(" Number of ingress controller in  %s namespace in the kubernetes cluster  : %s " % (namespace, len(k8sIngressControllerList)))

ingress_path_pods = k8sIngressController.get_list_of_path_and_associated_pod(k8sIngressControllerList)


def get_configmap_and_urls_info(k8sConfigmap):
    configmaps_list = k8sConfigmap.get_configmaps()
    configmaps_with_url_infos = []
    for configmap in configmaps_list:
        application_yaml_value = k8sConfigmap.get_configmap_application_yaml_data(configmap)
        raw_urls_data_infos = get_list_of_urls_in_string(application_yaml_value)

        configmaps_with_url_infos.append({
            "configmap": configmap,
            "application_yaml_value": application_yaml_value,
            "raw_urls_data_info" : raw_urls_data_infos
        })
    return configmaps_with_url_infos


def update_application_yaml(application_yaml_value, raw_urls_data_info, ingress_path_pod):
    url = urlunsplit(raw_urls_data_info)
    url_copy = url
    url_copy = url_copy.replace(raw_urls_data_info.netloc, ingress_path_pod["k8s_api"] )
    path = raw_urls_data_info.path
    new_path = path.replace(ingress_path_pod["path"], "")
    url_copy = url_copy.replace(path, new_path )

    application_yaml_value = application_yaml_value.replace(url, url_copy)
    return application_yaml_value



def process_data(configmaps_with_url_info, ingress_path_pods):
    application_yaml_value = configmaps_with_url_info["application_yaml_value"]
    raw_urls_data_infos = configmaps_with_url_info["raw_urls_data_info"]
    for raw_urls_data_info in raw_urls_data_infos:
        for path in ingress_path_pods:
            if path in raw_urls_data_info.path:
                application_yaml_value = update_application_yaml(application_yaml_value, raw_urls_data_info, ingress_path_pods["path"])
                break
    return application_yaml_value


def generate_configmap():
    configmaps_with_url_infos = get_configmap_and_urls_info(k8sConfigmap)
    for configmaps_with_url_info in configmaps_with_url_infos:
        old_application_yaml_value = configmaps_with_url_info["application_yaml_value"]
        application_yaml_value = process_data(configmaps_with_url_info, ingress_path_pods)
        if old_application_yaml_value is not application_yaml_value:
            configmap = k8sConfigmap.update_configmap_application_yaml_data(configmaps_with_url_info["configmap"], application_yaml_value)




if __name__ == '__main__':
    generate_configmap()


