data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners = [
    "amazon"]

  filter {
    name = "name"
    values = [
      "amzn2-ami-ecs-hvm-*-x86_64-ebs"]
  }
}

module "vpc" {
  source     = "git::https://github.com/cloudposse/terraform-aws-vpc.git?ref=0.11/master"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = "${var.attributes}"
  tags       = "${var.tags}"
  cidr_block = "${var.vpc_cidr_block}"
  enable_classiclink_dns_support = true
  enable_classiclink = true
  enable_dns_hostnames = true
}



module "subnets" {
  source              = "git::https://github.com/cloudposse/terraform-aws-dynamic-subnets.git?ref=0.30.0"
  availability_zones  =  var.availability_zones
  namespace           = "${var.namespace}"
  stage               = "${var.stage}"
  name                = "${var.name}"
  attributes          = "${var.attributes}"
  tags                = "${var.tags}"
  vpc_id              = "${module.vpc.vpc_id}"
  igw_id              = "${module.vpc.igw_id}"
  cidr_block          = "${module.vpc.vpc_cidr_block}"
  nat_gateway_enabled = "true"
}



module "security" {
  source              = "../security-group"
  tags                = var.tags
  name        = var.name
  vpc_id              = module.vpc.vpc_id
  vpc_cidr_block              = ["${var.vpc_cidr_block}"]
}

module "iam" {
  source              = "../iam"
  tags                = var.tags
  name                = var.name
}




data "template_file" "ami_id" {
  template = coalesce(lookup(var.cluster_instance_amis, var.region), data.aws_ami.amazon_linux_2.image_id)
}

data "template_file" "cluster_user_data" {
  template = coalesce(var.cluster_instance_user_data_template, file("${path.module}/cluster.tpl"))
  vars = {
    cluster_name = var.name
  }
}

resource "aws_launch_configuration" "cluster" {
  name_prefix = "cluster-${var.name}"
  image_id = data.template_file.ami_id.rendered
  instance_type = var.cluster_instance_type
  key_name = var.ssh_key_name

  iam_instance_profile = module.iam.aws_iam_instance_profile

  user_data = data.template_file.cluster_user_data.rendered

  security_groups = concat(list(module.security.security_group_id), var.security_groups)

  associate_public_ip_address = var.associate_public_ip_addresses == "yes" ? true : false


  root_block_device {
    volume_size = var.cluster_instance_root_block_device_size
    volume_type = var.cluster_instance_root_block_device_type
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "cluster" {
  name_prefix = "asg-${var.name}"
  vpc_zone_identifier = module.subnets.private_subnet_ids
  launch_configuration = aws_launch_configuration.cluster.name
  min_size = var.cluster_minimum_size
  max_size = var.cluster_maximum_size
  desired_capacity = var.cluster_desired_capacity
}

resource "aws_ecs_cluster" "cluster" {
  name = "${var.name}"
  tags       = "${var.tags}"
}