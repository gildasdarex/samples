require 'test_helper'

class GalleryControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get gallery_show_url
    assert_response :success
  end

end
