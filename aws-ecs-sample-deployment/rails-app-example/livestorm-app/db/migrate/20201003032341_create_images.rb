class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.string :photo
      t.string :ip_address
      t.integer :views, default: 0

      t.timestamps
    end
  end
end
