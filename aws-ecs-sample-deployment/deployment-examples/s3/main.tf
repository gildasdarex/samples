provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "tdarex.dev.ops"
  acl    = "private"

  tags = {
    Name        = "tdarex-Livestorm-test-bucket"
    Environment = "Dev"
  }
}