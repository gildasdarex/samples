

provider "aws" {
  region = "us-east-1"
  profile = "st"
}

#
#
#
#
resource "aws_eip" "default" {
  instance = "${aws_instance.vpnhost.id}"
  vpc      = true
}

# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "sonarvpn" {
  name        = "sonarvpn"
  description = "Used in the terraform"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port   = 943
    to_port     = 943
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # UDP access from anywhere
  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "vpnhost" {
  instance_type = "t2.nano"

  # Lookup the correct AMI based on the region
  # we specified
  ami = "ami-0f9cf087c1f27d9b1"

  key_name = "sonar-prod-vpn"

  #subnet_id = "subnet-0366341b9cbdd992f"
# Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.sonarvpn.id}"]

  user_data = "${file("userdata.sh")}"


  #Instance tags
  tags = {
    Name = "sonarvpn"
  }
}



module "ssh_key_pair" {
  source                = "git::https://github.com/cloudposse/terraform-aws-key-pair.git?ref=master"
  namespace             = "sonar"
  stage                 = "prod"
  name                  = "vpn"
  ssh_public_key_path   = "./secrets"
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
  chmod_command         = "chmod 600 %v"
}