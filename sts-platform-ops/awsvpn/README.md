# Sonar Trade VPN

 Setup SonarTrade's VPN.
 - Creates a nano server
 - security groups, for vpn access
 - generates the keypair for direct ssh to server

 This will deploy to the AWS account's default vpc. 
 So we can peer to other VPC's, with just one VPN.


### Deploying VPN

Steps to deploy a custom hosted VPN server.


#### Steps

```bash
terraform apply

# After deploying, need to ssh in, and setup VPN server

```
- https://hackernoon.com/using-a-vpn-server-to-connect-to-your-aws-vpc-for-just-the-cost-of-an-ec2-nano-instance-3c81269c71c2


### Clients
To Add new clients :
```bash
```
To Revoke existing clients:
```bash
```

### References 
- https://tunnelblick.net/downloads.html
