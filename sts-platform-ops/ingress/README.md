# ALB Ingress
Leveraging AWS-ALB-Ingress Controller.
- https://kubernetes-sigs.github.io/aws-alb-ingress-controller/  

This will create a few AWS resources, giving us an AWS loadbalancer to access our services. 




### Quick Deploy
NOTE: update the alb-ingress yaml with your AWS Key/Secret. (for now, later it wont be required)

Apply these configs, AFTER all the micro services are deployed
```bash
kubectl apply -f ./ingress/rbac-role.yaml
kubectl apply -f ./ingress/alb-ingress-controller.yaml

# once this is up, we link the services to the alb, with:
kubectl apply -f ./micros/01-combined-ingress.yaml
```

### Undeploy
BEFORE you destroy the terraform environment, delete the ALB config
```bash
kubectl delete -f ./micros/01-combined-ingress.yaml
kubectl delete -f ./ingress/alb-ingress-controller.yaml
```

..

---

## References
- https://kubernetes-sigs.github.io/aws-alb-ingress-controller/
- https://kubernetes-sigs.github.io/aws-alb-ingress-controller/guide/ingress/annotation/#annotations
- https://kubernetes-sigs.github.io/aws-alb-ingress-controller/guide/ingress/annotation/#target-group-attributes
- https://medium.com/@gregory.guillou/aws-eks-with-terraform-and-the-alb-ingress-controller-2202d931d2d2
- https://github.com/gregoryguillou/eks-workshop/blob/master/02-loadbalancer/alb-ingress-controller.yaml
- https://gist.github.com/micahhausler/4f3a2ee540f5714e6dd91b4bacace3ae