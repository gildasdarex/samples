#!/usr/bin/env bash

send_sms(){
  escapedText=$(echo $1 | sed 's/"/\"/g' | sed "s/'/\'/g" )
  json="{\"channel\": \"#$1\", \"text\": \"$escapedText\"}"
  curl -s -d "payload=$json" "https://hooks.slack.com/services/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"
}

export PATH=/home/ubuntu/.local/bin:$PATH

start_date=`aws ce get-cost-and-usage --time-period Start=$(date -u +"%Y-%m-%d" --date="-2 day"),End=$(date -u +"%Y-%m-%d" --date="-1 day") --granularity DAILY --metrics UnblendedCost | jq '.ResultsByTime[0].TimePeriod.Start'`
end_date=`aws ce get-cost-and-usage --time-period Start=$(date -u +"%Y-%m-%d" --date="-2 day"),End=$(date -u +"%Y-%m-%d" --date="-1 day") --granularity DAILY --metrics UnblendedCost | jq '.ResultsByTime[0].TimePeriod.End'`
amount=`aws ce get-cost-and-usage --time-period Start=$(date -u +"%Y-%m-%d" --date="-2 day"),End=$(date -u +"%Y-%m-%d" --date="-1 day") --granularity DAILY --metrics UnblendedCost | jq '.ResultsByTime[0].Total.UnblendedCost.Amount'`

message=" Aws cost usage from $start_date  to  $end_date  is  $amount USD"

echo $message

start_date="${start_date%\"}"
start_date="${start_date#\"}"

end_date="${end_date%\"}"
end_date="${end_date#\"}"

amount="${amount%\"}"
amount="${amount#\"}"

amount_50="50"
amount_60="60"
amount_75="75"

send_sms " Aws cost usage from $start_date  to  $end_date  is  $amount USD"
#daily_50=$(bc <<< "$amount - $amount_50")
#echo $daily_50

if (( $(echo "$amount > $amount_50" |bc -l) )); then
  send_sms " Aws cost usage from $start_date  to  $end_date  is higher than 50 USD"
fi

if (( $(echo "$amount > $amount_60" |bc -l) )); then
  send_sms " Aws cost usage from $start_date  to  $end_date  is higher than 60 USD"
fi


if (( $(echo "$amount > $amount_75" |bc -l) )); then
  send_sms " Aws cost usage from $start_date  to  $end_date  is higher than 75 USD"
fi

