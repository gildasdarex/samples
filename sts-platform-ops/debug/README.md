# Debug tools 


```bash
kubectl create -f shell.yaml

kubectl exec -it sonar-shell -- /bin/bash

```

Test Kafka

```bash
  kubectl -n default exec testclient -- /usr/bin/kafka-topics --zookeeper my-kafka-zookeeper:2181 --list
```
---
  
Once you have the testclient pod above running, you can list all kafka topics with:
```bash
 kubectl -n default exec testclient -- /usr/bin/kafka-topics --zookeeper my-kafka-zookeeper:2181 --list
```
---

To create a new topic:
```bash
  kubectl -n default exec testclient -- /usr/bin/kafka-topics --zookeeper my-kafka-zookeeper:2181 --topic test1 --create --partitions 1 --replication-factor 1
```
---

To listen for messages on a topic:
```bash
  kubectl -n default exec -ti testclient -- /usr/bin/kafka-console-consumer --bootstrap-server my-kafka:9092 --topic test1 --from-beginning
```
To stop the listener session above press: Ctrl+C

---
To start an interactive message producer session:
```bash
  kubectl -n default exec -ti testclient -- /usr/bin/kafka-console-producer --broker-list my-kafka-headless:9092 --topic test1
```




### References
- https://kubernetes.io/docs/tasks/debug-application-cluster/get-shell-running-container/

- https://medium.com/devopslinks/learning-kubernetes-by-doing-part-3-services-ed5bf7e2bc8e
-  kubectl run my-test --rm -i --tty --image ubuntu -- bash