# Cassandra Deployment




### Setup

```bash

# If you didnt already create the namespace
kubeclt create -f ../setup/01-namespace.yml


kubectl create -f ./cassandra-service.yaml

# Check to see if sevice is created
kubectl --namespace=sonartrade get svc cassandra

kubectl create -f ./cassandra-statefulset.yaml

# Check if the statefulset was created
kubectl --namespace=sonartrade get statefulset

# Should see 2 pods right now.
kubectl --namespace=sonartrade get pods

```
Once the pods are up.  The `get pods` should show:
```text
NAME          READY     STATUS    RESTARTS   AGE
cassandra-0   1/1       Running   0          17h
cassandra-1   1/1       Running   0          17h
```


#### Connect to a pod:

Check the Status, Shell into the node, or connect using CQLSH
```bash
    
 kubectl --namespace=sonartrade exec -it cassandra-0 -- nodetool status

 kubectl --namespace=sonartrade exec -ti cassandra-0 bash   

 kubectl --namespace=sonartrade exec -ti cassandra-0 -- cqlsh cassandra-0
  
# Check Host info 
 brew install jq
 kubectl --namespace=sonartrade get pods -l app=cassandra -o json | jq '.items[] | {"name": .metadata.name,"hostname": .spec.nodeName, "hostIP": .status.hostIP, "PodIP": .status.podIP}'
 
```

#### Manual Test Data
```bash

kubectl --namespace=sonartrade exec -it cassandra-0 -- cqlsh

```
```SQL
-- in the cqlsh shell:

CREATE KEYSPACE BitsoMarketService WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 3 };
CONSISTENCY QUORUM;
use classicmodels;
INSERT into offices(officeCode, city, phone, addressLine1, addressLine2, state, country ,postalCode, territory) values ('1','San Francisco','+1 650 219 4782','100 Market Street','Suite 300','CA','USA','94080','NA');
SELECT * FROM classicmodels.offices;

-- exit shell
```

```bash
# List all the nodes where this data is available:
kubectl --namespace=sonartrade exec -it cassandra-0 -- nodetool getendpoints classicmodels offices 1

##### Simulate Node Failure: (doest work on Minikube, since Minikube is the only Node)

NODE=`kubectl --namespace=sonartrade get pods cassandra-0 -o json | jq -r .spec.nodeName`
kubectl --namespace=sonartrade cordon ${NODE}
kubectl --namespace=sonartrade uncordon ${NODE}
 

kubectl --namespace=sonartrade exec cassandra-0 -- cqlsh -e 'select * from classicmodels.offices'
kubectl --namespace=sonartrade exec cassandra-1 -- cqlsh -e 'select * from classicmodels.offices'

 
``` 

#### Delete Setup
Just delete the stateful set, to remove cassandra instances
```bash

kubectl create -f ./cassandra-statefulset.yaml
```
---



#### Disaster Recovery
To handle the backing up of Cassandra data, we take 'snapshots' of the Volume Mounts used.
Allowing us to get a complete copy of all data.  But before we do that, we have to make sure all nodes have synced, and are not it the middle of writing/processing.

Taking a snapshot of EBS :
```bash

```
Recovering the Snapshots from EBS:
```bash

```



### Lagom's Reference for Cassandra
- https://www.lagomframework.com/documentation/1.4.x/java/CassandraServer.html


#### References
- https://portworx.com/kubernetes-cassandra-run-ha-cassandra-amazon-eks/
- https://github.com/IBM/Scalable-Cassandra-deployment-on-Kubernetes
- https://github.com/kubernetes/examples/tree/master/cassandra
- https://portworx.com/kubernetes-cassandra-run-ha-cassandra-amazon-eks/
- https://medium.com/merapar/deploy-a-high-available-cassandra-cluster-in-aws-using-kubernetes-bd8ba07bfcdd
- https://stedolan.github.io/jq/download/
