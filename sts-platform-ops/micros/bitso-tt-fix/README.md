# Bitso TT Fix Micro's


1. Update application.conf

```
lagom.cluster.join-self = true

cassandra.session-provider = akka.persistence.cassandra.ConfigSessionProvider
cassandra.contact-points = [${?STS_CASSANDRA_HOST1}, ${?STS_CASSANDRA_HOST2}]

cassandra-journal {
  replication-factor = 3
  max-message-batch-size = 100
}

cassandra-snapshot-store {
  replication-factor = 3
  write-consistency = "QUORUM"
  read-consistency = "QUORUM"
}

lagom.broker.kafka {
  service-name = ""
  brokers: ${?STS_KAFKA_HOST1}":"${?STS_KAFKA_PORT}","${?STS_KAFKA_HOST2}":"${?STS_KAFKA_PORT}
  #
}


lagom.services {
  cas_native = "tcp://"${?STS_CASSANDRA_HOST1}":"${?STS_CASSANDRA_PORT}

  "bitso-rest-service" = "https://api.bitso.com:443"
  "BitsoMarketService" = "http://bitsomarketservice"
  "BitsoAuthService" =  "http://bitso-auth-service"
}
```

2. Switch data dictionary
```
DataDictionary=TTFIX.xml
TransportDataDictionary=TTFIX.xml
AppDataDictionary=TTFIX.xml
```

3. Update bitso-tt's build.sbt
```
lazy val `bitso-tt-fix-service-impl` = (project in file("bitso-tt-fix-service/bitso-tt-fix-service-impl"))
>> under this
rpPackagingDockerCommmands := Vector(
      Cmd("RUN","apt-get update  -y "),
      Cmd("RUN","apt-get install -y openjfx"),
      Cmd("EXPOSE","4198"),
      Cmd("EXPOSE","4199")
    )

```

4. `sbt docker:publishLocal`
4. Update micros/bitso-tt-fix/02-svc.yaml with allocated elastic IP IDs (AWS -> VPC -> Elastic IPs -> Allocation ID)
5. kubectl apply -f micros/bitso-tt-fix/02-svc.yaml
6. EC2 -> Load balancers -> Remove target groups from newly created LB
7. EC2 -> Load balancers -> New ->  (type = network) ->
    - scheme: internet-facing
    - listeners: TCP:4199, TCP:4198
    - vpc: same as kubernetes, select both availability zones and the PUBLIC subnet, select preallocate IP
    - attach to one of the target groups previously removed
    - After new LB created, make sure its listening on both port 4199 and 4198


### Kubernetes Configs
- Lagom Micro
- UI
- NLB Ingress (Network Load Balancer, for FIX acceptors)

### Manual inserts

#### Affinity settings
```
    spec:
      restartPolicy: Always
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: sonartrade.com/apps
                operator: In
                values:
                - bitsottfixservice
      containers:
```         

#### SSL keys
```
            - name: SONAR_TT_FIX_SOCKET_KEY_STORE_FILE
              value: /etc/sonar/fix.bitso.sonartrade.com-full.pkcs12
            - name: SONAR_TT_FIX_SOCKET_KEY_STORE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: socket-key-store-password
                  key: socket-key-store-password.txt
```             

#### Volume mounts
```
          volumeMounts:
            - name: bitso-tt-pkcs12
              mountPath: "/etc/sonar/fix.bitso.sonartrade.com-full.pkcs12"
              readOnly: true
```


#### Volumes
```
      volumes:
        - name: bitso-tt-pkcs12
          secret:
            secretName: fix.bitso.sonartrade.com-full.pkcs12
```
