# Microservices Deployment

Using Lightbend Orchestration library

## Dependencies

- [Lightbend Orchestrator](https://developer.lightbend.com/docs/lightbend-orchestration/current/)
- [Docker](https://docs.docker.com/install/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

### Local Kubernetes Deployment
```bash
# First need to enable this:
minikube addons enable ingress

sbt compile
sbt deploy minikube
```

--- 

## Build / Deployment


### Building

#### Steps
1) Pull down monorepo 
    ```
    git clone git@bitbucket.org:sonartrading/sts-bitso-tt.git bitso-tt
    cd bitso-tt
    ./scripts/pull-all.sh
    ```
2) Login to aws `$(aws --profile=st ecr get-login --no-include-email --region us-east-1)`
3) run `sbt clean compile docker:publish` (docker will publish to ECR)
4) cd into sts-platform-ops
4) Regenerate Kubernetes configs: `./micros/scripts/rp.sh VERSION_NUMBER`
5) Redeploy all service `./micros/scripts/apply-impls.sh` or individual service `kubectl apply -f micros/bitso-tt-fix/00-full.yaml`

### Deployment

#### Steps
1) re-tag built docker images and push (see [AWS ECR (docker Repo)] below)    
2) update kubernetes yaml files with new image version (or name)
3) run `kubectl apply` , for the micro being updated


---

Auto generate Kubernetes from docker images of the lagom services:
(this step isn't needed now, since we've already generated the yaml files, and altered slightly)
```bash

rp generate-kubernetes-resources --generate-all "bitso-market-service-impl:1.0-SNAPSHOT" \
    --env JAVA_OPTS="-Dplay.crypto.secret=CHANGE_MARKET_SECRET"  \
    --namespace=sonartrade \
    --env STS_CASSANDRA_HOST1=cassandra-1.cassandra \
    --env STS_CASSANDRA_HOST2=cassandra-0.cassandra \
    --env STS_CASSANDRA_PORT=9042 \
    --env STS_KAFKA_HOST1=kafka-0.kafka-headless \ 
    --env STS_KAFKA_HOST2=kafka-1.kafka-headless \
    --env STS_KAFKA_PORT=9092 \
    --pod-controller-replicas 2  > ../platform/micros/bitso-market/00-full.yaml 

rp generate-kubernetes-resources --generate-all "bitso-auth-service-impl:1.0-SNAPSHOT" \
    --env BITSO_API_KEY="somekey" \
    --env JAVA_OPTS="-Dplay.crypto.secret=youmustchangeme4"  \
    --namespace=sonartrade --env STS_CASSANDRA_HOST1=cassandra-1.cassandra \
    --env STS_CASSANDRA_HOST2=cassandra-0.cassandra \
    --env STS_CASSANDRA_PORT=9042 \
    --env STS_KAFKA_HOST1=kafka-0.kafka-headless  \
    --env STS_KAFKA_HOST2=kafka-1.kafka-headless \
    --env STS_KAFKA_PORT=9092      \
    --pod-controller-replicas 2  > ../platform/micros/bitso-auth/00-full.yaml 

    
    
```

4) Make manual changes to configuration
    -

# GLEN'S WAY
sbt clean compile docker:publishLocal
docker tag ...
docker push
// edit 00-full.yaml
kubectl apply -f ...

# tdarex CURRENTLY WORKING ON
sbt clean compile docker:publish
./rp.sh
kubectl -f ...

#### AWS ECR (docker Repo) 
To push local docker images to AWS, we need to login to AWS ECR, then re-tag the local images.
Once the images are retagged, then they can be pushed to the correct place.  
The full length image name : version is referenced in the kubernates yaml files.
   
```
// Login
$(aws --profile=st ecr get-login --no-include-email --region us-east-1)

docker build -t marketui .
docker build -t ttfixui .
docker build -t authui .

// Backend
docker tag bitso-auth-service-impl:0.0.11 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-auth-service:v0.0.11
docker push 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-auth-service:v0.0.11

docker tag bitso-market-service-impl:0.0.11 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service:v0.0.11
docker push 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service:v0.0.11

docker tag bitso-tt-fix-service-impl:0.0.11 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-tt-fix-service:v0.0.11
docker push 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-tt-fix-service:v0.0.11

// UIs
docker tag marketui 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service-ui:m0.0.1
docker push 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service-ui:m0.0.1

docker tag ttfixui 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-tt-fix-ui:0.1.0
docker push 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-tt-fix-ui:0.1.0

docker tag authui 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-auth-service-ui:0.1.3
docker push 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-auth-service-ui:0.1.3
```

#### Apply Kubernetes
quick reference for applying all deployments
```bash
#DEPLOYING SERVICES
kubectl apply -f ./bitso-market/00-full.yaml
kubectl apply -f ./bitso-auth/00-full.yaml
kubectl apply -f ./bitso-tt-fix/00-full.yaml

#DEPLOYING UIs
kubectl apply -f ./bitso-market/01-ui.yaml
kubectl apply -f ./bitso-auth/01-ui.yaml
kubectl apply -f ./bitso-tt-fix/01-ui.yaml

#Deploy FIX NLB
kubectl apply -f ./bitso-tt-fix/02-svc.yaml

 
#Deploy Ingress  (required alb-ingress-controller first) 
kubectl apply -f ./01-combined-ingress.yaml

```

#### Delete NLB and ALB Before Terraform destroy
```bash
kubectl delete -f ./bitso-tt-fix/02-svc.yaml

kubectl delete -f ./01-combined-ingress.yaml
```


---
#### References
- https://developer.lightbend.com/docs/lightbend-orchestration/current/

