#!/usr/bin/env bash


## First Parameter
if [[ -z "$1" ]]; then
    echo "[ERROR]: First parameter (VERSION, eg "0.0.12") is required but missing. Run again with 'rp.sh VERSION PLAY_SECRET'"
    exit 1
fi

## Bitso keys
if [[ -z "DEPLOYMENT_BITSO_API_KEY" ]]; then
    echo "[ERROR]: Environment variable DEPLOYMENT_BITSO_API_KEY must be set"
    exit 1
fi

if [[ -z "DEPLOYMENT_BITSO_SECRET_KEY" ]]; then
    echo "[ERROR]: Environment variable DEPLOYMENT_BITSO_SECRET_KEY must be set"
    exit 1
fi

## FIX passwords
if [[ -z "FIX_PRICE_GATEWAY_LOGON_PASSWORD" ]]; then
    echo "[ERROR]: Environment variable FIX_PRICE_GATEWAY_LOGON_PASSWORD must be set"
    exit 1
fi

if [[ -z "FIX_ORDER_GATEWAY_LOGON_PASSWORD" ]]; then
    echo "[ERROR]: Environment variable FIX_ORDER_GATEWAY_LOGON_PASSWORD must be set"
    exit 1
fi

# Secret generator
randStr () {
     echo "$(head /dev/urandom | LC_CTYPE=C tr -dc A-Za-z0-9 | head -c64)"
}

# bitso-market-service
printf "\n[1/4] Generating Kubernetes deployment and service descriptor for bitso-market-service:$1\n"
marketPlaySecret=$(randStr)
echo "[info] Market service Play secret: $marketPlaySecret"

rp generate-kubernetes-resources --generate-pod-controllers --generate-services \
    "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service:$1" \
    --env ZOOKEEPER_HOSTS=zookeeper:2181 \
    --env archiveDir="/tmp" \
    --env BITSOTT_MARKET_SERVICE_ORDERS_LOG_LEVEL=DEBUG \
    --env BITSOTT_MARKET_SERVICE_TRADES_LOG_LEVEL=DEBUG \
    --service-type NodePort \
    --external-service cas_native=_cql._tcp.cassandra.sonartrade.svc.cluster.local \
    --external-service kafka_native=_broker._tcp.kafka-headless.sonartrade.svc.cluster.local \
    --external-service bitso-rest-service=https://api.bitso.com:443 \
    --pod-controller-image-pull-policy=Always \
    --namespace=sonartrade > ../sts-platform-ops/micros/bitso-market/00-full.yaml

# bitso-auth-service
printf "\n[2/4] Generating Kubernetes deployment and service descriptor for bitso-auth-service:$1\n"
authPlaySecret=$(randStr)
echo "[info] Auth service Play secret: $authPlaySecret"
echo "[info] Auth service BITSO_API_KEY: $DEPLOYMENT_BITSO_API_KEY"

rp generate-kubernetes-resources --generate-all --generate-pod-controllers --generate-services \
    "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-auth-service:$1" \
    --external-service cas_native=_cql._tcp.cassandra.sonartrade.svc.cluster.local \
    --external-service kafka_native=_broker._tcp.kafka-headless.sonartrade.svc.cluster.local \
    --env ZOOKEEPER_HOSTS=zookeeper:2181 \
    --env archiveDir="/tmp" \
    --service-type NodePort \
    --pod-controller-image-pull-policy=Always \
    --env STS_LOG_LEVEL=DEBUG \
    --env BITSOTT_LOG_LEVEL=DEBUG \
    --env BITSO_API_KEY=$DEPLOYMENT_BITSO_API_KEY \
    --env BITSO_SECRET_KEY=$DEPLOYMENT_BITSO_SECRET_KEY \
    --env STS_AUTH_REST_RATE_LIMIT=58 \
    --namespace=sonartrade > ../sts-platform-ops/micros/bitso-auth/00-full.yaml

# bitso-tt-fix-service
printf "\n[3/4] Generating Kubernetes deployment and service descriptor for bitso-tt-fix-service:$1\n"
fixPlaySecret=$(randStr)
echo "[info] Fix service Play secret: $fixPlaySecret"

rp generate-kubernetes-resources --generate-all --generate-pod-controllers --generate-services \
    "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-tt-fix-service:$1" \
    --external-service cas_native=_cql._tcp.cassandra.sonartrade.svc.cluster.local \
    --external-service kafka_native=_broker._tcp.kafka-headless.sonartrade.svc.cluster.local \
    --env ZOOKEEPER_HOSTS=zookeeper:2181 \
    --env STS_TT_FIX_FILE_STORE_PATH=/var/storage/bitso-tt-fix \
    --env STS_TT_FIX_FILE_LOG_PATH=/var/storage/bitso-tt-fix \
    --env archiveDir="/tmp" \
    --service-type NodePort \
    --env STS_LOG_LEVEL=DEBUG \
    --env BITSOTT_LOG_LEVEL=DEBUG \
    --env FIX_PRICE_GATEWAY_LOGON_PASSWORD=$FIX_PRICE_GATEWAY_LOGON_PASSWORD \
    --env FIX_ORDER_GATEWAY_LOGON_PASSWORD=test \
    --pod-controller-image-pull-policy=Always \
    --namespace=sonartrade > ../sts-platform-ops/micros/bitso-tt-fix/00-full.yaml

# ingress
#echo "\n[4/4] Generating Kubernetes ingress for all services (version = $1)"
#rp generate-kubernetes-resources --generate-ingress --ingress-name main-ingress \
#  "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-auth-service:$1" \
#  "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service:$1" \
#  "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-tt-fix-service:$1" \
#  --namespace=sonartrade > ../sts-platform-ops/micros/02-main-ingress.yaml
# printf "\n[4/4] Skipping generation of Kubernetes ingress for all services (version = $1)\n"

# add affinity settings to tt fix yaml
"${BASH_SOURCE%/*}/service_yaml_editor" add_affinity_settings

# set resource limits for micro services
"${BASH_SOURCE%/*}/service_yaml_editor" add_resource_limits

# add secrets settings to all micros yamls
"${BASH_SOURCE%/*}/service_yaml_editor" add_secrets_settings

# volume claim templates to tt fix
"${BASH_SOURCE%/*}/service_yaml_editor" convert_ttfix_to_statefulset

# add security contexts to tt fix to give access to volumes
"${BASH_SOURCE%/*}/service_yaml_editor" add_security_context

printf "\n[DONE] Successfully built all kubernetes resources for v$1 in ../sts-platform-ops/micros/\n"
