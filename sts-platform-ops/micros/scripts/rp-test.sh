#!/usr/bin/env bash


## First Parameter
if [[ -z "$1" ]]; then
    echo "[ERROR]: First parameter (VERSION, eg "0.0.12") is required but missing. Run again with 'rp.sh VERSION PLAY_SECRET'"
    exit 1
fi

## Bitso keys
if [[ -z "DEPLOYMENT_BITSO_API_KEY" ]]; then
    echo "[ERROR]: Environment variable BITSO_API_KEY must be set"
    exit 1
fi

if [[ -z "DEPLOYMENT_BITSO_SECRET_KEY" ]]; then
    echo "[ERROR]: Environment variable BITSO_SECRET_KEY must be set"
    exit 1
fi

# Secret generator
randStr () {
     echo "$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c64)"
}

# bitso-tt-fix-service
printf "\n[3/4] Generating Kubernetes deployment and service descriptor for bitso-tt-fix-service:$1\n"
fixPlaySecret=$(randStr)
echo "[info] Fix service Play secret: $fixPlaySecret"

rp generate-kubernetes-resources --generate-all --generate-pod-controllers --generate-services \
    "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-tt-fix-service:$1" \
    --external-service cas_native=_cql._tcp.cassandra.sonartrade.svc.cluster.local \
    --external-service kafka_native=_broker._tcp.kafka-headless.sonartrade.svc.cluster.local \
    --env JAVA_OPTS="-Dplay.crypto.secret=$marketPlaySecret"  \
    --env ZOOKEEPER_HOSTS=zookeeper:2181 \
    --env archiveDir="/tmp" \
    --pod-controller-image-pull-policy=Always \
    --namespace=sonartrade > ../../micros/bitso-tt-fix/000-full.yaml

# ingress
#echo "\n[4/4] Generating Kubernetes ingress for all services (version = $1)"
#rp generate-kubernetes-resources --generate-ingress --ingress-name main-ingress \
#  "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-auth-service:$1" \
#  "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service:$1" \
#  "379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-tt-fix-service:$1" \
#  --namespace=sonartrade > ../sts-platform-ops/micros/02-main-ingress.yaml
printf "\n[4/4] Skipping generation of Kubernetes ingress for all services (version = $1)\n"

printf "\n[DONE] Successfully built all kubernetes resources for v$1 in ../sts-platform-ops/micros/\n"
