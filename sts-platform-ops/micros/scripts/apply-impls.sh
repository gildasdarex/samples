#!/usr/bin/env bash

echo "[1/3] Applying bitso-auth/00-full.yaml"
kubectl apply -f micros/bitso-auth/00-full.yaml

echo "[2/3] Applying bitso-market/00-full.yaml"
kubectl apply -f micros/bitso-market/00-full.yaml

echo "[3/3] Applying bitso-tt-fix/00-full.yaml"
kubectl apply -f micros/bitso-tt-fix/00-full.yaml

echo "[DONE] Applied for all services"
