#!/bin/bash


# Secret generator
randStr () {
     echo "$(head /dev/urandom | LC_CTYPE=C tr -dc A-Za-z0-9 | head -c64)"
}

base64Secret() {
    echo $(echo -n $(randStr) | base64)
}


echo "Creating Play secrets"
secretsFolder=/tmp/$(randStr)

mkdir -p $secretsFolder

playSecret=$(randStr)
fileName=$secretsFolder/bitso-auth-play-secret-key.txt

echo "[info] Auth service Play secret: $playSecret"
echo "[info] Saving auth service Play secret in: $fileName"
echo $playSecret > $fileName
kubectl -n sonartrade create secret generic bitso-auth-play-secret-key --from-file=$fileName

playSecret=$(randStr)
fileName=$secretsFolder/bitso-market-play-secret-key.txt

echo "[info] Market service Play secret: $playSecret"
echo "[info] Saving market service Play secret in: $fileName"
echo $playSecret > $fileName

kubectl -n sonartrade create secret generic bitso-market-play-secret-key --from-file=$fileName

playSecret=$(randStr)
fileName=$secretsFolder/bitso-tt-fix-play-secret-key.txt

echo "[info] TT FIX service Play secret: $playSecret"
echo "[info] Saving TT FIX service Play secret in: $fileName"
echo $playSecret > $fileName

kubectl -n sonartrade create secret generic bitso-tt-fix-play-secret-key --from-file=$fileName
