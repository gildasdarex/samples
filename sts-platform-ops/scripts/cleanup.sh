#!/usr/bin/env bash


usage(){
  echo " To execute this script run the following command "
  echo " ./scripts/cleanup.sh --namespace <your_namespace> --aws_profile [your_aws_profile] --stage [stage]"
  echo " Full example is : ./scripts/cleanup.sh --namespace tdarex --aws_profile st --stage prod"
  echo " Required options are : namespace"
  echo " Default value for: aws_profile is st, stage is prod"

  exit 1
}

aws_profile="st"

optspec=":-:"
while getopts "$optspec" optchar; do
   case "${OPTARG}" in
      namespace)
         namespace="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      aws_profile)
         aws_profile="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      stage)
         stage="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      help)
        usage
         ;;
   esac
done


## First Parameter
if [[ -z "$namespace" ]]; then
    echo "namespace option is required but missing. Please run cleanup.sh --help  for more informations"
    exit 1
fi

check_dependencies(){
    # Ruby
    echo "Checking ruby installation..."
    if ! [ -x "$(command -v ruby)" ]; then
      echo 'Error: ruby is not installed.' >&2
      exit 1
    fi

    # Bundler
    echo "Checking bundler installation..."
    if ! [ -x "$(command -v bundle)" ]; then
      echo 'Error: bundler is not installed.' >&2
      exit 1
    fi
}

check_dependencies

# Deletes application load balancer
kubectl delete -f ingress/subdomain-ingress.yaml

# Deletes network load balancer
kubectl delete -f micros/bitso-tt-fix/02-svc.yaml


bundle install

cd ./aws
workspace_name=$namespace-$stage
echo $workspace_name
terraform workspace select $workspace_name || terraform workspace new $workspace_name
terraform workspace list

terraform destroy

terraform workspace select default && terraform workspace delete $workspace_name

cd ..

./scripts/delete-volumes.rb $namespace $stage
