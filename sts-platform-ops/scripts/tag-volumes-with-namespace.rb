#!/usr/bin/env ruby

require 'aws'
require 'aws-sdk-ec2'

# Takes namespace from first command line param or from env variable TF_VAR_namespace
# and tags all volumes associated to it with Name:Namespace,Value:#{namespaces}
begin
  namespace = ARGV[0] || ENV['TF_VAR_namespace']
  stage = ARGV[1] || ENV['TF_VAR_stage'] || 'prod'
  begin
    # Never tag volumes of namespace sonartrade! They may be production volumes
    puts "Cannot tag volumes; namespace is #{namespace || 'undefined'}"
    exit -1
  end if !namespace || namespace.empty? || namespace === 'sonartrade'

  client = Aws::EC2::Client.new
  volumes = client.describe_volumes.volumes
  volume_ids = volumes.map do |v|

    tags = v.tags.select { |t| t.key === "kubernetes.io/cluster/#{namespace}-#{stage}-eks-cluster" && t.value === 'owned' }
    v.volume_id unless tags.empty?
  end

  ids_to_tag = volume_ids.compact
  puts "#{ids_to_tag.size} found off #{volumes.size}"

  puts "Tagging volumes with namespace: #{ids_to_tag}"

  resp = client.create_tags({
    resources: ids_to_tag,
    tags: [
      {
        key: "Namespace",
        value: namespace,
      },
    ],
  }) unless ids_to_tag.empty?

  # do stuff
rescue Aws::EC2::Errors::ServiceError => error
  # rescues all service API errors
  puts 'Failed to tag volumes'
  raise error
end
