#!/usr/bin/env ruby

require 'aws'
require 'aws-sdk-ec2'

# Takes namespace from first command line param or from env variable TF_VAR_namespace
# and deletes all volumes associated to it
puts "Deleting volumes of namespace #{ARGV[0]}"
begin
  namespace = ARGV[0] || ENV['TF_VAR_namespace']
  stage = ARGV[1] || ENV['TF_VAR_stage'] || 'prod'
  begin
    # Never delete volumes of namespace sonartrade! They may be production volumes
    puts "Cannot delete volumes; namespace is #{namespace || 'undefined'}"
    exit -1
  end if !namespace || namespace.empty? || namespace === 'sonartrade'
  client = Aws::EC2::Client.new
  volumes = client.describe_volumes.volumes
  volume_ids = volumes.map do |v|

    tags = v.tags.select { |t| t.key === "kubernetes.io/cluster/#{namespace}-#{stage}-eks-cluster" && t.value === 'owned' }
    v.volume_id unless tags.empty?
  end

  ids_to_delete = volume_ids.compact
  puts "#{ids_to_delete.size} found off #{volumes.size}"

  puts "Deleting volumes #{ids_to_delete}"

  ids_to_delete.each do |id|
    client.delete_volume({ volume_id: id })
  end

  puts "Deleted volumes #{ids_to_delete}"

  # do stuff
rescue Aws::EC2::Errors::ServiceError
  # rescues all service API errors
  puts 'Failed to delete volumes'
end
