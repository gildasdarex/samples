#!/usr/bin/env ruby

require 'json'
require 'thor'

LABEL_REQUIREMENTS = {
  'db-messaging' => {
    'us-east-1a' => 1,
    'us-east-1b' => 1,
  },
}

class NodeManager < Thor
  desc 'label', 'Labels nodes'
  def label(scheme = 'default')
    nodes_json = JSON.parse(`kubectl get nodes -o json`)

    puts "labelling nodes..."
    nodes = nodes_json['items']

    zone_a_nodes = nodes.select { |n| n['metadata']['labels']['failure-domain.beta.kubernetes.io/zone'] == 'us-east-1a' }
    zone_b_nodes = nodes.select { |n| n['metadata']['labels']['failure-domain.beta.kubernetes.io/zone'] == 'us-east-1b' }

    zone_a_messaging_nodes = zone_a_nodes.select { |n| n['metadata']['labels']['sonartrade.com/apps'] == 'db-messaging' }
    if zone_a_messaging_nodes.length < 1
      name = zone_a_nodes[0]['metadata']['name']
      puts "labelling #{name} with sonartrade.com/apps=db-messaging for us-east-1a"
      status = system("kubectl label nodes #{name} sonartrade.com/apps=db-messaging")
      if status
        puts "successfully labelled #{name} with sonartrade.com/apps=db-messaging"
      else
        raise "Failed to label #{name} with sonartrade.com/apps=db-messaging"
      end
    else
      names = zone_a_messaging_nodes.map { |n| n['metadata']['name'] }
      puts "#{names.join(',')} already labelled with db-messaging in us-east-1a"
    end

    zone_b_messaging_nodes = zone_b_nodes.select { |n| n['metadata']['labels']['sonartrade.com/apps'] == 'db-messaging' }

    if zone_b_messaging_nodes.length < 1
      name = zone_b_nodes[0]['metadata']['name']
      puts "labelling #{name} with sonartrade.com/apps=db-messaging for us-east-1b"
      system("kubectl label nodes #{name} sonartrade.com/apps=db-messaging")
      if status
        puts "successfully labelled #{name} with sonartrade.com/apps=db-messaging"
      else
        raise "Failed to label #{name} with sonartrade.com/apps=db-messaging"
      end
    else
      names = zone_b_messaging_nodes.map { |n| n['metadata']['name'] }
      puts "#{names.join(',')} already labelled with db-messaging in us-east-1b"
    end

  rescue StandardError => e
      puts e.backtrace
      throw e
  end

end

NodeManager.start ARGV
