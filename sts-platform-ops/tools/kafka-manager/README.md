# Kafka Manager

Simple Kafka UI
- https://github.com/yahoo/kafka-manager


### Setup
```bash
kubectl apply -f kafka-manager-service.yml
kubectl apply -f kafka-manager.yml

#after minikube dashboard is up, we can use the proxy to piggy back access
http://127.0.0.1:51413/api/v1/namespaces/sonartrade/services/http:kafka-manager:/proxy/
```



