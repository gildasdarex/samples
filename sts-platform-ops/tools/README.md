# Ops Tools Deployment


- Grafana
- Prometheus
- Cassandra Reaper (http://cassandra-reaper.io/)
- Kafka manager (https://github.com/yahoo/kafka-manager)



---
#### ./aws or ./local 
```bash
 
 kubectl create -f prometheus-storageclass.yaml
 
 helm install -f prometheus-values.yaml stable/prometheus --name prometheus --namespace prometheus
 kubectl get all -n prometheus

```


### References
- https://docs.aws.amazon.com/eks/latest/userguide/dashboard-tutorial.html
- https://github.com/helm/charts/tree/master/stable/prometheus-operator
- https://github.com/coreos/prometheus-operator

- https://kubernetes.io/docs/concepts/storage/storage-classes/#aws-ebs
- https://prometheus.io/docs/alerting/alertmanager/