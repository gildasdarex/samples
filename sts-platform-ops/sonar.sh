#!/bin/bash

STARTTIME=$(date +%s)

################################################
# Check arguments
echo "[1 / 10] Checking required arguments"

usage(){
  echo " To execute this script run the following command "
  echo " sonar.sh --namespace your_namespace --vpc_cidr_block your_vpc_cidr_block --stage your_stage --aws_profile your_aws_profile "
  echo " Full example is : sonar.sh --namespace tdarex --vpc_cidr_block 172.27.0.0/16 --stage prod --aws_profile st --instance_type t3.large"
  echo " Required options are : namespace and vpc_cidr_block "
  echo " Default value for stage is  prod and for aws_profile is st"
  echo " Default value for instance_type t3.medium"
  exit 1
}

check_env_variable(){
  ## Bitso keys required to run rp.sh
    if [[ -z "BITSO_API_KEY" ]]; then
       echo "[ERROR]: Environment variable BITSO_API_KEY must be set"
       exit 1
    fi

    if [[ -z "BITSO_SECRET_KEY" ]]; then
        echo "[ERROR]: Environment variable BITSO_SECRET_KEY must be set"
        exit 1
    fi

     if [[ -z "DOCKER_REPOSITORY" ]]; then
        echo "[ERROR]: Environment variable DOCKER_REPOSITORY must be set"
        exit 1
    fi
}

check_dependencies(){
    # Check docker is running
    echo "CHECKING DOCKER..."
    if ! [ -x "$(command -v docker)" ]; then
      echo 'Error: docker is not installed.' >&2
      exit 1
    fi

    # Check AWS-cli is install
    echo "CHECKING AWS-CLI..."
    if ! [ -x "$(command -v aws)" ]; then
      echo 'Error: aws is not installed.' >&2
      exit 1
    fi

    # Check aws-iam-auth is install
    echo "CHECKING aws-iam-auth INSTALLED"
    if ! [ -x "$(command -v aws-iam-authenticator)" ]; then
      echo 'Error: aws-iam-authenticator is not installed.' >&2
      exit 1
    fi


    # check kubectl install , and version >= 1.10
    echo "CHECKING KUBECTL INSTALLED"
    if ! [ -x "$(command -v kubectl)" ]; then
      echo 'Error: kubectl is not installed.' >&2
      exit 1
    fi
}

aws_profile="st"
stage="prod"
instance_type="t3.medium"

optspec=":-:"
while getopts "$optspec" optchar; do
   case "${OPTARG}" in
      namespace)
         namespace="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      vpc_cidr_block)
         vpc_cidr_block="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      stage)
         stage="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      aws_profile)
         aws_profile="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      instance_type)
         instance_type="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      help)
        usage
         ;;
   esac
done

## First Parameter
if [[ -z "$namespace" ]]; then
    echo "namespace option is required but missing. Please run sonar.sh --help  for more informations"
    exit 1
fi

if [[ -z "$vpc_cidr_block" ]]; then
    echo "vpc_cidr_block option is required but missing. Please run sonar.sh --help  for more informations"
    exit 1
fi

check_env_variable

################################################
# Check pre-reqs
echo "[2 / 10] Checking dependencies"

check_dependencies

################################################
# Configure settings

# If aws is configured, find profile
export AWS_DEFAULT_PROFILE=$aws_profile
export TF_VAR_profile=$aws_profile
export TF_VAR_namespace=$namespace
export TF_VAR_vpc_cidr_block=$vpc_cidr_block
export TF_VAR_stage=$stage
export TF_VAR_instance_type=$instance_type

################################################
# Perform deployment
echo "[3 / 10] Initializing cluster (this may take a few minutes)"

cd aws

workspace_name=$namespace-$stage
terraform workspace select $workspace_name || terraform workspace new $workspace_name

echo "DEPLOYING EKS CLUSTER with workspace $workspace_name: This will fail unless terraform init has already been run"
terraform apply -auto-approve



echo "FINISHING CLUSTER INITIALIZATION"
# switch context
aws --profile=st eks update-kubeconfig --name $TF_VAR_namespace-$TF_VAR_stage-eks-cluster


#then rerun , since this always fails ..
kubectl apply -f config-map-aws-auth-$TF_VAR_namespace-$TF_VAR_stage-eks-cluster.yaml

cd ..

# Add labels to nodes
${BASH_SOURCE%/*}/scripts/node_manager.rb label

# Add cluster access permissions to all team members
${BASH_SOURCE%/*}/scripts/aws-auth-config-map add_permissions

echo "[4 / 10] Configuring cluster"
kubectl create -f ./setup/01-namespace.yml --validate=false
kubectl create -f ./setup/rbac.yaml --validate=false

secretsFolder=$HOME/.sonar/secrets/$stage
kubectl -n sonartrade create secret generic socket-key-store-password --from-file=$secretsFolder/socket-key-store-password.txt
kubectl -n sonartrade create secret generic fix.bitso.sonartrade.com-full.pkcs12 --from-file=$secretsFolder/fix.bitso.sonartrade.com-full.pkcs12
kubectl -n sonartrade create secret generic bitso-api-secret-keys --from-file=$secretsFolder/bitso-api-key.txt --from-file=$secretsFolder/bitso-secret-key.txt
kubectl -n sonartrade create secret generic price-gateway-password --from-file=$secretsFolder/price-gateway-password.txt
kubectl -n sonartrade create secret generic order-gateway-password --from-file=$secretsFolder/order-gateway-password.txt

# Create Play secrets in cluster
${BASH_SOURCE%/*}/scripts/create_play_secrets.sh

echo "[5 / 10] Deploying Cassandra"
kubectl create -f ./cassandra/cassandra-service.yaml --validate=false
kubectl create -f ./cassandra/cassandra-statefulset.yaml --validate=false

echo "[6 / 10] Deploying Zookeeper"
kubectl create -f ./zookeeper/11-zookeeper-service.yml --validate=false
kubectl create -f ./zookeeper/11-zookeeper-service-headless.yml --validate=false
kubectl create -f ./zookeeper/14-zookeeper-statefulset.yml --validate=false
kubectl create -f ./zookeeper/15-zookeeper-disruptionbudget.yml --validate=false

echo "[7 / 10] Deploying Kafka"
kubectl create -f ./kafka/21-kafka-service.yml --validate=false
kubectl create -f ./kafka/22-kafka-service-headless.yml --validate=false
kubectl create -f ./kafka/24-kafka-statefulset.yml --validate=false

echo "[8 / 10] Deploying microservices"
kubectl create -f ./micros/bitso-market/00-full.yaml --validate=false
kubectl create -f ./micros/bitso-auth/00-full.yaml --validate=false
kubectl create -f micros/bitso-tt-fix/config-map.yaml
kubectl create -f ./micros/bitso-tt-fix/storage-class.yaml --validate=false
kubectl create -f ./micros/bitso-tt-fix/storage-class-ssd.yaml --validate=false
kubectl create -f ./micros/bitso-tt-fix/00-full.yaml --validate=false

echo "[9 / 10] Deploying UIs"
kubectl create -f ./micros/bitso-market/01-ui.yaml --validate=false
kubectl create -f ./micros/bitso-auth/01-ui.yaml --validate=false
kubectl create -f ./micros/bitso-tt-fix/01-ui.yaml --validate=false

echo "[10 / 10] Deploying Kubernetes dashboard"
kubectl apply -f ./setup/aws/eks-admin.yaml --validate=false
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml --validate=false


# Generate configs for dns
./scripts/dns_config_generator generate_ingress $namespace $stage
./scripts/dns_config_generator generate_alb_controller $namespace $stage

# echo "Creating application load balancer"
kubectl create -f ingress/rbac-role.yaml
kubectl create -f ingress/subdomain-ingress.yaml
kubectl create -f ingress/external-dns.yaml

# Use this only when necessary
# kubectl apply -f ingress/alb-ingress-controller.yaml

ENDTIME=$(date +%s)
echo "[DONE] Finished deploying in $(($ENDTIME - $STARTTIME)) seconds. Now watch all the pods spin up, to 'Running' state"
kubectl get pods --all-namespaces=true -w

echo "Tagging namespace for volumes"
bundle install
./scripts/tag-volumes-with-namespace.rb $namespace
