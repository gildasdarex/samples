# Zookeeper Deployment

Kubernetes Deployment.





### Setup
```bash
    kubectl create -f ./11-zookeeper-service.yml
    kubectl create -f ./11-zookeeper-service-headless.yml
    kubectl create -f ./14-zookeeper-statefulset.yml
    kubectl create -f ./15-zookeeper-disruptionbudget.yml




```



### References
- https://zookeeper.apache.org/
- https://kubernetes.io/docs/tutorials/stateful-application/zookeeper/
