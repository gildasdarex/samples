

I- Create aws instance for sts-siege
   1- create the aws instance with terraform script; run the following commands
     - terraform init
     - terraform plan
     - terraform apply
     

II- Setup the aws instance
  1- Ssh on aws instance created after the step 1, the name of key that is used : sts-siege-app-instance-key
 
  2- Create the linux user that you want. The command is :
     - sudo adduser new_user
  
  3- Update ssh key for new user. The commands are :
     - sudo su - new_user
     - cd ~
     - mkdir .ssh
     - chmod 700 .ssh
     - touch .ssh/authorized_keys
     - chmod 600 .ssh/authorized_keys
     Copy the user public key in .ssh/authorized_keys file, Then new user would be able to connect now by using ssh
  
  4- Enable java for new user. The commands are :
      - sudo su - new_user
      - cd /commons
      - ./set-java-for-user.sh
      
  5- Create vnc session for new user. The commands are :
  
        - **vncserver :display_value -geometry 1440x900 -alwaysshared -dpi 96 -localhost**
        **display_value should be a number. the vnc port is determined by display_value. vnc_port = 5900 + display_value**
        
     
        

III- Connexion with vnc client

To access to sts-siege aws instance, run the following command to enable ssh tunnel on your local before connecting with vnc client

ssh -L vnc_port:localhost:vnc_port -N -f -l vnc_user vnc_host_ip

Note: 
   vnc_port = 5900 + display_value that you used in II
   vnc_user = user that you created in II
   vnc_host_ip = ip of aws instance created on I
   
Connect with vnc client; used localhost:vnc_port as vnc server address.

In the remote desktop:

    1- Go to your linux user workdir
       cd ~/workdir
       
    2- Check that java is configured properly
    
       * Run java -version
       
    3- if java is not installed properly run the following command
       * cd /commons
       * ./set-java-env.sh
       * source ~/.profile
       
    4- set env variables that are required :
    
       export BITSO_API_KEY="*******"
       export BITSO_SECRET_KEY="************"
       export ARTIFACTORY_USERNAME="*****"
       export ARTIFACTORY_PASSWORD="*******************"
       export ARTIFACTORY_HOST="*******"
       export archiveDir="/sts-siege/workdir"
       export GEMINI_API_KEY="*********"
       export GEMINI_SECRET_KEY="********"
       
       archiveDir should be /sts-siege/workdir . it is commom directory shared among all users
    
    5- Download sts-siege app by running the following commands:
         - sudo su - new_user
         - cd /commons
         - ./run-sts-siege.sh --app sts-siege --version 5.5.1-fatJar
        The jar will be downloaded into ~/workdir directory 
             
    6- run the app :
       java -jar app_name     