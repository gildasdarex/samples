provider "aws" {
  region = "${var.region}"
  profile = "${var.aws_profile}"
}

##################################################################
# Data sources to get VPC, subnet, security group and AMI details
##################################################################
data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = "${data.aws_vpc.default.id}"
}



module "ec2" {
  source = "git:https://github.com/terraform-aws-modules/terraform-aws-ec2-instance.git?ref=master"
  instance_count = 1
  name                        =  "${var.instance_name}"
  ami                         =  "${var.image_id}"
  key_name                    =  "${var.key_name}"
  instance_type               =  "${var.instance_type}"
  subnet_id                   =  "${element(data.aws_subnet_ids.all.ids, 0)}"
  vpc_security_group_ids      =  "${var.security_group_ids}"
  associate_public_ip_address = true
  tags = "${var.tags}"
}
