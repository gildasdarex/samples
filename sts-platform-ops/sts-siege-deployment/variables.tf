variable "instance_name" {
  description = "Name to be used for sts-siege instance"
}

variable "security_group_ids" {
  description = "Name to be used for sts-siege instance"
  type = "list"
  default     = ["sg-0407c1dee8bb3f42e"]
}

variable "instance_type" {
  description = "Instance type"
  type = "string"
  default     = "t2.small"
}

variable "aws_profile" {
  description = "The profile to use to create aws instance"
  default     = "st"
}

variable "region" {
  description = "region where the aws instance will be created"
  default     = "us-east-1"
}

variable "image_id" {
  type        = "string"
  default     = "ami-0c5b63ec54dd3fc38"
  description = "EC2 image ID to launch."
}

variable "instance_count" {
  description = "Number of instances to launch"
  default     = 1
}

variable "ami" {
  description = "ID of AMI to use for the instance"
}


variable "ebs_optimized" {
  description = "If true, the launched EC2 instance will be EBS-optimized"
  default     = false
}

variable "key_name" {
  description = "The key name to use for the instance"
  default     = "sts-siege-app-instance-key"
}

variable "private_key_path" {
  description = "Path to private key path"
}

variable "monitoring" {
  description = "If true, the launched EC2 instance will have detailed monitoring enabled"
  default     = true
}


variable "associate_public_ip_address" {
  description = "If true, the EC2 instance will have associated public IP address"
  default     = true
}

variable "tags" {
  description = "tags"
  type = "map"
  default     = {
    Namespace = "siege",
    Name = "sts-siege-app-prod"
  }
}
