
resource "null_resource" "java_vnc" {
  connection {
    type     = "ssh"
    private_key     = "${var.private_key_path}"
    host = "${element(module.ec2.public_ip, 0)}"
    user = "ubuntu"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir /development",
      "sudo chmod -R 777 /development",
      "sudo mkdir /commons",
      "sudo chmod -R 777 /commons",
      "sudo mkdir -p /sts-siege/workdir",
      "sudo chmod -R 777 /sts-siege/workdir"
    ]
  }


  provisioner "file" {
    source      = "scripts/install-java.sh"
    destination = "/development"
  }

  provisioner "file" {
    source      = "scripts/install-vnc.sh"
    destination = "/development"
  }

  provisioner "file" {
    source      = "scripts/run-sts-siege.sh"
    destination = "/commons"
  }

  provisioner "file" {
    source      = "scripts/set-java-for-user.sh"
    destination = "/commons"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /development/install-java.sh",
      "./development/install-java.sh"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /development/install-vnc.sh",
      "./development/install-vnc.sh"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /commons/set-java-for-user.sh"
    ]
  }
}
