#!/usr/bin/env bash

cd /opt
sudo curl -L -C - -b "oraclelicense=accept-securebackup-cookie" -O 'http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz'
sudo tar xzf jdk-8u131-linux-x64.tar.gz
cd jdk1.8.0_131/
echo "export JAVA_HOME=/opt/jdk1.8.0_131" >> ~/.profile
echo "export JRE_HOME=/opt/jdk1.8.0_131/jre" >> ~/.profile
echo  'export PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin' >> ~/.profile
source ~/.profile