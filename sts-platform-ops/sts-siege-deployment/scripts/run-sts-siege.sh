#!/bin/bash

# Usage `bash run.sh REPOSITORY VERSION`
# eg `bash ruh.sh sts-graph-makerarb 1.2.6-fatJar`
# or `bash run.sh sts-siege 1.2.6-fatJar` after setting ARTIFACTORY_USERNAME and ARTIFACTORY_PASSWORD env vars.

# Check artifactory username set
usage(){
  echo " To execute this script run the following command "
  echo " run.sh --app your_app --version app_version"
  echo " Full example is : run-sts-siege.sh --app sts-siege --version 5.5.1-fatJar "
  echo " Required options are : app and version "
  echo " app value should be sts-graph-makerarb or sts-siege"

  exit 1
}

check_env_variable(){
  if [ -z ${ARTIFACTORY_USERNAME+x} ]; then
    echo "Error: Env variable ARTIFACTORY_USERNAME is unset"
    exit 1
  fi

    # Check artifactory password set
    if [ -z ${ARTIFACTORY_PASSWORD+x} ]; then
        echo "Error: Env variable ARTIFACTORY_PASSWORD is unset"
        exit 1
    fi
}

check_options(){
   if [[ -z "$app" ]]; then
    echo "app option is required but missing. Please run run.sh --help  for more informations"
    exit 1
   fi

   if [[ -z "$version" ]]; then
    echo "version option is required but missing. Please run run.sh --help  for more informations"
    exit 1
   fi

  # Check repo
    if [[ ! $app =~ (sts-graph-makerarb|sts-siege)$ ]]; then
        echo "Error: Repository should be sts-graph-makerarb or sts-siege"
        exit 1
    fi

    # Check version format
    if [[ ! $version =~ [0-9]+\.[0-9]+\.[0-9]+-fatJar$ ]]; then
        echo "Error: Version number must be in format 1.2.3-fatJar. Note that the 'J' is uppercase."
        exit 1
    fi
}

optspec=":-:"
while getopts "$optspec" optchar; do
   case "${OPTARG}" in
      app)
         app="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      version)
         version="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      help)
        usage
         ;;
   esac
done

check_env_variable

check_options

cd ~
mkdir workdir
cd workdir

# Download jar
curl -O -u $ARTIFACTORY_USERNAME:$ARTIFACTORY_PASSWORD -g -X GET "http://35.184.122.101:8081/artifactory/libs-release-local/com/sonartrade/$app/$version/$app-$version.jar"


if [ $? -eq 0 ]; then
	echo "Download successful!"
	echo "Run with: java -jar $app-$version.jar"
else
    echo "Download failed!"
    exit 1
fi