#!/usr/bin/env bash

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       keys.path)
          keys_path="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done


for file in keys_path/*; do
    value=`cat $file`
    echo -e "$value"  >> ~/.ssh/authorized_keys
done
