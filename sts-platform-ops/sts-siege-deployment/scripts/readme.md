
1- Go to your linux user workdir
   cd ~/workdir
   
2- Check that java is configured properly

   * Run java -version
   
3- if java is not installed properly run the following command
   * ./set-java-env.sh
   * source ~/.profile
   
4- set env variables that are required :

   export BITSO_API_KEY="*******"
   export BITSO_SECRET_KEY="************"
   export ARTIFACTORY_USERNAME="*****"
   export ARTIFACTORY_PASSWORD="*******************"
   export ARTIFACTORY_HOST="*******"
   export archiveDir="/sts-siege/workdir"
   export GEMINI_API_KEY="*********"
   export GEMINI_SECRET_KEY="********"
   
   archiveDir should be /sts-siege/workdir . it is commom directory shared among all users
   
5- download sts-siege app, then run :
   ./run.sh --app sts-siege --version 5.5.1-fatJar
   
6- run the app :
   java -jar app_name     