# Developer Notes

The following are relevant for our cluster deployment processes and will help in maintaining and further developing our services.

# Cluster Creation

## Prerequisites

* terraform version 0.11.14
* See Secrets Setup section

### ENV Variables

* DOCKER_REPOSITORY=379276526719.dkr.ecr.us-east-1.amazonaws.com

### CIDR Block

The cluster will be created on its own VPC with a CIDR block which should be chosen before creation. The CIDR conventions are listed in 1password.

The convention for clusters is:

```
Staging: 172.2X.0.0/16
sonartradeuat2	172.24.0.0/16
sonartradeuat3	172.25.0.0/16
sonartradeuat7	172.27.0.0/16

Production: 172.3X.0.0/16
Default / VPN	172.31.0.0/16
PRODUCTION	172.30.0.0/16

Dev: 172.1X.0.0/16
tdarex	172.16.0.0/16
Gildas	172.17.0.0/16
tgildas	172.18.0.0/16
```

In any case the range should be within 172.16.0.0–172.31.255.255 as per [IETF convention](https://tools.ietf.org/html/rfc1918).

### Namespace
Namespace parameter acts as a tracking tag for all AWS resources besides being used as name of terraform workspace.

### Instance Type
We currently use 2 AWS instances for worker nodes. The recommended size is t3a.large for staging and t3a.medium for dev clusters.

## Creation Script
To create a new cluster with services and UIs running, run the following command:

```
./sonar.sh --namespace <namespace> --vpc_cidr_block <CIDR_block> --stage staging --aws_profile <aws_profile> --instance_type <type>
```

Example:
```
./sonar.sh --namespace sonartradeuat2 --vpc_cidr_block 172.22.0.0/16 --stage staging --aws_profile st --instance_type t3a.large
```




## Network Load Balancer(NLB) creation

We expose tt-fix-service to TT's FIX gateway on this IP:
- 3.210.47.29
 at ports 4198 and 4199.

Further, we allocated another EIP to make our NLB have another target group.
- 18.213.89.10

To create NLB, run:
```
kubectl apply -f micros/bitso-tt-fix/02-svc.yaml
```

However [currently](https://github.com/kubernetes/kubernetes/issues/63959) kubernetes allocated AWS NLB does not support using pre-allocated EIPs for loadbalancer target groups. EKS uses kubernetes version 1.13. Once EKS starts using a version that has the fix, we can stop doing the following manual task:

Making an NLB available on EIP:
1. In AWS console, open the NLB and take a screenshot of the target groups before deleting the target groups. WARNING: Do not delete the NLB, as Kubernetes controller will recreate another one. Wait for a minute for the target groups to be available for our new NLB.
2. Create a new NLB with name `<namespace>-<stage>`(Example: sonartradeuat2-staging).
3. As target groups, choose the ones deleted above as options on TCP ports 4198 and 4199.
4. Find on which node tt-fix-service is running and its region. Make 3.210.47.29 as the IP of that target group.
5. After the NLB is created, edit the target group which has port 4199 and update it to point to the corresponding port in target group.

## UI Access

Access to cluster UIs and market service API is possible at the endpoints in the following format:
`<service-name>.<namespace>-<stage>.cluster.sonar.internal`

For example, market service UI for sonartradeuat2's cluster can be accessed at `http://bitso-market-service-ui.sonartradeuat2-staging.cluster.sonar.internal`
However achieving this adds the cost of an application load balancer which is ~$0.0225 per hour. So this is disabled by default.

To get UI access to the cluster, run:
```
kubectl apply -f ingress/alb-ingress-controller.yaml
kubectl apply -f ingress/subdomain-ingress.yaml
```

For the DNS setup to work, add 172.31.0.2(our default VPC's DNS server) as your primary DNS server. And VPN should be connected for the access to the services to work. This is because all services resolve to internal IPs(internal to our network but external to cluster) of the pods.

## Testing Creation
```
curl -v 3.210.47.29:4198
curl -v 3.210.47.29:4199
```

Run integration tests.

# Getting Access to Existing Cluster

## Update Kubeconfig
If the cluster is created by someone else, the local kubeconfig has to be updated first.
Update the kubeconfig from aws eks cli:
```
aws --profile=st eks update-kubeconfig --name <namespace>-<stage>-eks-cluster
kubectl --namespace=sonartrade config get-contexts // verify that context switched
```

Example: `aws --profile=st eks update-kubeconfig --name sonartradeuat2-staging-eks-cluster`

## Accessing Kubernetes Dashboard

1. Run proxy in a separate terminal:
```
kubectl proxy --disable-filter=true
```
2. Get token to access dashboard:
```
kubectl  -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
```
3. Hit the URL at:

http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login

4. In case of using multiple clusters at the same time, after changing kubectl context, run proxy on a different port as:
```
kubectl proxy --disable-filter=true -p <port>
```

and access dashboard at `http://localhost:<port>/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login`



# Cluster Cleanup

## Remove logging-monitoring peering

```
cd logging-monitoring/aws-vpc-peering/
terraform destroy -lock=false
```

## Cleanup UIs

```
# From top level of project
# Ensure using correct Kubernetes context
kubectl delete -f ingress/subdomain-ingress.yaml
```

## Cleanup NLB(s)

1. Delete the manually created NLB
2. Ensure correct context selected
3. Run:
```
kubectl delete -f micros/bitso-tt-fix/02-svc.yaml
```

Now the load balancers will be empty in AWS console.

## Cleanup Cluster

```
bundle install
./scripts/cleanup.sh --namespace <your_namespace> --aws_profile [your_aws_profile] --stage [stage]
```
Example: ./scripts/cleanup.sh --namespace gildas --aws_profile st --stage prod


# Secrets Setup

Secrets required by cluster are managed using kubernetes [secrets](https://kubernetes.io/docs/concepts/configuration/secret/).
All secrets needed for cluster deployment should be in files on the host used for deployment.
The secrets needed by a pod in a cluster can be made available either as an environment variable or as a file.
While running deployment script, all secrets required should be available on the deployment machine at the following location:
`~/.sonar/secrets/<stage>/<secretname>`.
In other words, all secrets required by cluster should be already copied in this directory from 1password, before running deployment script.

There are two steps to using secrets in the cluster.
1. Creating a secret
2. Configuring the service to use a secret

## Quick Setup

Ensure these environment variables are set:

- BITSO_API_KEY
- BITSO_SECRET_KEY
- FIX_SOCKET_KEY_STORE_PASSWORD
- FIX_PRICE_GATEWAY_LOGON_PASSWORD
- FIX_ORDER_GATEWAY_LOGON_PASSWORD


Then load these env vars into secrets directory.
```
# Auth Service
echo -n $STAGING_BITSO_API_KEY > ~/.sonar/secrets/staging/bitso-api-key.txt
echo -n $STAGING_BITSO_SECRET_KEY > ~/.sonar/secrets/staging/bitso-secret-key.txt
echo -n "$(head /dev/urandom | LC_CTYPE=C tr -dc A-Za-z0-9 | head -c64)" > ~/.sonar/secrets/staging/bitso-auth-play-secret-key.txt

# Market Service
echo -n "$(head /dev/urandom | LC_CTYPE=C tr -dc A-Za-z0-9 | head -c64)" > ~/.sonar/secrets/staging/bitso-market-play-secret-key.txt

# TT FIX Service
echo -n "$(head /dev/urandom | LC_CTYPE=C tr -dc A-Za-z0-9 | head -c64)" > ~/.sonar/secrets/staging/bitso-tt-fix-play-secret-key.txt
echo -n $STAGING_FIX_SOCKET_KEY_STORE_PASSWORD > ~/.sonar/secrets/staging/socket-key-store-password.txt
echo -n $STAGING_FIX_PRICE_GATEWAY_LOGON_PASSWORD > ~/.sonar/secrets/staging/price-gateway-password.txt
echo -n $STAGING_FIX_ORDER_GATEWAY_LOGON_PASSWORD > ~/.sonar/secrets/staging/order-gateway-password.txt
```

At the end you should see:

```
~/.sonar/secrets/staging$ ls

bitso-api-key.txt		bitso-market-play-secret-key.txt  bitso-tt-fix-play-secret-key.txt	price-gateway-password.txt
bitso-auth-play-secret-key.txt	bitso-secret-key.txt		  fix.bitso.sonartrade.com-full.pkcs12	socket-key-store-password.txt
```

## Secrets Creation
This is done in `sonar.sh` script before the cluster services are created. An example from the current script that creates a socket-key-store-password for tt-fix service is:

```
kubectl -n sonartrade create secret generic socket-key-store-password --from-file=$secretsFolder/socket-key-store-password.txt
```

## Configuration

Secrets are injected into the deployments(Ex: `micros/bitso-tt-fix/00-full.yaml`) by the `micros/scripts/service_yaml_editor` script which is run from `rp.sh` at the time of new releases. The script automatically injects the secrets based on configuration in `SERVICE_SECRET_MAPPING`.

```
service_yaml_editor add_secrets_settings
```

The configuration for each service is of the format:

```
"<service-name>" => {
  "envs": {
      "<env-where-app-accesses-the-secret>": {
        "secret": "<kubernetes-secret-name>"
        "secretKey": "<key-within-secret>"
      }
    },
  "files": [
    {
      name: "<secret-name>",
      secretName: "<kubernetes-secret-name>", // optional, defaults to name
      items: [
        {
          secretKey: "<key-within-secret>",
          path: "<path-where-file-is-created>", // optional, defaults to secretKey
          env: "<optional-env-which-will-contain-the-file-name>", //optional
        }
      ]
    }
  ]
}
```

Each kubernetes secret is a key value store. The secretKey in the configuration is the key with which the secret has been created.
Most keys have a single key-value pair.
One example with multiple key pairs, is username and password information(store something like `{"username": "<username>", "password": <password>}` by kubernetes).

All secrets which you want available as env vars should be configured in envs of service secret config.
All secrets whose contents should be available to the app through a file should be configured in files.

Each file secret is mounted as a volume at `/etc/sonar/secrets/<name>`.

Each secret file will be created as `/etc/sonar/secrets/<secretName>/<secretKey>`.
For applications which expect the secret file path in an env variable like the way TT FIX service expects the secret store file, env key can be used.

Example configuration can be found in service_yaml_editor script.

# Common Issues

## Volume Affinity Issues During Cluster Creation

If volume affinity settings do not match with node affinity settings of tt-fix-server, run:
```
kubectl delete -f micros/bitso-tt-fix/storage-class.yaml
```
* Find the zone in which tt-fix-service's node is running. Change zone in storage-class.yaml
* Delete stateful set and persistent volume claim of ttfix service.
* Run:
```
kubectl apply -f micros/bitso-tt-fix/storage-class.yaml
kubectl apply -f micros/bitso-tt-fix/00-full.yaml
```

## Access Issues

The `config-map-aws-auth-<namespace>-<stage>-eks-cluster.yaml` generated by creation script contains permissions for cluster based on AWS access. The yaml itself is generated by terraform, then updated by `aws-auth-config-map` script to add access for our team members, then applied to kubectl.

Sometimes the file is not created in time and the access will be blocked. In such cases, run the `aws-auth-config-map` script and then run:

```
kubectl apply -f config-map-aws-auth-<namespace>-<stage>-eks-cluster.yaml
```

## AWS Volume Creating
Sometimes the AWS volume may be stuck in creating state. This prevents pods of a deployment or statefulset from coming up as 'timeout expired waiting for volumes to create'. This can be addressed by:
1. Scale down the deployement to 0 pods or delete the statefulset.
2. Delete the statefulset, pvc and the AWS volume(from AWS console) in that order.
3. Even the PVC deletion may fail. See https://github.com/kubernetes/kubernetes/issues/69697 for addressing the issue.
4. Recreate the statefulset or scale up the deployment.

Sample error message:
```
pod has unbound immediate PersistentVolumeClaims
AttachVolume.Attach failed for volume "pvc-cede2a09-c509-11e9-9ef3-0295e5c97908" : "Error attaching EBS volume \"vol-06c28d8a804e1395c\"" to instance "i-08d9e49cea1f6b744" since volume is in "creating" state
Unable to mount volumes for pod "bitsottfixservice-v0-1-38-0_sonartrade(ceded8ea-c509-11e9-9ef3-0295e5c97908)": timeout expired waiting for volumes to attach or mount for pod "sonartrade"/"bitsottfixservice-v0-1-38-0". list of unmounted volumes=[tt-fix-data]. list of unattached volumes=[tt-fix-data secrets-bitso-sonar-keystore default-token-qhn8h]
```

## Secret Inconsistency
Sometimes due to endline character of OSX and invisible characters, an additional character will stick around in the kubectl generated secret causing application logons to fail and other similar auth issues. Truncate the secret file to appropriate size to make sure the number of characters is correct.

>truncate -s <length_of_secret> <secret_file_name>

# Useful Commands

* Following logs of a pod
```
kubectl logs -f <pod>
```
* Terraform state locked in inconsistent state. This is usually due to SIGINT during script run or when someone else is changing state at the same time. Use this remedy only in the former case. In the latter case, wait for their run to complete. One way to confirm is to note down the lock id and noticing that it does not change.
```
terraform force-unlock LOCKID
```
* Logging into a pod:
```
kubectl exec -n <kubernetes-namespace> -it <pod-name> bash
```
Example: `kubectl exec -it bitsottfixservice-v0-1-5-8685d575f8-22zwv -n sonartrade bash`  
or if there is no bash on the pod:
```
kubectl exec -n <kubernetes-namespace> -it <pod-name> sh
```
* Getting kubernetes dashboard token:
```
kubectl  -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
```



----
