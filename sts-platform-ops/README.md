
Relevant docs


R Studio
- rstudio-server/README.md

Bitso-TT
- Monitoring: logging-monitorgin/Readme.md
- Main cluster and microservices: ./DOCUMENTATION.md


Copy to RDS ruby script not located in this project

Using Terraform to manage our AWS resources

Using Kubernetes in AWS (EKS)

Deploying all components of the platform to Kubernetes.


## AWS Deployment

### Dependencies

- ruby, bundler
- AWS CLI
- aws-iam-authenticator
- kubectl
- jq

### Instructions
1. Setup access:
    - login to aws console
    - then get enter a link like this:
    `https://console.aws.amazon.com/iam/home?region=us-east-1#/users/tdarex?section=security_credentials`
    - then go to `Access Keys`  .. and `create  access key`

2. Configure AWS profile:
    ```bash
    aws  configure  --profile st

    AWS Access Key ID [None]:
    AWS Secret Access Key [None]:
    Default region name [None]: us-east-1
    Default output format [None]: json
    ```
3. Choose a namespace:

    Edit `export TF_VAR_namespace=tdarex` accordingly in sonar.sh.
    Update kubectl context accordingly `aws --profile=st eks update-kubeconfig --name charles-prod-eks-cluster`

4. Ensure no CIDR conflict. Check the deployed VPCs. Compare to aws/variables.tf. Update `vpc_cidr_block` if there is an IP collision.
4. Run deployment script:
    `./sonar.sh`

5. Turn on proxy:
    `kubectl proxy --disable-filter=true`

6. Do manual steps
    - Inbound settings for worker security groups.
    ```
    // 1. Go to AWS website and go to EC2 Dashboard
    // 2. Select "Security Groups" under "Network & Security"
    // 3. There should be a security group named 'name-prod-eks-workers'
    // 4. Click on it
    // 5. Select the "Inbound" tab and hit "Edit"
    // 6. "Add Rule": Type=All traffic, Protocol=All, Port Range=0-65535, Source=Custom, IP=172.31.0.0/16 (IP of VPC with VPN), Description=VPN to VPC
    ```
    - Subnet routing
    ```
    // 1. Go to AWS website and go to VPC Dashboard
    // 2. Select "Route Tables" under "Virtual Private Cloud"
    // 3. Select the public route, like 'name-prod-eks-public'
    // 4. "Edit routes" -> "Add route"
    // 5. Destination=172.31.0.0/16	Target=Peering connection
    // 6. Repeat the same thing for both of the 'name-prod-eks-private' subnets
    ```
7. Test service:
    - Ping market service: `http://127.0.0.1:8001/api/v1/namespaces/tdarex3/services/http:bitsomarketservice:80/proxy/ping/detailed`
    - Kubernetes dashboard:
    ```
    # http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
    kubectl  -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
    ```       
    - Create a pod in the same cluster:
    ```
    kubectl  --namespace=sonartrade run test-pod --rm -i --tty --image ubuntu -- bash
    apt update
    apt install -y curl iputils-ping net-tools wget openjdk-8-jre
    wget http://apache.mirror.h1.nl/kafka/2.3.0/kafka_2.11-2.3.0.tgz
    tar xzf kafka_2.11-2.3.0.tgz
    ./kafka_2.11-2.3.0/bin/kafka-console-consumer.sh --bootstrap-server 172.28.27.17:9092 --topic bitso-order-req-topic
    ./kafka_2.11-2.3.0/bin/kafka-console-consumer.sh --bootstrap-server 172.28.27.17:9092 --topic bitso-auth-user-order-topic

    // reattach
    kubectl --namespace=sonartrade attach test-pod-bd48f78d9-wt2zd -c test-pod -i -t
    ```
    - SSH into a pod:
    ```
    kubectl get pods --namespace=sonartrade
    kubectl --namespace=sonartrade exec -ti POD_NAME /bin/bash
    ```                
    - Listen over kafka: `~/opt/kafka_2.11-2.2.0/bin/kafka-console-consumer.sh --bootstrap-server 172.28.13.164:9092 --topic bitso-order-req-topic`
8. Tear down:
    `cd aws & terraform destroy`

9. Redeploy a UI service:
    ```
    docker build -t marketui .
    docker build -t ttfixui .
    docker build -t authui .

    $(aws --profile=st ecr get-login --no-include-email --region us-east-1) // login
    docker tag marketui 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service-ui:latest22
    docker push 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service-ui:latest11

    // Modify 01-ui.yaml to point to the new resource
    kubectl apply -f micros/bitso-market/01-ui.yaml
    ```
10. Redeploy a backend service:
    0. Make sure sts-common/StsTopicCreator.java sets `DEFAULT_ZOOKEEPER_HOSTS=zookeeper:2181` and not localhost.
    00. Make this change to BitsoScaleHelper
    ```
    private BitsoScaleHelper() {
        try {

          Path path = File.createTempFile("bitso", "json").toPath();
          Files.copy(ClassLoader.getSystemResourceAsStream(BitsoMarketServiceConfig.BITSO_SCALE_MAP_FILE), path, StandardCopyOption.REPLACE_EXISTING);

          byte[] mapData = Files.readAllBytes(path);
              //NOTE: this won't work , when the file is bundled in a jar.
              //Paths.get(BitsoMetaDataCollector.class.getClassLoader().getResource(BITSO_SCALE_MAP_FILE).toURI()));

          ObjectMapper objectMapper = new ObjectMapper();
          this.bookScaleMap = objectMapper.readValue(mapData, new TypeReference<HashMap<CurrencyPair, BookScale>>() {
          });

        } catch (IOException  e) {
          LOGGER.error("Could not read bitso scale map json file");
          bookScaleMap = null;
        }
      }
    ```
    000. Copy market-service-impl/prod.conf.example to application.conf
    0000. If FIX service, change the configs.
    ```
    DataDictionary=TTFIX.xml
    TransportDataDictionary=TTFIX.xml
    AppDataDictionary=TTFIX.xml
    ```
    1. `sbt clean compile docker:publishLocal`
    2. $(aws --profile=st ecr get-login --no-include-email --region us-east-1)
    3. docker tag bitso-market-service-impl:0.0.1 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service:v0.0.1
    4. docker push 379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service:v0.0.1
    5. Update bitso-market/00-full.yaml with new docker image name `379276526719.dkr.ecr.us-east-1.amazonaws.com/bitso-market-service:v0.0.1`. Note I edited the existing yaml, I did not run `rp` to regenerate.
    6. `kubectl apply -f micros/bitso-market/00-full.yaml`
    7. New pod successfully spun up but see logs.
11. Test production endpoints
    ```
    // Install https://github.com/danielstjules/wsc
    wsc ws://172.28.18.46:9000/bitsomarketservice/marketdata
    wsc ws://localhost:52473/connectivity-changed-stream
12. Troubleshooting
    ```
    // Clear docker
    $ docker images
    $ docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
    $ docker images | grep "none"
    $ docker rmi $(docker images | grep "none" | awk '/ / { print $3 }')
    ```
### Local Setup

Install minikube : https://kubernetes.io/docs/tasks/tools/install-minikube/
Basically, on mac :
```bash
brew cask install minikube

# or
brew cask upgrade minikube

```

#### Quick start
example: https://kubernetes.io/docs/setup/minikube/#quickstart
```bash
minikube start
kubectl get pod
```

If you get the annoying error `192.168.99.100:8443: connect: connection refused`
Run the following sequence:
```bash
minikube stop
kubectl config use-context minikube
minikube start
```
Then you should be able to run:
```bash
kubectl get nodes
kubectl get pods
kubectl get services

minikube dashboard
```
`minikube dashboard` will run a proxy, from 127.0.0.1 to the VirtualMachines Address.




#### Quick Setup
Run all.

- Setup
- Cassandra
- Zookeeper
- Kafka
- Micros (pending)

```bash

#If you've already started .. skip
minikube start --cpus 4 --memory 4096

# Need this enabled
minikube addons enable ingress


kubectl create -f ./setup/01-namespace.yml
kubectl create -f ./setup/rbac.yaml

kubectl create -f ./cassandra/cassandra-service.yaml
kubectl create -f ./cassandra/cassandra-statefulset.yaml


kubectl create -f ./zookeeper/11-zookeeper-service.yml
kubectl create -f ./zookeeper/11-zookeeper-service-headless.yml
kubectl create -f ./zookeeper/14-zookeeper-statefulset.yml
kubectl create -f ./zookeeper/15-zookeeper-disruptionbudget.yml

kubectl create -f ./kafka/21-kafka-service.yml
kubectl create -f ./kafka/22-kafka-service-headless.yml
kubectl create -f ./kafka/24-kafka-statefulset.yml

#
# Under ./sts-bitso:  
eval $(minikube docker-env)  
sbt docker:publishLocal

#then back to this project ./platform:
kubectl create -f ./micros/bitso-market/00-full.yaml
kubectl delete -f ./micros/bitso-market/00-full.yaml

kubectl create -f ./micros/bitso-market/01-ui.yaml
kubectl delete -f ./micros/bitso-market/01-ui.yaml

# pending:
kubectl create -f ./micros/bitso-auth/00-full.yaml
kubectl create -f ./micros/bitso-tt-fix/00-full.yaml


minikube dashboard

```


#### Quick Walk around
```bash
 #Find all the pods running ( all running instances of containers )
 kubectl get pods --all-namespaces

 #Just SonarTrade's
 kubectl get pods --namespace=sonartrade


```

#### Build Micro Yaml files
```bash

#Assuming you ran 'sbt docker:publishLocal' in sts-bitso project

#here
eval $(minikube docker-env)

# Build Market Configs
rp generate-kubernetes-resources --generate-all "bitso-market-service-impl:1.0-SNAPSHOT" --env JAVA_OPTS="-Dplay.crypto.secret=youmustchangeme4"  --namespace=sonartrade --env STS_CASSANDRA_HOST1=cassandra-1.cassandra --env STS_CASSANDRA_HOST2=cassandra-0.cassandra --env STS_CASSANDRA_PORT=9042 --env STS_KAFKA_HOST1=kafka-0.kafka-headless  --env STS_KAFKA_HOST2=kafka-1.kafka-headless --env STS_KAFKA_PORT=9092      --akka-cluster-join-existing  > ./micros/bitso-market/00-full.yaml

# Build Market Configs
rp generate-kubernetes-resources --generate-all "bitso-auth-service-impl:1.0-SNAPSHOT" --env BITSO_API_KEY="somekey"  --env BITSO_SECRET_KEY="somesecret"  --env JAVA_OPTS="-Dplay.crypto.secret=youmustchangeme4"  --namespace=sonartrade --env STS_CASSANDRA_HOST1=cassandra-1.cassandra --env STS_CASSANDRA_HOST2=cassandra-0.cassandra --env STS_CASSANDRA_PORT=9042 --env STS_KAFKA_HOST1=kafka-0.kafka-headless  --env STS_KAFKA_HOST2=kafka-1.kafka-headless --env STS_KAFKA_PORT=9092   --env archiveDir=/tmp   --akka-cluster-join-existing  > ./micros/bitso-auth/00-full.yaml

# Build Market Configs
rp generate-kubernetes-resources --generate-all "bitso-tt-fix-service-impl:1.0-SNAPSHOT" --env BITSO_API_KEY="somekey" --env BITSO_SECRET_KEY="somesecret" --env JAVA_OPTS="-Dplay.crypto.secret=youmustchangeme4"  --namespace=sonartrade --env STS_CASSANDRA_HOST1=cassandra-1.cassandra --env STS_CASSANDRA_HOST2=cassandra-0.cassandra --env STS_CASSANDRA_PORT=9042 --env STS_KAFKA_HOST1=kafka-0.kafka-headless  --env STS_KAFKA_HOST2=kafka-1.kafka-headless --env STS_KAFKA_PORT=9092       --akka-cluster-join-existing   > ./micros/bitso-tt-fix/00-full.yaml

# In each file generated, we have to overwrite 1 line.  to support the 1 instance akka cluster
# for the `RP_DYN_JAVA_OPTS`
# set the value to :
# value: "-Dakka.discovery.kubernetes-api.pod-namespace=$RP_NAMESPACE -Dakka.remote.netty.tcp.hostname=$RP_ENDPOINT_0_BIND_HOST -Dakka.remote.netty.tcp.port=2551 -Dakka.cluster.seed-nodes.0=akka.tcp://application@$RP_ENDPOINT_0_BIND_HOST:2551"

#Now you can run (assuming minikube is up)
kubectl create -f ./micros/bitso-market/00-full.yaml
kubectl create -f ./micros/bitso-auth/00-full.yaml
kubectl create -f ./micros/bitso-tt-fix/00-full.yaml


```


---
##### Debug Minikube Issues
Minikube stores its data in `~/.minikube` directory.
If your minikube in SNAFU'd .. just `minikube stop` , then delete that directory.

---
# Environments
Initialy, we'll only have a persistent Production Environment.   
All other environments are only deployed then undeployed when not being used.

---

## Development/Staging in AWS
All the steps to deploy a full stack, is followed in the `sonar.sh` file


---
## Production
For production, ***dont*** use the `sonar.sh` file.  
..

### Deployment Layers
Each layer is deployed separately in productions, and subsequent deployments are also separate

- AWS / Terraform   (via `terraform`) , in the ./aws dir
- Kubernetes setup (via `kubectl`) , are the yaml files in ./setup , ./tools
- Core Services (via `kubectl`) , are the yaml files in `./cassandra` `./kafka` , `./zookeepr`
- MicroService (via `kubectl`), yaml files in `./micros/*/00-full.yaml`
- Micro Ui's  (via `kubectl`) , yaml files in `./micros/*/01-ui.yaml`
- LoadBalancer/DNS (via `kubectl`)  
- Monitoring , (via `kubectl`) , yaml files in `./tools`

#### Notes:
Once the AWS layer is deployed (ie: `terraform apply`) , it will not be undeployed.
This will give us static IP addresses, provided via the Elastic IP (EIP) interfaces in AWS.  


### Secrets


Secrets can be created using `kubectl`. See [here](https://kubernetes.io/docs/concepts/configuration/secret/).

However AWS keys should not even be placed as secrets on the cluster. This creates an apportunity for attacking infrastructure from the application. We use [kube2iam](https://github.com/jtblin/kube2iam) for granting access to directly assign roles to pods.

See [here](https://kubernetes-sigs.github.io/aws-alb-ingress-controller/guide/walkthrough/echoserver/#kube2iam-setup) for a reference implementation.

1. Find the role associated woth your K8S nodes. Once you found take note of the full arn:
    Ex: arn:aws:iam::3792765123239:role/tgildas-prod-eks-workers
2. Create the role, called <namespace>-<stage>-k8s-alb-controller, attach the policy `sonartrade-alb-policy` and add a Trust Relationship like:
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::3792723236719:role/tgildas-prod-eks-workers"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```
3. The new role will have the arn: arn:aws:iam:::XXXXXXXXXXXX:role/<namespace>-<stage>-k8s-alb-controller
4. Update the alb-ingress-controller.yaml with the following:
    - cluster name
    - the arn role in annotation:
        iam.amazonaws.com/role: arn:aws:iam:::37927612323719:role/tgildas-k8s-alb-controller

## Service UI Access(Application Load Balancer)

Access to cluster UIs and market service API is possible at the endpoints in the following format:
`<service-name>.<namespace>-<stage>.cluster.sonar.internal`

For example, market service UI for tgildas's cluster can be accessed at `http://bitso-market-service-ui.tgildas-prod.cluster.sonar.internal`
However achieving this adds the cost of an application load balancer which is ~$0.0225 per hour. So this is disabled by default.

### Setup

Firstly for the DNS setup to work, add 172.31.0.2(our default VPC's DNS server) as your primary DNS server. And VPN should be connected for the access to the services to work. This is because all services resolve to internal IPs(internal to our network but external to cluster) of the pods.

To setup the UIs run:

> kubectl apply -f ingress/alb-ingress-controller.yaml
> kubectl apply -f ingress/subdomain-ingress.yaml

To bring down the load balancer, run:

> kubectl delete -f ingress/subdomain-ingress.yaml

This step is also run as part of the cleanup script. So there is no need for manual deletion if cleanup script is run.

### Working

The components which made the UI access possible are:
1. Ingress
2. ALB Ingress Controller
3. External DNS Controller

#### Ingress
An Ingress configures up the sub-domain based routes for each UI service and marketservice API. Only services of type NodePort can be exposed by ingress. This can be found in `ingress/subdomain-ingress.yaml`.

We use a [name based virtual hosting](https://kubernetes.io/docs/concepts/services-networking/ingress/#name-based-virtual-hosting) for the ingress.

#### ALB Ingress Controller

[ALB Ingress Controller](https://github.com/kubernetes-sigs/aws-alb-ingress-controller) creates an Application Load Balancer based on the rules in the ingress. This can be found in `ingress/alb-ingress-controller.yaml`. The appropriate targets are automatically created by the controller according to the naming scheme above.

#### External DNS Controller

[External DNS Controller](https://github.com/kubernetes-incubator/external-dns) watches loadbalancer ingresses/services and create DNS names for the same in a Route53 DNS zone. We use a DNS zone `sonar.internal` created in Route53. This configuration can be found in `ingress/external-dns.yaml`


### Possible Future Work
The same effect can be achieved using nginx-ingress controller as well. However nginx ingress controller creates an ELB(classic load balancer) which is slightly costlier than ALB. Also external dns controller with this load balancer creates A records with the public IPs of the nodes instead of the IPs of the service pods. But these public IPs will not be accessible as the worker nodes are in a private subnet. Hence this approach failed. ALB ingress(load balancer) could bypass this problem because it is a part of AWS ecosystem.

A cheaper possible way would have been to write an nginx service with the name based virtual routing setup and routes created to the service in route53 without needing any ALB or ELB. However it needs further more time to write code for this.



### Subsequent Deployments
After the full stack is deployed in production, all future deployments will be incremental `kubectl apply`, of existing yaml/configs
- MicroServices / Micro UI's:
  - New Docker Image is uploaded to AWS Docker Repository
  - Micro Yaml is updated with the new image version
  - `kubectl apply` is used to rollout the updated yaml     

### Cleaning up AWS cluster
Install ruby and install bundler(using `gem install bundler`)
From `sts-platform-ops` folder, run

> ./scripts/cleanup.sh

#### Backups

---
##### References
- https://kubernetes.io/docs/reference/kubectl/cheatsheet/
- https://github.com/mfactory-lab/lagom-on-kube/tree/master/kube/manuell
- https://github.com/lightbend/reactive-sandbox
