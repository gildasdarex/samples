
## Deploy / Teardown
NOTE : to deploy or destroy logging-monitoring, You need to use terraform 0.11.14

I- HOW TO DEPLOY LOGGING-MONITORING CLUSTER

1- logging-monitoring scripts folder
2- Run the following command to deploy the cluster

   ./k8s-deploy.sh --namespace logging-monitoring --vpc_cidr_block 172.29.0.0/16 --stage staging --aws_profile st --instance_type t3.large
   
   NOTE: You can update the values of each option
   
3- Create aws vpc peering connection between sonartrade apps cluster and logging-monitoring cluster

Go to aws console and :

 - Go to VPC section and get the value of sonartradeuatX-staging-eks vpc id
 - Go to aws-vpc-peering/terraform.tfvars and set the value of sonartrade_services_vpc_id with the new value that you get previously
 - Go to Route table section and use the filter to get all the route table associated to sonartradeuatX-staging-eks VPC.
   You must have 4 rows after the filter.
 - 
   
   
4- Deploy elk apps by running the following commands(be sure that you are in logging-monitoring directory) :
   - kubectl apply -f 1_k8s-global/namespace.yml
   - kubectl apply -f 1_k8s-global/aws-storage.yml
   - kubectl apply -f 2_elasticsearch
   - kubectl apply -f 3_kibana


II- How to cleanup logging and monitoring

  - First we need to destroy the aws vpc peering connection between sonartrade cluster and logging-monitoring
    cluster. For this purpose, Run the following commands:
  
     - cd logging-monitoring/aws-vpc-peering
     - terraform init
     - terraform state pull
     - terraform destroy -lock=false
     
 
  - We can now destroy the logging-monitoring by running the following commands:

      - cd logging-monitoring/aws
      - terraform init
      - terraform state pull
      - terraform workspace select logging-monitoring-staging
      - terraform destroy -lock=false
      
   You can also destroy the environment by running destroy-k8s.sh script in scripts folder
   ./destroy-k8s.sh   
   
 ## Accessing a deployed instance
 
1. Update to the correct Kubernetes context:
    ```
     aws eks update-kubeconfig --name NAMESPACE-STAGE-eks-cluster
     aws eks update-kubeconfig --name logging-monitoring-staging-eks-cluster
    ```
2. Run 
   kubectl config get-contexts
   
   To get the context id . it should looks like 
   arn:aws:eks:us-east-1:xxxxxx:cluster/logging-monitoring-staging-eks-cluster
   
3. switch to logging-monitoring cluster by running
   kubectl config use-context arn:aws:eks:us-east-1:xxxxxx:cluster/logging-monitoring-staging-eks-cluster
    

4. Port forward to the deployed Kibana
    ```
    kubectl port-forward service/kibana 5601 -n=sonartrade      
    ```

5. Go to: `http://localhost:5601/app/kibana#/discover`



 ## Redeploy logstash after deploying new version of bisto-tt-fix service
 
 
 1. Update pvc used by logstash deployment: 
      - Open logging-monitoring/logstash/logstash.yml file
      - In volumes section, search persistence volume claim used ; by example
      
        persistentVolumeClaim:
                      claimName: tt-fix-data-bitsottfixservice-v0-1-51-0
      -  Update v0-1-51-0 to the last version of bitso-tt-fix service that is deployed
      -  Then run :          
         ```
          $ kubectl delete deployments logging-logstash -n sonartrade
          $ kubectl apply -f logstash/logstash.yml
         ```
http://localhost:5601/app/kibana#/discover?_g=(filters:!())&_a=(columns:!(_source),filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'filebeat-*',key:'@timestamp',negate:!f,params:(query:'2019-09-10T06:50:04.815Z'),type:phrase,value:'Sep%2010,%202019%20@%2006:50:04.815'),query:(match:('@timestamp':(query:'2019-09-10T06:50:04.815Z',type:phrase)))),('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'filebeat-*',key:kubernetes.labels.app,negate:!f,params:(query:bitsottfixservice),type:phrase,value:bitsottfixservice),query:(match:(kubernetes.labels.app:(query:bitsottfixservice,type:phrase))))),index:'filebeat-*',interval:auto,query:(language:kuery,query:''),sort:!('@timestamp',desc))

http://localhost:5601/app/kibana#/discover?_g=(filters:!())&_a=(columns:!(_source),filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'filebeat-*',key:'@timestamp',negate:!f,params:(query:'2019-09-10T05:23:47.975Z'),type:phrase,value:'Sep%2010,%202019%20@%2006:50:04.815'),query:(match:('@timestamp':(query:'2019-09-10T05:23:47.975Z',type:phrase)))),('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'filebeat-*',key:kubernetes.labels.app,negate:!f,params:(query:bitsottfixservice),type:phrase,value:bitsottfixservice),query:(match:(kubernetes.labels.app:(query:bitsottfixservice,type:phrase))))),index:'filebeat-*',interval:auto,query:(language:kuery,query:''),sort:!('@timestamp',desc))


## Access to logging monitoring tools
 - Grafana : kubectl port-forward service/grafana 3000 -n=mon
 - Prometheus : kubectl port-forward service/prometheus-service 8080 -n=mon
 
 
## Curator  
Curator is configured currently to delete event fom filebeat and prices-gateway index

```
    # Remember, leave a key empty if there is no value.  None will be a string,
        # not a Python "NoneType"
        #
        # Also remember that all examples have 'disable_action' set to True.  If you
        # want to use this action as a template, be sure to set this to False after
        # copying it.
        actions:
          1:
            action: delete_indices
            description: "Delete events from filebeat index that have some age"
            options:
              timeout_override:
              continue_if_exception: False
              disable_action: False
              ignore_empty_list: True
            filters:
            - filtertype: pattern
              kind: prefix
              value: filebeat-
            - filtertype: age
              source: name
              direction: older
              timestring: '%Y.%m.%d'
              unit: days
              unit_count: 3
              field:
              stats_result:
              epoch:
              exclude: False
          2:
            action: delete_indices
            description: "Delete events from prices-gateway index that have some age"
            options:
              timeout_override:
              continue_if_exception: False
              disable_action: False
              ignore_empty_list: True
            filters:
            - filtertype: pattern
              kind: prefix
              value: prices-gateway-
            - filtertype: age
              source: name
              direction: older
              timestring: '%Y.%m.%d'
              unit: days
              unit_count: 3
              field:
              stats_result:
              epoch:
              exclude: False      
  ```
According to configuration above , we have 2 delete action
  - first delete action , delete all event from index that is prefixed by filebeat
    ```
        filtertype: pattern
        kind: prefix
        value: filebeat-
        ```
    and that have 3 days as age
    
    ```
            filtertype: age
            source: name
            direction: older
            timestring: '%Y.%m.%d'
            unit: days
            unit_count: 3
            field:
            stats_result:
            epoch:
            exclude: False
            ```
    
    - second delete action , delete all event from index that is prefixed by prices-gateway
        ```
            filtertype: pattern
            kind: prefix
            value: prices-gateway-
            ```
        and that have 3 days as age
        
        ```
                filtertype: age
                source: name
                direction: older
                timestring: '%Y.%m.%d'
                unit: days
                unit_count: 3
                field:
                stats_result:
                epoch:
                exclude: False
        
        
        ```
        
- To deploy curator , run  :
```
 kubectl apply -f curator/    
 ```    
 
 - To watch curator job , run  :
 ```
  kubectl get jobs -n sonartrade --watch    
  ```    
  
 - Output will looks like
  
    NAME                 COMPLETIONS   DURATION   AGE
    curator-1571301000   0/1                      0s
    curator-1571301000   0/1           0s         0s
    curator-1571301000   1/1           9s         9s
    
 -  Get pods by running 
    kubectl get pods -n sonartrade --selector=job-name=curator-1571301000 --output=jsonpath={.items[].metadata.name}
    
 - View cron pods logs by running
   kubectl logs curator-1571301000-kjxjq -n sonartrade 
   
## TAKE/RESTORE A BACKUP OF ELASTICSEARCH INDICES
  ### TAKE A BACKUP OF ELASTICSEARCH INDICES
  
  - CREATE S3 BUCKET IN AWS named prod-elasticsearch for prod environment
  
  - CREATE ELASTICSEARCH SNAPSHOT REPOSITORY
  ```
   curl -XPUT 'http://localhost:9200/_snapshot/s3_repository?verify=false&pretty' -d'
   {
    "type": "s3",
    "settings": {
      "bucket": "prod-elasticsearch",
      "access_key": "...",
      "secret_key": "..."
    }
   }'
    
   ```
   
   type : we used “s3” to specify AWS S3 service\
   settings.bucket : AWS S3 bucket name\
   access_key : user’s access key\
   secret_key : user’s secret key\
   
   
  - BACKUP ELASTICSEARCH DATA : CREATE THE SNAPSHOT
  
  ```
     curl -XPUT "http://localhost:9200/_snapshot/s3_repository/snapshot1?wait_for_completion=true" -d'
     {
       "indices": "index_1, index_2, index_3", 
       "ignore_unavailable": true,
       "include_global_state": false
     }
     
   ```
      
   snapshot1 : is the name of snapshot to create.\
   wait_for_completion : this directive tells the command to wait for the snapshot to complete before returning status information, which could be a problem if you are doing a snapshot of a lot of data. If you omit this parameter the command will return immediately.\
   indices : specify which indices to backup.\
   ignore_unavailable : if index doesn’t exist skip to the next index in the list otherwise break execution if is set to false.\
   include_global_state : setting it to false prevent Elastic to put the global cluster state from being put in the snapshot, this allow to restore the snapshot on another cluster with different attributes.\

   ### RESTORE A BACKUP OF ELASTICSEARCH INDICES
   
   To restore the data in new elasticsearch cluster,  after creating the new elasticsearch cluster, do step I and II . Then restore the data by running 
   
   ```
        curl -s -XPOST --url "http://localhost:9200/_snapshot/s3_repository/snapshot1/_restore" -d'
        {
           "indices": "index_1,index_2",
           "ignore_unavailable": true,
           "include_global_state": false,
           "rename_pattern": "(\\w+)",
           "rename_replacement": "$1_dev"
        }'

        
   ```