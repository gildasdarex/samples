terraform {
  backend "s3" {
    region = "us-east-1"
    profile = "st"
    bucket  = "prod.ops"
    key     = "terraform/sts-platform-ops/prod/logging/terraform.tfstate"
    dynamodb_table = "terraform-state-prod-logging-lock-table"
  }
}

provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}

# This `label` is needed to prevent `count can't be computed` errors
module "label" {
  source     = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=0.11/master"
  namespace  = "${var.namespace}"
  name       = "${var.name}"
  stage      = "${var.stage}"
  delimiter  = "${var.delimiter}"
  attributes = "${var.attributes}"
  tags       = "${var.tags}"
  enabled    = "${var.enabled}"
}

# This `label` is needed to prevent `count can't be computed` errors
module "cluster_label" {
  source     = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=0.11/master"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  delimiter  = "${var.delimiter}"
  attributes = ["${compact(concat(var.attributes, list("cluster")))}"]
  tags       = "${var.tags}"
  enabled    = "${var.enabled}"
}

locals {
  # The usage of the specific kubernetes.io/cluster/* resource tags below are required
  # for EKS and Kubernetes to discover and manage networking resources
  # https://www.terraform.io/docs/providers/aws/guides/eks-getting-started.html#base-vpc-networking
  tags = "${merge(var.tags, map("kubernetes.io/cluster/${module.label.id}", "shared"))}"
}

# https://github.com/cloudposse/terraform-aws-vpc
module "vpc" {
  source     = "git::https://github.com/cloudposse/terraform-aws-vpc.git?ref=0.11/master"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = "${var.attributes}"
  tags       = "${local.tags}"
  cidr_block = "${var.vpc_cidr_block}"
}

module "subnets" {
  source              = "git::https://github.com/cloudposse/terraform-aws-dynamic-subnets.git?ref=0.11.0"
  availability_zones  = ["${var.availability_zones}"]
  namespace           = "${var.namespace}"
  stage               = "${var.stage}"
  name                = "${var.name}"
  attributes          = "${var.attributes}"
  tags                = "${merge(var.tags,
                            map(
                              "kubernetes.io/cluster/${var.name}", "shared",
                              "kubernetes.io/role/internal-elb", ""
                            )
                          )}"
  region              = "${var.region}"
  vpc_id              = "${module.vpc.vpc_id}"
  igw_id              = "${module.vpc.igw_id}"
  cidr_block          = "${module.vpc.vpc_cidr_block}"
  nat_gateway_enabled = "true"

  #tags  :  kubernetes.io/cluster/<CLUSTERNAME>  = shared
  # kubernetes.io/role/elb 1
}


module "eks_cluster" {
  source                  = "git::https://github.com/cloudposse/terraform-aws-eks-cluster.git?ref=0.11/master"
  namespace               = "${var.namespace}"
  stage                   = "${var.stage}"
  name                    = "${var.name}"
  attributes              = "${var.attributes}"
  tags                    = "${var.tags}"
  vpc_id                  = "${module.vpc.vpc_id}"
  kubernetes_version      = "${var.kubernetes_version}"

  subnet_ids              = ["${module.subnets.public_subnet_ids}"]
  allowed_security_groups = ["${var.allowed_security_groups_cluster}"]

  # `workers_security_group_count` is needed to prevent `count can't be computed` errors
  workers_security_group_ids   = ["${module.eks_workers.security_group_id}"]
  workers_security_group_count = 1

  allowed_cidr_blocks = ["${var.allowed_cidr_blocks_cluster}"]
  enabled             = "${var.enabled}"
}

module "eks_workers" {
  source                             = "git::https://github.com/cloudposse/terraform-aws-eks-workers.git?ref=0.11/master"
  namespace                          = "${var.namespace}"
  stage                              = "${var.stage}"
  name                               = "${var.name}"
  attributes                         = "${var.attributes}"
  tags                               = "${var.tags}"
  image_id                           = "${var.image_id}"
  eks_worker_ami_name_filter         = "${var.eks_worker_ami_name_filter}"
  instance_type                      = "${var.instance_type}"
  vpc_id                             = "${module.vpc.vpc_id}"
  subnet_ids                         = ["${module.subnets.private_subnet_ids[0]}"]
  health_check_type                  = "${var.health_check_type}"
  min_size                           = "${var.min_size}"
  max_size                           = "${var.max_size}"
  wait_for_capacity_timeout          = "${var.wait_for_capacity_timeout}"
  associate_public_ip_address        = "${var.associate_public_ip_address}"
  cluster_name                       = "${module.cluster_label.id}"
  cluster_endpoint                   = "${module.eks_cluster.eks_cluster_endpoint}"
  cluster_certificate_authority_data = "${module.eks_cluster.eks_cluster_certificate_authority_data}"
  cluster_security_group_id          = "${module.eks_cluster.security_group_id}"
  allowed_security_groups            = ["${var.allowed_security_groups_workers}"]
  allowed_cidr_blocks                = ["${var.allowed_cidr_blocks_workers}"]
  enabled                            = "${var.enabled}"
  key_name                           = "${var.eks_nodes_ssh_key_name}"

  # Auto-scaling policies and CloudWatch metric alarms
  autoscaling_policies_enabled           = "${var.autoscaling_policies_enabled}"
  cpu_utilization_high_threshold_percent = "${var.cpu_utilization_high_threshold_percent}"
  cpu_utilization_low_threshold_percent  = "${var.cpu_utilization_low_threshold_percent}"
}

# Add an alb policy to the eks workers' IAM role
//resource "aws_iam_role_policy_attachment" "alb_policy_application_to_cluster_role" {
//  role       = "${module.eks_workers.worker_role_name}"
//  policy_arn = "${var.alb_policy_arn}"
//}

# Add an external dns policy to the eks workers' IAM role
//resource "aws_iam_role_policy_attachment" "dns_policy_application_to_cluster_role" {
//  role       = "${module.eks_workers.worker_role_name}"
//  policy_arn = "${var.dns_policy_arn}"
//}

# https://github.com/terraform-community-modules/tf_aws_nat

//module "vpc_peering" {
//  enabled = false
//  source           = "git::https://github.com/cloudposse/terraform-aws-vpc-peering.git?ref=tags/0.1.4"
//  namespace           = "${var.namespace}"
//  stage               = "${var.stage}"
//  name                = "${var.name}"
//  requestor_vpc_id = "${module.vpc.vpc_id}"
//  acceptor_vpc_id  = "vpc-27822a5e"
//  auto_accept  = true
//}

/*
resource "aws_vpc_peering_connection" "vpc_peering" {
  peer_vpc_id = "vpc-27822a5e"   # target , is the default VPC , hosting VPN   vpc1
  vpc_id = "${module.vpc.vpc_id}" #vpc2
  auto_accept = true
  accepter {
    allow_remote_vpc_dns_resolution = true
  }
  requester {
    allow_remote_vpc_dns_resolution = true
  }
  tags {
    Name = "Bridging back to DEFAULT"
    Namespace = "${var.namespace}"
  }
}



resource "aws_route" "default_to_eks_peering_route" {
  route_table_id         = "rtb-d31a6eab"  # default vpc's masgter route
  destination_cidr_block = "${module.vpc.vpc_cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.vpc_peering.id}"
}

resource "aws_route" "eks_to_default_peering_route" {
  route_table_id         = "${module.vpc.vpc_main_route_table_id}"  # default vpc's masgter route
  destination_cidr_block = "172.31.0.0/16"
  vpc_peering_connection_id ="${aws_vpc_peering_connection.vpc_peering.id}"
}

resource "aws_route" "private_vpn_route_entry" {
  //module.subnets.private_route_table_ids has the same number of element with var.availability_zones
  count = "${length(var.availability_zones)}"
  route_table_id            = "${module.subnets.private_route_table_ids[count.index]}"
  destination_cidr_block = "172.31.0.0/16"
  vpc_peering_connection_id ="${aws_vpc_peering_connection.vpc_peering.id}"
}

resource "aws_route" "public_vpn_route_entry" {
  //module.subnets.public_route_table_ids has always 1 element
  route_table_id            = "${module.subnets.public_route_table_ids[0]}"
  destination_cidr_block = "172.31.0.0/16"
  vpc_peering_connection_id ="${aws_vpc_peering_connection.vpc_peering.id}"
}


#
# Add an inbound rule, to allow the Default VPC (i.e.: the VPN) , access to the worker nodes
#
resource "aws_security_group_rule" "allow_all_from_default" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "all"
  cidr_blocks = ["172.31.0.0/16"] #Default VPC's Address space, where the VPN lives
  security_group_id = "${module.eks_workers.security_group_id}"
}
*/


//module "rds_cluster_aurora_postgres" {
//  source          = "git::https://github.com/cloudposse/terraform-aws-rds-cluster.git?ref=master"
//  name            = "postgres"
//  engine          = "aurora-postgresql"
//  cluster_family  = "aurora-postgresql9.6"
//  cluster_size    = "1"
//  namespace           = "${var.namespace}"
//  stage               = "${var.stage}"
//  admin_user      = "admin1"
//  admin_password  = "Test123456789"
//  db_name         = "dbname"
//  db_port         = "5432"
//  instance_type   = "db.r4.large"
//  vpc_id          = "${module.vpc.vpc_id}"
//  security_groups = ["sg-xxxxxxxx"]
//  subnets         = ["${module.subnets.public_subnet_ids}"]
//  zone_id         = "Zxxxxxxxx"
//}
