namespace="sonartradeuat8-logging"
sonartrade_services_vpc_id = "vpc-0cdd68f8660894823"
sonartrade_services_vpc_cidr_block = "172.22.0.0/16"
sonartrade_services_main_route_table_id = "rtb-06ce4bd5359d36a8b"
sonartrade_services_private_route_table_ids = ["rtb-05165e5b93be28581", "rtb-001ea995433d58cb4"]
sonartrade_services_public_route_table_id = "rtb-0e6beee5ceee22727"
sonartrade_services_main_security_group_id = "sg-010f6c0ff83766895"

logging_monitoring_services_vpc_id = "vpc-0872181b4e126b5b2"
logging_monitoring_services_vpc_cidr_block = "172.32.0.0/16"
logging_monitoring_services_main_route_table_id = "rtb-0155bab90c0eb03f7"
logging_monitoring_services_private_route_table_ids = ["rtb-0588483f623ac2da0", "rtb-0e385ba68fff05cfd"]
logging_monitoring_services_public_route_table_id = "rtb-01ab3343b016934f2"
logging_service_services_main_security_group_id = "sg-0152fe5caf376497a"