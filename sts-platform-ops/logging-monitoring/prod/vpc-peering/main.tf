provider "aws" {
  profile = "${var.profile}"
  region = "${var.region}"
}

terraform {
  backend "s3" {
    region = "us-east-1"
    profile = "st"
    bucket  = "prod.ops"
    key     = "terraform/sts-platform-ops/prod/sonartrade-logging-vpc-peering/terraform.tfstate"
    dynamodb_table = "terraform-state-prod-logging-vpc-peering-lock-table"
  }
}

resource "aws_vpc_peering_connection" "sonartrade_services_logging_monitoring_services_vpc_peering_connection" {
  peer_vpc_id = "${var.logging_monitoring_services_vpc_id}"   # target , is the elk  VPC , hosting VPN   vpc1
  vpc_id = "${var.sonartrade_services_vpc_id}" #vpc2
  auto_accept = true
  accepter {
    allow_remote_vpc_dns_resolution = true
  }
  requester {
    allow_remote_vpc_dns_resolution = true
  }
  tags {
    Name = "sonartrade-logging-monitoring"
    Namespace = "${var.namespace}"
  }
}

resource "aws_route" "sonartrade_services_peering_main_route_with_logging_monitoring_services" {
  route_table_id         = "${var.sonartrade_services_main_route_table_id}"
  destination_cidr_block = "${var.logging_monitoring_services_vpc_cidr_block}"
  vpc_peering_connection_id ="${aws_vpc_peering_connection.sonartrade_services_logging_monitoring_services_vpc_peering_connection.id}"
}

resource "aws_route" "sonartrade_services_private_main_routes_with_logging_monitoring_services" {
  count = "${length(var.sonartrade_services_private_route_table_ids)}"
  route_table_id            = "${var.sonartrade_services_private_route_table_ids[count.index]}"
  destination_cidr_block = "${var.logging_monitoring_services_vpc_cidr_block}"
  vpc_peering_connection_id ="${aws_vpc_peering_connection.sonartrade_services_logging_monitoring_services_vpc_peering_connection.id}"
}

resource "aws_route" "sonartrade_services_public_main_routes_with_logging_monitoring_services" {
  route_table_id            = "${var.sonartrade_services_public_route_table_id}"
  destination_cidr_block = "${var.logging_monitoring_services_vpc_cidr_block}"
  vpc_peering_connection_id ="${aws_vpc_peering_connection.sonartrade_services_logging_monitoring_services_vpc_peering_connection.id}"
}


resource "aws_security_group_rule" "sonartrade_services_allow_all_from_logging_monitoring_services" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "all"
  cidr_blocks = ["${var.logging_monitoring_services_vpc_cidr_block}"]
  security_group_id = "${var.sonartrade_services_main_security_group_id}"
}


resource "aws_route" "logging_monitoring_services_peering_main_route_with_sonartrade_services" {
  route_table_id         = "${var.logging_monitoring_services_main_route_table_id}"
  destination_cidr_block = "${var.sonartrade_services_vpc_cidr_block}"
  vpc_peering_connection_id ="${aws_vpc_peering_connection.sonartrade_services_logging_monitoring_services_vpc_peering_connection.id}"
}


resource "aws_route" "logging_monitoring_services_private_main_routes_with_sonartrade_services" {
  count = "${length(var.logging_monitoring_services_private_route_table_ids)}"
  route_table_id            = "${var.logging_monitoring_services_private_route_table_ids[count.index]}"
  destination_cidr_block = "${var.sonartrade_services_vpc_cidr_block}"
  vpc_peering_connection_id ="${aws_vpc_peering_connection.sonartrade_services_logging_monitoring_services_vpc_peering_connection.id}"
}

resource "aws_route" "logging_monitoring_services_public_main_route_with_sonartrade_services" {
  route_table_id            = "${var.logging_monitoring_services_public_route_table_id}"
  destination_cidr_block = "${var.sonartrade_services_vpc_cidr_block}"
  vpc_peering_connection_id ="${aws_vpc_peering_connection.sonartrade_services_logging_monitoring_services_vpc_peering_connection.id}"
}

resource "aws_security_group_rule" "logging_monitoring_services_allow_all_from_sonartrade_services" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "all"
  cidr_blocks = ["${var.sonartrade_services_vpc_cidr_block}"]
  security_group_id = "${var.logging_service_services_main_security_group_id}"
}