#!/bin/bash

STARTTIME=$(date +%s)

################################################
# Check arguments
echo "[1 / 10] Checking required arguments"

usage(){
  echo " To execute this script run the following command "
  echo " k8s-deploy.sh --namespace your_namespace --vpc_cidr_block your_vpc_cidr_block --stage your_stage --aws_profile your_aws_profile "
  echo " Full example is : k8s-deploy.sh --namespace tdarex --vpc_cidr_block 172.27.0.0/16 --stage prod --aws_profile st --instance_type t3.large"
  echo " Required options are : namespace and vpc_cidr_block "
  echo " Default value for stage is  prod and for aws_profile is st"
  echo " Default value for instance_type t3.medium"
  exit 1
}

check_env_variable(){
  ## Bitso keys required to run rp.sh
    if [[ -z "BITSO_API_KEY" ]]; then
       echo "[ERROR]: Environment variable BITSO_API_KEY must be set"
       exit 1
    fi

    if [[ -z "BITSO_SECRET_KEY" ]]; then
        echo "[ERROR]: Environment variable BITSO_SECRET_KEY must be set"
        exit 1
    fi

     if [[ -z "DOCKER_REPOSITORY" ]]; then
        echo "[ERROR]: Environment variable DOCKER_REPOSITORY must be set"
        exit 1
    fi
}

check_dependencies(){
    # Check docker is running
    echo "CHECKING DOCKER..."
    if ! [ -x "$(command -v docker)" ]; then
      echo 'Error: docker is not installed.' >&2
      exit 1
    fi

    # Check AWS-cli is install
    echo "CHECKING AWS-CLI..."
    if ! [ -x "$(command -v aws)" ]; then
      echo 'Error: aws is not installed.' >&2
      exit 1
    fi

    # Check aws-iam-auth is install
    echo "CHECKING aws-iam-auth INSTALLED"
    if ! [ -x "$(command -v aws-iam-authenticator)" ]; then
      echo 'Error: aws-iam-authenticator is not installed.' >&2
      exit 1
    fi


    # check kubectl install , and version >= 1.10
    echo "CHECKING KUBECTL INSTALLED"
    if ! [ -x "$(command -v kubectl)" ]; then
      echo 'Error: kubectl is not installed.' >&2
      exit 1
    fi
}

aws_profile="st"
stage="prod"
instance_type="t3.medium"

optspec=":-:"
while getopts "$optspec" optchar; do
   case "${OPTARG}" in
      namespace)
         namespace="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      vpc_cidr_block)
         vpc_cidr_block="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      stage)
         stage="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      aws_profile)
         aws_profile="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      instance_type)
         instance_type="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
      help)
        usage
         ;;
   esac
done

## First Parameter
if [[ -z "$namespace" ]]; then
    echo "namespace option is required but missing. Please run sonar.sh --help  for more informations"
    exit 1
fi

if [[ -z "$vpc_cidr_block" ]]; then
    echo "vpc_cidr_block option is required but missing. Please run sonar.sh --help  for more informations"
    exit 1
fi

check_env_variable

################################################
# Check pre-reqs
echo "[2 / 10] Checking dependencies"

check_dependencies

################################################
# Configure settings

# If aws is configured, find profile
export AWS_DEFAULT_PROFILE=$aws_profile
export TF_VAR_profile=$aws_profile
export TF_VAR_namespace=$namespace
export TF_VAR_vpc_cidr_block=$vpc_cidr_block
export TF_VAR_stage=$stage
export TF_VAR_instance_type=$instance_type

################################################
# Perform deployment
echo "[3 / 10] Initializing cluster (this may take a few minutes)"

cd ../aws

#if [ ! -d ".terraform" ]; then
#  terraform init
#fi

workspace_name=$namespace-$stage
terraform workspace select $workspace_name || terraform workspace new $workspace_name

echo "DEPLOYING EKS CLUSTER: This will fail unless terraform init has already been run"
terraform init
terraform plan -auto-approve -lock=false
terraform apply -auto-approve -lock=false



echo "FINISHING CLUSTER INITIALIZATION "
aws --profile=st eks update-kubeconfig --name $TF_VAR_namespace-$TF_VAR_stage-eks-cluster
kubectl apply -f config-map-aws-auth-$TF_VAR_namespace-$TF_VAR_stage-eks-cluster.yaml

