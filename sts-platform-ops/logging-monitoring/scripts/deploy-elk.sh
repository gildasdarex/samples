#!/usr/bin/env bash

stage="prod"
optspec=":-:"
while getopts "$optspec" optchar; do
   case "${OPTARG}" in
      stage)
         stage="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
         ;;
   esac
done

workdir=""

if [ $stage == "prod" ]
then
   workdir="../prod"
elif [ $stage == "staging" ]
then
   workdir="../staging"
else
   echo "Unsupported stage value"
   exit
fi

cd $workdir

kubectl apply -f logging/1_k8s-global/namespace.yml
kubectl apply -f logging/6_elk
kubectl apply -f logging/curator
kubectl apply -f logging/elastalert