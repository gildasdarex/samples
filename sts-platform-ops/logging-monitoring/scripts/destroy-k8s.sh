#!/bin/bash

echo "Starting destroy aws vpc peering resources between sonartrade cluster and logging-monitoring cluster"
cd ../aws-vpc-peering
terraform init
terraform state pull
terraform destroy -lock=false

echo "destroy aws vpc perring resources with success"


echo "Starting destroy logging-monitoring cluster"

cd ../aws

terraform init
terraform state pull
terraform workspace select logging-monitoring-staging
terraform destroy -lock=false

echo "destroy  logging-monitoring cluster with success"