#!/bin/bash

cd ..

kubectl apply -f 4_beats_init/
kubectl apply -f 5_beats_agents
kubectl apply -f logstash/logstash.yml

kubectl apply -f prometheus/alertmanager/
kubectl apply -f prometheus/prometheus/
kubectl apply -f prometheus/kube-state-metrics/
kubectl apply -f prometheus/node-exporter/
kubectl apply -f prometheus/grafana/
