namespace="sonartradeuatprod2-logging"
sonartrade_services_vpc_id = "vpc-0d4fd3babafc189c5"
sonartrade_services_vpc_cidr_block = "172.28.0.0/16"
sonartrade_services_main_route_table_id = "rtb-078d3139d96bb8896"
sonartrade_services_private_route_table_ids = ["rtb-0f26fcd43083f1bbe", "rtb-0d0944a629c64ced4"]
sonartrade_services_public_route_table_id = "rtb-0e3b16c46ecf7cbc2"
sonartrade_services_main_security_group_id = "sg-0d41dac6adf2b6fae"

logging_monitoring_services_vpc_id = "vpc-0872181b4e126b5b2"
logging_monitoring_services_vpc_cidr_block = "172.32.0.0/16"
logging_monitoring_services_main_route_table_id = "rtb-0155bab90c0eb03f7"
logging_monitoring_services_private_route_table_ids = ["rtb-0588483f623ac2da0", "rtb-0e385ba68fff05cfd"]
logging_monitoring_services_public_route_table_id = "rtb-01ab3343b016934f2"
logging_service_services_main_security_group_id = "sg-0152fe5caf376497a"