variable "profile" {
  type        = "string"
  default     = "st"
  description = "AWS CLI config profile"
}

variable "region" {
  type        = "string"
  default     = "us-east-1"
  description = "AWS Region"
}


variable "sonartrade_services_vpc_id" {
  type        = "string"
}

variable "sonartrade_services_vpc_cidr_block" {
  type        = "string"
}

variable "sonartrade_services_main_route_table_id" {
  type        = "string"
}

variable "sonartrade_services_private_route_table_ids" {
  type        = "list"
}

variable "sonartrade_services_public_route_table_id" {
  type        = "string"
}

variable "sonartrade_services_main_security_group_id" {
  type        = "string"
}

variable "namespace" {
  type        = "string"
}

variable "logging_monitoring_services_vpc_id" {
  type        = "string"
}

variable "logging_monitoring_services_vpc_cidr_block" {
  type        = "string"
}

variable "logging_monitoring_services_main_route_table_id" {
  type        = "string"
}

variable "logging_monitoring_services_private_route_table_ids" {
  type        = "list"
}

variable "logging_monitoring_services_public_route_table_id" {
  type        = "string"
}

variable "logging_service_services_main_security_group_id" {
  type        = "string"
}
