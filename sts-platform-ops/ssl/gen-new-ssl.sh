#!/usr/bin/env bash

namespace=fix.bitso.sonartrade.com

DETAILS_O="Sonar Trading LLC"
DETAILS_OU="Bitso TT"
DETAILS_C="US"
DETAILS_ST="Virginia"
DETAILS_L="us-east-1"

# Secret generator
#randStr () {
#     echo "$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c64)"
#}

printf "\n==============================================\n"
printf "\nWARNING! RECORD PASSWORDS NOW. THEY WILL NEVER BE SHOWN AGAIN.\n\n"

#productionPassword=password
productionPassword=1a2b3c
echo "SECRET PASSWORD for private key: $productionPassword"

#devPassword=password
devPassword=1a2b3c
echo "Dev password for public key store: $devPassword"
printf "==============================================\n\n"

mkdir -p private/ssl
cd private/ssl
rm *

echo "[1/5] Generating SSL key and production keystore"
openssl req -x509 -sha256 -nodes \
            -days 730 \
            -newkey rsa:4096 \
            -subj "/C=${DETAILS_C}/ST=${DETAILS_ST}/L=${DETAILS_L}/O=${DETAILS_O}/OU=${DETAILS_OU}/CN=${namespace}" \
            -passout pass:${productionPassword} \
            -keyout ${namespace}_private.key \
            -out ${namespace}_public.pem

echo "[2/5] Combining key and cert into PEM"
cat ${namespace}_private.key > ${namespace}_full.pem
cat ${namespace}_public.pem >> ${namespace}_full.pem

echo "[3/5] Exporting production keystore as PKCS12"
openssl pkcs12  -export \
                -passout pass:${productionPassword} \
                -in ${namespace}_full.pem \
                -out ${namespace}_full.pkcs12

echo "[4/5] Generating dev keystore for public key (this should be used by integration test)"
keytool -import \
        -alias ${namespace}_public \
        -noprompt \
        -storepass $devPassword \
        -keypass $devPassword \
        -keystore ${namespace}_public.ks \
        -file ${namespace}_public.pem

echo "[5/5] Copying files to bitso-tt and integration tests"
#cp ${namespace}_full.pkcs12 ../../../bitso-tt/bitso-tt-fix-service/bitso-tt-fix-service-impl/src/main/resources/
#cp ${namespace}_public.ks ../../../bitso-tt/integration-test/src/test/resources/
cp ${namespace}_full.pkcs12 ../../../bitso-tt/integration-test/src/test/resources/
cp ${namespace}_public.ks ../../../bitso-tt/bitso-tt-fix-service/bitso-tt-fix-service-impl/src/main/resources/
