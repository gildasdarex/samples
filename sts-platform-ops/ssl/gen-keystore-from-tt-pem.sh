#!/usr/bin/env bash

namespace=fix.bitso.sonartrade.com
fixServicePass=AsDQHLSAeHbBtjND3RHpxih2jf27Kizixvz6gdF9TwAFerF5g5Nuug8g9X8K08GK
src=tt-fix-ssl-cert.pem
dst=../../bitso-tt/bitso-tt-fix-service/bitso-tt-fix-service-impl/src/main/resources
#dst=~/.sonar/secrets/staging

keytool -import \
        -alias ${namespace}_public \
        -noprompt \
        -storepass $fixServicePass \
        -keypass $fixServicePass \
        -keystore ${namespace}_tt_public.ks \
        -file $src

echo "Done generating keystore"
cp ${namespace}_tt_public.ks $dst
echo "Moved keystore to Bitso FIX Service to $dst"
