## OpenSSL notes

### tl;dr

Keys are generated in `private/ssl/` and copied to bitso-market-service and bitso-tt/integration-tests.

```
# Generate new SSL keys
./ssl/gen-new-ssl.sh

# View cert to share with TT
keytool -printcert -file private/ssl/fix.bitso.sonartrade.com_public.pem
```

### Design decisions

- RSA 4096 key, not elliptical: Its more supported. 2048 is usually recommended.
- Create key using Java `keytool`, not `openssl` directly: Key ultimately needs to be imported into keystore. Easier to create keystore and key at same time.
- Single key for both price and order gateways: No need to double the number of keys.
- New keys for each exchange integration: No single point of failure.
- No SSL authentication: Authentication can be done through FIX layer. Certs will only be distributed to a single client.

### Version used

```
$ openssl version -a
OpenSSL 1.0.2g  1 Mar 2016
built on: reproducible build, date unspecified
platform: debian-amd64
options:  bn(64,64) rc4(16x,int) des(idx,cisc,16,int) blowfish(idx) 
compiler: cc -I. -I.. -I../include  -fPIC -DOPENSSL_PIC -DOPENSSL_THREADS -D_REENTRANT -DDSO_DLFCN -DHAVE_DLFCN_H -m64 -DL_ENDIAN -g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wl,-Bsymbolic-functions -Wl,-z,relro -Wa,--noexecstack -Wall -DMD32_REG_T=int -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DMD5_ASM -DAES_ASM -DVPAES_ASM -DBSAES_ASM -DWHIRLPOOL_ASM -DGHASH_ASM -DECP_NISTZ256_ASM
```

### References:

- https://geekflare.com/openssl-commands-certificates/
- https://deliciousbrains.com/https-locally-without-browser-privacy-errors/
- https://github.com/ssllabs/research/wiki/SSL-and-TLS-Deployment-Best-Practices
- https://quickfixj.org/usermanual/2.1.0/usage/secure_communications.html
- https://docs.oracle.com/cd/E19509-01/820-3503/6nf1il6er/index.html
- https://www.sslshopper.com/article-most-common-java-keytool-keystore-commands.html
- https://stackoverflow.com/questions/39093600/quickfix-j-ca-certificate-validation

This is not relevant documentation. Only if Sonar were to connect into TT.

- https://library.tradingtechnologies.com/tt-fix/Managing_FIX_Sessions.html