#!/usr/bin/env bash

# RUN THIS FROM ssl/ directory

src=tt-fix-ssl-cert
namespace=fix.bitso.sonartrade.com
fixServicePass=AsDQHLSAeHbBtjND3RHpxih2jf27Kizixvz6gdF9TwAFerF5g5Nuug8g9X8K08GK
dst=../../bitso-tt/bitso-tt-fix-service/bitso-tt-fix-service-impl/src/main/resources


openssl x509 -outform der -in $src.pem -out $src.der

keytool -import -alias $namespace \
                -keystore ${namespace}_tt_public.ks \
                -noprompt \
                -storepass $fixServicePass \
                -file $src.der

echo "Done generating keystore"
cp ${namespace}_tt_public.ks $dst
echo "Moved keystore to Bitso FIX Service to $dst"
