#  
#  Local Alternative Minikube setup.

- https://github.com/lightbend/reactive-sandbox
- https://github.com/helm/helm

```bash
# Install Helm and add the Lightbend repository
brew install kubernetes-helm
helm init
helm repo add lightbend-helm-charts https://repo.lightbend.com/helm-charts
helm update

# Install the reactive-sandbox
helm install lightbend-helm-charts/reactive-sandbox --name reactive-sandbox
```