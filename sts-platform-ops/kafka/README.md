# Kafka Deployment



###Setup
```bash

kubectl create -f ./01-namespace.yml

kubectl create -f ./volumes/local-persistent-volume.yml
kubectl create -f ./volumes/local-persistent-volumeclaim.yml

kubectl create -f ./21-kafka-service.yml
kubectl create -f ./22-kafka-service-headless.yml

kubectl create -f ./24-kafka-statefulset.yml


```


### Helm Setup 
```bash
helm install --name my-kafka --namespace sonartrade incubator/kafka
```

### References 
- https://github.com/Yolean/kubernetes-kafka


### TODO:
Need too turn off the confluent metrics:
`confluent.support.metrics.enable=false`
https://docs.confluent.io/current/proactive-support.html
    

