#!/usr/bin/env bash
# create group
echo "Creating ftpusers group ..."
sudo addgroup ftpusers

echo "Restarting ssh service ..."
service ssh restart
