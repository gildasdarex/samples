#!/bin/bash

echo "Creating home dir and authorized_keys file for ftpuser ..."

# Adding SSH FTP user
# Create Home Directory + .ssh Directory
sudo mkdir -p /home/ftpuser/.ssh

# Create Authorized Keys File
sudo touch /home/ftpuser/.ssh/authorized_keys

# Create User + Set Home Directory
sudo useradd -d /home/ftpuser ftpuser

echo "Adding ftpuser  user to ftpusers  user group ..."
# Add User to sudo Group
usermod -aG ftpusers ftpuser

echo  "Setting Permissions for ftpuser ..."
sudo chown -R ftpuser:ftpuser /home/ftpuser/
sudo chown root:root /home/ftpuser
sudo chmod 700 /home/ftpuser/.ssh
sudo chmod 644 /home/ftpuser/.ssh/authorized_keys

# Change password for the user
echo "Creating password for ftpuser ..."
sudo passwd ftpuser

echo "WARNING !!! UPDATE SSH KEY  /home/ftpuser/.ssh/authorized_keys !!!"
echo "WARNING !!! RESTART SSH service 'sudo service ssh service' after update"
