#!/usr/bin/env bash
# Give ftpusers group access to folder
# NOTE: THIS IS SPECIFIC TO SETUP
# NOTE: DIRs are SPECIFIC TO SETUP; FOR INTG1/DEV1 & DEMO1. On both bootstrap and swarm nodes /asklytics dir has logs, datafiles etc.
echo "Giving ftpusers group permissions to directories ..."
echo "WARNING: DIRs are SPECIFIC TO SETUP; FOR INTG1/DEV1 & DEMO1. On both bootstrap and swarm nodes /asklytics dir has logs, datafiles etc."
echo "Giving ftpusers group permissions to /asklytics (THIS COULD TAKE A WHILE AS ITS RECURSIVE; IT COULD INCLUDE NFS MOUNTS !!!)..."
sudo chgrp -R ftpusers /asklytics

echo "Restarting ssh service ..."
service ssh restart
