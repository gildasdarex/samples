# Starting and Stopping Services All (registry and UI)
    
    docker-compose stop
    docker-compose start

# Starting and Stopping registry

    docker-compose stop registry
    docker-compose start registry
    
# Creating and Destroying Services    
 STOP the services from being automatically restarted by Supervisor

    supervisorctl stop docker-registry-services 

Manually create services

    docker-compose up -d
    docker-compuse down

 START the services to be automatically restarted by Supervisor

    supervisorctl start docker-registry-services 

# Deleting Docker Images
1. Delete images using registry ui

        http://registry-host-ip 
     
2. Run script to recover disk space 

        /etc/docker/registry/clean-docker-registry-disk.sh

# CREATING A NEW SETUP
## !!!! NOTE !!!! 
    
    1. If creating a new setup, the mount location info, IP information in the configuration files in etc-docker-registry needs to be updated
    2. Users & passwords created during setup creation will be overwritten by  etc-docker-registry/auth/.htpasswd file. If you want keep the 
    users created during setup, then do not copy etc-docker-registry/auth/.htpasswd.

1. Create a new Setup using instructions in  [Creating Docker Registry Setup](https://docs.google.com/document/d/1zztze5aetXs7kA7N60aZval5bdv-pfdjrN1vEaFXLpY/edit)

2. Copy Content 
    
    Copy scripts and configurations for docker compose
    
        copy etc-docker-registry folder to /etc/docker/registry

    Copy  config for supervisor to auto restart services on reboot
    
        copy files in etc-supervisor-config.d/* to /etc/supervisor/conf.d
        
3. Make set-permissions-to-files.sh executable 
    
        chmod 700 ./set-permissions-to-files.sh
4. run 

         sudo service docker restart      

5. Start the docker services
    
        cd /etc/docker/registry

        docker-compose up -d

6. Verify docker Registry by 

       docker login -u username -p password http://registry-host-ip:5000

7. Create and push a sample docker image to the registry
See sections "Step 6 — Publishing to Your Private Docker Registry" and "Step 7 — Pulling From Your Private Docker Registry" in [How To Set Up a Private Docker Registry on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-18-04)

      
7. Verify docker UI is running by going to
        
        http://registry-host-ip 

8. Put docker service to managed by Supervisor so that it auto restarts when machine is rebooted
Have supervisor to read and start using the configuration 

        supervisorctl reread

        supervisorctl update

9.  ensure the Supervisor is restarted after a system reboot.

        service supervisor restart
                        