docker-compose stop registry
docker-compose run --rm registry bin/registry garbage-collect /etc/docker/registry/config.yml
docker-compose start registry
