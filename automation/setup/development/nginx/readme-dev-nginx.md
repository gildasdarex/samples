##### **_DEV1 NGINX SERVER README_**
 
**1. Install Nginx Server**

https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04
https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04-quickstart
RUN Nginx  on demo-bootstrap node

To Install, run the following commands

    sudo apt update
    sudo apt install nginx

Status, Starting, Stopping Nginx
    
    systemctl status nginx
    systemctl start nginx
    systemctl stop nginx
    
Reloading Config without Stopping Nginx

    nginx -s reload

In case something does not work as expected, you may try to find out the reason in access.log and error.log files in the directory /usr/local/nginx/logs or /var/log/nginx. 

Log File Location
    
    /usr/local/nginx/logs 
    
    /var/log/nginx

Look at /etc/nginx/nginx.conf for location of log files

  
**2. Create Home Page for Dev1 and Demo1 Monitoring Apps**

nginx site name
We use internal.dev.asklytics.io but there is no DNS mapping. So, to access it, use the IP address that nginx server is bound to/listening on.

1. Create the directory for site internal.dev.asklytics.io

    sudo mkdir -p /var/www/internal.dev.asklytics.io/html

2. Assign ownership of the directory

    sudo chown -R $USER:$USER /var/www/internal.dev.asklytics.io/html

3. Assign permissions of your web

    sudo chmod -R 755 /var/www/internal.dev.asklytics.io/html

4. Create index.html
    
    sudo nano /var/www/internal.dev.asklytics.io/html/index.html

5. Copy internal-dev-index.html to /var/www/internal.dev.asklytics.io/html/index.html

        
**3. Configuring Nginx Server Site Configuration**

a) make a copy of exist default file at /etc/nginx/sites-available.

    sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak_$(date +%s)

b) update  parameters in /etc/nginx/sites-available/default 
In the server {} block, update list port to listen on private IP for the default_server, the root dir for the site, and the index to pick up  internal-dev-index.html

    listen 10.46.0.25:80 default_server;

    root /var/www/internal.dev.asklytics.io/html;

    index internal-dev-index.html index.html index.htm;

Alternately, you can copy etc_nginx_sites_available/default to /etc/nginx/sites-available/default


c) reload config or restart

    sudo systemctl stop nginx
    sudo systemctl start nginx
    sudo systemctl status nginx
    

d) Update access and error log location in /etc/nginx/nginx.conf

** 4. Test Server **
The IP should be IP to which the nginx server is bound to/listening on. 10.46.0.25 is the private IP is dev1-bootstrap. 
Execute that command from any droplet that is part of the private network

    curl -v http://10.46.0.25

It should return the html from the index page (i.e. internal-dev-index.html)


