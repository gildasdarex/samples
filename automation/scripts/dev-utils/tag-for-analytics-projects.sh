#########################################
# This script creates a snapshot build of a product.
# This is the first step in releasing the product.
# WARNING !!!!! THIS SHOULD *NOT* BE USED BY DEVELOPERS TO BUILD PROJECTS !!!!!!!
# Purpose of creating snapshots
# a) ensuring there are no build breaks
# b) functional testing
# c) creating a release for production

# Step 1: delete local-maven-repo so that all previous builds do not exist
# it is because currently we do have the jars/wars names to include the build number nor do we put the build# in meta file inside the jar/war
# Step 2: delete m2 cache so that all the dependencies cached are removed
# this is to ensure that we can catch all build breaks
# Step 3: clone all projects (from $branch as input; default to dev)
# Step 4: build all projects
# Step 5: if build fails, abort
# Step 6: if build passes, get last build# for the project set
# Step 7: increment build#
# Step 8: Tag all projects with the build#
# Step 9: save the last build#
# step 10: merge the properties file that has the last build#

##########################################
#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for release-builds folder"
echo "please ensure that you installed wget in your laptop /n"
echo "##############################################################"


echo "did you already give the permissions to release-builds folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
username=$2
password=$3
tagName=$4

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptdir="$(dirname "$parentdir")"
releaseHelperDir="$parentdir/release-builds/release-helpers"
propertiesFilesDir="$parentdir/properties-files"


#ask to user if he would like to generate release
echo "WARNING!! This will alter the code base and create remote branches on git. Do NOT do this without approval!!! Are you sure you want to proceed? "
read -p "yes or no " yn
if [[ "${yn}" != "yes" ]]
 then
  exit
fi


echo " WARNING!! This will alter the code base and create remote branches on git."
read -n 1 -s -p "Press any key to continue"


if [ "$#" -eq 8 ]
   then
   branch=$5
   projects_properties=$6
   projects_dependencies_name_properties=$7
   projects_publish_name_properties=$8
elif [ "$#" -eq 7 ]
   then
   branch=$5
   projects_properties=$6
   projects_dependencies_name_properties=$7
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 6 ]; then
   branch=$5
   projects_properties=$6
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 5 ]; then
   branch=$5
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 4 ]; then
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi

#gradle cache clean
rm -rf $HOME/.gradle/caches/

tag_project () {
    # $1 parameter is the name of the project to tag
    # $2 is the value of the build number
    read -n 1 -s -p "WARNING!! $1 will be tagged with build number $2 . Press any key to tag"
    cd "$repository_dir/$1"
    git tag "$2"
    git push --tags origin $branch
}


projectTypeListAsArray=(analytics)


for projectType in ${projectTypeListAsArray[@]}
do

   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)

   for app in ${appsArray[@]}
   do
      tag_project $app $tagName
   done
done