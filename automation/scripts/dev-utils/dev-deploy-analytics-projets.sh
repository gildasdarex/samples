#########################################
# This script creates a snapshot build of a product.
# This is the first step in releasing the product.
# WARNING !!!!! THIS SHOULD *NOT* BE USED BY DEVELOPERS TO BUILD PROJECTS !!!!!!!
# Purpose of creating snapshots
# a) ensuring there are no build breaks
# b) functional testing
# c) creating a release for production

# Step 1: delete local-maven-repo so that all previous builds do not exist
# it is because currently we do have the jars/wars names to include the build number nor do we put the build# in meta file inside the jar/war
# Step 2: delete m2 cache so that all the dependencies cached are removed
# this is to ensure that we can catch all build breaks
# Step 3: clone all projects (from $branch as input; default to dev)
# Step 4: build all projects
# Step 5: if build fails, abort
# Step 6: if build passes, get last build# for the project set
# Step 7: increment build#
# Step 8: Tag all projects with the build#
# Step 9: save the last build#
# step 10: merge the properties file that has the last build#

##########################################
#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for release-builds folder"
echo "please ensure that you installed wget in your laptop /n"
echo "##############################################################"


echo "did you already give the permissions to release-builds folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
tomcat_dir=$2

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
releaseHelperDir="$parentdir/release-builds/release-helpers"
propertiesFilesDir="$parentdir/properties-files"

if [ "$#" -eq 13 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties=${10}
   projects_publish_name_properties=${11}
   projects_modules_properties=${12}
   list_of_apps_to_deploy=${13}
elif [ "$#" -eq 12 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties=${10}
   projects_publish_name_properties=${11}
   projects_modules_properties=${12}
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 11 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties=${10}
   projects_publish_name_properties=${11}
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 10 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties=${10}
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 9 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 8 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 7 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties="$propertiesFilesDir/app.xml.name.properties"
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 6 ]; then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/app.xml.name.properties"
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 5 ]; then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties="$propertiesFilesDir/apps.url.path.properties"
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/app.xml.name.properties"
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 4 ]; then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties="$propertiesFilesDir/apps.xml.path.properties"
   apps_url_path_properties="$propertiesFilesDir/apps.url.path.properties"
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/app.xml.name.properties"
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 3 ]; then
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   apps_exploded_war_path_properties="$propertiesFilesDir/apps.exploded.war.name.properties"
   apps_xml_path_properties="$propertiesFilesDir/apps.xml.path.properties"
   apps_url_path_properties="$propertiesFilesDir/apps.url.path.properties"
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/apps.xml.name.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 2 ]; then
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   apps_exploded_war_path_properties="$propertiesFilesDir/apps.exploded.war.name.properties"
   apps_xml_path_properties="$propertiesFilesDir/apps.xml.path.properties"
   apps_url_path_properties="$propertiesFilesDir/apps.url.path.properties"
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/apps.xml.name.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
else
   echo "invalid number of argument."
   exit
fi

#start tomcat
if ! lsof -i:8080
then
 $tomcat_dir/bin/startup.sh
fi

#get number of level of app to deploy
#by project relative to level_1 need to be deployed before project with level_2
nbrLevel=$(sed -n '$=' $deployment_level_properties)

listOfAppToDeploy=$(cat $list_of_apps_to_deploy)
listOfAppToDeployArray=($listOfAppToDeploy)


#this function is used to deploy app..to deploy, we copy the project web.xml
#from his location provided in apps_xml_path_properties file(apps.xml.path.properties) to tomcat/conf/Catalina/localhost
deploy_app (){
  echo "----------------------------- copy $1 web.xml to tomcat/conf/Catalina/localhost------------------"
  appXmlPath=$(${releaseHelperDir}/_get-app-web-xml-path.sh $1 "$apps_xml_path_properties")
  appWebXmlName=$(${releaseHelperDir}/_get-app-web-xml-name.sh $1 "$apps_xml_name_properties")
  if test -s $tomcat_dir/conf/Catalina/localhost/$appWebXmlName ; then
    rm $tomcat_dir/conf/Catalina/localhost/$appWebXmlName
  fi
  cp $appXmlPath $tomcat_dir/conf/Catalina/localhost/$appWebXmlName
}


#this function is used to unzip war folder for each project..
unzip_war () {
    # $1 parameter is the name of the project: by example infra-camel-service
    #one project can have many modules..we need to look and deployed each of them
    #by example infra-camel-service have asklytics-camel-analytics-service asklytics-camel-aws-extraction-service asklytics-camel-provisioning-service
    #as module..each of them need to be unzip
    echo "------------------------------unzip  $1 war ----------------------------------------"
    appPublishNanes=$(${releaseHelperDir}/_get-project-module-publish-name.sh $1 $projects_publish_name_properties)
    appPublishNanesArray=($appPublishNanes)

    for appPublishName in ${appPublishNanesArray[@]}
    do
      publishNameRepoPath="${repository_dir}/local-maven-repo/com/asklytics/$appPublishName"
      appArtefactType=$(${releaseHelperDir}/_get-project-artefact-type.sh $1 "$propertiesFilesDir/projects-artefact-type.properties")

      if [[ $appArtefactType == "war" ]]
       then
          appExplodedWarPath=$(${releaseHelperDir}/_get-app-exploded-path.sh $1 "$apps_exploded_war_path_properties")
          appArtefactPath=$(${releaseHelperDir}/_get-project-artefact-path.sh $1 "$propertiesFilesDir/apps-artefact-type-path.properties")
          publishNameRepoFullPath="$publishNameRepoPath/$appArtefactPath"

          if [  -d "$appExplodedWarPath" ]
          then
            rm -R $appExplodedWarPath
          fi

          if test -s ${publishNameRepoFullPath}/$appPublishName*.war ; then
            unzip ${publishNameRepoFullPath}/$appPublishName*.war -d "$appExplodedWarPath"
          fi

      fi
    done
}

#unzip all war in their folders
if [ "$#" -eq 3 ]
   then
   IFS='/' read -ra listOfAppToDeployArray <<< "$3"
   for app in "${listOfAppToDeployArray[@]}"; do
      appModules=$(${releaseHelperDir}/_get-app-modules-name.sh $app $projects_modules_properties)
      appModulesArray=($appModules)
      for moduleName in ${appModulesArray[@]}
        do
         unzip_war $moduleName
      done
   done
else
    projectTypeList=$(${releaseHelperDir}/_get-projects-type-name.sh $projects_properties)
    projectTypeListAsArray=($projectTypeList)
    for projectType in ${projectTypeListAsArray[@]}
      do
      apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
      appsArray=($apps)
#      if [ ! ${#appsArray[@]} -eq 0 ]; then
#        lastArrayApp=$appsArray
#      fi
      for app in ${appsArray[@]}
        do
        appModules=$(${releaseHelperDir}/_get-app-modules-name.sh $app $projects_modules_properties)
        appModulesArray=($appModules)
        for moduleName in ${appModulesArray[@]}
         do
          unzip_war $moduleName
        done
      done
    done
fi



i=1 ; while [[ $i -le $nbrLevel ]] ; do
    level="level_$i"
    appLevel=$(${releaseHelperDir}/_get-level-apps.sh $level "$deployment_level_properties")
    appLevelArray=($appLevel)
    lastAppToDeploy="lastAppToDeployForTest"
    for appToDeploy in ${appLevelArray[@]}
     do
      inarray=$(echo ${listOfAppToDeployArray[@]} | grep -o ${appToDeploy} | wc -w)
      echo "in array value is $inarray for app $appToDeploy"
      if [[ ! $inarray -eq 0 ]]; then
        deploy_app $appToDeploy
        lastAppToDeploy=$appToDeploy
      fi
    done
    echo "lastAppToDeploy is $lastAppToDeploy"
    #check if last app is deployed
    if [[ $lastAppToDeploy != "lastAppToDeployForTest" ]]; then
       status="404"
       appUrlPath=$(${releaseHelperDir}/_get-app-url-path.sh $lastAppToDeploy "$apps_url_path_properties")
       while [[ $status = "404" ]] ; do
         echo "----------------------------- check if  $lastAppToDeploy is available------------------"
         status=$(wget --spider -S "$appUrlPath" 2>&1 | grep "HTTP/" | awk '{print $2}')
       done
       echo "----------------------------- $lastAppToDeploy is available------------------"
    fi
    ((i = i + 1))
done