#########################################
# This script creates a snapshot build of a product.
# This is the first step in releasing the product.
# WARNING !!!!! THIS SHOULD *NOT* BE USED BY DEVELOPERS TO BUILD PROJECTS !!!!!!!
# Purpose of creating snapshots
# a) ensuring there are no build breaks
# b) functional testing
# c) creating a release for production

# Step 1: delete local-maven-repo so that all previous builds do not exist
# it is because currently we do have the jars/wars names to include the build number nor do we put the build# in meta file inside the jar/war
# Step 2: delete m2 cache so that all the dependencies cached are removed
# this is to ensure that we can catch all build breaks
# Step 3: clone all projects (from $branch as input; default to dev)
# Step 4: build all projects
# Step 5: if build fails, abort
# Step 6: if build passes, get last build# for the project set
# Step 7: increment build#
# Step 8: Tag all projects with the build#
# Step 9: save the last build#
# step 10: merge the properties file that has the last build#

##########################################
#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for release-builds folder"
echo "please ensure that you installed wget in your laptop /n"
echo "##############################################################"


echo "did you already give the permissions to release-builds folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
tomcat_dir=$2
lib_repository=$3

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
releaseHelperDir="$parentdir/release-builds/release-helpers"
propertiesFilesDir="$parentdir/properties-files"

if [ "$#" -eq 13 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties=${10}
   projects_publish_name_properties=${11}
   projects_modules_properties=${12}
   list_of_apps_to_deploy=${13}
elif [ "$#" -eq 12 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties=${10}
   projects_publish_name_properties=${11}
   projects_modules_properties=${12}
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 11 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties=${10}
   projects_publish_name_properties=${11}
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 10 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties=${10}
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 9 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties=$9
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 8 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties=$8
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 7 ]
   then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties=$7
   apps_xml_name_properties="$propertiesFilesDir/app.xml.name.properties"
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 6 ]; then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties=$6
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/app.xml.name.properties"
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 5 ]; then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties=$5
   apps_url_path_properties="$propertiesFilesDir/apps.url.path.properties"
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/app.xml.name.properties"
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 4 ]; then
   apps_exploded_war_path_properties=$4
   apps_xml_path_properties="$propertiesFilesDir/apps.xml.path.properties"
   apps_url_path_properties="$propertiesFilesDir/apps.url.path.properties"
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/app.xml.name.properties"
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 3 ]; then
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   apps_exploded_war_path_properties="$propertiesFilesDir/apps.exploded.war.name.properties"
   apps_xml_path_properties="$propertiesFilesDir/apps.xml.path.properties"
   apps_url_path_properties="$propertiesFilesDir/apps.url.path.properties"
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/apps.xml.name.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
elif [ "$#" -eq 2 ]; then
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   apps_exploded_war_path_properties="$propertiesFilesDir/apps.exploded.war.name.properties"
   apps_xml_path_properties="$propertiesFilesDir/apps.xml.path.properties"
   apps_url_path_properties="$propertiesFilesDir/apps.url.path.properties"
   deployment_level_properties="$propertiesFilesDir/deployment.level.properties"
   apps_xml_name_properties="$propertiesFilesDir/apps.xml.name.properties"
   projects_modules_properties="$propertiesFilesDir/projects-modules.properties"
   list_of_apps_to_deploy="$propertiesFilesDir/list-of-apps-to-deploy.txt"
else
   echo "invalid number of argument."
   exit
fi

#needle="-"
#libPath=$tomcat_dir/lib
#
#
#for file in $(ls $libPath)
#do
#    echo "$file"
#    numberOfOccurrences=$(grep -o "$needle" <<< "$file" | wc -l)
#    libNameWithVersion=$(echo $file | awk 'BEGIN {FS=".jar" } ; { print $1 }')
#    libVersion="${libNameWithVersion##*-}"
#    libName="${libNameWithVersion%-*}"
#done

needle="-"
libPath=$tomcat_dir/lib
listOfLibraryArray=$(ls $libPath)
#listOfLibraryArray=($listOfLibrary)

for file in $listOfLibraryArray
do
    numberOfOccurrences=$(grep -o "$needle" <<< "$file" | wc -l)
    libNameWithVersion=$(echo $file | awk 'BEGIN {FS=".jar" } ; { print $1 }')
    libVersion="${libNameWithVersion##*-}"
    libName="${libNameWithVersion%-*}"


    libs=$($releaseHelperDir/_get-commons-library.sh "$libName" $tomcat_dir)
    libsArray=$($libs)
    echo "-------------------------------------------------- $file"
    echo $libs
    echo "************************************************** $file"
done