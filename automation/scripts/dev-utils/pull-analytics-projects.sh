#!/usr/bin/env bash

##
# 1st PARAM: root dir where git project needs to be checkout to
#
# 2nd PARAM: git username
#
# 3rd PARAM: git password
#
# 4rd PARAM: array list of projects to pull;
# NOTE: support for pulling specific projects requires 4 parameters to be passed. It defaults to dev branch.
# Since the 5th parameter cannot be passed, pulling specific projects is supported only for pulling dev.
#
# 5th PARAM: git branch to checkout
# When want to pull a specific branch, it uses the 5th parameter. The 4th parameter is ignored and it pull all branches.
# So, when want to pull a specific branch, it will pull all projects and not specific project but the 4th param still
# needs to be passed so that there can be a 5th parameter for the branch name. The 4th param still needs to be passed in
# the 4th property can be any value as its ignored
#
# 6th PARAM:
# projects_properties -  a relative path to properties file (including file name) that contains the list of main projects to pull
# If its not passed, it uses "$propertiesFilesDir/project-set-analytics.properties"
# It contains the following keys
# analytics-projects -  List of git projects
# example
#       analytics-projects=commons invoiced-client email-send-service infra-security infra-rest-client infra-service ui-data-service modeler extract-aws infra-prov-service infra-camel-service ml-api-app analytics-service

# analytics-last_build_num - last build number
# example
#       analytics-last_build_num=0201

# analytics-last_release_num - last release number
# example
#       analytics-last_release_num=1.0.7
#
# This script may use only value of analytics-projects as the other values do not seem relevant to git pull

#7th PARAM:
# projects_dependencies_name_properties - -  a relative path to properties file (including file name) that contains
# the list of git projects that are main projects (listed in 6th param)
# default: projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
# WARNING: This script does not seem to use this property

echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for dev-utils folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to dev-utils folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

pull() {
    # $1 parameter is the name of the project#
   cd "$repository_dir/$1"
   git checkout $branch
   git pull origin $branch
}

#init all parameters
repository_dir=$1
username=$2
password=$3


scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
releaseHelperDir="$parentdir/release-builds/release-helpers"
propertiesFilesDir="$parentdir/properties-files"

# if no. of arguments passed to the script is 8
if [ "$#" -eq 8 ]
   then
   branch=$5
   projects_properties=$6
   projects_dependencies_name_properties=$7
   projects_publish_name_properties=$8
# if no. of arguments passed to the script is 7
elif [ "$#" -eq 7 ]
   then
   branch=$5
   projects_properties=$6
   projects_dependencies_name_properties=$7
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
# if no. of arguments passed to the script is 6
elif [ "$#" -eq 6 ]; then
   branch=$5
   projects_properties=$6
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
# if no. of arguments passed to the script is 5
elif [ "$#" -eq 5 ]; then
   branch=$5
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
# if no. of arguments passed to the script is 4
elif [ "$#" -eq 4 ]; then
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   branch="dev"
# if no. of arguments passed to the script is 3
elif [ "$#" -eq 3 ]; then
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi

# if # of arguments passed to the script is 4
if [ "$#" -eq 4 ]
   then
   IFS='/' read -ra appsArray <<< "$4"
   for app in "${appsArray[@]}"; do
    pull $app
   done
else
    projectTypeList=$(${releaseHelperDir}/_get-projects-type-name.sh $projects_properties)
    projectTypeListAsArray=($projectTypeList)

    # Step 3: pull all projects (from $branch as input; default to dev)
    for projectType in ${projectTypeListAsArray[@]}
    do
       apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
       appsArray=($apps)

       for app in ${appsArray[@]}
       do
         pull $app
       done
    done
fi
