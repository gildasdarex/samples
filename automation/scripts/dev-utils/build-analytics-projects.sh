 #!/bin/bash

  optspec=":-:"
  while getopts "$optspec" optchar; do
      case "${OPTARG}" in
        repo.dir)
          REPO_DIR="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        username)
          USERNAME="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        password)
          PASSWORD="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        incbuild)
          INCBUILD="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      esac
  done

if [[ $INCBUILD == "true" ]]; then
  ./build-analytics-projects-inc.sh $REPO_DIR $USERNAME $PASSWORD
fi

if [[ $INCBUILD == "false" ]]; then
  ./build-analytics-projects-dev.sh $REPO_DIR $USERNAME $PASSWORD
fi
