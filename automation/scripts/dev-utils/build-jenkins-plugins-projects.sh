#########################################
# This script creates a snapshot build of a product.
# This is the first step in releasing the product.
# WARNING !!!!! THIS SHOULD *NOT* BE USED BY DEVELOPERS TO BUILD PROJECTS !!!!!!!
# Purpose of creating snapshots
# a) ensuring there are no build breaks
# b) functional testing
# c) creating a release for production

# Step 1: delete local-maven-repo so that all previous builds do not exist
# it is because currently we do have the jars/wars names to include the build number nor do we put the build# in meta file inside the jar/war
# Step 2: delete m2 cache so that all the dependencies cached are removed
# this is to ensure that we can catch all build breaks
# Step 3: clone all projects (from $branch as input; default to dev)
# Step 4: build all projects
# Step 5: if build fails, abort
# Step 6: if build passes, get last build# for the project set
# Step 7: increment build#
# Step 8: Tag all projects with the build#
# Step 9: save the last build#
# step 10: merge the properties file that has the last build#

##########################################
#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for release-builds folder"
echo "please ensure that you installed wget in your laptop /n"
echo "##############################################################"


echo "did you already give the permissions to release-builds folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
username=$2
password=$3

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
releaseHelperDir="$parentdir/release-builds/release-helpers"
propertiesFilesDir="$parentdir/properties-files"

if [ "$#" -eq 8 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties=$7
   projects_modules_properties=$8

elif [ "$#" -eq 7 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties=$7
   projects_modules_properties="$propertiesFilesDir/plugins-modules.properties"
elif [ "$#" -eq 6 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties="$propertiesFilesDir/plugins-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/plugins-modules.properties"
elif [ "$#" -eq 5 ]; then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties="$propertiesFilesDir/plugins-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/plugins-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/plugins-modules.properties"
elif [ "$#" -eq 4 ]; then
   branch=$4
   projects_properties="$propertiesFilesDir/project-set-plugins.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/plugins-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/plugins-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/plugins-modules.properties"
elif [ "$#" -eq 3 ]; then
   projects_properties="$propertiesFilesDir/project-set-plugins.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/plugins-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/plugins-publish-names.properties"
   projects_modules_properties="$propertiesFilesDir/plugins-modules.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi


build_project () {
     # $1 parameter is the name of the project
    appPublishNanes=$(${releaseHelperDir}/_get-project-module-publish-name.sh $1 $projects_publish_name_properties)
    appPublishNanesArray=($appPublishNanes)

    for appPublishName in ${appPublishNanesArray[@]}
    do
      publishNameRepoPath="$repository_dir/local-maven-repo/com/asklytics/$appPublishName"
      rm -R $publishNameRepoPath
    done

    projectArtefactType=$(${releaseHelperDir}/_get-app-type.sh $1 "$propertiesFilesDir/app-type.properties")
    cd "$repository_dir/$1"

    echo "------------------------------ clean  $projectArtefactType project : $1 ----------------------------------------"
    gradle clean
    echo "------------------------------ build  $projectArtefactType project: $1  ----------------------------------------"
    gradle jpi
    echo "------------------------------ publish  $projectArtefactType project: $1 ----------------------------------------"
    gradle publishPlugins
}

check_if_project_successful_build () {
    # $1 parameter is the name of the project#
    #project is successuf build if we find his directory under local-maven-repo
    echo "------------------------------check if $1 build is sucessful----------------------------------------"
    appPublishNanes=$(${releaseHelperDir}/_get-project-module-publish-name.sh $1 $projects_publish_name_properties)
    appPublishNanesArray=($appPublishNanes)

    for appPublishName in ${appPublishNanesArray[@]}
    do
      publishNameRepoPath="${repository_dir}/local-maven-repo/com/asklytics/$appPublishName"
      if [ ! -d $publishNameRepoPath ]; then
        echo "build failed for project $1"
        echo "script will end due of this error"
        exit
      else
        appArtefactType=$(${releaseHelperDir}/_get-project-artefact-type.sh $1 "$propertiesFilesDir/projects-artefact-type.properties")

        if [[ $appArtefactType == "war" ]]
        then
          echo "do you want to rename $1 war according to build number ? "
          #ask to user if he would like to generate release
          read -p "yes or no " rename
          if [[ "${rename}" == "yes" ]]
             then
                echo "now enter build number ? "
                read -p "b#= " buildNumber
                appArtefactPath=$(${releaseHelperDir}/_get-project-artefact-path.sh $1 "$propertiesFilesDir/apps-artefact-type-path.properties")
                publishNameRepoFullPath="$publishNameRepoPath/$appArtefactPath"
                mv ${publishNameRepoFullPath}/$appPublishName*.war "$publishNameRepoFullPath/$appPublishName-b$buildNumber.war"
          fi
        fi
      fi
    done
}




projectTypeListAsArray=(jenkins-plugin)

for projectType in ${projectTypeListAsArray[@]}
  do
  apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
  appsArray=($apps)
  for app in ${appsArray[@]}
    do
    build_project $app
    appModules=$(${releaseHelperDir}/_get-app-modules-name.sh $app $projects_modules_properties)
    appModulesArray=($appModules)
    for moduleName in ${appModulesArray[@]}
     do
      check_if_project_successful_build $moduleName
    done
  done
done