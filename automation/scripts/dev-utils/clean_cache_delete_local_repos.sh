#########################################
# This script creates a snapshot build of a product.
# This is the first step in releasing the product.
# WARNING !!!!! THIS SHOULD *NOT* BE USED BY DEVELOPERS TO BUILD PROJECTS !!!!!!!
# Purpose of creating snapshots
# a) ensuring there are no build breaks
# b) functional testing
# c) creating a release for production

# Step 1: delete local-maven-repo so that all previous builds do not exist
# it is because currently we do have the jars/wars names to include the build number nor do we put the build# in meta file inside the jar/war
# Step 2: delete m2 cache so that all the dependencies cached are removed
# this is to ensure that we can catch all build breaks
# Step 3: clone all projects (from $branch as input; default to dev)
# Step 4: build all projects
# Step 5: if build fails, abort
# Step 6: if build passes, get last build# for the project set
# Step 7: increment build#
# Step 8: Tag all projects with the build#
# Step 9: save the last build#
# step 10: merge the properties file that has the last build#

##########################################
#!/usr/bin/env bash



#init all parameters
repository_dir=$1



ls  "$repository_dir" | while read -r file; do
      if [[ -d "$repository_dir/$file" ]]; then
         if [ "$file" == "local-maven-repo" ]; then
           echo "----------local-maven-repo is  not deleted "
         elif [ "$file" == "local-flat-repo" ]; then
            echo "----------local-flat-repo is  not deleted "
         else
            echo "-------------- delete $file-------"
            rm -r "$repository_dir/$file"
         fi
      fi
done