#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for dev-utils folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to dev-utils folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

pull() {
    # $1 parameter is the name of the project#
   cd "$repository_dir/$1"
   git checkout $branch
   git pull origin $branch
}

#init all parameters
repository_dir=$1
username=$2
password=$3


scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
releaseHelperDir="$parentdir/release-builds/release-helpers"
propertiesFilesDir="$parentdir/properties-files"


if [ "$#" -eq 8 ]
   then
   branch=$5
   projects_properties=$6
   projects_dependencies_name_properties=$7
   projects_publish_name_properties=$8
elif [ "$#" -eq 7 ]
   then
   branch=$5
   projects_properties=$6
   projects_dependencies_name_properties=$7
   projects_publish_name_properties="$propertiesFilesDir/plugins-publish-names.properties"
elif [ "$#" -eq 6 ]; then
   branch=$5
   projects_properties=$6
   projects_dependencies_name_properties="$propertiesFilesDir/plugins-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/plugins-publish-names.properties"
elif [ "$#" -eq 5 ]; then
   branch=$5
   projects_properties="$propertiesFilesDir/project-set-plugins.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/plugins-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/plugins-publish-names.properties"
elif [ "$#" -eq 4 ]; then
   projects_properties="$propertiesFilesDir/project-set-plugins.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/plugins-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/plugins-publish-names.properties"
   branch="dev"
elif [ "$#" -eq 3 ]; then
   projects_properties="$propertiesFilesDir/project-set-plugins.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/plugins-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/plugins-publish-names.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi


if [ "$#" -eq 4 ]
   then
   IFS='/' read -ra appsArray <<< "$4"
   for app in "${appsArray[@]}"; do
    pull $app
   done
else
    projectTypeList=$(${releaseHelperDir}/_get-projects-type-name.sh $projects_properties)
    projectTypeListAsArray=($projectTypeList)

    # Step 3: pull all projects (from $branch as input; default to dev)
    for projectType in ${projectTypeListAsArray[@]}
    do
       apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
       appsArray=($apps)

       for app in ${appsArray[@]}
       do
         pull $app
       done
    done
fi
