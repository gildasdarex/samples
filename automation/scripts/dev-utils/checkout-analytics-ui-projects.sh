#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for dev-utils folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to dev-utils folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
username=$2
password=$3

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptdir="$(dirname "$parentdir")"
releaseHelperDir="$parentdir/release-builds/release-helpers"
propertiesFilesDir="$parentdir/properties-files"



if [ "$#" -eq 7 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties=$7
elif [ "$#" -eq 6 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 5 ]; then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 4 ]; then
   branch=$4
   projects_properties="$propertiesFilesDir/project-set-release-analytics-ui.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 3 ]; then
   projects_properties="$propertiesFilesDir/project-set-release-analytics-ui.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi


projectTypeListAsArray=(analytics_ui)

# Step 3: clone all projects (from $branch as input; default to dev)


for projectType in ${projectTypeListAsArray[@]}
do
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)

   for app in ${appsArray[@]}
   do
      cd "$repository_dir/$app"
      git checkout $branch
   done
done