#!/bin/bash

root_dir=$1
projects=$2
cd $root_dir
globalBuild=0
echo $projects
IFS=';' read -r -a projectsArray <<< "$projects"
echo $projectsArray

for project in "${projectsArray[@]}"
do
while read line; do
file=$line
echo $file
if [ -f "$file" ]
then
  echo "$file found."

  while IFS=': ' read -r key value
  do
    key=$(echo $key | tr '-' '_')
    eval ${key}=\${value}
  done < "$file"

  inc_build_num="$((${Implementation_Build}+1))"
   if [ $globalBuild -lt $inc_build_num ]
   then
     globalBuild=$inc_build_num
   fi
else
  echo "$file not found."
fi
done <<< "$(find $project -wholename '*/src/main/resources/META-INF/MANIFEST.MF')"
cd $root_dir
done
echo "Next Build Number"$globalBuild


cd $root_dir
for project in "${projectsArray[@]}"
do
while read line; do
file=$line
echo $file
if [ -f "$file" ]
then
  
  inc=$globalBuild
  sed -i "s/^Implementation-Build: .*/Implementation-Build: $inc/"  $file
  DIR=$(dirname "${file}")
  cd $DIR
  git add .
  git commit -m "Updated Build Number $globalBuild"
  tag="b"$globalBuild
  git tag  $tag
  git push
  git push --tags
  cd $root_dir
else
  echo "$file not found."
fi
done <<< "$(find $project -wholename '*/src/main/resources/META-INF/MANIFEST.MF')"
cd $root_dir
done
