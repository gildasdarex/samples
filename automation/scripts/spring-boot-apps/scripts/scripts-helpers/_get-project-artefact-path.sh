#!/usr/bin/env bash
#########################################
# This script return the list of the aaps according to projects type.
#  data is read from projects.properties file
# by example ml-api-app analytics-app analytics-service is return for grails_ui type

##########################################
project_name=$1
project_artefact_type_path_properties=$2


while IFS='' read -r line || [[ -n "$line" ]]; do
    artefact=$(cut -d "=" -f 1 <<< "$line")
    if [[ $artefact == "$project_name" ]]
    then
      artefactPath=$(cut -d "=" -f 2 <<< "$line")
      echo -e $artefactPath
      exit
    fi
done < "$project_artefact_type_path_properties"