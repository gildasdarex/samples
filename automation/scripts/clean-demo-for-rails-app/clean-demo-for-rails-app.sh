#!/usr/bin/env bash



clean_collectd_directory () {
  # Clean collectd directory
  rm -R /var/lib/collectd/csv
  rm -R /var/lib/collectd/asklytics
  rm /var/lib/collectd.zip
}

pull_influxdb_rails_gem () {
  # Clean collectd directory
  cd $repo_dir/al-influxdb-rails
  git pull
}

pull_demo_app () {
  # Clean collectd directory
  cd $repo_dir/al-storefront-rails
  git pull
}

redeploy_app () {
  cd $repo_dir/al-storefront-rails
  bundle update

  #clean all the logs
  > log/app.log
  > log/development.log

  nohup rails s -b $server_ip >> log/app.log &
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
        repo.dir)
          repo_dir="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        server.ip)
          server_ip="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

#Stop rails app
kill -9 $(lsof -t -i:3000)

clean_collectd_directory

pull_influxdb_rails_gem

pull_demo_app

redeploy_app

#Start collectd service
service collectd start

