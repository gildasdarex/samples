#!/usr/bin/env bash


parse_regular_file () {
  echo "Replacing all occurences of $pattern_key  by $pattern_value in $template_dir file"

  filename="$(basename $template_dir)"

  #The command below should work on linux and mac . but if it failed on linux system please replace -e by -i in the command
  sed -e "s/$pattern_key/$pattern_value/g" "$template_dir" > $output_dir/$filename
}

parse_directory () {
 echo "Replacing all occurences of $pattern_key  by $pattern_value in $template_dir directory"
 for file in $template_dir/*; do
    parse_regular_file $file
 done
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
        template.dir)
          template_dir="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        output.dir)
          output_dir="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        pattern.key)
          pattern_key="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
        pattern.value)
          pattern_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
    esac
done

if [ ! -d "$output_dir" ]
then
    echo "$output_dir does not exist . The script will create it"
    mkdir -p "$output_dir"
fi

if [[ -d $template_dir ]]; then
    echo "$template_dir is a directory . All files inside $template_dir will be parsed"
    parse_regular_file $template_dir
elif [[ -f $template_dir ]]; then
    echo "$template_dir is a regular file . $template_dir will be parsed"
    parse_regular_file $template_dir
else
    echo "$template_dir is not valid file"
    exit 1
fi