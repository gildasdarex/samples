#!/usr/bin/env bash

"/asklytics/logs/spring/"
allPlatformLogFiles=( "/asklytics/logs/activemq/node-01/activemq-audit.log"  "/asklytics/logs/activemq/node-01/activemq.log" \
    "/asklytics/logs/activemq/node-02/activemq-audit.log"  "/asklytics/logs/activemq/node-02/activemq.log" \
    "/asklytics/logs/activemq/node-03/activemq-audit.log"  "/asklytics/logs/activemq/node-03/activemq.log"
)

#filesActiveMq=(     "/asklytics/logs/activemq/node-01/activemq-audit.log"  "/asklytics/logs/activemq/node-01/activemq.log" \
#    "/asklytics/logs/activemq/node-02/activemq-audit.log"  "/asklytics/logs/activemq/node-02/activemq.log" \
#    "/asklytics/logs/activemq/node-03/activemq-audit.log"  "/asklytics/logs/activemq/node-03/activemq.log" )

echo -e "Verify Existence of Platform  Log files ..."

for file in "${allPlatformLogFiles[@]}"
 do
    echo -e "\tVerify Existence of file ${file} ..."
    if [[ -f ${file} ]]; then
        echo -e "\t\tFile Exists"
        if [[ ! -s ${file} ]] ; then
                  echo -e "\t\tWARNING File is Empty!"
        else
            echo -e "\t\tCurrent Time:" date  +"%m-%d-%Y %H:%M:%S"
            echo -e "\t\tLast modified:" date -r ${file}  "+%m-%d-%Y %H:%M:%S"
        fi
    else
        echo -e "\t\tWARNING ! $file does NOT exist"
    fi
done