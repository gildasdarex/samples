#!/usr/bin/env bash

usage () {
    echo -e "/nUse this script to create  "
    echo -e " ./verify-files-exist.sh "
    echo -e "\t--files.list absolute_path_to_file_with_list_of_files "
}

print_option_values() {
    echo ""
    echo " Options values sent to script are : "
    echo " files.list = $files_list "
}

check_required_options_and_env_variable() {
    echo ""
    echo " Check required options "

    if [[ -z ${files_list+x} ]]; then
     echo "files.list is missing "
     exit
    fi
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       # text file that contains the list of flies to check if they exist
       files.list)
          files_list="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       help)
          usage
          ;;
    esac
done

print_option_values
check_required_options_and_env_variable

if [[ ! -f ${files_list} ]]; then
    echo "Invalid Input: Input  does not exist ${files_list}"
    exit 1
fi

while IFS= read -r line
do
  echo "$line"

done < "$files_list"


