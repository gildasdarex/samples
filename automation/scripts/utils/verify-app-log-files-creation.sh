#!/usr/bin/env bash

"/asklytics/logs/spring/"
allAppLogFiles=("/asklytics/logs/spring/access-security.log" "/asklytics/logs/spring/analytics-service-error.log"  \
    "/asklytics/logs/spring/analytics-service-performance.log" "/asklytics/logs/spring/analytics-service-sql.log" \
    "/asklytics/logs/spring/analytics-service-std.log"  "/asklytics/logs/spring/camel-analytics-app.log" \
    "/asklytics/logs/spring/camel-analytics-error.log" "/asklytics/logs/spring/camel-analytics.log" \
    "/asklytics/logs/spring/camel-analytics-sql.log"  "/asklytics/logs/spring/camel-infra-prov-error.log" \
    "/asklytics/logs/spring/camel-infra-prov-sql.log" "/asklytics/logs/spring/camel-prov-app.log" \
    "/asklytics/logs/spring/camel-prov.log" "/asklytics/logs/spring/infra-prov-service-error.log" \
    "/asklytics/logs/spring/infra-prov-service-std.log" "/asklytics/logs/spring/infra-service-error.log" \
    "/asklytics/logs/spring/infra-service-sql.log" "/asklytics/logs/spring/infra-service-std.log" \
    "/asklytics/logs/spring/ml-api-error.log" "/asklytics/logs/spring/ml-api-performance.log" "/asklytics/logs/spring/ml-api-sql.log" \
    "/asklytics/logs/spring/ml-api-std.log" "/asklytics/logs/spring/ui-data-service-error.log" \
    "/asklytics/logs/spring/ui-data-service-sql.log" "/asklytics/logs/spring/ui-data-service-std.log" \
    "/asklytics/logs/nginx/error.log"  "/asklytics/logs/nginx/ui_access.log"  "/asklytics/logs/nginx/ui_error.log" \
    "/asklytics/logs/nginx/external-proxy-access.log"  "/asklytics/logs/nginx/external-proxy-error.log" \
    "/asklytics/logs/python/housekeeper/AsklyticsGlobal.log" "/asklytics/logs/python/housekeeper/PyLyticsServer.log"
)

#filesSpring=("/asklytics/logs/spring/access-security.log" "/asklytics/logs/spring/analytics-service-error.log"  \
#"/asklytics/logs/spring/analytics-service-performance.log" "/asklytics/logs/spring/analytics-service-sql.log" \
#"/asklytics/logs/spring/analytics-service-std.log"  "/asklytics/logs/spring/camel-analytics-app.log" \
#"/asklytics/logs/spring/camel-analytics-error.log" "/asklytics/logs/spring/camel-analytics.log" \
#"/asklytics/logs/spring/camel-analytics-sql.log"  "/asklytics/logs/spring/camel-infra-prov-error.log" \
#"/asklytics/logs/spring/camel-infra-prov-sql.log" "/asklytics/logs/spring/camel-prov-app.log" \
#"/asklytics/logs/spring/camel-prov.log" "/asklytics/logs/spring/infra-prov-service-error.log" \
#"/asklytics/logs/spring/infra-prov-service-std.log" "/asklytics/logs/spring/infra-service-error.log" \
#"/asklytics/logs/spring/infra-service-sql.log" "/asklytics/logs/spring/infra-service-std.log" \
#"/asklytics/logs/spring/ml-api-error.log" "/asklytics/logs/spring/ml-api-performance.log" "/asklytics/logs/spring/ml-api-sql.log" \
#"/asklytics/logs/spring/ml-api-std.log" "/asklytics/logs/spring/ui-data-service-error.log" \
#"/asklytics/logs/spring/ui-data-service-sql.log" "/asklytics/logs/spring/ui-data-service-std.log")
#
#filesAngularNginx=( "/asklytics/logs/nginx/error.log"  "/asklytics/logs/nginx/ui_access.log"  "/asklytics/logs/nginx/ui_error.log" )
#filesPoxyServersNginx=( "/asklytics/logs/nginx/external-proxy-access.log"  "/asklytics/logs/nginx/external-proxy-error.log" )
#filesPython=( "/asklytics/logs/python/housekeeper/AsklyticsGlobal.log" "/asklytics/logs/python/housekeeper/PyLyticsServer.log" )

echo -e "Verify Existence of App Log files ..."

for file in "${allAppLogFiles[@]}"
 do
    echo -e "\tVerify Existence of file ${file} ..."
    if [[ -f ${file} ]]; then
        echo -e "\t\tFile Exists"
        if [[ ! -s ${file} ]] ; then
                  echo -e "\t\tWARNING File is Empty!"
        else
            echo -e "\t\tCurrent Time:" date  +"%m-%d-%Y %H:%M:%S"
            echo -e "\t\tLast modified:" date -r ${file}  "+%m-%d-%Y %H:%M:%S"
        fi
    else
        echo -e "\t\tWARNING ! $file does NOT exist"
    fi
done
