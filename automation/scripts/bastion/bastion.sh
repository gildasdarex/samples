#!/usr/bin/env bash

#!/bin/bash

port=8123

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
        ssh.key.path)
          ssh_key_path="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        bastion.host)
          bastion_host="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        user)
          user="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        port)
          port="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

#if [ $# -lt 1 ]; then
#
#    echo "Usage: bastion_socks.sh <Bastion IP address>"
#
#    exit 0
#
#else

echo Bastion Proxy to $bastion_host Started using Localhost:$port.  Press Ctrl-C to End.

# Uncomment line with ssh command below for Mac
# update location and name of your pem key
# update “username” with your username
 ssh -i $ssh_key_path -D $port -C -N $user@$bastion_host

# Uncomment line with ssh command below for Windows
# update location and name of your pem key
# update “username” with your username
#    ssh -i C:/dev/asklytics/ssh-keys/my.pem -D 8123 -C -N username@$1

#fi

