#!/usr/bin/env bash
#########################################
# This script return the list of the aaps according to projects type.
#  data is read from projects.properties file
# by example ml-api-app analytics-app analytics-service is return for grails_ui type

##########################################
project=$1
projects_modules_properties=$2


while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ "$line" == *$project* ]]
    then
      appModules=$(cut -d "=" -f 2 <<< "$line")
      echo -e $appModules
      exit
    fi
done < "$projects_modules_properties"