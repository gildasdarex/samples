#!/usr/bin/env bash
#########################################
# This script return the list of the dependencies name that need to be update for specific project.
#  data is read from app-type.properties file
# by grails is return for ml-api-app project

##########################################
repository_dir=$1
branch=$2
project_set_properties=$3
releaseHelperDir=$4

clone_project_with_ssh () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone ssh://git@asklytics.repositoryhosting.com/asklytics/$1.git -b "$branch"
}

clone_project_with_https () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone https://asklytics.repositoryhosting.com/git/asklytics/$1.git -b "$branch"
}


read -p " Did you configure ssh? yes or no ?" sshYn

projectSetList=$(${releaseHelperDir}/_get-projects-type-name.sh $project_set_properties)
projectSetListAsArray=($projectSetList)

# Step 3: clone all projects (from $branch as input; default to dev)
for projectSetType in ${projectSetListAsArray[@]}
do
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectSetType $project_set_properties)
   appsArray=($apps)

   for app in ${appsArray[@]}
   do
      if [ ! -d "$repository_dir/$app" ]; then
          if [[ "${sshYn}" == "yes" ]]
            then
              clone_project_with_ssh "$app"
          else
              clone_project_with_https "$app"
          fi
      fi
   done
done