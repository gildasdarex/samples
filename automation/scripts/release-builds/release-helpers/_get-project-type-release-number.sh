#!/usr/bin/env bash
#########################################
# This script return the build number according to projects type.
#  data is read from projects.properties file
# by example 0001 is return for grails_ui type
##########################################
project_type_name=$1
project_properties=$2

key="$project_type_name-last_release_num"

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ "$line" == $key* ]]
    then
      buildNumber=$(cut -d "=" -f 2 <<< "$line")
      echo -e $buildNumber
      exit
    fi
done < "$project_properties"
