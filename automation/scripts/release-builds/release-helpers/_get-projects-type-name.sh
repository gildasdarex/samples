#!/usr/bin/env bash
#########################################
# This script return the list of the  type of projects.
#  data is read from projects.properties file
# types of projects is now: analystics_back_end + grails_ui; angular_app; survey_app; taaas

##########################################

project_properties=$1

declare -a projects_type
counter=0
project_type_list=""

get_project_type () {
  item=$1
  itemData=$(tr '-' $' ' <<< "$item" )
}

while IFS='' read -r line || [[ -n "$line" ]]; do
    data=$(tr '=' $' ' <<< "$line" )
    arrayData=($data)
    firstItem=$arrayData
    get_project_type $firstItem
    if [[ "$itemData" == *projects ]]
    then
      projects_type+=($(tr '-' $' ' <<< "$itemData" ))
    fi
done < "$project_properties"

for key in "${!projects_type[@]}"; do
    project_type_list="$project_type_list ${projects_type[$key]}"
done

echo -e $project_type_list