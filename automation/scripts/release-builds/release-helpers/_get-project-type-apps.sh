#!/usr/bin/env bash
#########################################
# This script return the list of the aaps according to projects type.
#  data is read from projects.properties file
# by example ml-api-app analytics-app analytics-service is return for grails_ui type

##########################################
project_type_name=$1
project_properties=$2

key="$project_type_name-projects"

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ "$line" == $key* ]]
    then
      projectsList=$(cut -d "=" -f 2 <<< "$line")
      echo -e $projectsList
      exit
    fi
done < "$project_properties"