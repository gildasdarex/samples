#!/usr/bin/env bash
#########################################
# This script increment the build number.
##########################################
buildNumber=$(echo $1 | sed 's/\(.\{1\}\)/\1. /g')

increment_build_number ()
{
  declare -a part=( ${buildNumber//\./ } )
  declare -i carry=1

  for (( CNTR=${#part[@]}-1; CNTR>=0; CNTR-=1 )); do
    len=${#part[CNTR]}
    new=$((part[CNTR]+carry))
    [ ${#new} -gt $len ] && carry=1 || carry=0
    [ $CNTR -gt 0 ] && part[CNTR]=${new: -len} || part[CNTR]=${new}
  done
  new="${part[*]}"
  echo -e "${new// /}"
}

increment_build_number $buildNumber
