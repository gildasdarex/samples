#!/usr/bin/env bash
#########################################
# This script return the list of the dependencies name that need to be update for specific project.
#  data is read from app-type.properties file
# by grails is return for ml-api-app project

##########################################
project=$1
app_type_properties=$2

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ "$line" == $project* ]]
    then
      appType=$(cut -d "=" -f 2 <<< "$line")
      echo -e $appType
      exit
    fi
done < "$app_type_properties"