#!/usr/bin/env bash
repository_dir=$1
version_json_file=$2
projects=("ml-api-app" "commons" "infra-security" "auth-service" "infra-service" "ui-data-service" "analytics-service" "infra-camel-service" "extract-aws" "infra-prov-service" "modeler" "email-send-service" "analytics-app" "infra-rest-client" "offer-survey-data-access" "offer-survey-app")

read -p "WARNING!! This will alter the code base and tag on master branch on git. Do NOT do this without approval!!! Are you sure you want to continue? yes or no" yn
if [[ "${yn}" != "yes" ]]
 then
  exit
fi


read -n 1 -s -p "WARNING!! This will alter the code base and create remote branches on git . Press any key to continue"



scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"


for project in ${projects[@]}
do
   cd "$parentdir/release-helpers"
   #get build and release number
   buildNumber=$(./_get-build-number.sh $project $version_json_file)
   releaseNumber=$(./_get-release-number.sh $project $version_json_file)
   dependencyName=$(./_get-project-version-dependency-name.sh $project $version_json_file)

   #update version for tis projevt in all gradle.properties file
   ./_update-version-in-gradle-properties.sh $repository_dir $buildNumber $releaseNumber $dependencyName

   #create a build tag on dev branch
   #access to project dir
   cd "$repository_dir/$project"

   releaseBranch="release_$releaseNumber"
   masterBranch="master"

   git checkout $releaseBranch

   #refresh all tags
   git fetch --tags --force

   # merge release branch with the new version number into master
   git checkout $masterBranch
   git merge --no-ff "b$buildNumber"
   git tag "r$releaseNumber" $masterBranch
   git push --tags origin $masterBranch
done

cd "$parentdir/release-helpers"
echo "dev_build" > history.txt

./_bump-build-number-drynext.sh $version_json_file

