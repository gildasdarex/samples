#!/usr/bin/env bash
echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for release-builds folder"
echo "please ensure that you installed wget in your laptop /n"
echo "##############################################################"


echo "did you already give the permissions to release-builds folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
username=$2
password=$3

#scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
#parentdir="$(dirname "$scriptDir")"

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptdir="$(dirname "$parentdir")"
releaseHelperDir="$parentdir/release-helpers"
propertiesFilesDir="$scriptdir/properties-files"




if [ "$#" -eq 7 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties=$7
elif [ "$#" -eq 6 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 5 ]; then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties="$propertiesFilesDir/projects-snapshot-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 4 ]; then
   branch=$4
   projects_properties="$propertiesFilesDir/project-set-release-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-snapshot-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 3 ]; then
   projects_properties="$propertiesFilesDir/project-set-release-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-snapshot-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi


projectTypeListAsArray=(analytics)

# Step 5: build all apps
for projectType in ${projectTypeListAsArray[@]}
do
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)
   for app in ${appsArray[@]}
   do
      dependencyNames=$(${releaseHelperDir}/_get-project-module-snapshot-name.sh $app $projects_dependencies_name_properties)
      dependencyNamesArray=($dependencyNames)
      for dependencyName in ${dependencyNamesArray[@]}
      do
         ${releaseHelperDir}/_update-snapshot-in-gradle-properties.sh $repository_dir $dependencyName
      done
   done
done
