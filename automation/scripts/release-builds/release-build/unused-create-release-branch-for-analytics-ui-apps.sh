#!/usr/bin/env bash
echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for release-builds folder"
echo "please ensure that you installed wget in your laptop /n"
echo "##############################################################"


echo "did you already give the permissions to release-builds folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
username=$2
password=$3

#scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
#parentdir="$(dirname "$scriptDir")"

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptdir="$(dirname "$parentdir")"
releaseHelperDir="$parentdir/release-helpers"
propertiesFilesDir="$scriptdir/properties-files"


if [ "$#" -eq 7 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties=$7
elif [ "$#" -eq 6 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 5 ]; then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 4 ]; then
   branch=$4
   projects_properties="$propertiesFilesDir/project-set-release-analytics-ui.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 3 ]; then
   projects_properties="$propertiesFilesDir/project-set-release-analytics-ui.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi


update_build_number_in_projects_properties () {
    # $1 parameter is the name of the project type
    # $2 is the value of the build number
    key="$1-last_build_num"
    sed -i '' "s/$key=.*/$key=$2/g" "$projects_properties"
}


projectTypeListAsArray=(analytics_ui)

# Step 5: build all apps
for projectType in ${projectTypeListAsArray[@]}
do
   buildNumber=$(${releaseHelperDir}/_get-project-type-build-number.sh $projectType $projects_properties)
   releaseNumber=$(${releaseHelperDir}/_get-project-type-release-number.sh $projectType $projects_properties)
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)
   for app in ${appsArray[@]}
   do
      dependencyNames=$(${releaseHelperDir}/_get-project-module-dependencies-name.sh $app $projects_dependencies_name_properties)
      dependencyNamesArray=($dependencyNames)
      for dependencyName in ${dependencyNamesArray[@]}
      do
         ${releaseHelperDir}/_update-version-in-gradle-properties.sh $repository_dir $releaseNumber $dependencyName
      done
      cd "$repository_dir/$app"

      devBranch="dev"
      releaseBranch="release_$releaseNumber"

      git checkout $branch

      #refresh all tags
      git fetch --tags --force

      # create the release branch from previous tags
      git checkout -b $releaseBranch "b$buildNumber"
      #git tag "b$buildNumber" $releaseBranch
      git push  origin $releaseBranch
   done
   #nextBuildNumber=$(${releaseHelperDir}/_bump-build-number-drynext.sh $buildNumber)
   #update_build_number_in_projects_properties  $projectType $nextBuildNumber
done

#cd "$parentdir/release-helpers"
#echo "master_build" > history.txt
#
#read -n 1 -s -p "Step 2: create release branch is already completed. Step 3: release on master will start .Would you like to continue?yes/no "yn
#if [[ "${yn}" != "yes" ]]
# then
#  exit
#else
#  cd "$parentdir/release-build"
#  ./create-release-to-master.sh $repository_dir $version_json_file
#fi