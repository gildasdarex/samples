#!/usr/bin/env bash
echo '------------------------------ release start----------------------------------------'
repository_dir=$1
version_json_file=$2
clone=$3 #boolean parameter that tell to script to clone all projects or not..if clone=false, then all projects is in repository_dir

projects=("ml-api-app" "commons" "commons-ws" "data-collector-transformer" "entity-model" "data-access"  "infra-security" "auth-service" "infra-service" "ui-data-service" "analytics-service" "infra-camel-service" "extract-aws" "infra-prov-service" "modeler" "email-send-service" "analytics-app" "infra-rest-client" "offer-survey-data-access" "offer-survey-app")
devBranch="dev"
masterBranch="master"

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"


#ask to user if he would like to generate release
read -p "WARNING!! This will alter the code base and create remote branches on git. Do NOT do this without approval!!! Are you sure you want to proceed? yes or no" yn
if [[ "${yn}" != "yes" ]]
 then
  exit
fi

read -n 1 -s -p "WARNING!! This will alter the code base and create remote branches on git . Press any key to continue"

clone_all_projects () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone ssh://git@asklytics.repositoryhosting.com/asklytics/$1.git -b "$devBranch"
}

checkout_dev_master_branch_for_all_projects(){
    for project in ${projects[@]}
      do
        cd "$repository_dir/$project"
        # ensure you are on latest develop  & master
        git checkout "$devBranch"
        git pull origin "$devBranch"

        git checkout "$masterBranch"
        git pull origin "$masterBranch"

        git checkout "$devBranch"
    done
}

create_build(){
#get build and release number
    read -n 1 -s -p "WARNING!! This will alter the code base and create tag for dev branch on git . Press any key to continue"
    cd "$parentdir/snapshot-build"
    ./create-build.sh $repository_dir $version_json_file
}

create_release_branch(){
#get build and release number
    read -n 1 -s -p "WARNING!! This will alter the code base and create release branch on git . Press any key to continue"
    cd "$parentdir/release-build"
    create-release-branch-for-analytics-apps.sh $repository_dir  $version_json_file
}

create_release_to_master_branch(){
#get build and release number
    read -n 1 -s -p "WARNING!! This will alter the code base and create master branch on git . Press any key to continue"
    cd "$parentdir/release-build"
    create-release-to-master.sh $repository_dir $version_json_file
}
#clone all projects if clone parameter value is true
if [ "$clone" = 'true' ];
then
  for project in ${projects[@]}
  do
    clone_all_projects $project
  done
fi

checkout_dev_master_branch_for_all_projects

cd "$parentdir/release-helpers"
historyData=$(cat history.txt)

case "$historyData" in
   "dev_build")
      echo "------------------------------ Step 1 create tag on dev branch----------------------------------------"
      create_build
   ;;
   "release_build")
      echo "------------------------------ Step 1: tag on dev branch  is already completed. Step 2: creation on release branch will start----------------------------------------"
      create_release_branch
   ;;
   "master_build")
     echo "------------------------------ Step 2: create release branch is already completed. Step 3: release on master will start---------------------------"
     create_release_to_master_branch
   ;;
esac