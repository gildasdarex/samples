#########################################
# This script creates a snapshot build of a product.
# This is the first step in releasing the product.
# WARNING !!!!! THIS SHOULD *NOT* BE USED BY DEVELOPERS TO BUILD PROJECTS !!!!!!!
# Purpose of creating snapshots
# a) ensuring there are no build breaks
# b) functional testing
# c) creating a release for production

# Step 1: delete local-maven-repo so that all previous builds do not exist
# it is because currently we do have the jars/wars names to include the build number nor do we put the build# in meta file inside the jar/war
# Step 2: delete m2 cache so that all the dependencies cached are removed
# this is to ensure that we can catch all build breaks
# Step 3: clone all projects (from $branch as input; default to dev)
# Step 4: build all projects
# Step 5: if build fails, abort
# Step 6: if build passes, get last build# for the project set
# Step 7: increment build#
# Step 8: Tag all projects with the build#
# Step 9: save the last build#
# step 10: merge the properties file that has the last build#

##########################################
#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for release-builds folder /n"
echo "please ensure that you installed wget in your laptop /n"
echo "##############################################################"

#ask to user if he would like to generate release
read -p "did you already give the permissions to release-builds folder and installed wget ? yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
username=$2
password=$3

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
releaseHelperDir="$parentdir/release-helpers"



#ask to user if he would like to generate release
read -p " /n WARNING!! This will alter the code base and create remote branches on git. Do NOT do this without approval!!! /n Are you sure you want to proceed? yes or no " yn
if [[ "${yn}" != "yes" ]]
 then
  exit
fi

read -n 1 -s -p " /n WARNING!! This will alter the code base and create remote branches on git . Press any key to continue"



if [ "$#" -eq 7 ]
   then
   projects_properties=$4
   projects_dependencies_name_properties=$5
   projects_publish_name_properties=$6
   branch=$7
elif [ "$#" -eq 6 ]
   then
   projects_properties=$4
   projects_dependencies_name_properties=$5
   projects_publish_name_properties=$6
   branch="dev"
elif [ "$#" -eq 5 ]; then
   projects_properties=$4
   projects_dependencies_name_properties=$5
   projects_publish_name_properties="$releaseHelperDir/projects-publish-names.properties"
   branch="dev"
elif [ "$#" -eq 4 ]; then
   projects_properties=$4
   projects_dependencies_name_properties="$releaseHelperDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$releaseHelperDir/projects-publish-names.properties"
   branch="dev"
elif [ "$#" -eq 3 ]; then
   projects_properties="$releaseHelperDir/projects.properties"
   projects_dependencies_name_properties="$releaseHelperDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$releaseHelperDir/projects-publish-names.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi

#gradle cache clean
rm -rf $HOME/.gradle/caches/


remove local-maven-repo directory if it exists
if [ -d "$repository_dir/local-maven-repo" ]
then
    rm -R "$repository_dir/local-maven-repo"
fi

#create an empty local-maven-repo directory
sudo mkdir -p "$repository_dir/local-maven-repo"
sudo chmod -R 777 "$repository_dir/local-maven-repo"

clone_project_with_ssh () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone ssh://git@asklytics.repositoryhosting.com/asklytics/$1.git -b "$branch"
}

clone_project_with_https () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone https://asklytics.repositoryhosting.com/git/asklytics/$1.git -b "$branch"
}

build_project () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir/$1"
    echo "------------------------------ clean  $1 project----------------------------------------"
    gradle clean
    echo "------------------------------ build  $1 project----------------------------------------"
    gradle build
    echo "------------------------------ publish  $1 project----------------------------------------"
    gradle publish
}

build_angular_project () {
    # $1 parameter is the name of the project to clone
    echo "------------------------------ build angular app $1 project----------------------------------------"
    cd "$repository_dir/$1/analytics-ui-view/angular2-app/"
    rm -rf node_modules
    npm install typescript@^2.1.0
    npm install typescript@^2.1.5
    npm install
    ng build --dev
}

check_if_project_successful_build () {
    # $1 parameter is the name of the project#
    #project is successuf build if we find his directory under local-maven-repo
    echo "------------------------------check if $1 build is sucessful----------------------------------------"
    appPublishNanes=$(${releaseHelperDir}/_get-project-module-publish-name.sh $1 $projects_publish_name_properties)
    appPublishNanesArray=($appPublishNanes)

    for appPublishName in ${appPublishNanesArray[@]}
    do
      publishNameRepoPath="$repository_dir/local-maven-repo/com/asklytics/$appPublishName"
      if [ ! -d $publishNameRepoPath ]; then
       echo "build failed for project $1"
       echo "script will end due of this error"
       exit
      fi
    done
}


check_if_angular_project_successful_build () {
    # $1 parameter is the name of the project
    #project is successuf build if we find his dist directory under project
    echo "------------------------------check if $1 build is sucessful----------------------------------------"
    appPublishNanes=$(${releaseHelperDir}/_get-project-module-publish-name.sh $1 $projects_publish_name_properties)
    appPublishNanesArray=($appPublishNanes)

    for appPublishName in ${appPublishNanesArray[@]}
    do
      publishNameRepoPath="$repository_dir/$appPublishName/dist"
      if [! -d "$publishNameRepoPath" ]; then
       echo "build failed for project $1"
       echo "script will end due of this error"
       exit
      fi
    done
}

update_build_number_in_projects_properties () {
    # $1 parameter is the name of the project type
    # $2 is the value of the build number
    sed -i '' "s/\"$1\":.*/\"$1\":\"$2\",/g" "$projects_properties"
}

tag_angular_dist_folder () {
    # $1 parameter is the name of the project to tag
    # $2 is the value of the build number
    # $3 is the name of project
    angularAppsPublishNames=$(${releaseHelperDir}/_get-project-module-publish-name.sh $1 $projects_publish_name_properties)
    angularAppsPublishNamesArray=($angularAppsPublishNames)

    for angularAppsName in ${angularAppsPublishNamesArray[@]}
    do
      angularAppPublishNameRepoPath="$repository_dir/$angularAppsName/dist"
      cd $angularAppPublishNameRepoPath
      zip -r "$3_snapshot-b$2.zip" $angularAppPublishNameRepoPath
    done
}

tag_project () {
    # $1 parameter is the name of the project to tag
    # $2 is the value of the build number
    read -n 1 -s -p "WARNING!! $1 will be tagged with build number $2 . Press any key to tag"
    cd "$repository_dir/$1"
    git tag "b$2" $branch
    git push --tags origin $branch
}

projectTypeList=$(${releaseHelperDir}/_get-projects-type-name.sh $projects_properties)
projectTypeListAsArray=($projectTypeList)

# Step 3: clone all projects (from $branch as input; default to dev)
for projectType in ${projectTypeListAsArray[@]}
do
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)

   for app in ${appsArray[@]}
   do
      if [ ! -d "$repository_dir/$app" ]; then
          if [[ "${sshYn}" == "yes" ]]
            then
              clone_project_with_ssh "$app"
          else
              clone_project_with_https "$app"
          fi
      fi
   done
done

#Step 4: download jars that need to be in  local-flat-repo
if [ ! -d "$repository_dir/local-flat-repo" ]
then
    mkdir -p "$repository_dir/local-flat-repo"
    cd "$repository_dir/local-flat-repo"
    wget --http-user="$username" --http-password="$password" https://asklytics.repositoryhosting.com/webdav/asklytics_analytics-service/com.asklytics.matlab.algos-1.0.04.jar
    wget --http-user="$username" --http-password="$password" https://asklytics.repositoryhosting.com/webdav/asklytics_analytics-service/javabuilder-Matlab-2016a.jar
fi


# Step 5: build all apps
for projectType in ${projectTypeListAsArray[@]}
do
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)
   for app in ${appsArray[@]}
   do
      if [[ "$projectType" != "angular_app" ]]; then
        build_project $app
        check_if_project_successful_build $app
      else
         build_angular_project $app
         check_if_angular_project_successful_build $app
      fi
   done
done

for projectType in ${projectTypeListAsArray[@]}
do
   #step 6 : get project type build number
   buildNumber=$(${releaseHelperDir}/_get-project-type-build-number.sh $projectType $projects_properties)
   #Step 7: increment build number
   nextBuildNumber=$(${releaseHelperDir}/_bump-build-number-drynext.sh $buildNumber)
   #nextBuildNumber="0200"

   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)

   for app in ${appsArray[@]}
   do
      #get all module dependency name for a specific project
#      moduleDependenciesNames=$(${releaseHelperDir}/_get-project-module-dependencies-name.sh $app $projects_dependencies_name_properties)
#      moduleDependenciesNameArray=($moduleDependenciesNames)
#      #Step 8: update gradle.properties file with build number
#      for moduleDependenciesName in ${moduleDependenciesNameArray[@]}
#      do
#        if [[ "$projectType" != "angular_app" ]]; then
#           echo "buildNumber: $buildNumber---nextBuildNumber: $nextBuildNumber ---moduleDependenciesName: $moduleDependenciesName"
#           ${releaseHelperDir}/_update-version-in-gradle-properties.sh "$repository_dir" $nextBuildNumber $moduleDependenciesName
##       else
##        tag_angular_dist_folder $projectType $nextBuildNumber $moduleDependenciesName
#        fi
#      done
      #Step 9: tag the project
      tag_project $app $nextBuildNumber
   done

   #step 10: merge the properties file that has the last build#
   update_build_number_in_projects_properties  $projectType $nextBuildNumber
done
