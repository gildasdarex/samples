#########################################
# This script creates a snapshot build of a product.
# This is the first step in releasing the product.
# WARNING !!!!! THIS SHOULD *NOT* BE USED BY DEVELOPERS TO BUILD PROJECTS !!!!!!!
# Purpose of creating snapshots
# a) ensuring there are no build breaks
# b) functional testing
# c) creating a release for production

# Step 1: delete local-maven-repo so that all previous builds do not exist
# it is because currently we do have the jars/wars names to include the build number nor do we put the build# in meta file inside the jar/war
# Step 2: delete m2 cache so that all the dependencies cached are removed
# this is to ensure that we can catch all build breaks
# Step 3: clone all projects (from $branch as input; default to dev)
# Step 4: build all projects
# Step 5: if build fails, abort
# Step 6: if build passes, get last build# for the project set
# Step 7: increment build#
# Step 8: Tag all projects with the build#
# Step 9: save the last build#
# step 10: merge the properties file that has the last build#

##########################################
#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for release-builds folder"
echo "please ensure that you installed wget in your laptop /n"
echo "##############################################################"


echo "did you already give the permissions to release-builds folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
username=$2
password=$3

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptdir="$(dirname "$parentdir")"
releaseHelperDir="$parentdir/release-helpers"
propertiesFilesDir="$scriptdir/properties-files"


#ask to user if he would like to generate release
echo "WARNING!! This will alter the code base and create remote branches on git. Do NOT do this without approval!!! Are you sure you want to proceed? "
read -p "yes or no " yn
if [[ "${yn}" != "yes" ]]
 then
  exit
fi


echo " WARNING!! This will alter the code base and create remote branches on git."
read -n 1 -s -p "Press any key to continue"


if [ "$#" -eq 7 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties=$7
elif [ "$#" -eq 6 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 5 ]; then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 4 ]; then
   branch=$4
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 3 ]; then
   projects_properties="$propertiesFilesDir/project-set-analytics.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi

#gradle cache clean
rm -rf $HOME/.gradle/caches/


#remove local-maven-repo directory if it exists
if [ -d "$repository_dir/local-maven-repo" ]
then
    rm -R "$repository_dir/local-maven-repo"
fi

#create an empty local-maven-repo directory
sudo mkdir -p "$repository_dir/local-maven-repo"
sudo chmod -R 777 "$repository_dir/local-maven-repo"

clone_project_with_ssh () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone ssh://git@asklytics.repositoryhosting.com/asklytics/$1.git -b "$branch"
}

clone_project_with_https () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone https://asklytics.repositoryhosting.com/git/asklytics/$1.git -b "$branch"
}

build_project () {
    # $1 parameter is the name of the project to clone
    projectArtefactType=$(${releaseHelperDir}/_get-app-type.sh $1 "$propertiesFilesDir/app-type.properties")
    cd "$repository_dir/$1"
    case "$projectArtefactType" in
       "spring")
          echo "------------------------------ clean  $projectArtefactType project : $1 ----------------------------------------"
          gradle clean
          echo "------------------------------ build  $projectArtefactType project: $1  ----------------------------------------"
          gradle build
          echo "------------------------------ publish  $projectArtefactType project: $1 ----------------------------------------"
          gradle publish
       ;;
       "spring-boot")
          echo "------------------------------ clean  $projectArtefactType project: $1 ----------------------------------------"
          gradle clean
          echo "------------------------------ build  $projectArtefactType project: $1 ----------------------------------------"
          gradle build
          echo "------------------------------ publish $projectArtefactType project: $1 ----------------------------------------"
          gradle publish
       ;;
       "grails")
          echo "------------------------------ clean  $projectArtefactType project: $1 ----------------------------------------"
          gradle clean
          echo "------------------------------ build  $projectArtefactType project: $1 ----------------------------------------"
          gradle build
          echo "------------------------------ publish $projectArtefactType project: $1 ----------------------------------------"
          gradle publish
       ;;
    esac
}

check_if_project_successful_build () {
    # $1 parameter is the name of the project#
    #project is successuf build if we find his directory under local-maven-repo
    echo "------------------------------check if $1 build is sucessful----------------------------------------"
    appPublishNanes=$(${releaseHelperDir}/_get-project-module-publish-name.sh $1 $projects_publish_name_properties)
    appPublishNanesArray=($appPublishNanes)

    for appPublishName in ${appPublishNanesArray[@]}
    do
      publishNameRepoPath="$repository_dir/local-maven-repo/com/asklytics/$appPublishName"
      if [ ! -d $publishNameRepoPath ]; then
        echo "build failed for project $1"
        echo "script will end due of this error"
        exit
      fi
    done
}


update_build_number_in_projects_properties () {
    # $1 parameter is the name of the project type
    # $2 is the value of the build number
    key="$1-last_build_num"
    sed -i '' "s/$key=.*/$key=$2/g" "$projects_properties"
}

tag_project () {
    # $1 parameter is the name of the project to tag
    # $2 is the value of the build number
    read -n 1 -s -p "WARNING!! $1 will be tagged with build number $2 . Press any key to tag"
    cd "$repository_dir/$1"
    git tag "b$2" $branch
    git push --tags origin $branch
}


projectTypeListAsArray=(analytics)

# Step 3: clone all projects (from $branch as input; default to dev)
echo " Did you configure ssh? "
read -p "yes or no ? " sshYn
for projectType in ${projectTypeListAsArray[@]}
do
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)

   for app in ${appsArray[@]}
   do
      if [ ! -d "$repository_dir/$app" ]; then
          if [[ "${sshYn}" == "yes" ]]
            then
              clone_project_with_ssh "$app"
          else
              clone_project_with_https "$app"
          fi
      fi
   done
done

# Matlab is no longer used
#Step 4: download jars that need to be in  local-flat-repo
# if [ ! -d "$repository_dir/local-flat-repo" ]
# then
#    mkdir -p "$repository_dir/local-flat-repo"
#    cd "$repository_dir/local-flat-repo"
#    wget --http-user="$username" --http-password="$password" https://asklytics.repositoryhosting.com/webdav/asklytics_analytics-service/com.asklytics.matlab.algos-1.0.04.jar
#    wget --http-user="$username" --http-password="$password" https://asklytics.repositoryhosting.com/webdav/asklytics_analytics-service/javabuilder-Matlab-2016a.jar
# fi


# Step 5: build all apps
for projectType in ${projectTypeListAsArray[@]}
do
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)
   for app in ${appsArray[@]}
   do
      build_project $app
      check_if_project_successful_build $app
   done
done

for projectType in ${projectTypeListAsArray[@]}
do
   #step 6 : get project type build number
   buildNumber=$(${releaseHelperDir}/_get-project-type-build-number.sh $projectType $projects_properties)
   #Step 7: increment build number
   nextBuildNumber=$(${releaseHelperDir}/_bump-build-number-drynext.sh $buildNumber)

   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)

   for app in ${appsArray[@]}
   do
      tag_project $app $buildNumber
   done

   #step 10: merge the properties file that has the last build#
   update_build_number_in_projects_properties  $projectType $nextBuildNumber
done