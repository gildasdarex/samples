#!/bin/bash

#Help###########
if [ "$1" = "-h" ]
then
  echo "Options: "
  echo "-d directory to clone the repos to"
  echo "-u git username"
  echo "-p git password"
  echo "-b git branch to clone"
  echo "Config :"
  echo "Configurations are loaded from config.sh file.pylytics_projects_git_repo array contains the repos to clone."
  echo "New repo can be added with next index to the array."
  exit 0
fi
##################

#import the config
. ./config-blu.sh
##################

#defaults
DIREC="./"
BRANCH="dev"

#Get the cmd line arguments
while getopts d:u:p:b: option
do
 case "${option}"
 in
 u) USER=${OPTARG};;
 d) DIREC=${OPTARG};;
 p) PASSWORD=${OPTARG};;
 b) BRANCH=${OPTARG};;
 esac
done
###########################
cd $DIREC

#Execute clone commands
for repo in "${blulytics_projects_git_repo[@]}"
do
   : 
   #generate git command
   git_cmd="git clone -b $BRANCH $repos_protocol://$USER:$PASSWORD@$repo"
   #execute the command
   echo "Cloning $BRANCH of repo $repo in to $DIREC"
   $git_cmd
done
###########################