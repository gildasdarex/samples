#!/bin/bash

#Help###########
if [ "$1" = "-h" ]
then
  echo "Make sure docker with anaconda is installed"
  echo "            apt install docker.io"
  echo "            docker pull continuumio/anaconda3"
  echo "Options: "
  echo "    -d directory to where the pylytics (algo-python-1) repos is stored. Default = ./"
  echo "    -b git branch of the repo to use for building. Default = dev"
  echo "    -m mode of the build 'intg' or 'prod'. intg adds SNAPSHOT and timestamp to the name"
  echo "Config :"
  echo "    Configurations are loaded from ./config.sh file"
  echo "build_out_dir is the ouput directory of generated wheels."
  echo "build_version is used as version in creating the wheel"
  exit 0
fi
##################

#import the config
. ./config.sh
echo "Loaded configuration from ./config.sh file ...."

##################

#defaults
DIREC="./"
BRANCH="dev"
MODE="intg"
SETUP="setup.py"
VERSION=$build_version
TIMESTAMP=$(date +%s)

#Get the cmd line arguments
while getopts d:b:m: option
do
 case "${option}"
 in
 m) MODE=${OPTARG};;
 d) DIREC=${OPTARG};;
 b) BRANCH=${OPTARG};;
 esac
done
###########################
cd $DIREC

#decide intg or prod
if [ "$MODE" == "intg" ]
then
    VERSION="$VERSION-SNAPSHOT-$TIMESTAMP"
fi
##########################



getBuildNumber(){
echo ""
echo "Getting build number ..."
root_dir=$1
projects=$2
returnRes=$3
cd $root_dir
globalBuild=0
IFS=';' read -r -a projectsArray <<< "$projects"
echo "Using root_dir=$root_dir projects=$projects returnRes=$returnRes  projectsArray=$projectArray ..."

for project in "${projectsArray[@]}"
do
while read line; do
file=$line
echo "Looking for file with name: $file ..."
if [ -f "$file" ]
then
  echo "$file found."

  while IFS=': ' read -r key value
  do
    key=$(echo $key | tr '-' '_')
    eval ${key}=\${value}
  done < "$file"

  inc_build_num="$((${Implementation_Build}+1))"
   if [ $globalBuild -lt $inc_build_num ]
   then
     globalBuild=$inc_build_num
   fi
else
  echo "getBuildNumber():  file not found. file=$file"
fi
done <<< "$(find $project -wholename '*/build.txt')"
cd $root_dir
done

echo "globalBuild=$globalBuild"

echo "... finished getBuildNumber()"
}

incrementBuildNumber(){
echo ""
echo "Incrementing Build Number ..."
root_dir=$1
projects=$2
globalBuild=$3
buildDate=$(date)
buildTimestamp=$(date +%s)
echo "Using root_dir=$root_dir projects=$projects globalBuild=$globalBuild  projectsArray=$projectArray ..."

cd $root_dir
for project in "${projectsArray[@]}"
do
while read line; do
file=$line
echo "Looking for file with name: $file ..."
if [ -f "$file" ]
then

  inc=$globalBuild
  sed -i "s/^Implementation-Build: .*/Implementation-Build: $inc/"  $file
  sed -i "s/^Build-Date: .*/Build-Date: $buildDate/"  $file
  sed -i "s/^Build-Timestamp: .*/Build-Timestamp: $buildTimestamp/"  $file
 DIR=$(dirname "${file}")
  cd $DIR
  #git add .
  #git commit -m "Updated Build Number $globalBuild"
  #tag="b"$globalBuild
  #git tag  $tag
  #git push
  #git push --tags
  cd $root_dir
else
  echo "incrementBuildNumber() file not found. file=$file"
fi
done <<< "$(find $project -wholename '*/build.txt')"
cd $root_dir
done

echo "... finished incrementBuildNumber()"
}

commitBuildNumber(){
echo ""
echo "Committing Build Number ..."
root_dir=$1
projects=$2
globalBuild=$3
echo "Using root_dir=$root_dir projects=$projects globalBuild=$globalBuild  projectsArray=$projectArray ..."
cd $root_dir
for project in "${projectsArray[@]}"
do
while read line; do
file=$line
echo "Looking for file with name: $file ..."
if [ -f "$file" ]
then
  DIR=$(dirname "${file}")
  cd $DIR
  git add .
  git commit -m "Updated Build Number $globalBuild"
  tag="b"$globalBuild
  git tag  $tag
  git push
  git push --tags
  cd $root_dir
else
  echo "$file not found."
fi
done <<< "$(find $project -wholename '*/build.txt')"
cd $root_dir
done
}


identifyIdentityAndVersion() {
echo "Executing  'whoami' on docker instance ..."
docker exec -it pylytics-build /bin/bash -c "whoami"
echo ""
echo "Requires python 3.6 or higher. Executing  'python --version' on docker instance ..."
docker exec -it pylytics-build /bin/bash -c "python --version"
echo ""
}


#Start docker
echo "Starting docker..."
docker run --name="pylytics-build" -d -v $DIREC:$DIREC -i -t "continuumio/anaconda3:2019.03"

identifyIdentityAndVersion

#Build
echo "Building pylytics apps..."
git_checkout_cmd="git checkout $BRANCH"
build_cmd="python $SETUP --al-version $VERSION bdist_wheel -d $build_out_dir"
d="algo-python-1"

getBuildNumber $DIREC $d

echo "Build Command being used:"  $build_cmd
echo "Will look for $SETUP in dir $DIREC/$d ..."
( docker exec -it pylytics-build /bin/bash -c "cd $DIREC/$d; pwd; python --version; $git_checkout_cmd; $build_cmd" )
incrementBuildNumber $DIREC $d $globalBuild

#for d in *; do
#  if [ -d "$d" ]; then
#    #rm -f $DIREC/$d/build.txt
#    #echo "1011" >  $DIREC/$d/build.txt
#    echo "Looking in root dir:" $DIREC "subdir:" $d
#    getBuildNumber $DIREC $d
#    ( docker exec -it build-docker /bin/bash -c "cd $DIREC/$d; pwd; python --version; $git_checkout_cmd; $build_cmd" )
#    incrementBuildNumber $DIREC $d $globalBuild
#     #commitBuildNumber $DIREC $d $globalBuild
#  fi
#done
###########################

echo "Wheel file output dir: $DIREC/$build_out_dir "

#Kill al docker instances
docker kill $(docker ps -q)
docker rm -f pylytics-build
