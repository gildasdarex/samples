#!/bin/bash

#Help###########
if [ "$1" = "-h" ]
then
  echo "Make sure docker with anaconda is installed" 
  echo "            apt install docker.io"
  echo "            docker pull continuumio/anaconda3:2019.03"
  echo "Options: "
  echo "    -d directory to where the pylytics repos are stored. Default = ./"
  echo "    -b git branch of the repo to use for building. Default = dev"
  echo "    -m mode of the build 'intg' or 'prod'. intg adds SNAPSHOT and timestamp to the name"
  echo "Config :"
  echo "    Configurations are loaded from ./config-pylytics-tests.sh file"
  echo "build_out_dir is the ouput directory of generated wheels."
  echo "build_version is used as version in creating the wheel"
  exit 0
fi
##################

#import the config
. ./config-pylytics-tests.sh
echo "Loaded configuration from ./config-pylytics-tests.sh file ...."

##################

#defaults
DIREC="./"
BRANCH="dev"
MODE="intg"
SETUP="setup_tests.py"
VERSION=$build_version
TIMESTAMP=$(date +%s)

#Get the cmd line arguments
while getopts d:b:m: option
do
 case "${option}"
 in
 m) MODE=${OPTARG};;
 d) DIREC=${OPTARG};;
 b) BRANCH=${OPTARG};;
 esac
done
###########################
cd $DIREC

#decide intg or prod
if [ "$MODE" == "intg" ]
then 
    VERSION="$VERSION-SNAPSHOT-$TIMESTAMP"
fi
##########################

#Start docker 
echo "Starting docker..."
docker run --name="build-docker" -d -v $DIREC:$DIREC -i -t "continuumio/anaconda3:2019.03"

#Build
echo "Building pylytics apps..."
git_checkout_cmd="git checkout $BRANCH"
build_cmd="python $SETUP --al-version $VERSION bdist_wheel -d $build_out_dir"
echo $build_cmd
for d in *; do
  if [ -d "$d" ]; then
    ( docker exec -it build-docker /bin/bash -c "cd $DIREC/$d; $git_checkout_cmd; $build_cmd" )
  fi
done
###########################
echo "Wheel file output dir: $DIREC/$build_out_dir "
#Kill al docker instances
docker kill $(docker ps -q)
docker rm -f build-docker