#/bin/bash

 wheels_container_dir="/development/wheels"

 ##### Project 1 data ##############
 pylytics_projects[0]="algo-python-1"
 pylytics_projects_version[0]="1.0.0"
 pylytics_projects_wheel[0]="asklytics_algos-1.0.10_SNAPSHOT_1517439942-py3-none-any.whl"
 pylytics_projects_port[0]="8000"
 pylytics_projects_fs[0]="/development/pylytics/data:/fss/data;/development/pylytics/debug:/fss/debug;/development/pylytics/logging:/fss/logging"
####################################
