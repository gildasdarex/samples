#!/bin/bash


#Help###########
if [ "$1" = "-h" ]
then
  echo "Options: "
  echo "The only argument is the app/project name."
fi

#import the config
. ./config.sh
##################

APP_NAME="$1"

for i in "${!pylytics_projects[@]}"; do 
    name="${pylytics_projects[$i]}"
    if [ "$name" = "$APP_NAME" ]
    then
        wheel="${pylytics_projects_wheel[$i]}"
        port="${pylytics_projects_port[$i]}"
        fs="${pylytics_projects_fs[$i]}"
        echo $name $wheel $port
        docker stop $APP_NAME
	docker rm $APP_NAME
	./start-pylytics.sh $APP_NAME
    fi
done


