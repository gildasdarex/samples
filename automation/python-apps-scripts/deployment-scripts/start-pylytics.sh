#!/bin/bash

#import the config
. ./config.sh
##################


#Help###########
if [ "$1" = "-h" ]
then
  echo "Options: "
  echo "The only argument is the app/project name."
  echo "Config:"
  echo "Each index of pylytics_projects,pylytics_projects_version etc corresponds to an application."
  echo "separate the fs path with ; to hae multiple fs"
fi

APP_NAME="$1"

for i in "${!pylytics_projects[@]}"; do 
    name="${pylytics_projects[$i]}"
    echo $name $APP_NAME
    if [ "$name" = "$APP_NAME" ]
    then
        wheel="${pylytics_projects_wheel[$i]}"
        port="${pylytics_projects_port[$i]}"
        fs="${pylytics_projects_fs[$i]}"
        echo $name $wheel $port

        volume_str=""
        IFS=';' read -ra vol <<< "$fs"
        echo "Mounts:"
        for j in "${!vol[@]}"; do
            volume_str="$volume_str -v ${vol[$j]}"
            echo "${vol[$j]}"
        done

        docker run --restart unless-stopped --name="${pylytics_projects[$i]}" -d \
        -v $wheels_container_dir:/wheels \
        $volume_str \
        -i -t -p $port:$port continuumio/anaconda3:2019.03 \
        /bin/bash -c "pip install /wheels/$wheel; pylytics_rest_server" || { echo "If Docker is already running , you can try restart-pylytics.sh to restart the server in this docker."; exit 1; }
        echo"**********Starting ${pylytics_projects[$i]}*************"
        echo "*********Showing logs PRESS CTRL+C to exit logs********"
        docker logs -f ${pylytics_projects[$i]} 
    fi
done
