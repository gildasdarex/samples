source env.txt

apt-get update
wget https://repo.percona.com/apt/percona-release_0.1-4.jessie_all.deb
dpkg -i percona-release_0.1-4.jessie_all.deb
apt-get update
apt-get install -y pmm-client

pmm-admin config --server $PMM_SERVER
pmm-admin add mysql --user $MYSQL_USER --password $MYSQL_PASSWORD --host 127.0.0.1
pmm-admin add proxysql:metrics