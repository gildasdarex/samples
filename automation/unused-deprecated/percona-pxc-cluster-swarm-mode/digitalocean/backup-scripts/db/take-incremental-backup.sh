#!/bin/bash
. config.sh

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       percona.container.id)
          percona_container_id="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       username)
          username="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       password)
          password="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       base.dir)
         base_dir="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

NEW_FOLDER="$(date +%F\-%H-%M-%S)"

rm -f backup.log

docker exec -it $percona_container_id sh -c "xtrabackup --user=$username --password=$password --backup  --incremental-basedir=$base_dir --target-dir=/mysql-backup/INC_$NEW_FOLDER" &>backup.log

if [[ $? -eq 0 ]] ; then
sentry-cli send-event -m "MySQL DB : Incremental Backup Successful : $NEW_FOLDER" --logfile backup.log
else
sentry-cli send-event -m "MySQL DB : Incremental Backup FAILED : $NEW_FOLDER" --logfile backup.log
fi 

