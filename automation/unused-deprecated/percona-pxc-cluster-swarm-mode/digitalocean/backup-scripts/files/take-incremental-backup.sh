#!/bin/bash

. config.sh

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       data)
          data="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       base.dir)
          base_dir="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ "$data" = "fs" ];then
   direc=$FS
elif [ "$data" = "properties" ]; then
   direc=$PROPERTIES
elif [ "$data" = "pylytics" ]; then
   direc=$PYLYTICS
else
   echo "Invalid Options"
   exit 1
fi

NEW_FOLDER="$(date +%F\-%H-%M-%S)"
mkdir -p $BACKUP_DIR/$data/INC-$NEW_FOLDER
rsync -avr $BACKUP_DIR/$data/$base_dir/ $BACKUP_DIR/$data/INC-$NEW_FOLDER
rsync -avr $direc/ $BACKUP_DIR/$data/INC-$NEW_FOLDER


if [[ $? -eq 0 ]] ; then
sentry-cli send-event -m "$data BACKUP : Incremental Backup Successful : $NEW_FOLDER"
else
sentry-cli send-event -m "$data BACKUP : Incremental Backup FAILED : $NEW_FOLDER"
fi 
