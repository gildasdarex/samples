#!/usr/bin/env bash
#Mounts volume to Nodes
#This DOESNOT formats the volumes because of the risk. Initial formatting should be done manually
#by attaching the volume to bootstrap node
#Volumes should have been already been attched to the node precusor to running this script.

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       do.token.file)
          do_token_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       vol.static.path.file)
          vol_static_path_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done
export do_token=$(cat "$do_token_file")
export vol_static_path=$(cat "$vol_static_path_file")

jq -c '.[]' "$nodes_file" | while read i; do {
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    image=`echo ${i} | jq '.image'`
    db_vol=`echo ${i} | jq '.db_vol'`

    name_clean=$(echo $name | tr -d '"')
    db_vol_clean=$(echo $db_vol | tr -d '"')

    echo "Mounting $db_vol_clean on to $name_clean"
    docker-machine ssh $name_clean "sudo mkdir -p $vol_static_path; \
      sudo mount -o discard,defaults /dev/disk/by-id/scsi-0DO_Volume_$db_vol_clean $vol_static_path; \
      echo /dev/disk/by-id/scsi-0DO_Volume_$db_vol_clean $vol_static_path ext4 defaults,nofail,discard 0 0 | sudo tee -a /etc/fstab; \
      mkdir -p $vol_static_path/data; \
      touch $vol_static_path/$db_vol_clean"
} < /dev/null; done
