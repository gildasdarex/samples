#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       do.token.file)
          do_token_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done


export do_token=$(cat "$do_token_file")
main_manager_name="not-defined"
main_manager_name_file_path=$HOME/main_manager_name.txt
node_list_file_path=$HOME/nodes_list.txt
nodes_list=""

jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    image=`echo ${i} | jq '.image'`


    cmd=`echo docker-machine create --driver digitalocean --digitalocean-access-token "$do_token" --digitalocean-image "$image" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-private-networking    "$name"`
    eval $cmd

    while :
    do
      get_machine_command=`echo docker-machine ls --filter name=$name --format "{{.Name}}"`
      machine=`eval $get_machine_command`
      if [ "$machine" != "$name" ]; then
        break
      fi
    done
    node=`echo ${node_type} | sed 's/\"//g'`
    node_name=`echo ${name} | sed 's/\"//g'`
    nodes_list="$node_name:$node,$nodes_list"

    touch $node_list_file_path
    > "$node_list_file_path"
    echo "$nodes_list" >> "$node_list_file_path"

    if [ "$node" == "manager" ]; then
        if [ "$main_manager_name" == "not-defined" ]; then
          main_manager_name=$name
          touch $main_manager_name_file_path
          > "$main_manager_name_file_path"
          echo "$main_manager_name" >> "$main_manager_name_file_path"
        fi;
    fi;
done

main_manager_name=$(cat "$main_manager_name_file_path")
# Initialize Swarm Manager and tokens

main_manager_name=`echo ${main_manager_name} | sed 's/\"//g'`

docker-machine ssh $main_manager_name "docker swarm init \
        --listen-addr $(docker-machine ip ${main_manager_name}) \
            --advertise-addr $(docker-machine ip ${main_manager_name})"

export worker_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token worker -q")

export manager_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token manager -q")


nodes=`cat $node_list_file_path`
nodes=`echo ${nodes} | sed 's/\"//g'`


for node in $(echo $nodes | sed "s/,/ /g")
do
    echo $node
    nodes_infos=$(echo $node | tr ":" "\n")
    for nodes_info in $nodes_infos
    do
        if [ $nodes_info == "manager" ]
        then
           node_type="manager"
        elif [  $nodes_info == "worker" ]
        then
           node_type="worker"
        else
           name=$nodes_info
        fi
    done

    if [ "$name" == "$main_manager_name" ]; then
      continue
    fi;

    if [ "$node_type" == "worker" ]; then
        token=$worker_token
    else
        token=$manager_token
    fi;

    docker-machine ssh "$name" "docker swarm join \
        --token=${token} \
            --listen-addr $(docker-machine ip ${name}) \
                --advertise-addr $(docker-machine ip ${name}) \
                    $(docker-machine ip ${main_manager_name})"
done