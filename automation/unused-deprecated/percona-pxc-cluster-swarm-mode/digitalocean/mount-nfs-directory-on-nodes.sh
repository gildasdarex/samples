#!/usr/bin/env bash

. properties/nfs-properties.sh
optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

jq -c '.[]' "$nodes_file" | while read i; do {
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    image=`echo ${i} | jq '.image'`
    name_clean=$(echo $name | tr -d '"')
    
    docker-machine ssh $name_clean "sudo apt-get --assume-yes install nfs-common; \
      sudo mkdir -p $nfs_client_mount_dir; \
      sudo mount -o noac  $nfs_server_private_ip:$nfs_shared_dir $nfs_client_mount_dir;"
} < /dev/null; done
