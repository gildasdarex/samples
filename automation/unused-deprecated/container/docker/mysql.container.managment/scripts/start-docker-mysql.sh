#!/usr/bin/env bash

help_mesage(){
  echo "Usage : start_docker_mysql command "
  echo " "
  echo "Options : "
  echo "--env     environement that will be use to create docker container(dev, prod, qa )"
  echo " "
  echo "--properties.file.location    the location of the properties files."
  echo " "
  echo "--properties.file.name   The name of the properties file. this properties file must be located in the path that you specified for <<--properties.file.location>> option"
  echo "                         You can have multiple one properties file for each environement. this script will read the properties file according to the value of <<--env>> "
  echo "                         option and must have .properties as extension"

  echo "                         example : start_docker_mysql --env dev --properties.file.location /path/to/location --properties.file.name mysql"
  echo "                         The script will read properties from file /path/to/location/mysql-dev.properties"
  echo " "
  echo " "

  echo "List of properties : "
  echo " "
  echo "mysql.version    the version of mysql to install"
  echo " "
  echo "mysql.host.publish.port    the port that will be used in your host to publish mysql service that run into the container"
  echo " "
  echo "mysql.root.password    the root user password"
  echo " "
  echo "mysql.custom.config.file.location    the location of your custom mysql config. Into this dir your need to create a file my-custom.cnf and put your mysql config here"
  echo " "
  echo "mysql.container.storage.dir    the dir that will be used by the container to store data"
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       help)
          help_mesage
          exit
          ;;
       env)
          env_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.location)
          properties_file_location_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.name)
          properties_file_name_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z "$env_value" ]
      then
        env_value="dev"
fi

if [ ! -z "$properties_file_name_value" ]
then
      if [ -z "$properties_file_location_value" ]
      then
        echo "properties.file.location option is missing"
        exit
      fi
fi

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptHelperFilesDir="$scriptDir/mysql.scripts.helpers"
#propertiesFilesDir="$parentdir/mysql.properties.files"

if [ -z "$properties_file_name_value" ]
then
      if [ -z "$properties_file_name_value" ]
      then
        mysql_version="5.7"
        mysql_host_publish_port="3306"
        mysql_root_password="root"
        mysql_custom_config_file_location="/root/container/mysql-container/conf.d"
        if [ ! -d "$mysql_custom_config_file_location" ]
        then
            mkdir -p "$mysql_custom_config_file_location"
        fi
        mysql_container_storage_dir="/storage/docker/mysql-datadir"
        if [ ! -d "$mysql_container_storage_dir" ]
        then
            mkdir -p "$mysql_container_storage_dir"
        fi
      fi
else
      properties_file_location_full_path="${properties_file_location_value}/${properties_file_name_value}-${env_value}.properties"

      mysql_version=$(${scriptHelperFilesDir}/_get-properties.sh "mysql.version" "$properties_file_location_full_path")
      mysql_host_publish_port=$(${scriptHelperFilesDir}/_get-properties.sh "mysql.host.publish.port" "$properties_file_location_full_path")
      mysql_root_password=$(${scriptHelperFilesDir}/_get-properties.sh "mysql.root.password" "$properties_file_location_full_path")
      mysql_custom_config_file_location=$(${scriptHelperFilesDir}/_get-properties.sh "mysql.custom.config.file.location" "$properties_file_location_full_path")
      mysql_container_storage_dir=$(${scriptHelperFilesDir}/_get-properties.sh "mysql.container.storage.dir" "$properties_file_location_full_path")

      if [ -z "$mysql_version" ]
      then
        mysql_version="5.7"
      fi

      if [ -z "$mysql_host_publish_port" ]
      then
        mysql_host_publish_port="3306"
      fi

      if [ -z "$mysql_root_password" ]
      then
        mysql_version="root"
      fi

      if [ -z "$mysql_custom_config_file_location" ]
      then
        mysql_custom_config_file_location="/root/container/mysql-container/conf.d"
        if [ ! -d "$mysql_custom_config_file_location" ]
        then
            mkdir -p "$mysql_custom_config_file_location"
        fi
      fi

       if [ -z "$mysql_container_storage_dir" ]
      then
        mysql_container_storage_dir="/storage/docker/mysql-datadir"
        if [ ! -d "$mysql_container_storage_dir" ]
        then
            mkdir -p "$mysql_container_storage_dir"
        fi
      fi
fi




container_name="mysql-container-${env_value}-${mysql_version}"

cid_exited=$(docker ps -q -f status=exited -f name=^/${container_name}$)

if [ ! "${cid_exited}" ]; then
  docker run --detach --name="${container_name}" --env="MYSQL_ROOT_PASSWORD=${mysql_root_password}" --publish "${mysql_host_publish_port}":3306 --volume="${mysql_custom_config_file_location}":/etc/mysql/conf.d --volume="${mysql_container_storage_dir}":/var/lib/mysql mysql:"${mysql_version}" --restart unless-stopped
else
  docker start $container_name
fi


cid_running=$(docker ps -q -f status=running -f name=^/${container_name}$)

if [ ! "${cid_running}" ]; then
  echo "mysql docker container failed to start for env = ${env_value} and for version = ${mysql_version}"
else
  echo "mysql docker container started for env = ${env_value} and for version = ${mysql_version}"
  echo "mysql version = ${mysql_version}"
  echo "mysql port = ${mysql_host_publish_port}"
  echo "mysql root passord = ${mysql_root_password}"
  echo "mysql config location = ${mysql_custom_config_file_location}"
  echo "mysql data storage dir = ${mysql_container_storage_dir}"
fi
