#!/usr/bin/env bash

property=$1
property_file=$2

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ "$line" == $property* ]]
    then
      property_value=$(cut -d "=" -f 2 <<< "$line")
      echo -e $property_value
      exit
    fi
done < "$property_file"