#!/usr/bin/env bash


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       env)
          env_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.location)
          properties_file_location_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.name)
          properties_file_name_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       influxdb.ip)
          influxdb_ip="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       influxdb.port)
          influxdb_port="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z "$env_value" ]
      then
        env_value="dev"
fi

if [ -z "$influxdb_ip" ]
      then
       echo "influxdb ip option is missing"
fi

if [ -z "$influxdb_port" ]
      then
       echo "influxdb port option is missing"
fi

if [ ! -z "$properties_file_name_value" ]
then
      if [ -z "$properties_file_location_value" ]
      then
        echo "properties.file.location option is missing"
        exit
      fi
fi

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptHelperFilesDir="$scriptDir/chronograf.scripts.helpers"

if [ -z "$properties_file_name_value" ]
then
      if [ -z "$properties_file_name_value" ]
      then
        chronograf_version="1.3"

        chronograf_host_http_api_port="8888"

        chronograf_container_http_api_port="8888"


        chronograf_container_storage_dir="/storage/docker/dev/chronograf-datadir"
        if [ ! -d "$chronograf_container_storage_dir" ]
        then
            mkdir -p "$chronograf_container_storage_dir"
        fi
      fi
else

        properties_file_location_full_path="${properties_file_location_value}/${properties_file_name_value}-${env_value}.properties"

        chronograf_version=$(${scriptHelperFilesDir}/_get-properties.sh "chronograf.version" "$properties_file_location_full_path")

        chronograf_host_http_api_port=$(${scriptHelperFilesDir}/_get-properties.sh "chronograf.host.http.api.port" "$properties_file_location_full_path")

        chronograf_container_http_api_port=$(${scriptHelperFilesDir}/_get-properties.sh "chronograf.container.http.api.port" "$properties_file_location_full_path")

        chronograf_container_storage_dir=$(${scriptHelperFilesDir}/_get-properties.sh "chronograf.container.storage.dir" "$properties_file_location_full_path")

        if [ -z "$chronograf_version" ]
        then
          chronograf_version="1.3"
        fi

        if [ -z "$chronograf_host_http_api_port" ]
        then
          chronograf_host_http_api_port="8888"
        fi

        if [ -z "$chronograf_container_http_api_port" ]
        then
          chronograf_container_http_api_port="8888"
        fi


        if [ -z "$chronograf_container_storage_dir" ]
        then
          chronograf_container_storage_dir="/storage/docker/dev/chronograf-datadir"
          if [ ! -d "$chronograf_container_storage_dir" ]
           then
            mkdir -p "$chronograf_container_storage_dir"
          fi
        fi
fi


container_name="chronograf-container-${env_value}-${chronograf_version}"

cid_exited=$(docker ps -q -f status=exited -f name=^/${container_name}$)

if [ ! "${cid_exited}" ]; then
  docker run --detach --name="${container_name}" --publish "${chronograf_host_http_api_port}":"${chronograf_container_http_api_port}"  -v "${chronograf_container_storage_dir}":/var/lib/chronograf  chronograf:"${chronograf_version}" --influxdb-url=http://"$influxdb_ip":"$influxdb_port" --restart unless-stopped
else
  docker start $container_name
fi


cid_running=$(docker ps -q -f status=running -f name=^/${container_name}$)

if [ ! "${cid_running}" ]; then
  echo "chronograf docker container failed to start for env = ${env_value} and for version = ${chronograf_version}"
else
  echo "chronograf docker container started for env = ${env_value} and for version = ${chronograf_version}"
fi
