#!/usr/bin/env bash
apt-get update
apt-get install curl
apt-get install unzip
apt-get install zip
sudo apt-get install jq
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
apt-get install git
sdk install java
sdk install gradle
sdk install grails