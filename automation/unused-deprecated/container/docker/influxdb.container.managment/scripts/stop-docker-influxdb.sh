#!/usr/bin/env bash

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       env)
          env_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.location)
          properties_file_location_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.name)
          properties_file_name_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptHelperFilesDir="$scriptDir/influxdb.scripts.helpers"

if [ -z "$properties_file_name_value" ]
then
      if [ -z "$properties_file_name_value" ]
      then
        influxdb_version="1.3"

        influxdb_host_http_api_port="8086"
        influxdb_host_administrator_interface_port="8083"
        influxdb_host_graphite_support_port="2003"

        influxdb_container_http_api_port="8086"
        influxdb_container_administrator_interface_port="8083"
        influxdb_container_graphite_support_port="2003"

        influxdb_custom_config_file_location="/root/container/influxdb-container/dev"
        if [ ! -d "$influxdb_custom_config_file_location" ]
        then
            mkdir -p "$mysql_custom_config_file_location"
        fi
        influxdb_container_storage_dir="/storage/docker/dev/influxdb-datadir"
        if [ ! -d "$influxdb_container_storage_dir" ]
        then
            mkdir -p "$influxdb_container_storage_dir"
        fi
      fi
else

        properties_file_location_full_path="${properties_file_location_value}/${properties_file_name_value}-${env_value}.properties"

        influxdb_version=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.version" "$properties_file_location_full_path")

        influxdb_host_http_api_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.host.http.api.port" "$properties_file_location_full_path")
        influxdb_host_administrator_interface_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.host.administrator.interface.port" "$properties_file_location_full_path")
        influxdb_host_graphite_support_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.host.graphite.support.port" "$properties_file_location_full_path")

        influxdb_container_http_api_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.container.http.api.port" "$properties_file_location_full_path")
        influxdb_container_administrator_interface_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.container.administrator.interface.port" "$properties_file_location_full_path")
        influxdb_container_graphite_support_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.container.graphite.support.port" "$properties_file_location_full_path")

        influxdb_custom_config_file_location=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.custom.config.file.location" "$properties_file_location_full_path")
        influxdb_container_storage_dir=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.container.storage.dir" "$properties_file_location_full_path")

        if [ -z "$influxdb_version" ]
        then
          influxdb_version="1.3"
        fi

        if [ -z "$influxdb_host_http_api_port" ]
        then
          influxdb_host_http_api_port="8086"
        fi

        if [ -z "$influxdb_host_administrator_interface_port" ]
        then
          influxdb_host_administrator_interface_port="8083"
        fi


        if [ -z "$influxdb_host_graphite_support_port" ]
        then
          influxdb_host_graphite_support_port="2003"
        fi


        if [ -z "$influxdb_container_http_api_port" ]
        then
          influxdb_container_http_api_port="8086"
        fi

        if [ -z "$influxdb_container_administrator_interface_port" ]
        then
          influxdb_container_administrator_interface_port="8083"
        fi

        if [ -z "$influxdb_container_graphite_support_port" ]
        then
          influxdb_container_graphite_support_port="2003"
        fi

        if [ -z "$influxdb_custom_config_file_location" ]
        then
          influxdb_custom_config_file_location="/root/container/influxdb-container/dev/influxdb.conf"
          if [ ! -d "/root/container/influxdb-container/dev/" ]
          then
            mkdir -p "/root/container/influxdb-container/dev/"
          fi
        fi

        if [ -z "$influxdb_container_storage_dir" ]
        then
          influxdb_container_storage_dir="/storage/docker/dev/influxdb-datadir"
          if [ ! -d "$influxdb_container_storage_dir" ]
           then
            mkdir -p "$influxdb_container_storage_dir"
          fi
        fi
fi


container_name="influxdb-container-${env_value}-${mysql_version}"

cid_running=$(docker ps -q -f status=running -f name=^/${container_name}$)

if [ ! "${cid_running}" ]; then
  echo "No container is running for env = ${env_value} and for influxdb version = ${influxdb_version}"
else
  docker stop $container_name
fi