#!/usr/bin/env bash

help_mesage(){
  echo "Usage : start_docker_influxdb command "
  echo " "
  echo "Options : "
  echo "--env     environement that will be use to create docker container(dev, prod, qa )"
  echo " "
  echo "--properties.file.location    the location of the properties files."
  echo " "
  echo "--properties.file.name   The name of the properties file. this properties file must be located in the path that you specified for <<--properties.file.location>> option"
  echo "                         You can have multiple one properties file for each environement. this script will read the properties file according to the value of <<--env>> "
  echo "                         option and must have .properties as extension"

  echo "                         example : start_docker_influxdb --env dev --properties.file.location /path/to/location --properties.file.name influxdb"
  echo "                         The script will read properties from file /path/to/location/mysql-influxdb.properties"
  echo " "
  echo " "

  echo "List of properties : "
  echo " "
  echo "influxdb.version    the version of influxdb to install"
  echo " "
  echo "influxdb.container.http.api.port   the port that will be used in your container for api request. Specify this value only if you modify it in your custom config file"
  echo " "
  echo "influxdb.container.administrator.interface.port    the port that will be used in your container for admin interface. Specify this value only if you modify it in your custom config file"
  echo " "
  echo "influxdb.container.graphite.support.port    the port that will be used in your container for graphite. Specify this value only if you modify it in your custom config file"
  echo " "
  echo "influxdb.host.http.api.port   the port that will be used in your container to publish influxdb api service that run into the container"
  echo " "
  echo "influxdb.host.administrator.interface.port     the port that will be used in your container to publish influxdb admin interface that run into the container"
  echo " "
  echo "influxdb.host.graphite.support.port    the port that will be used in your container to publish graphite service that run into the container"
  echo " "
  echo "influxdb.custom.config.file.location    the location of your custom influxdb config. Into this dir your need to create a file influxdb.conf and put your influxdb config here"
  echo " "
  echo "influxdb.container.storage.dir    the dir that will be used by the container to store data"
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       help)
          help_mesage
          exit
          ;;
       env)
          env_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.location)
          properties_file_location_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.name)
          properties_file_name_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z "$env_value" ]
      then
        env_value="dev"
fi

if [ ! -z "$properties_file_name_value" ]
then
      if [ -z "$properties_file_location_value" ]
      then
        echo "properties.file.location option is missing"
        exit
      fi
fi

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptHelperFilesDir="$scriptDir/influxdb.scripts.helpers"

if [ -z "$properties_file_name_value" ]
then
      if [ -z "$properties_file_name_value" ]
      then
        influxdb_version="1.3"

        influxdb_host_http_api_port="8086"
        influxdb_host_administrator_interface_port="8083"
        influxdb_host_graphite_support_port="2003"

        influxdb_container_http_api_port="8086"
        influxdb_container_administrator_interface_port="8083"
        influxdb_container_graphite_support_port="2003"

        influxdb_custom_config_file_location="/root/container/influxdb-container/dev"
        if [ ! -d "$influxdb_custom_config_file_location" ]
        then
            mkdir -p "$mysql_custom_config_file_location"
        fi
        influxdb_container_storage_dir="/storage/docker/dev/influxdb-datadir"
        if [ ! -d "$influxdb_container_storage_dir" ]
        then
            mkdir -p "$influxdb_container_storage_dir"
        fi
      fi
else

        properties_file_location_full_path="${properties_file_location_value}/${properties_file_name_value}-${env_value}.properties"

        influxdb_version=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.version" "$properties_file_location_full_path")

        influxdb_host_http_api_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.host.http.api.port" "$properties_file_location_full_path")
        influxdb_host_administrator_interface_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.host.administrator.interface.port" "$properties_file_location_full_path")
        influxdb_host_graphite_support_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.host.graphite.support.port" "$properties_file_location_full_path")

        influxdb_container_http_api_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.container.http.api.port" "$properties_file_location_full_path")
        influxdb_container_administrator_interface_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.container.administrator.interface.port" "$properties_file_location_full_path")
        influxdb_container_graphite_support_port=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.container.graphite.support.port" "$properties_file_location_full_path")

        influxdb_custom_config_file_location=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.custom.config.file.location" "$properties_file_location_full_path")
        influxdb_container_storage_dir=$(${scriptHelperFilesDir}/_get-properties.sh "influxdb.container.storage.dir" "$properties_file_location_full_path")

        if [ -z "$influxdb_version" ]
        then
          influxdb_version="1.3"
        fi

        if [ -z "$influxdb_host_http_api_port" ]
        then
          influxdb_host_http_api_port="8086"
        fi

        if [ -z "$influxdb_host_administrator_interface_port" ]
        then
          influxdb_host_administrator_interface_port="8083"
        fi


        if [ -z "$influxdb_host_graphite_support_port" ]
        then
          influxdb_host_graphite_support_port="2003"
        fi


        if [ -z "$influxdb_container_http_api_port" ]
        then
          influxdb_container_http_api_port="8086"
        fi

        if [ -z "$influxdb_container_administrator_interface_port" ]
        then
          influxdb_container_administrator_interface_port="8083"
        fi

        if [ -z "$influxdb_container_graphite_support_port" ]
        then
          influxdb_container_graphite_support_port="2003"
        fi

        if [ -z "$influxdb_custom_config_file_location" ]
        then
          influxdb_custom_config_file_location="/root/container/influxdb-container/dev/influxdb.conf"
          if [ ! -d "/root/container/influxdb-container/dev/" ]
          then
            mkdir -p "/root/container/influxdb-container/dev/"
          fi
        fi

        if [ -z "$influxdb_container_storage_dir" ]
        then
          influxdb_container_storage_dir="/storage/docker/dev/influxdb-datadir"
          if [ ! -d "$influxdb_container_storage_dir" ]
           then
            mkdir -p "$influxdb_container_storage_dir"
          fi
        fi
fi


container_name="influxdb-container-${env_value}-${influxdb_version}"

cid_exited=$(docker ps -q -f status=exited -f name=^/${container_name}$)

if [ ! "${cid_exited}" ]; then
  docker run --detach --name="${container_name}" --publish "${influxdb_host_http_api_port}":"${influxdb_container_http_api_port}" --publish "${influxdb_host_administrator_interface_port}":"${influxdb_container_administrator_interface_port}" --publish "${influxdb_host_graphite_support_port}":"${influxdb_container_graphite_support_port}" -v "${influxdb_custom_config_file_location}":/etc/influxdb/influxdb.conf:ro -v "${influxdb_container_storage_dir}":/var/lib/influxdb influxdb:"${influxdb_version}" -config /etc/influxdb/influxdb.conf --restart unless-stopped
else
  docker start $container_name
fi


cid_running=$(docker ps -q -f status=running -f name=^/${container_name}$)

if [ ! "${cid_running}" ]; then
  echo "influxdb docker container failed to start for env = ${env_value} and for version = ${influxdb_version}"
else
  echo "influxdb docker container started for env = ${env_value} and for version = ${influxdb_version}"
fi
