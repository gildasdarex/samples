#!/usr/bin/env bash

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       env)
          env_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.location)
          properties_file_location_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       volumes.file.path)
          volumes_file_path="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.name)
          properties_file_name_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptHelperFilesDir="$scriptDir/embed.tomcat.scripts.helpers"
#propertiesFilesDir="$parentdir/mysql.properties.files"

properties_file_location_full_path="${properties_file_location_value}/${properties_file_name_value}-${env_value}.properties"

app_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.name" "$properties_file_location_full_path")
app_version=$(${scriptHelperFilesDir}/_get-properties.sh "app.version" "$properties_file_location_full_path")
app_war_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.war.name" "$properties_file_location_full_path")
app_docker_image_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.docker.image.name" "$properties_file_location_full_path")
app_spring_config_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.spring.config.name" "$properties_file_location_full_path")
app_spring_config_location=$(${scriptHelperFilesDir}/_get-properties.sh "app.spring.config.location" "$properties_file_location_full_path")
app_logs_file_path=$(${scriptHelperFilesDir}/_get-properties.sh "app.logs.file.location" "$properties_file_location_full_path")
log4j2_file_path=$(${scriptHelperFilesDir}/_get-properties.sh "app.log4j2.file.location" "$properties_file_location_full_path")
sentry_file_path=$(${scriptHelperFilesDir}/_get-properties.sh "app.sentry.file.location" "$properties_file_location_full_path")
app_logs_file_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.logs.file.name" "$properties_file_location_full_path")
log4j2_file_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.log4j2.file.name" "$properties_file_location_full_path")
sentry_file_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.sentry.file.name" "$properties_file_location_full_path")


logs_volume="/development/logs"
log4j2_volume="/development/log4j2"
sentry_volume="/development/sentry"
logs_file_in_container="$logs_volume/$app_logs_file_name"
log4j2_file_in_container="$log4j2_volume/$log4j2_file_name"
sentry_file_in_container="$sentry_volume/$sentry_file_name"
logs_file_in_host="$app_logs_file_path/$app_logs_file_name"
log4j2_file_in_host="$log4j2_file_path/$log4j2_file_name"
sentry_file_in_host="$sentry_file_path/$sentry_file_name"
volume_in_container_properties="/development/properties"
volume_in_container_spring_config="/development/asklytics/config/spring"
volume_in_container_logs="/development/logs"



app_spring_properties_file="$app_spring_config_location/$app_spring_config_name.properties"
active_profile=$(${scriptHelperFilesDir}/_get-properties.sh "spring.profiles.active" "$app_spring_properties_file")
app_spring_profile_properties_file="$app_spring_config_location/$app_spring_config_name-$active_profile.properties"
port=$(${scriptHelperFilesDir}/_get-properties.sh "server.port" "$app_spring_profile_properties_file")

volume=""
volumes_json_config=`cat volumes_file_path`
jq -c '.[]' "$volumes_json_config" | while read i; do
    hostPath=`echo ${i} | jq '.hostPath'`
    containerPath=`echo ${i} | jq '.containerPath'`
    volume="$volume -v $hostPath:$containePath"
done

container_name="docker-$app_name-$app_version"
cid_exited=$(docker ps -q -f status=exited -f name=^/${container_name}$)

if [ ! "${cid_exited}" ]; then
  #docker run --detach --env SPRING_CONFIG_LOCATION="$volume_in_container_spring_config/" --env SPRING_CONFIG_NAME="$app_spring_config_name" --env SPRING_APP_LOG_PATH="$volume_in_container_logs" -p "${port}":"${port}" -v "$app_spring_config_location:$volume_in_container_spring_config" -v "$app_logs_file_path:$volume_in_container_logs"  -v "$app_fs_location:$volume_in_container_fs" -v "$app_properties_location:$volume_in_container_properties" --name "$container_name" "${app_docker_image_name}"
  docker run --detach --env SENTRY_FILE_PATH="$sentry_file_in_container"
                      --env LOGGING_FILE_PATH="$log4j2_file_in_container"
                      --env SPRING_CONFIG_LOCATION="$volume_in_container_spring_config/"
                      --env SPRING_CONFIG_NAME="$app_spring_config_name"
                      --env SPRING_APP_LOG_PATH="$logs_file_in_container"
                      -p "${port}":"${port}"
                      -v "$app_spring_config_location:$volume_in_container_spring_config"
                      -v "$logs_file_in_host:$logs_file_in_container"
                      -v "$log4j2_file_in_host:$log4j2_file_in_container"
                      -v "$sentry_file_in_host:$sentry_file_in_container"
                      "$volume"
                      --name "$container_name"
                      "${app_docker_image_name}"
else
  docker start $container_name
fi


cid_running=$(docker ps -q -f status=running -f name=^/${container_name}$)

if [ ! "${cid_running}" ]; then
  echo "$app_name docker container failed to start for env = ${env_value} and for version = ${app_version}"
else
  echo "$app_name docker container started for env = ${env_value} and for version = ${app_version}"
fi

