#!/usr/bin/env bash

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       env)
          env_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.location)
          properties_file_location_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       properties.file.name)
          properties_file_name_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptHelperFilesDir="$scriptDir/embed.tomcat.scripts.helpers"

properties_file_location_full_path="${properties_file_location_value}/${properties_file_name_value}-${env_value}.properties"

#echo $env_value
#echo $properties_file_location_full_path

app_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.name" "$properties_file_location_full_path")
app_version=$(${scriptHelperFilesDir}/_get-properties.sh "app.version" "$properties_file_location_full_path")
app_war_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.war.name" "$properties_file_location_full_path")
#app_war_path=$(${scriptHelperFilesDir}/_get-properties.sh "app.war.path" "$properties_file_location_full_path")
app_docker_image_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.docker.image.name" "$properties_file_location_full_path")
app_spring_config_name=$(${scriptHelperFilesDir}/_get-properties.sh "app.spring.config.name" "$properties_file_location_full_path")
app_spring_config_location=$(${scriptHelperFilesDir}/_get-properties.sh "app.spring.config.location" "$properties_file_location_full_path")
#app_spring_server_port=$(${scriptHelperFilesDir}/_get-properties.sh "app.spring.server.port" "$properties_file_location_full_path")
app_fs_location=$(${scriptHelperFilesDir}/_get-properties.sh "app.fs.location" "$properties_file_location_full_path")
app_properties_location=$(${scriptHelperFilesDir}/_get-properties.sh "app.properties.location" "$properties_file_location_full_path")


parentdir="$(dirname "$scriptDir")"

cd $parentdir

docker build -t  "$app_docker_image_name" --build-arg WAR_NAME="$app_war_name" .