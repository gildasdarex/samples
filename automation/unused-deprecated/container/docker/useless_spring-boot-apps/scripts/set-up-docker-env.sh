#!/usr/bin/env bash
apt-get update
apt-get install -y curl
apt-get install -y unzip
apt-get install -y zip
apt-get install -y jq
apt-get install -y git
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk install java
sdk install gradle