#!/usr/bin/env bash

#init all parameters
repository_dir="/development/repos/asklytics"
#username=${username}
#password=${password}
#app=${app}

#echo ${username}
#echo ${password}
#echo ${app}

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
#parentdir="$(dirname "$scriptDir")"
scriptHelperFilesDir="$scriptDir/scripts-helpers"
propertiesFilesDir="$scriptDir/properties-files"

apps_analytics_list_properties="$propertiesFilesDir/apps-analytics-list.properties"

#echo ${scriptDir}
#echo ${parentdir}
#echo ${scriptHelperFilesDir}
#echo ${propertiesFilesDir}





if [ "$#" -eq 1 ]
   then
   branch=$1
else
   branch="dev"
fi

#gradle cache clean
rm -rf $HOME/.gradle/caches/


#remove local-maven-repo directory if it exists
if [ -d "$repository_dir/local-maven-repo" ]
then
    rm -R "$repository_dir/local-maven-repo"
fi

#create an empty local-maven-repo directory
mkdir -p "$repository_dir/local-maven-repo"
chmod -R 777 "$repository_dir/local-maven-repo"

clone_project_with_ssh () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone ssh://git@asklytics.repositoryhosting.com/asklytics/$1.git -b "$branch"
}

clone_project_with_https () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone https://${username}:${password}@asklytics.repositoryhosting.com/git/asklytics/$1.git -b "$branch"
}

build_project () {
    # $1 parameter is the name of the project
    cd "$repository_dir/$1"

    echo "------------------------------ clean project $1 ----------------------------------------"
    gradle clean
    echo "------------------------------ build project $1  ----------------------------------------"
    gradle build
    echo "------------------------------ publish project $1 ----------------------------------------"
    gradle publish
}

appForLevel1=$(${scriptHelperFilesDir}/_get-asklytics-apps-by-level.sh 1 $apps_analytics_list_properties)
#appForLevel1Array=(commons infra-security infra-rest-client email-send-service modeler extract-aws)
appForLevel1Array=($appForLevel1)

for appForLevel in ${appForLevel1Array[@]}
    do
    clone_project_with_https $appForLevel
done

clone_project_with_https ${app}



#Step 4: download jars that need to be in  local-flat-repo
if [ ! -d "$repository_dir/local-flat-repo" ]
then
    mkdir -p "$repository_dir/local-flat-repo"
    cd "$repository_dir/local-flat-repo"
    wget --http-user="${username}" --http-password="${password}" https://asklytics.repositoryhosting.com/webdav/asklytics_analytics-service/com.asklytics.matlab.algos-1.0.04.jar
    wget --http-user="${username}" --http-password="${password}" https://asklytics.repositoryhosting.com/webdav/asklytics_analytics-service/javabuilder-Matlab-2016a.jar
fi



for appForLevel in ${appForLevel1Array[@]}
    do
    build_project $appForLevel
done

build_project $app