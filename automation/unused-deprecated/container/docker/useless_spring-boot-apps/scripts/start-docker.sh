#!/usr/bin/env bash


docker_image=$6
volume_fs=$1
volume_properties=$2
volume_in_container_fs="/development/fs"
volume_in_container_properties="/development/properties"

username=$3
password=$4
app=$5

#spring_properties_files="/development/properties/spring"

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
scriptHelperFilesDir="$scriptDir/scripts-helpers"
propertiesFilesDir="$scriptDir/properties-files"

apps_analytics_spring_config_name_properties="$propertiesFilesDir/apps-analytics-spring-config-name.properties"
apps_artefact_type_path_properties="$propertiesFilesDir/apps-artefact-type-path.properties"
apps_publish_names_properties="$propertiesFilesDir/apps-publish-names.properties"



app_spring_config_name=$(${scriptHelperFilesDir}/_get-project-spring-config-name.sh $app "$apps_analytics_spring_config_name_properties")
app_spring_properties_file="$volume_properties/spring/$app_spring_config_name.properties"

active_profile=$(${scriptHelperFilesDir}/_get-project-active-profile.sh "spring.profiles.active" "$app_spring_properties_file")

app_spring_profile_properties_file="$volume_properties/spring/$app_spring_config_name-$active_profile.properties"

port=$(${scriptHelperFilesDir}/_get-project-port.sh "server.port" "$app_spring_profile_properties_file")

echo $port

docker run -it --env username="$username" --env app="$app" --env password="$password" -p "${port}":"${port}" -v "$volume_fs:$volume_in_container_fs"  -v "$volume_properties:$volume_in_container_properties"  --name "docker-$app" "$docker_image"