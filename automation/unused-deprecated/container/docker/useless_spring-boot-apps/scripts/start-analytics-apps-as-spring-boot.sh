#!/usr/bin/env bash

#init all parameters
repository_dir="/development/repos/asklytics"
#app=$2

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
scriptHelperFilesDir="$scriptDir/scripts-helpers"
propertiesFilesDir="$scriptDir/properties-files"

apps_analytics_spring_config_name_properties="$propertiesFilesDir/apps-analytics-spring-config-name.properties"
apps_artefact_type_path_properties="$propertiesFilesDir/apps-artefact-type-path.properties"
apps_publish_names_properties="$propertiesFilesDir/apps-publish-names.properties"

#mkdir -p /development/properties/spring

workdir="/development/properties/spring"

#cp "$propertiesFilesDir/spring-apps-properties-files"/*  "$workdir/."

appPublishName=$(${scriptHelperFilesDir}/_get-project-module-publish-name.sh ${app} $apps_publish_names_properties)

publishNameRepoPath="${repository_dir}/local-maven-repo/com/asklytics/$appPublishName"

appArtefactPath=$(${scriptHelperFilesDir}/_get-project-artefact-path.sh ${app} "$apps_artefact_type_path_properties")
publishNameRepoFullPath="$publishNameRepoPath/$appArtefactPath/"

app_spring_config_name=$(${scriptHelperFilesDir}/_get-project-spring-config-name.sh ${app} "$apps_analytics_spring_config_name_properties")

warFullPath=$(find "$publishNameRepoFullPath" -type f -name "*.war")
replace=""
warName=$(echo ${warFullPath/$publishNameRepoFullPath/$replace})

cd $publishNameRepoFullPath

java -jar "$warName" --spring.config.location="$workdir/" --spring.config.name="$app_spring_config_name"
