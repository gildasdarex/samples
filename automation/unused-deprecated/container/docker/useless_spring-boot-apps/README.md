Install the docker image
 image name = asklytics-app-docker
 
 - clone or pull automation project
 - go to docker/spring-boot-apps inside automation folder
 - run docker build -t  asklytics-app-docker .


1 - run start-docker.sh script

  this script run docker container to start a spring boot app. This script take as parameters:
   
   - volume_fs
   
   this folder is the repository where analysis result and others is stored 
   in the host(no in docker container). this folder contain the following dirs:
       -analysis-results
       -was
       -collectd
       -MLAAS
       -STATUS
       
   volume_fs will be mounted in container
   
   - volume_properties
   this foledr is the repository that contains :
      - spring folder(location where spring properties files are located)
      - data-retension-policy.properties
      - email-validator.properties
      - spring-data-access.properties
      - rest-app-url-location.properties
      - .....
      
      
   - username   
   This is your username for asklytics repository
   
   - password
   this is your password for asklytics repository
   
   - app
   this is the app that you want to run in docker container(infra-service, ui-data-service)
   
   - docker image name
   this is the mane that you provided durimg image creation(asklytics-app-docker)
      