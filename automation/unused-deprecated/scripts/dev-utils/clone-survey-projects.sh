#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for dev-utils folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to dev-utils folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters
repository_dir=$1
username=$2
password=$3

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
releaseHelperDir="$parentdir/release-builds/release-helpers"
propertiesFilesDir="$parentdir/properties-files"


if [ "$#" -eq 7 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties=$7
elif [ "$#" -eq 6 ]
   then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties=$6
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 5 ]; then
   branch=$4
   projects_properties=$5
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 4 ]; then
   branch=$4
   projects_properties="$propertiesFilesDir/project-set-survey-app.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
elif [ "$#" -eq 3 ]; then
  projects_properties="$propertiesFilesDir/project-set-survey-app.properties"
   projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
   projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
   branch="dev"
else
   echo "invalid number of argument."
   exit
fi


#gradle cache clean
rm -rf $HOME/.gradle/caches/


#remove local-maven-repo directory if it exists
if [ -d "$repository_dir/local-maven-repo" ]
then
    rm -R "$repository_dir/local-maven-repo"
fi

#create an empty local-maven-repo directory
sudo mkdir -p "$repository_dir/local-maven-repo"
sudo chmod -R 777 "$repository_dir/local-maven-repo"

clone_project_with_ssh () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone ssh://git@asklytics.repositoryhosting.com/asklytics/$1.git -b "$branch"
}

clone_project_with_https () {
    # $1 parameter is the name of the project to clone
    cd "$repository_dir"
    echo "------------------------------ clone  $1 project----------------------------------------"
    git clone https://asklytics.repositoryhosting.com/git/asklytics/$1.git -b "$branch"
}

projectTypeList=$(${releaseHelperDir}/_get-projects-type-name.sh $projects_properties)
projectTypeListAsArray=($projectTypeList)

# Step 3: clone all projects (from $branch as input; default to dev)
echo " Did you configure ssh? "
read -p "yes or no ? " sshYn

for projectType in ${projectTypeListAsArray[@]}
do
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)

   for app in ${appsArray[@]}
   do
      if [ ! -d "$repository_dir/$app" ]; then
          if [[ "${sshYn}" == "yes" ]]
            then
              clone_project_with_ssh "$app"
          else
              clone_project_with_https "$app"
          fi
      fi
   done
done