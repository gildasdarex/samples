#!/bin/bash


usage () {
    echo ""
    echo " Use this script to create nfs server in digitalocean . To use this script, please run this command "
    echo " ./init-service-network.sh  --cluster.name name_of_your_cluster  "
    echo ""
    echo " --cluster.name name of the cluster   "
}

print_option_values() {
    echo ""
    echo " Options values sent to script are : "
    echo " cluster.name = $cluster_name "
}



check_required_options_and_env_variable() {
    echo ""
    echo " Check required options "

    if [ -z ${cluster_name+x} ]; then
     echo "cluster.name is missing "
     exit
    fi

    if [ ! -f $asklytics_servers_file_path ]
    then
        echo "file $asklytics_servers_file_path not found"
        exit 1
    fi

}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

echo "                 THE SCRIPT TO CREATE DOCKER SWARM NETWORK  STARTED            "

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


main_manager=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`

main_manager=`echo ${main_manager} | sed 's/\"//g'`

docker-machine ssh  $main_manager "docker network create -d overlay demo-asklytics-net" </dev/null

echo "                 LIST ALL NETWORKS           "
docker-machine ssh  $main_manager "docker network ls" </dev/null



echo "                 THE SCRIPT TO CREATE DOCKER SWARM NETWORK  ENDED           "
