#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""


mount_on_nfs_server () {
    echo ""
    echo "   START MOUNT  NFS CLIENT($1)  NFS SERVER($2)  NFS SERVER DIRECTORY($3)        "


    nfs_client_name=`echo ${1} | sed 's/\"//g'`
    nfs_server_name=`echo ${2} | sed 's/\"//g'`
    nfs_server_directory=`echo ${3} | sed 's/\"//g'`

    # remove quotes from the name
    nfs_server_name=`echo ${nfs_server_name} | sed 's/\"//g'`
    nfs_server_directory=`echo ${nfs_server_directory} | sed 's/\"//g'`
    nfs_client_name=`echo ${nfs_client_name} | sed 's/\"//g'`

    # get private ip of nfs server nfs_server_name from $asklytics_servers_file_path
    nfs_server_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_server_name "$nfs_server_name" '.servers | .[] | select(.droplet_name == $nfs_server_name) | .droplet_private_ip'`
    # get private ip of nfs server nfs client from $asklytics_servers_file_path
    nfs_client_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_client_name "$nfs_client_name" '.servers | .[] | select(.droplet_name == $nfs_client_name) | .droplet_private_ip'`

    nfs_server_private_ip=`echo ${nfs_server_private_ip} | sed 's/\"//g'`
    nfs_client_private_ip=`echo ${nfs_client_private_ip} | sed 's/\"//g'`

    echo "   PRIVATE IP OF NFS SERVER IS   $nfs_server_private_ip     "
    echo "   PRIVATE IP OF NFS CLIENT IS   $nfs_client_private_ip     "


    #fcheck if we have already one entry for $nfs_server_directory in the /etc/exports for $nfs_server_name
    found_mount_dir=`docker-machine ssh $nfs_server_name "sudo grep $nfs_server_directory /etc/exports" </dev/null`
    #found_mount_dir=`docker-machine ssh $nfs_server_name "sudo cat /etc/exports"`

    etc_export_line="  $nfs_client_private_ip(rw,sync,no_root_squash,no_subtree_check)"

    echo "  CREATE DIRECTORY($nfs_server_directory) on  NFS SERVER($nfs_server_name)      "
    docker-machine ssh $nfs_server_name "sudo mkdir -p $nfs_server_directory" </dev/null

     if test -z "$found_mount_dir"  ; then
       echo "    NO ENTRY IN /etc/exports FILE ON NFS SERVER($nfs_server_name) FOR DIRECTORY($nfs_server_directory)    "
       echo "    ADD LINE TO MOUNT DIRECTORY($nfs_server_directory) ON NFS SERVER($nfs_server_name) WITH NFS CLIENT($nfs_client_name)     "
       docker-machine ssh $nfs_server_name "sudo echo \"$nfs_server_directory $etc_export_line\" >> /etc/exports" </dev/null
     else
        echo "     ENTRY FOUND IN /etc/exports FILE ON NFS SERVER($nfs_server_name) FOR DIRECTORY($nfs_server_directory)    "
        echo "    UPDATE LINE TO MOUNT DIRECTORY($nfs_server_directory) ON NFS SERVER($nfs_server_name) WITH NFS CLIENT($nfs_client_name)     "
        nfs_server_directory_esc=$(echo "$nfs_server_directory" | sed 's/\//\\\//g')
        #https://askubuntu.com/questions/537967/appending-to-end-of-a-line-using-sed
        docker-machine ssh $nfs_server_name "sed \"/$nfs_server_directory_esc/ s/$/$etc_export_line/\" /etc/exports | sudo tee /etc/exports" </dev/null
     fi

    docker-machine ssh $nfs_server_name "sudo systemctl restart nfs-kernel-server" </dev/null

    echo "   END MOUNT  NFS CLIENT($1)  NFS SERVER($2)  NFS SERVER DIRECTORY($3)        "
}


mount_on_nfs_client () {
    echo ""
    echo "   START MOUNT  NFS CLIENT($1)  NFS SERVER($2)  NFS SERVER DIRECTORY($3)  NFS CLIENT DIRECTORY($4)      "

    nfs_client_name=`echo ${1} | sed 's/\"//g'`
    nfs_server_name=`echo ${2} | sed 's/\"//g'`
    nfs_server_directory=`echo ${3} | sed 's/\"//g'`
    nfs_client_directory=`echo ${4} | sed 's/\"//g'`

    nfs_server_name=`echo ${nfs_server_name} | sed 's/\"//g'`
    nfs_server_directory=`echo ${nfs_server_directory} | sed 's/\"//g'`
    nfs_client_name=`echo ${nfs_client_name} | sed 's/\"//g'`
    nfs_client_directory=`echo ${nfs_client_directory} | sed 's/\"//g'`

    nfs_server_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_server_name "$nfs_server_name" '.servers | .[] | select(.droplet_name == $nfs_server_name) | .droplet_private_ip'`
    nfs_client_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_client_name "$nfs_client_name" '.servers | .[] | select(.droplet_name == $nfs_client_name) | .droplet_private_ip'`

    nfs_server_private_ip=`echo ${nfs_server_private_ip} | sed 's/\"//g'`
    nfs_client_private_ip=`echo ${nfs_client_private_ip} | sed 's/\"//g'`

    echo "   PRIVATE IP OF NFS SERVER IS   $nfs_server_private_ip     "
    echo "   PRIVATE IP OF NFS CLIENT IS   $nfs_client_private_ip     "

    docker-machine ssh $nfs_client_name "sudo mkdir -p $nfs_client_directory" </dev/null
    docker-machine ssh $nfs_client_name "sudo mount -o noac $nfs_server_private_ip:$nfs_server_directory $nfs_client_directory" </dev/null
    docker-machine ssh $nfs_client_name "sudo echo $nfs_server_private_ip:$nfs_server_directory $nfs_client_directory nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0 >> /etc/fstab" </dev/null

    echo "   END MOUNT  NFS CLIENT($1)  NFS SERVER($2)  NFS SERVER DIRECTORY($3)  NFS CLIENT DIRECTORY($4)      "

}


usage () {
    echo ""
    echo " Use this script to create nfs server in digitalocean . To use this script, please run this command "
    echo " ./mount-nfs-server-and-client-directory.sh --data.mount.mapping.file absolute_path_to_mount_config_file --cluster.name name_of_your_cluster  "
    echo ""
    echo " --data.mount.mapping.file value should be the absolute path of the json file that contains the informations how to mmount directory between nfs server and docker swarm node "
    echo " --cluster.name name of the cluster   "
}

print_option_values() {
    echo ""
    echo " Options values sent to script are : "
    echo " data.mount.mapping.file = $mount_mapping_file "
    echo " cluster.name = $cluster_name "
}

check_required_options_and_env_variable() {
    echo ""
    echo " Check required options "

    if [ -z ${mount_mapping_file+x} ]; then
     echo "nodes.file is missing "
     exit
    fi

    if [ -z ${cluster_name+x} ]; then
     echo "cluster.name is missing "
     exit
    fi

    if [[ -z "${do_token}" ]]; then
      echo "do_token env variable  is undefined"
      exit 1
    fi

    if [ ! -f $asklytics_servers_file_path ]
    then
        echo "file $asklytics_servers_file_path not found"
        exit 1
    fi
}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       #path of the file that contains the mount mapping
       data.mount.mapping.file)
          mount_mapping_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       # name of the cluster
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       help)
          usage
          ;;
    esac
done

echo "                 THE SCRIPT TO MOUNT DIRECTORY BETWEEN NFS SERVER AND NFS CLIENT   STARTED            "


asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"

print_option_values
check_required_options_and_env_variable



jq -c '.[]' "$mount_mapping_file" | while read i; do
    droplet_name=`echo ${i} | jq '.droplet_name'`
    nfs_mounts=`echo ${i} | jq '.nfs_mounts'`

    droplet_name=`echo ${droplet_name} | sed 's/\"//g'`

    echo "      MOUNTING PROCESS FOR  NFS CLIENT  $droplet_name  WILL START       "

    echo "${nfs_mounts}" | jq -c '.[]' | while read nfs_mount; do


        nfs_server_name=`echo ${nfs_mount} | jq '.nfs_server_name'`
        nfs_server_directory=`echo ${nfs_mount} | jq '.nfs_server_directory'`
        nfs_client_directory=`echo ${nfs_mount} | jq '.nfs_client_directory'`

        nfs_server_name=`echo ${nfs_server_name} | sed 's/\"//g'`
        nfs_server_directory=`echo ${nfs_server_directory} | sed 's/\"//g'`
        nfs_client_directory=`echo ${nfs_client_directory} | sed 's/\"//g'`


        mount_on_nfs_server $droplet_name $nfs_server_name $nfs_server_directory
        mount_on_nfs_client $droplet_name $nfs_server_name $nfs_server_directory $nfs_client_directory
    done
done


echo "                 THE SCRIPT TO MOUNT DIRECTORY BETWEEN NFS SERVER AND NFS CLIENT   ENDED            "
