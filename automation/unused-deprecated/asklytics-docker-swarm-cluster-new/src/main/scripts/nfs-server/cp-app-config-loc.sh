#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""

cp_directory () {
   echo ""
   echo "                 COPY DIRECTORY($1) ON BOOTSTRAP NODE TO  DIRECTORY($2) ON NFS SERVER($3)  STARTED  "

   dir_or_file_on_boostrap_node=`echo ${1} | sed 's/\"//g'`
   dir_or_file_on_nfs_server=`echo ${2} | sed 's/\"//g'`
   nfs_server_droplet_name=`echo ${3} | sed 's/\"//g'`
   AL_DEPLOYMENT_ROOT=`echo ${4} | sed 's/\"//g'`

   nfs_server_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_server_name "$nfs_server_droplet_name" '.servers | .[] | select(.droplet_name == $nfs_server_name) | .droplet_private_ip'`
   main_manager_private_ip=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_private_ip'`

   echo "                 CREATE DIRECTORY($dir_or_file_on_nfs_server) ON NFS SERVER($nfs_server_droplet_name)       "
   docker-machine ssh $nfs_server_droplet_name "mkdir -p $dir_or_file_on_nfs_server" </dev/null

   if [ $dir_or_file_on_boostrap_node != "null" ]
   then
       echo "     CREATE TMP DIRECTORY IN BOOTSTRAP NODE        "
       mkdir tmp
       echo "     CREATE TMP DIRECTORY IN BOOTSTRAP NODE        "
       echo "     COPY CONTENT OF  $AL_DEPLOYMENT_ROOT/$dir_or_file_on_boostrap_node TO TMP DIRECTORY IN BOOTSTRAP NODE        "
       cp -r $AL_DEPLOYMENT_ROOT/$dir_or_file_on_boostrap_node tmp

       echo "    SEARCH AND REPLACE main_manager_private_ip  STRING IN TMP DIRECTORY BY  MANAGER NODE IP($main_manager_private_ip)        "
       sed -i "s/#manager_ip/$main_manager_private_ip/g" $(find tmp/ -type f)

       base_dir=`basename $AL_DEPLOYMENT_ROOT/$dir_or_file_on_boostrap_node`
       echo "                 COPY TMP  DIRECTORY($2) ON NFS SERVER($3)   "
       docker-machine scp -r "tmp/$base_dir/" $nfs_server_droplet_name:$dir_or_file_on_nfs_server/. </dev/null

       echo "    DELETE TMP DIRECTORY        "
       rm -r tmp/
   fi

      echo "                 COPY DIRECTORY($1) ON BOOTSTRAP NODE TO  DIRECTORY($2) ON NFS SERVER($3) ENDED    "

}


usage () {
    echo ""
    echo " Use this script to create nfs server in digitalocean . To use this script, please run this command "
    echo " ./create-docker-swarm-cluster.sh --nodes.file absolute_path_to_node_json_file --do.users.data absolute_path_to_cloud_init_file --cluster.name name_of_your_cluster  "
    echo ""
    echo " --nodes.file value should be the absolute path of the json file that contains the informations about the nodes of the docker swarm to create "
    echo " --do.users.data is the absolute path to cloud init file that could be used to setup the server. It is not required  "
    echo " --cluster.name name of the cluster   "
}

print_option_values() {
    echo ""
    echo " Options values sent to script are : "
    echo " config.properties.json = $config_and_properties_json "
    echo " cluster.name = $cluster_name "
}

print_env_values() {
    echo ""
    echo " Env  values required by script are : "
    echo " AL_DEPLOYMENT_ROOT = $AL_DEPLOYMENT_ROOT "
}

check_required_options_and_env_variable() {
    echo ""
    echo " Check required options "

    if [ -z ${nodes_file+x} ]; then
     echo "nodes.file is missing "
     exit
    fi

    if [ -z ${cluster_name+x} ]; then
     echo "cluster.name is missing "
     exit
    fi

    if [[ -z "${do_token}" ]]; then
      echo "do_token env variable  is undefined"
      exit 1
    fi

    if [ ! -f $asklytics_servers_file_path ]
    then
        echo "file $asklytics_servers_file_path not found"
        exit 1
    fi

    if [[ -z "${AL_DEPLOYMENT_ROOT}" ]]; then
      echo "AL_DEPLOYMENT_ROOT env variable  is undefined."
      echo "Please set it in /etc/environment as the script does not find the env variable when set in the shell"
      echo "Please remember to source /etc/environment before running the script"
      echo "Once finished deploying, please remove the AL_DEPLOYMENT_ROOT from /etc/environment"
      exit 1
    fi
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       config.properties.json)
          config_and_properties_json="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

echo "                 THE SCRIPT TO COPY CONFIG FILE AND DIRECTORY ON NFS SERVER   STARTED            "

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


print_option_values
print_env_values
check_required_options_and_env_variable



list_keys=()
while IFS='' read -r line; do
   list_keys+=("$line")
done < <(jq 'keys[]' "$config_and_properties_json")


for i in "${list_keys[@]}"
do
   # or do whatever with individual element of the array
   key_value=`cat ${config_and_properties_json} | jq  ".$i"`

   echo "${key_value}" | jq -c '.[]' | while read i; do
        dir_or_file_on_boostrap_node=`echo ${i} | jq '.dir_or_file_on_boostrap_node'`
        dir_or_file_on_nfs_server=`echo ${i} | jq '.dir_or_file_on_nfs_server'`
        nfs_server_droplet_name=`echo ${i} | jq '.nfs_server_droplet_name'`

        nfs_server_droplet_name=`echo ${nfs_server_droplet_name} | sed 's/\"//g'`
        dir_or_file_on_nfs_server=`echo ${dir_or_file_on_nfs_server} | sed 's/\"//g'`
        dir_or_file_on_boostrap_node=`echo ${dir_or_file_on_boostrap_node} | sed 's/\"//g'`


        cp_directory $dir_or_file_on_boostrap_node $dir_or_file_on_nfs_server $nfs_server_droplet_name $AL_DEPLOYMENT_ROOT
   done

done

echo "                 THE SCRIPT TO COPY CONFIG FILE AND DIRECTORY ON NFS SERVER   ENDED            "

