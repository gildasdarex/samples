#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""
asklytics_volumes_file_path=""

usage () {
    echo ""
    echo " Use this script to create nfs server in digitalocean . To use this script, please run this command "
    echo " ./create-nfs-server.sh --nodes.file absolute_path_to_node_json_file --do.users.data absolute_path_to_cloud_init_file --cluster.name name_of_your_cluster"
    echo ""
    echo " --nodes.file value absolute path of the json file that contains info about nfs servers to create "
    echo " --do.users.data absolute path to cloud init file that should be used to setup the server. It is NOT REQUIRED"
    echo " --cluster.name name of the docker swarm cluster"
}

print_option_values() {
    echo ""
    echo " Options values sent to script are : "
    echo " nodes.file = $nodes_file "
    echo " do.users.data = $do_users_data_file "
    echo " cluster.name = $cluster_name "
}

check_required_options_and_env_variable() {
    echo ""
    echo " Check required options ..."

    if [ -z ${nodes_file+x} ]; then
     echo "nodes.file is missing "
     usage
     exit 1
    fi

    if [ -z ${cluster_name+x} ]; then
     echo "cluster.name is missing "
     usage
     exit 1
    fi

    if [[ -z "${do_token}" ]]; then
      echo "do_token env variable  is undefined. Set in /etc/environment and run source /etc/environment"
      echo "When scripts are run as sudo, the env variable from the shell is not passed to the script."
      echo "We need to create a aldeployment_profile file that contains env variables."
      echo "Then source aldeployment_profile at beginning of this script."
      exit 1
    fi
}


update_servers () {
    # $1 parameter is the name of the project to clone
    #get droplet id
    echo ""
    echo "   ADD NFS SERVER $1 TO SERVERS JSON FILE $asklytics_servers_file_path IN PROGRESS     "
    sleep 2m

    droplet_name=`echo ${1} | sed 's/\"//g'`
    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    #get droplet private ip
    droplet_id=`echo ${droplet_id} | sed 's/\"//g'`
    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`
    #droplet_name=`echo ${1} | sed 's/\"//g'`
    private_ip=`echo ${private_ip} | sed 's/\"//g'`
    droplet_type=`echo ${2} | sed 's/\"//g'`

    servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
    > $asklytics_servers_file_path
    echo $servers >> $asklytics_servers_file_path
    echo "   ADD NFS SERVER $name TO SERVERS JSON FILE $asklytics_servers_file_path END WITH SUCCESS     "

}

attach_volume_to_nfs_server () {
    # $1 parameter is the name of the project to clone
    #get droplet id
    echo ""
    echo "   ATTACH VOLUME $2 TO NFS SERVER $1 IN PROGRESS     "
    droplet_name=`echo ${1} | sed 's/\"//g'`
    volume_name=`echo ${2} | sed 's/\"//g'`
    region=`echo ${3} | sed 's/\"//g'`
    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    droplet_id=`echo ${droplet_id} | sed 's/\"//g'`


    volume_id=`cat $asklytics_volumes_file_path | jq --arg volume_name "$volume_name" '.volumes | .[] | select(.name == $volume_name) | .volume_id'`
    volume_id=`echo ${volume_id} | sed 's/\"//g'`

    echo " ATTACH VOLUME $2 (volume_id = ${volume_id}) TO NFS SERVER $1 (droplet_id = ${droplet_id}) IN PROGRESS"

    #curl -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"type\": \"attach\", \"droplet_id\": \"$droplet_id\", \"region\": \"$region\"}'" "https://api.digitalocean.com/v2/volumes/$volume_id/actions"

    curl -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"type\": \"attach\", \"droplet_id\": \"$droplet_id\", \"region\": \"$region\"}'" "https://api.digitalocean.com/v2/volumes/$volume_id/actions"
    echo "   ATTACH VOLUME $2 TO NFS SERVER $1 END WITH SUCCESS     "
}

create_output_file() {
    echo "Creating output to $asklytics_servers_file_path ..."
    if [ ! -f $asklytics_servers_file_path ]
    then
        mkdir -p /asklytics/cluster/$cluster_name/output/
    else
        #If file already exists, move it so that the script does not overwrite.
        mv /asklytics/cluster/$cluster_name/output/servers.json /asklytics/cluster/$cluster_name/output/servers-$(date +%s).json
    fi
    # initializing output file
    touch /asklytics/cluster/$cluster_name/output/servers.json
    echo {\"servers\": []} >> $asklytics_servers_file_path
}

mount_nfs_vol_on_droplets() {
    # Loop inside each element in the json config file
    echo "Mounting volumes on droplets configured in $nodes_files ..."
    echo ""
    jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    snapshot_id=`echo ${i} | jq '.snapshot_id'`
    tags=`echo ${i} | jq '.tags'`
    volume_mapping=`echo ${i} | jq '.volume_mapping'`

    echo ""
    echo "       STARTING TO CREATE THE NFS SERVER NAMED  $name            "

    cmd=""
    if [ -z ${do_users_data_file+x} ]; then
      cmd=`echo docker-machine create --driver digitalocean --digitalocean-ipv6 --digitalocean-monitoring --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-tags "$tags" "$name"`
      echo ""
      echo "  RUN COMMAND $cmd "
    else
      cmd=`echo docker-machine create --driver digitalocean --digitalocean-ipv6 --digitalocean-monitoring --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-userdata "$do_users_data_file" --digitalocean-tags "$tags" "$name"`
      echo ""
      echo "  RUN COMMAND $cmd "
    fi
    eval $cmd
    echo "        FINISHING TO CREATE THE NFS SERVER NAMED  $name             "
    update_servers $name "nfs_server"



    droplet_name=`echo ${name} | sed 's/\"//g'`
    # TODO - check if volume is mounted with success

    # Loop inside each element of volume mapping
    # blockstorage_volume_name is the name of the volume that we create with create-volume.sh script
    # mount_point is the path where will be mounted inside the droplet
    echo "${volume_mapping}" | jq -c '.[]' | while read i; do
        volume_name=`echo ${i} | jq '.blockstorage_volume_name'`
        mount_point=`echo ${i} | jq '.nfs_server_mount_point'`

        mount_point=`echo ${mount_point} | sed 's/\"//g'`
        volume_name=`echo ${volume_name} | sed 's/\"//g'`

        attach_volume_to_nfs_server $name $volume_name $region


        echo ""
        echo "       Starting to Mount $volume_name on DROPLET $droplet_name ON PATH $mount_point ......"

        # we mount volume in droplet in loop because we mounting could failed because droplet is not running
        echo "Some operations will be retried ad droplet may not be running, or failures due to intermittent issues,  digitalocean infrastructure being slow."
        for i in $(seq 1 5); do
          echo "Checking status of droplet $droplet_name ..."
          status=`docker-machine status $droplet_name`
          echo "Current status of droplet $droplet_name is $status "
          if [ $status == "Running" ]
          then
             echo "Creating directory $mount_point on droplet $droplet_name ..."
             docker-machine ssh $droplet_name mkdir -p $mount_point </dev/null

             for j in $(seq 1 5); do
               echo "Configuring $volume_name on droplet $droplet_name by installing mkfs.ext4 TRY NUMBER = $j ..."
               docker-machine ssh $droplet_name "sudo mkfs.ext4 /dev/disk/by-id/scsi-0DO_Volume_$volume_name" </dev/null
               last_status_code=$?
               if [ $last_status_code -ne 0 ];then
                 sleep 1m
                 continue
               else
                echo "Successfully Configured $volume_name on droplet $droplet_name by installing mkfs.ext4"
                 break
               fi
             done
            #TODO Handling the case where config was not successful
            # echo "FAILED to configure $volume_name on droplet $droplet_name by installing mkfs.ext4"
            # exit 1

            #TODO - There is a retr number in the line below. But it is not in the loop for j
             echo "Mounting NFS Volume on $droplet_name Volume Name: $volume_name Mount Point:$mount_point TRY NUMBER = $j ..."
             echo "Running Command -  mount -o discard,defaults,noatime /dev/disk/by-id/scsi-0DO_Volume_$volume_name $mount_point       "
             docker-machine ssh $droplet_name "mount -o discard,defaults,noatime /dev/disk/by-id/scsi-0DO_Volume_$volume_name $mount_point" </dev/null
             echo "Adding mount to fstab for auto remount when droplet is restarted..."
             #TODO - backup the fstab file with the file name to include -$(date +%s)
             docker-machine ssh $droplet_name "echo '/dev/disk/by-id/scsi-0DO_Volume_$volume_name $mount_point ext4 defaults,nofail,discard 0 0' | sudo tee -a /etc/fstab" </dev/null
             #TODO - verify that the mount was successful by accessing it.
             # https://www.cyberciti.biz/faq/howto-check-if-a-directory-exists-in-a-bash-shellscript/
             break;
          else
             echo "Droplet must be in 'Running' status. Waiting 1 minute to try again ..."
             sleep 1m
             continue
          fi
        done

        echo ""
        echo "       MOUNT $volume_name IN DROPLET $droplet_name ON PATH $mount_point ...... ENDED                  "
        echo ""
    done
done
}

read_options_to_script() {
    optspec=":-:"
    while getopts "$optspec" optchar; do
        case "${OPTARG}" in
           # json config file that contains the list of nfs server to create
           nodes.file)
              nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
              ;;
           # yaml file that contains digital ocean metadata to use to initialize the droplet
           do.users.data)
              do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
              ;;
           # name of the cluster
           cluster.name)
              cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
              ;;
           help)
              usage
              ;;
        esac
    done
}

read_options_to_script
print_option_values
check_required_options_and_env_variable

echo "                 THE SCRIPT TO CREATE NFS SERVERS STARTED               "


# file where all droplets info is saved
asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"
# file where all volumes are saved
asklytics_volumes_file_path="/asklytics/cluster/$cluster_name/output/volumes.json"

echo "Using server config file:$asklytics_servers_file_path"
echo "Output will be saved in $asklytics_volumes_file_path"
echo ""

create_output_file
mount_nfs_vol_on_droplets


echo "                 THE SCRIPT TO CREATE NFS SERVERS ENDED                           "
