#!/usr/bin/env bash

#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console




optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       do.users.data)
          do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       git.user)
          git_user="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       git.password)
          git_password="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi

jq -c '.[]' "$nodes_file" | while read i; do
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    snapshot_id=`echo ${i} | jq '.snapshot_id'`
    tags=`echo ${i} | jq '.tags'`

    region=`echo ${region} | sed 's/\"//g'`
    name=`echo ${name} | sed 's/\"//g'`
    size=`echo ${size} | sed 's/\"//g'`
    snapshot_id=`echo ${snapshot_id} | sed 's/\"//g'`
    tags=`echo ${tags} | sed 's/\"//g'`

    echo "##################################### Starting to create bootstrap node $name ################################"

    #curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer b7d03a6947b217efb6f3ec3bd3504582" -d '{"name":"example.com","region":"nyc3","size":"s-1vcpu-1gb","image":"ubuntu-16-04-x64","ssh_keys":[107149],"backups":false,"ipv6":true,"user_data":null,"private_networking":null,"volumes": null,"tags":["web"]}' "https://api.digitalocean.com/v2/droplets"
    #curl -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"name\": \"$name\", \"size\": \"$size\", \"region\": \"$region\", \"private_networking\": \"true\", \"ipv6\": \"true\", \"monitoring\": \"true\", \"tags\": [$tags], \"image\": \"$snapshot_id\"}'" "https://api.digitalocean.com/v2/droplets"
    cmd=`echo curl -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"name\": \"$name\", \"size\": \"$size\", \"region\": \"$region\", \"private_networking\": \"true\", \"ipv6\": \"true\", \"monitoring\": \"true\", \"image\": \"$snapshot_id\"}'" "https://api.digitalocean.com/v2/droplets"`
    echo $cmd
    eval $cmd
    echo "##################################### Finish to create bootstrap node $name ###################################"


done