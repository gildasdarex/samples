#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""
select_main_manager="false"
select_main_manager_name=""

usage () {
    echo ""
    echo " Use this script to create nfs server in digitalocean . To use this script, please run this command "
    echo " ./create-docker-swarm-cluster.sh --nodes.file absolute_path_to_node_json_file --do.users.data absolute_path_to_cloud_init_file --cluster.name name_of_your_cluster  "
    echo ""
    echo " --nodes.file value should be the absolute path of the json file that contains the informations about the nodes of the docker swarm to create "
    echo " --do.users.data is the absolute path to cloud init file that could be used to setup the server. It is not required  "
    echo " --cluster.name name of the cluster   "
}

print_option_values() {
    echo ""
    echo " Options values sent to script are : "
    echo " nodes.file = $nodes_file "
    echo " do.users.data = $do_users_data_file "
    echo " cluster.name = $cluster_name "
}

check_required_options_and_env_variable() {
    echo ""
    echo " Check required options "

    if [ -z ${nodes_file+x} ]; then
     echo "nodes.file is missing "
     exit
    fi

    if [ -z ${cluster_name+x} ]; then
     echo "cluster.name is missing "
     exit
    fi

    if [[ -z "${do_token}" ]]; then
      echo "do_token env variable  is undefined"
      exit 1
    fi
}

create_node() {
    name=`echo ${1} | sed 's/\"//g'`
    region=`echo ${2} | sed 's/\"//g'`
    snapshot_id=`echo ${3} | sed 's/\"//g'`
    size=`echo ${4} | sed 's/\"//g'`
    tags=`echo ${5} | sed 's/\"//g'`
    do_users_data_file=`echo ${6} | sed 's/\"//g'`

    echo "       STARTING TO CREATE THE CLUSTER NODE NAMED  $name            "
    cmd=""
    if [ -z ${do_users_data_file+x} ]; then
      cmd=`echo docker-machine create --driver digitalocean --digitalocean-ipv6 --digitalocean-monitoring --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-tags "$tags" "$name"`
      echo ""
      echo "  RUN COMMAND $cmd "
    else
      cmd=`echo docker-machine create --driver digitalocean --digitalocean-ipv6 --digitalocean-monitoring --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-userdata "$do_users_data_file" --digitalocean-tags "$tags" "$name"`
      echo ""
      echo "  RUN COMMAND $cmd "
    fi
    eval $cmd
    echo "        FINISHING TO CREATE NODE NAMED  $name   "
}

update_servers () {
        echo "   ADD CLUSTER NODE $1 TO SERVERS JSON FILE $asklytics_servers_file_path IN PROGRESS     "


    droplet_name=`echo ${1} | sed 's/\"//g'`

    check_droplet=`cat $asklytics_servers_file_path | jq --arg droplet_name "$droplet_name" '.servers | .[] | select(.droplet_name == $droplet_name) | .droplet_name'`
    check_droplet=`echo ${check_droplet} | sed 's/\"//g'`

    echo "        CHECKING IF NODE $1 IS ALREADY IN OUTPUT FILE  $asklytics_servers_file_path         "


    if [ $check_droplet == $droplet_name ]
    then
      echo "        NODE $1 IS ALREADY IN OUTPUT FILE  $asklytics_servers_file_path              "
      return 1
    fi

    # get droplet id - docker-machine inspect droplet_name returns json response that contains droplet id
    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    #get droplet private ip
    droplet_id=`echo ${droplet_id} | sed 's/\"//g'`
    # get droplet private ip - use digital ocean api to get private ip of the droplet
    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`
    #droplet_name=`echo ${1} | sed 's/\"//g'`
    private_ip=`echo ${private_ip} | sed 's/\"//g'`
    droplet_type=`echo ${2} | sed 's/\"//g'`

    # Read $asklytics_servers_file_path and add new droplet
    if [ -z "$3" ]
    then
         servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
    else
         servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type, "main_manager": "true"}]'`
    fi

    > $asklytics_servers_file_path
    echo $servers >> $asklytics_servers_file_path

    echo " NODE $1 IS ADDED IN OUTPUT FILE  $asklytics_servers_file_path WITH SUCCESS      "

}


add_server_to_swarm () {
    echo " ADDING NODE $1 TO DOCKER SWARM CLUSTER  $cluster_name   "

    node_type=`echo ${1} | sed 's/\"//g'`
    name=`echo ${2} | sed 's/\"//g'`
    main_manager_name=`echo ${3} | sed 's/\"//g'`
    manager_token=`echo ${4} | sed 's/\"//g'`
    worker_token=`echo ${5} | sed 's/\"//g'`
    manager_node_ip=`echo ${6} | sed 's/\"//g'`

    manager_node_ip=`docker-machine ip $main_manager_name`
    token=""

    if [ $node_type == "manager" ]
    then
       token=$manager_token
    elif [  $node_type == "worker" ]
    then
       token=$worker_token
    else
       exit
    fi

    for i in $(seq 1 5); do
        echo "     ADDING NODE $1 TO DOCKER SWARM CLUSTER  $cluster_name  - TRY NUMBER $i       "

        cmd=`echo docker-machine ssh $name docker swarm leave`
        echo $cmd
        eval $cmd
        docker-machine ssh $name docker swarm join --token=$token $manager_node_ip:2377
        status=`docker-machine ssh $name docker info | grep Swarm | sed 's/Swarm: //g' </dev/null`

        if [ $status != "active" ]; then
            sleep 30
            if (( $i == 5 )); then
                echo "     FAILED TO ADD NODE $1 TO DOCKER SWARM CLUSTER  $cluster_name     "
                return 1
            else
               continue
            fi
        else
            break;
        fi
    done
    echo "     ADD NODE $1 TO DOCKER SWARM CLUSTER  $cluster_name  WITH SUCCESS      "

}

function isSwarmNode(){
    is_swarm=`docker-machine ssh $1 "docker info | grep Swarm | sed 's/Swarm: //g'" </dev/null`
    if [ $is_swarm == "inactive" ]; then
        echo false;
    else
        echo true;
    fi
}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       # json config file that contains the list of droplets to add
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        # yaml file that contains digital ocean metadata to use to initialize the droplet
       do.users.data)
          do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        # name of the cluster
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       help)
          usage
          ;;
    esac
done


echo "                 THE SCRIPT TO CREATE DOCKER SWARM CLUSTER   STARTED            "

print_option_values
check_required_options_and_env_variable

# file where all droplets informations will be saved
asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


if [ ! -f $asklytics_servers_file_path ]
then
    mkdir -p /asklytics/cluster/$cluster_name/output/
    touch /asklytics/cluster/$cluster_name/output/servers.json
    echo {\"servers\": []} >> $asklytics_servers_file_path
fi


# Loop inside each element in the json config file
jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    snapshot_id=`echo ${i} | jq '.snapshot_id'`
    tags=`echo ${i} | jq '.tags'`

    node_type=`echo ${node_type} | sed 's/\"//g'`
    name=`echo ${name} | sed 's/\"//g'`


    echo "             DETECT IF MACHINE NAMED  $name  ALREADY EXISTS                  "
    docker_machine_exists=`docker-machine ls -q | grep "$name"`

    if test -z "$docker_machine_exists"  ; then
        echo "             MACHINE NAMED  $name  DOESN'T EXIST                         "
        create_node $name $region $snapshot_id $size $tags $do_users_data_file
    else
        echo "             MACHINE NAMED  $name  ALREADY EXIST                  "
    fi



    echo "       CHECKING IF NODE $name IS RUNNING OR NOT           "

    for i in $(seq 1 5); do
      status=`docker-machine status $name`
      if [ $status == "Running" ]
      then
         echo "    $droplet_name  IS RUNNING                         "
         break;
      else
         echo "       $droplet_name IS NOT RUNNING . CURRENT STATUS IS $status         "
         echo "       WAIT 1 MIN TO CHECK NEW STATUS OF DROPLET $droplet_name          "
         sleep 1m
         continue
      fi
    done


    if [ $node_type == "manager" ]; then
        if [ $select_main_manager == "false" ]; then
          update_servers $name $node_type "main_manager"
          select_main_manager="true"
          select_main_manager_name=$name
        else
            update_servers $name $node_type
        fi;
     else
        update_servers $name $node_type
    fi;
done


main_manager_name=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`
main_manager_name=`echo ${main_manager_name} | sed 's/\"//g'`
manager_node_ip=`docker-machine ip $main_manager_name`

echo ""
echo "   CHECK IF DOCKER SWARM IS ALREADY INITIALIZED      "
is_manager_part_of_swarm=`isSwarmNode "$main_manager_name"`

if [ $is_manager_part_of_swarm == "false" ]; then
   echo "   DOCKER SWARM  INITIALIZATION IN PROGRESS     "
   docker-machine ssh $main_manager_name docker swarm init --listen-addr $manager_node_ip --advertise-addr $manager_node_ip
   echo "   DOCKER SWARM INITIALIZED   WITH SUCCESS   "
else
   echo "   DOCKER SWARM IS ALREADY INITIALIZED      "
fi;


worker_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token worker -q")

manager_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token manager -q")



jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    name=`echo ${i} | jq '.name'`
    labels=`echo ${i} | jq '.labels'`

    node_type=`echo ${node_type} | sed 's/\"//g'`
    name=`echo ${name} | sed 's/\"//g'`
    labels=`echo ${labels} | sed 's/\"//g'`

    echo "   ADD NODE $name TO  DOCKER SWARM        "


    if [ $name != $main_manager_name ]; then

        is_manager_part_of_swarm=`isSwarmNode "$name"`

        if [ $is_manager_part_of_swarm == "false" ]; then
             add_server_to_swarm $node_type $name $main_manager_name $manager_token $worker_token $manager_node_ip
        fi;
    fi;

    for label in $(echo $labels | sed "s/,/ /g")
    do
        echo "           ADD LABEL $label TO  NODE $name              "
        #https://unix.stackexchange.com/questions/66154/ssh-causes-while-loop-to-stop
        docker-machine ssh $main_manager_name "docker node update --label-add ${label}=true  ${name}" </dev/null
    done

done


echo "                 THE SCRIPT TO CREATE DOCKER SWARM NODE  ENDED                           "
