#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console


update_servers () {
    echo "#############  Add droplet $1 to output directory ############# "

    droplet_name=`echo ${1} | sed 's/\"//g'`
    # get droplet id - docker-machine inspect droplet_name returns json response that contains droplet id
    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    #get droplet private ip
    droplet_id=`echo ${droplet_id} | sed 's/\"//g'`
    # get droplet private ip - use digital ocean api to get private ip of the droplet
    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`
    #droplet_name=`echo ${1} | sed 's/\"//g'`
    private_ip=`echo ${private_ip} | sed 's/\"//g'`
    droplet_type=`echo ${2} | sed 's/\"//g'`

    # Read $asklytics_servers_file_path and add new droplet
    if [ -z "$3" ]
    then
         servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
    else
         servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type, "main_manager": "true"}]'`
    fi

    > $asklytics_servers_file_path
    echo $servers >> $asklytics_servers_file_path

    echo "#############  droplet $1  added to output directory with success ############# "
}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       # json config file that contains the list of droplets to add
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        # yaml file that contains digital ocean metadata to use to initialize the droplet
       do.users.data)
          do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        # name of the cluster
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z ${cluster_name+x} ]; then
 echo "cluster.name is missing "
 exit
fi

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"

if [ ! -f $asklytics_servers_file_path ]
then
    mkdir -p /asklytics/cluster/$cluster_name/output/
    touch /asklytics/cluster/$cluster_name/output/servers.json
    echo {\"servers\": []} >> $asklytics_servers_file_path
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi

main_manager_name=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`
main_manager_name=`echo ${main_manager_name} | sed 's/\"//g'`

echo "Main manager node is $main_manager_name"

worker_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token worker -q")

manager_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token manager -q")


jq -c '.[]' "$nodes_file" | while read i; do
    # read json property from json file
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    snapshot_id=`echo ${i} | jq '.snapshot_id'`
    tags=`echo ${i} | jq '.tags'`

     node_type=`echo ${node_type} | sed 's/\"//g'`
    name=`echo ${name} | sed 's/\"//g'`


    cmd=`echo docker-machine create --driver digitalocean --digitalocean-ipv6 --digitalocean-monitoring --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-userdata "$do_users_data_file" --digitalocean-tags "$tags" "$name"`
    eval $cmd

    sleep 1m

    update_servers $name $node_type

    node_type=`echo ${node_type} | sed 's/\"//g'`

    name=`echo ${name} | sed 's/\"//g'`

    manager_node_ip=`docker-machine ip $main_manager_name`

    token=""

    for i in $(seq 1 5); do
        echo "Add $name to cluster : $i try"

        cmd=`echo docker-machine ssh $name docker swarm leave`
        echo $cmd
        eval $cmd

        if [ $node_type == "manager" ]
        then
           token=$manager_token
        elif [  $node_type == "worker" ]
        then
           token=$worker_token
        else
           continue
        fi

        docker-machine ssh $name docker swarm leave
        echo "Add $name to docker swarm cluster as manager or worker node"
        sleep 2m
        docker-machine ssh $name docker swarm join --token=$token $manager_node_ip:2377
        status=`docker-machine ssh $name docker info | grep Swarm | sed 's/Swarm: //g'`
        echo "Status for $name node after try $i is $status"
        if [ $status != "active" ]; then
            continue
        else
            break;
        fi
    done

done