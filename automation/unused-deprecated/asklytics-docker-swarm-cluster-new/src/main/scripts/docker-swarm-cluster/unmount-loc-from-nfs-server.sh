#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path="/asklytics/output/servers.json"

nfs_server_private_ip=` cat $asklytics_servers_file_path | jq '.servers | .[] | select(.type == "nfs_server") | .droplet_private_ip'`


unmount_data_from_file () {
    # $1 parameter is the path of the file that contains the json info
    jq -c '.[]' "$1" | while read i; do
        droplet_names=`echo ${i} | jq '.droplet_names'`
        nfs_client_directory=`echo ${i} | jq '.nfs_client_directory'`

        droplet_names=`echo "$droplet_names" | sed 's/[][]//g'`

        for droplet_name in $(echo $droplet_names | sed "s/,/ /g")
        do
            droplet_name=`echo "$droplet_name" | sed -e 's/^"//' -e 's/"$//'`

            docker-machine ssh $droplet_name "sudo umount $nfs_client_directory"
            docker-machine ssh $droplet_name "df"

        done
    done
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       unmount.json)
          unmount_json="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [[ -d $unmount_json ]]; then
    echo "$unmount_json is a directory"
    for entry in "$unmount_json"/*
    do
      unmount_data_from_file $entry
    done
elif [[ -f $unmount_json ]]; then
    echo "$unmount_json is a file"
    unmount_data_from_file $mount_json
else
    echo "$unmount_json is not valid file or directory"
    exit 1
fi



#jq -c '.[]' "$mount_json" | while read i; do
#    droplet_names=`echo ${i} | jq '.droplet_names'`
#    nfs_server_directory=`echo ${i} | jq '.nfs_server_directory'`
#    nfs_client_directory=`echo ${i} | jq '.nfs_client_directory'`
#
#    droplet_names=`echo "$droplet_names" | sed 's/[][]//g'`
#
#    for droplet_name in $(echo $droplet_names | sed "s/,/ /g")
#    do
#        droplet_name=`echo "$droplet_name" | sed -e 's/^"//' -e 's/"$//'`
#
#        docker-machine ssh $droplet_name "sudo mount -o noac $nfs_server_private_ip:$nfs_server_directory $nfs_client_directory"
#
#    done
#
#done