#!/usr/bin/env bash
#!/usr/bin/env bash

#!/usr/bin/env bash

asklytics_servers_file_path=""
asklytics_volumes_file_path=""



attach_volume_to_nfs_server () {
    # $1 parameter is the name of the project to clone
    #get droplet id
    droplet_name=`echo ${1} | sed 's/\"//g'`
    volume_name=`echo ${2} | sed 's/\"//g'`

    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    droplet_id=`echo ${droplet_id} | sed 's/\"//g'`


    volume_id=`cat $asklytics_volumes_file_path | jq --arg volume_name "$volume_name" '.volumes | .[] | select(.name == $volume_name) | .volume_id'`
    volume_id=`echo ${volume_id} | sed 's/\"//g'`

    region=`cat $asklytics_servers_file_path | jq --arg volume_name "$volume_name" '.volumes | .[] | select(.name == $volume_name) | .region'`
    region=`echo ${region} | sed 's/\"//g'`

    echo "Starting to attach  nfs server $droplet_name with id $droplet_id to volume $volume_name with id $volume_id"

    cmd=` echo curl -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"type\": \"attach\", \"droplet_id\": \"$droplet_id\", \"region\": \"$region\"}'" "https://api.digitalocean.com/v2/volumes/$volume_id/actions"`
    echo $cmd
    eval $cmd

    # TODO - check if volume is attached with success
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       volumes.mapping.file)
          volumes_mapping_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z ${cluster_name+x} ]; then
 echo "cluster.name is missing "
 exit
fi

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"
asklytics_volumes_file_path="/asklytics/cluster/$cluster_name/output/volumes.json"



if [ ! -f $asklytics_servers_file_path ]
then
    echo "No server is created"
    exit
fi

if [ ! -f $asklytics_volumes_file_path ]
then
    echo "No volume is created"
    exit
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi

jq -c '.[]' "$volumes_mapping_file" | while read i; do
    volume_name=`echo ${i} | jq '.volume_name'`
    mount_point=`echo ${i} | jq '.mount_point'`
    droplet_name=`echo ${i} | jq '.droplet_name'`

    attach_volume_to_nfs_server $name $volume_name

    for i in $(seq 1 5); do
          status=`docker-machine status $droplet_name`
          if [ $status == "Running" ]
          then
             echo ".................. $droplet_name is running........................."

             echo "Starting to mount  nfs server $droplet_name to volume $volume_name"
             docker-machine ssh $droplet_name mkdir -p $mount_point
             docker-machine ssh $droplet_name "sudo mkfs.ext4 /dev/disk/by-id/scsi-0DO_Volume_$volume_name"
             cmd=`echo docker-machine ssh $droplet_name "mount -o discard,defaults,noatime /dev/disk/by-id/scsi-0DO_Volume_$volume_name $mount_point"`
             echo $cmd
             eval $cmd

             #docker-machine ssh $droplet_name mount -o discard,defaults,noatime "/dev/disk/by-id/scsi-0DO_Volume_$volume_name" $mount_point
             docker-machine ssh $droplet_name "echo '/dev/disk/by-id/scsi-0DO_Volume_$volume_name $mount_point ext4 defaults,nofail,discard 0 0' | sudo tee -a /etc/fstab"
             echo "End to mount  nfs server $droplet_name to volume $volume_name"
             break;
          else
             echo ".................. $droplet_name is not running . Current status is $status........................."
             sleep 1m
             continue
          fi
    done
done