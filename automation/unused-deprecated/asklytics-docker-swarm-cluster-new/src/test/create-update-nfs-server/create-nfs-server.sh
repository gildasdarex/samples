#!/usr/bin/env bash

#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""
asklytics_volumes_file_path=""



update_servers () {
    # $1 parameter is the name of the project to clone
    #get droplet id
    droplet_name=`echo ${1} | sed 's/\"//g'`
    echo "Add node $droplet_name info to servers.json file"
    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    echo "Node $droplet_name id is : $droplet_id"
    #get droplet private ip
    droplet_id=`echo ${droplet_id} | sed 's/\"//g'`
    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`
    echo "Node $droplet_name private ip is : $private_ip"

    #droplet_name=`echo ${1} | sed 's/\"//g'`
    private_ip=`echo ${private_ip} | sed 's/\"//g'`
    droplet_type=`echo ${2} | sed 's/\"//g'`

    servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
    > $asklytics_servers_file_path
    echo $servers >> $asklytics_servers_file_path
}

attach_volume_to_nfs_server () {
    # $1 parameter is the name of the project to clone
    #get droplet id
    droplet_name=`echo ${1} | sed 's/\"//g'`
    volume_name=`echo ${2} | sed 's/\"//g'`
    region=`echo ${3} | sed 's/\"//g'`
    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    droplet_id=`echo ${droplet_id} | sed 's/\"//g'`


    volume_id=`cat $asklytics_volumes_file_path | jq --arg volume_name "$volume_name" '.volumes | .[] | select(.name == $volume_name) | .volume_id'`
    volume_id=`echo ${volume_id} | sed 's/\"//g'`

    echo "Starting to attach  nfs server $droplet_name with id $droplet_id to volume $volume_name with id $volume_id"

    #curl -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"type\": \"attach\", \"droplet_id\": \"$droplet_id\", \"region\": \"$region\"}'" "https://api.digitalocean.com/v2/volumes/$volume_id/actions"

    cmd=` echo curl -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"type\": \"attach\", \"droplet_id\": \"$droplet_id\", \"region\": \"$region\"}'" "https://api.digitalocean.com/v2/volumes/$volume_id/actions"`
    echo $cmd
    eval $cmd

    # TODO - check if volume is attached with success

#    response_code=`curl -s -o /dev/null -w '%{http_code}' -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"type\": \"attach\", \"droplet_id\": \"$droplet_id\", \"region\": \"$region\"}'" "https://api.digitalocean.com/v2/volumes/$volume_id/actions"`
#
#    echo "End attach  nfs server $droplet_name with id $droplet_id to volume $volume_name with id $volume_id"
#
#    if [ $response_code != "202" ]; then
#        echo "failed to mount volume $volume_name with droplet $droplet_name "
#        exit 1
#    else
#        echo "mount volume $volume_name with droplet $droplet_name "
#    fi
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       do.users.data)
          do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z ${cluster_name+x} ]; then
 echo "cluster.name is missing "
 exit
fi

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"
asklytics_volumes_file_path="/asklytics/cluster/$cluster_name/output/volumes.json"


if [ ! -f $asklytics_servers_file_path ]
then
    mkdir -p /asklytics/cluster/$cluster_name/output/
    touch /asklytics/cluster/$cluster_name/output/servers.json
    echo {\"servers\": []} >> $asklytics_servers_file_path
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi

jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    snapshot_id=`echo ${i} | jq '.snapshot_id'`
    tags=`echo ${i} | jq '.tags'`
    volume_mapping=`echo ${i} | jq '.volume_mapping'`


    echo "Starting to create nfs server $name"
    cmd=`echo docker-machine create --driver digitalocean --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-userdata "$do_users_data_file" --digitalocean-tags "$tags" "$name"`
    eval $cmd
    echo "Ending the creation nfs server $name"

    sleep 2m

    echo "Add nfs server $name to servers json file"
    update_servers $name "nfs_server"

    droplet_name=`echo ${name} | sed 's/\"//g'`

    # TODO - check if volume is mounted with success


    echo "${volume_mapping}" | jq -c '.[]' | while read i; do
        volume_name=`echo ${i} | jq '.blockstorage_volume_name'`
        mount_point=`echo ${i} | jq '.nfs_server_mount_point'`

        mount_point=`echo ${mount_point} | sed 's/\"//g'`
        volume_name=`echo ${volume_name} | sed 's/\"//g'`

        attach_volume_to_nfs_server $name $volume_name $region
        echo "End to attach  nfs server $droplet_name to volume $volume_name"


        for i in $(seq 1 5); do

          status=`docker-machine status $droplet_name`
          if [ $status == "Running" ]
          then
             echo ".................. $droplet_name is running........................."

             echo "Starting to mount  nfs server $droplet_name to volume $volume_name"
             docker-machine ssh $droplet_name mkdir -p $mount_point </dev/null
             docker-machine ssh $droplet_name "sudo mkfs.ext4 /dev/disk/by-id/scsi-0DO_Volume_$volume_name" </dev/null
             docker-machine ssh $droplet_name "mount -o discard,defaults,noatime /dev/disk/by-id/scsi-0DO_Volume_$volume_name $mount_point" </dev/null
             #cmd=`echo docker-machine ssh $droplet_name "mount -o discard,defaults,noatime /dev/disk/by-id/scsi-0DO_Volume_$volume_name $mount_point"`
             #echo $cmd
             #eval $cmd

             #docker-machine ssh $droplet_name mount -o discard,defaults,noatime "/dev/disk/by-id/scsi-0DO_Volume_$volume_name" $mount_point
             docker-machine ssh $droplet_name "echo '/dev/disk/by-id/scsi-0DO_Volume_$volume_name $mount_point ext4 defaults,nofail,discard 0 0' | sudo tee -a /etc/fstab" </dev/null
             echo "End to mount  nfs server $droplet_name to volume $volume_name"
             break;
          else
             echo ".................. $droplet_name is not running . Current status is $status........................."
             sleep 1m
             continue
          fi
        done
    done
done