#!/bin/bash

echo "Deploying Application Databases ...."
. ../../registry.config.sh
echo ""
echo ""
REGISTRY_URL=$registry docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always db-layer
echo ""
echo "Done Deploying Application Databases ...."
echo " "
echo "Listing Docker Services ... "
docker service ls



