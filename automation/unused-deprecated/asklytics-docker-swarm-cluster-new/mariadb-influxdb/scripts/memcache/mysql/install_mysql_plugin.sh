#!/usr/bin/env bash


help_mesage(){
  echo "Usage : start_docker_mysql command "
  echo " "
  echo "Options : "
  echo "--env     environement that will be use to create docker container(dev, prod, qa )"
  echo " "
  echo "--username     the username to use to connect to mysql"
  echo " "
  echo "--passoword     the password to use to connect to mysql"
  echo " "
  echo "--plugin     the plugin to install(for memcached plugin, provide memcached as value  )"
  echo " "
  echo "--properties.file.location    the location of the properties files."
  echo " "
  echo "--properties.file.name   The name of the properties file. this properties file must be located in the path that you specified for <<--properties.file.location>> option"
  echo "                         You can have multiple one properties file for each environement. this script will read the properties file according to the value of <<--env>> "
  echo "                         option and must have .properties as extension"

  echo "                         example : start_docker_mysql --env dev --properties.file.location /path/to/location --properties.file.name mysql"
  echo "                         The script will read properties from file /path/to/location/mysql-dev.properties"
  echo " "
  echo " "

  echo "List of properties : "
  echo " "
  echo "mysql.version    the version of mysql to install"
  echo " "
  echo "mysql.host.publish.port    the port that will be used in your host to publish mysql service that run into the container"
  echo " "
  echo "mysql.root.password    the root user password"
  echo " "
  echo "mysql.custom.config.file.location    the location of your custom mysql config. Into this dir your need to create a file my-custom.cnf and put your mysql config here"
  echo " "
  echo "mysql.container.storage.dir    the dir that will be used by the container to store data"
}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       help)
          help_mesage
          exit
          ;;
       server.ip)
          server_ip_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       username)
          username_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       password)
          password_value="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       script.path)
          script_path="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done



mysql --host="$server_ip_value" --user="$username_value" --password="$password_value"  -e "source $script_path"
mysql --host="$server_ip_value" --user="$username_value" --password="$password_value"  -e "INSTALL PLUGIN daemon_memcached soname 'libmemcached.so'"
