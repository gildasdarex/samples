#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       do.token.file)
          do_token_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done


export do_token=$(cat "$do_token_file")
main_manager_name="not-defined"
main_manager_name_file_path=$HOME/main_manager_name.txt

main_manager_name=$(cat "$main_manager_name_file_path")
# Initialize Swarm Manager and tokens
main_manager_name=`echo ${main_manager_name} | sed 's/\"//g'`

node_list_file_path=$HOME/label_nodes_list.txt
nodes_list=""

jq -c '.[]' "$nodes_file" | while read i; do
    name=`echo ${i} | jq '.name'`
    apps=`echo ${i} | jq '.apps'`

    node_name=`echo ${name} | sed 's/\"//g'`
    apps=`echo ${apps} | sed 's/\"//g'`
    nodes_list="$node_name:$apps;$nodes_list"

    touch $node_list_file_path
    > "$node_list_file_path"
    echo "$nodes_list" >> "$node_list_file_path"
done


nodes=`cat $node_list_file_path`
nodes=`echo ${nodes} | sed 's/\"//g'`


for node in $(echo $nodes | sed "s/;/ /g")
do
    nodes_infos=(${node//:/ })
    name=${nodes_infos[0]}
    apps=${nodes_infos[1]}

    if [ ! -z "$apps" -a "$apps" != " " -a "$apps" != "null" ]; then
        name=`echo ${name} | sed 's/\"//g'`
        apps=`echo ${apps} | sed 's/\"//g'`

        for app in $(echo $apps | sed "s/,/ /g")
        do
            docker-machine ssh "$main_manager_name" "docker node update \
            --label-add ${app}=true  ${name}"
        done
    fi

#    name=`echo ${name} | sed 's/\"//g'`
#    apps=`echo ${apps} | sed 's/\"//g'`
#
#
#    for app in $(echo $apps | sed "s/,/ /g")
#    do
#        docker-machine ssh "$main_manager_name" "docker node update \
#        --label-add ${app}=true  ${name}"
#    done
done