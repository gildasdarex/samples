#!/bin/bash

. config-blu.sh
. ../registry.config.sh

MODE="intg"
optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
      version)
        VERSION="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      wheels.dir)
        WHL_PATH="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      pylytics.wheels.dir)
        PYLYTICS_PATH="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      pylytics.version)
        PYLYTICS_VERSION="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      apps)
        APPS_IN="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      mode)
        MODE="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
    esac
done

if [ "$MODE" == "prod" ]; then
    REG_URL=$prod_registry
else
    REG_URL=$intg_registry
fi

docker login $REG_URL
if [ $? -ne 0 ]
    then
        echo "Cannot login to registry. Please try again."
        exit 1
    fi

if [[ -n "$APPS_IN" ]]; then
  IFS=',' read -ra APPS_TO_EXEC <<< "$APPS_IN"
else
  APPS_TO_EXEC=("${APPS[@]}")
fi  
echo "Apps to Dockerize : ${APPS_TO_EXEC[*]}"


rm -rf $temp_work_dir_path
mkdir $temp_work_dir_path
cp -r ./docker-src/* $temp_work_dir_path
cp -r $WHL_PATH/* $temp_work_dir_path

rm -rf $temp_work_dir_pylytics_path
mkdir $temp_work_dir_pylytics_path
cp -r $PYLYTICS_PATH/* $temp_work_dir_pylytics_path
cd $temp_work_dir_pylytics_path
latest_pylytics_whl=`ls -t1 $PYLYTICS_APP_NAME-$PYLYTICS_VERSION* |  head -n 1`
cp $PYLYTICS_PATH/$latest_pylytics_whl $temp_work_dir_path

cd $temp_work_dir_path

for i in "${APPS_TO_EXEC[@]}"
do
    app_name_in_whl=${APPS_NAME_IN_WHEEL_MAP[$i]}
    latest_whl=`ls -t1 $app_name_in_whl-$VERSION* |  head -n 1`
    #get snapshot timestamp is present in the whl
    snapshot=$latest_whl
    snapshot=${snapshot#$app_name_in_whl-$VERSION}
    snapshot=${snapshot%-py3-none-any.whl}
    snapshot=${snapshot#_}
  
    version_with_snapshot="$VERSION"
    if [ ! -z "$snapshot" -a "$snapshot" != " " ]; then
     version_with_snapshot="$VERSION-$snapshot"
    fi

    cmd="docker build --no-cache -t $app_name_in_whl:v$version_with_snapshot -t $app_name_in_whl:latest \
        --build-arg version=$version_with_snapshot \
        --build-arg whl=$latest_whl  \
        --build-arg pylytics_whl=$latest_pylytics_whl \
        --build-arg app_name=$app_name_in_whl\
        --file=./Dockerfile ."

   $cmd

    docker tag $app_name_in_whl:v$version_with_snapshot $REG_URL/$app_name_in_whl:v$version_with_snapshot
    docker push $REG_URL/$app_name_in_whl:v$version_with_snapshot

    docker tag $app_name_in_whl:latest $REG_URL/$app_name_in_whl:latest 
    docker push $REG_URL/$app_name_in_whl:latest
done
