#!/bin/bash

. config.sh
. ../registry.config.sh

MODE="intg"
optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
      version)
        VERSION="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      wheels.dir)
        WHL_PATH="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      apps)
        APPS_IN="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      mode)
        MODE="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
    esac
done

if [ "$MODE" == "prod" ]; then
    REG_URL=$prod_registry
else
    REG_URL=$intg_registry
fi

docker login $REG_URL
if [ $? -ne 0 ]
    then
        echo "Cannot login to registry. Please try again."
        exit 1
    fi

if [[ -n "$APPS_IN" ]]; then
  IFS=',' read -ra APPS_TO_EXEC <<< "$APPS_IN"
else
  APPS_TO_EXEC=("${APPS[@]}")
fi  
echo "Apps to Dockerize : ${APPS_TO_EXEC[*]}"



cur_dir=$(pwd)
rm -rf /temp-whls-unpacked
mkdir /temp-whls-unpacked
cd /temp-whls-unpacked
 declare -A BUILD_NUMS 
for i in "${APPS_TO_EXEC[@]}"
do
   app_name_in_whl=${APPS_NAME_IN_WHEEL_MAP[$i]}
    latest_whl=`ls -t1 $app_name_in_whl-$VERSION* |  head -n 1`

    mkdir $app_name_in_whl
   cd $app_name_in_whl
    latest_war=`ls -t1 $WHL_PATH/$app_name_in_whl-$VERSION* |  head -n 1`
    unzip $latest_war
    file=/temp-whls-unpacked/$app_name_in_whl/pylytics/build.txt
   echo $file
   if [ -f "$file" ]
   then
    while IFS=': ' read -r key value
    do
    echo $key
     key=$(echo $key | tr '-' '_')
    echo $key $value
    BUILD_NUMS[${app_name_in_whl}_${key}]=${value}
    done < "$file"
  fi
   cd ..
done

cd $cur_dir

rm -rf $temp_work_dir_path
mkdir $temp_work_dir_path
cp -r ./docker-src/* $temp_work_dir_path
cp -r $WHL_PATH/* $temp_work_dir_path

cd $temp_work_dir_path

for i in "${APPS_TO_EXEC[@]}"
do
    app_name_in_whl=${APPS_NAME_IN_WHEEL_MAP[$i]}
    latest_whl=`ls -t1 $app_name_in_whl-$VERSION* |  head -n 1`
    #get snapshot timestamp is present in the whl
    snapshot=$latest_whl
    snapshot=${snapshot#$app_name_in_whl-$VERSION}
    snapshot=${snapshot%-py3-none-any.whl}
    snapshot=${snapshot#_}
     implbld="_Implementation_Build"
   appBuild=${BUILD_NUMS[${app_name_in_whl}${implbld}]}


    version_with_snapshot="$VERSION"
    if [ ! -z "$snapshot" -a "$snapshot" != " " ]; then
     version_with_snapshot="$VERSION-$snapshot"
    fi
    echo "docker build --no-cache -t $app_name_in_whl:v$VERSION-$appBuild -t $app_name_in_whl:latest \
 --build-arg version=$VERSION-$appBuild \
 --build-arg whl=$latest_whl  
 --build-arg app_name=$app_name_in_whl\
 --file=./Dockerfile ."

    cmd="docker build --no-cache -t $app_name_in_whl:v$VERSION-$appBuild -t $app_name_in_whl:latest \
        --build-arg version=$VERSION-$appBuild \
        --build-arg whl=$latest_whl  
        --build-arg app_name=$app_name_in_whl\
        --file=./Dockerfile ."
    $cmd

    echo "Tagging and Pushing to Docker Registry ..."
    echo 'Running : docker tag ' $app_name_in_whl':v'$VERSION-$appBuild $REG_URL'/'$app_name_in_whl':v'$VERSION'-'$appBuild
    docker tag $app_name_in_whl:v$VERSION-$appBuild $REG_URL/$app_name_in_whl:v$VERSION-$appBuild

    echo '!!! If docker push fails, please run the command below again !!!'
    echo 'Running :  docker push '$REG_URL'/'$app_name_in_whl':v'$VERSION'-'$appBuild
    docker push $REG_URL/$app_name_in_whl:v$VERSION-$appBuild

    echo 'Running : docker tag '$app_name_in_whl':latest' $REG_URL'/'$app_name_in_whl':latest'

    docker tag $app_name_in_whl:latest $REG_URL/$app_name_in_whl:latest
    echo '!!! If docker push fails, please run the command below again !!!'
    echo 'Running : docker push' $REG_URL'/'$app_name_in_whl':latest'
    docker push $REG_URL/$app_name_in_whl:latest
done
