#!/bin/bash

temp_work_dir_path="/home/temp_docker_tests_apps"
temp_work_dir_pylytics_path="/home/temp_docker_pylytics_apps"


APPS[0]="pylytics_tests"


typeset -A APPS_NAME_IN_WHEEL_MAP
APPS_NAME_IN_WHEEL_MAP["pylytics_tests"]="asklytics_algos_tests"


PYLYTICS_APP_NAME="asklytics_algos"