temp_work_dir_path="/home/temp_docker_backend_apps"

APPS[0]="asklytics-infra"
APPS[1]="asklytics-prov-service"
APPS[2]="asklytics-camel-analytics-service"
APPS[3]="asklytics-camel-provisioning-service"
APPS[4]="asklytics-analytics-service"
APPS[5]="asklytics-ui-data-service"
APPS[6]="asklytics-ml-api-app"
