#!/bin/bash

. config.sh
. ../registry.config.sh

printarr() { declare -n __p="$1"; for k in "${!__p[@]}"; do printf "%s=%s\n" "$k" "${__p[$k]}" ; done ;  }  

MODE="intg"
optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
      version)
        VERSION="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      asklytics.db.utility.jar.path)
        asklytics_db_utility_jar_path="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      asklytics.db.utility.jar.name)
        asklytics_db_utility_jar_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      wars.dir)
        WARS_PATH="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      apps)
        APPS_IN="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      mode)
        MODE="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
    esac
done

if [ "$MODE" == "prod" ]; then
    REG_URL=$prod_registry
else
    REG_URL=$intg_registry
fi


docker login $REG_URL
if [ $? -ne 0 ]
    then
        echo "Cannot login to registry. Please try again."
        exit 1
    fi

if [[ -n "$APPS_IN" ]]; then
  IFS=',' read -ra APPS_TO_EXEC <<< "$APPS_IN"
else
  APPS_TO_EXEC=("${APPS[@]}")
fi  
echo "Apps to Dockerize : ${APPS_TO_EXEC[*]}"


cur_dir=$(pwd)
rm -rf /temp-wars-unpacked
mkdir /temp-wars-unpacked
cd /temp-wars-unpacked
 declare -A BUILD_NUMS 
for i in "${APPS_TO_EXEC[@]}"
do
    app=$i
    mkdir $app
   cd $app
    latest_war=`ls -t1 $WARS_PATH/$app-$VERSION* |  head -n 1`
    unzip $latest_war
    file=/temp-wars-unpacked/$app/WEB-INF/classes/META-INF/MANIFEST.MF
   echo $file
   if [ -f "$file" ]
   then
    while IFS=': ' read -r key value
    do
    echo $key
     key=$(echo $key | tr '-' '_')
    echo $key $value
    BUILD_NUMS[${app}_${key}]=${value}
    done < "$file"
  fi
   cd ..
done


cd $cur_dir
rm -rf $temp_work_dir_path
mkdir $temp_work_dir_path
cp -r ./docker-src/* $temp_work_dir_path
cp -r $WARS_PATH/* $temp_work_dir_path
#cp $asklytics_db_utility_jar $temp_work_dir_path/.

cd $temp_work_dir_path

for i in "${APPS_TO_EXEC[@]}"
do
    app=$i
    latest_war=`ls -t1 $app-$VERSION* |  head -n 1`
    #get snapshot timestamp is present in the war
    snapshot=$latest_war
    snapshot=${snapshot#$app-$VERSION}
    snapshot=${snapshot%.war}
    snapshot=${snapshot#-}
   implbld="_Implementation_Build"
   appBuild=${BUILD_NUMS[${app}${implbld}]}
    version_with_snapshot="$VERSION"
    if [ ! -z "$snapshot" -a "$snapshot" != " " ]; then
     version_with_snapshot="$VERSION-$snapshot"
    fi
    
    cmd="docker build --no-cache -t $i:v$VERSION-$appBuild -t $i:latest \
        --build-arg version=$VERSION-$appBuild \
        --build-arg war=$latest_war
        --build-arg asklytics_db_utility=$asklytics_db_utility_jar_name
        --build-arg app_name=$app\
        --file=./Dockerfile ."
    $cmd
    echo " "
    echo "Tagging and Pushing to Docker Registry ..."
    echo 'Running : docker tag '$i':v'$VERSION'-'$appBuild $REG_URL'/'$i':v'$VERSION'-'$appBuild
    docker tag $i:v$VERSION-$appBuild $REG_URL/$i:v$VERSION-$appBuild
    echo '!!! If docker push fails, please run the command below again !!!'
    echo 'Running : docker push '$REG_URL'/'$i':v'$VERSION'-'$appBuild
    docker push $REG_URL/$i:v$VERSION-$appBuild
    echo " "
    echo 'Running : docker tag '$i':latest' $REG_URL'/'$i':latest'
    docker tag $i:latest $REG_URL/$i:latest
    echo '!!! If docker push fails, please run the command below again !!!'
    echo 'Running : docker push '$REG_URL'/'$i':latest'
    docker push $REG_URL/$i:latest
done
