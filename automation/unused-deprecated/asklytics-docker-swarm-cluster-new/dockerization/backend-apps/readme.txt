
./build-backend-apps-docker.sh
builds the backend java app docker
arguments
-v (required) = version of wars creating the docker of.
-d (required) = directory where the docker is present. The latest war of the version will be used
                to create the docker.
-a (optional) = comma separated value of app names to build the docker of.
                if omitted all the apps specified in the config.sh file will be built

example :
./build-backend-apps-docker.sh -d /home/george/dev/repos/wars -v 1.0.14 -a asklytics-infra,asklytics-analytics-service,asklytics-ui-data-service

config.sh
is the config file used to build dockers
temp_work_dir_path = temp working dir of the script. Its here the wars and dockerfile is copied
for the dockerfile to be in context.
APPS = array of all app names.