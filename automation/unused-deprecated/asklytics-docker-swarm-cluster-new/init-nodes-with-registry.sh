#!/usr/bin/env bash

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       registry.ip)
          registry_ip="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       username)
          username="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       password)
          password="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;

    esac

done

jq -c '.[]' "$nodes_file" | while read i; do {
    node_type=`echo ${i} | jq '.node_type'`
    name=`echo ${i} | jq '.name'`
    name_clean=$(echo $name | tr -d '"')

        echo $name_clean
    docker-machine scp -r ./properties/docker $name_clean:/etc/

    docker-machine ssh $name_clean -t "service docker restart;"
    docker-machine ssh $name_clean -t "docker login ${registry_ip} -u ${username} -p ${password}"

} < /dev/null; done
