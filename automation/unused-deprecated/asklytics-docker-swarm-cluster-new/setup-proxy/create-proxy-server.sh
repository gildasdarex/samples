#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""
select_main_manager="false"
select_main_manager_name=""





optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       do.users.data)
          do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z ${cluster_name+x} ]; then
 echo "cluster.name is missing "
 exit
fi

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


if [ ! -f $asklytics_servers_file_path ]
then
    mkdir -p /asklytics/cluster/$cluster_name/output/
    touch /asklytics/cluster/$cluster_name/output/servers.json
    echo {\"servers\": []} >> $asklytics_servers_file_path
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi

jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    snapshot_id=`echo ${i} | jq '.snapshot_id'`
    tags=`echo ${i} | jq '.tags'`
    #volume_id=`echo ${i} | jq '.volume_id'`

    echo "..........................Starting to create cluster node $name...................................."
    cmd=`echo docker-machine create --driver digitalocean --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-userdata "$do_users_data_file" --digitalocean-tags "$tags" "$name"`
    echo $cmd
    eval $cmd
    echo "..........................Finish to create cluster node $name...................................."

     node_type=`echo ${node_type} | sed 's/\"//g'`
     name=`echo ${name} | sed 's/\"//g'`

    echo "..........................Check if cluster node $name is running or not...................................."

    for i in $(seq 1 5); do
      status=`docker-machine status $name`
      if [ $status == "Running" ]
      then
         echo ".................. $droplet_name is running........................."
         break;
      else
         echo ".................. $droplet_name is not running . Current status is $status........................."
         sleep 1m
         continue
      fi
    done

     echo "..........................Finish to check if cluster node $name is running or not...................................."



    if [ $node_type == "manager" ]; then
        if [ $select_main_manager == "false" ]; then
          update_servers $name $node_type "main_manager"
          select_main_manager="true"
          select_main_manager_name=$name
        else
            update_servers $name $node_type
        fi;
     else
        update_servers $name $node_type
    fi;

    echo "..........................Finish to create cluster node $name...................................."

done


main_manager_name=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`
main_manager_name=`echo ${main_manager_name} | sed 's/\"//g'`
manager_node_ip=`docker-machine ip $main_manager_name`

echo "..........................Main manager node is  $main_manager_name...................................."


docker-machine ssh $main_manager_name docker swarm init --listen-addr $manager_node_ip --advertise-addr $manager_node_ip

worker_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token worker -q")

manager_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token manager -q")



jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    name=`echo ${i} | jq '.name'`
    labels=`echo ${i} | jq '.labels'`

    node_type=`echo ${node_type} | sed 's/\"//g'`
    name=`echo ${name} | sed 's/\"//g'`
    labels=`echo ${labels} | sed 's/\"//g'`

    if [ $name != $main_manager_name ]; then
        add_server_to_swarm $node_type $name $main_manager_name $manager_token $worker_token $manager_node_ip
    fi;

    for label in $(echo $labels | sed "s/,/ /g")
    do
        echo "............... Add label $label to node $name.................."
        #https://unix.stackexchange.com/questions/66154/ssh-causes-while-loop-to-stop
        docker-machine ssh $main_manager_name "docker node update --label-add ${label}=true  ${name}" </dev/null
    done

done