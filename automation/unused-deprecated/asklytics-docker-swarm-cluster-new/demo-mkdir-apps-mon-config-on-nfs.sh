echo "Creating directories for Config of App Dbs ...."
mkdir -p /mnt/demo_vol_configs/configs/apps/config/db/mariadb
mkdir -p /mnt/demo_vol_configs/configs/apps/config/db/influxdb
mkdir -p /mnt/demo_vol_configs/configs/apps/config/db/elasticsearch
echo "Finished creating directories for Config of App Dbs"
echo " "
echo " "
echo " Listing directories ... "
ls -R /mnt/demo_vol_configs/configs/apps | awk '
/:$/&&f{s=$0;f=0}
/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}
NF&&f{ print s"/"$0 }'
echo " "

echo "Creating directories for Config of Monitoring  ...."

mkdir -p /mnt/demo_vol_configs/configs/monitoring/config/db/prometheus
mkdir -p /mnt/demo_vol_configs/configs/monitoring/config/app/grafana
mkdir -p /mnt/demo_vol_configs/configs/monitoring/config/app/alertmanager
mkdir -p /mnt/demo_vol_configs/configs/monitoring/config/app/elk
echo "Finished creating directories for Config of Monitoring"
echo " "
echo " "
echo " Listing directories ... "
ls -R /mnt/demo_vol_configs/configs/monitoring | awk '
/:$/&&f{s=$0;f=0}
/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}
NF&&f{ print s"/"$0 }'
echo " "
