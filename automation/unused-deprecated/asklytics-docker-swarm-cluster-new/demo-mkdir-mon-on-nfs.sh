echo "Creating directories for monitoring storage ...."
mkdir -p /mnt/demo_vol_storage_mon/monitoring/storage/mon/db/prometheus
mkdir -p /mnt/demo_vol_storage_mon/monitoring/storage/mon/app/grafana
mkdir -p /mnt/demo_vol_storage_mon/monitoring/storage/mon/app/alertmanager
mkdir -p /mnt/demo_vol_storage_mon/monitoring/storage/mon/app/elk
echo "Finished creating directories for monitoring storage"
echo " "
echo " Listing directories ... "
ls -R /mnt/demo_vol_storage_mon | awk '
/:$/&&f{s=$0;f=0}
/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}
NF&&f{ print s"/"$0 }'
echo " "

