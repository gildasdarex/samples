var supervisor = require("supervisord-eventlistener");

const IncomingWebhook = require('@slack/client').IncomingWebhook;
const url = process.env.SLACK_WEBHOOK_URL;
const webhook = new IncomingWebhook(url);


supervisor.on("event", function(type, headers, data) {
     var events = ["PROCESS_STATE_STARTING","PROCESS_STATE_RUNNING","PROCESS_STATE_BACKOFF","PROCESS_STATE_EXITED","PROCESS_STATE_STOPPED","PROCESS_STATE_FATAL",
"PROCESS_STATE_UNKNOWN"]	
    if(events.indexOf(type)>-1){
			
	
var d = new Date();
var n = d.toUTCString();
var  sendString = " Intg Setup : "+data.processname+" : "+type+" at "+n 
    webhook.send(sendString, function(err, res) {});

}

});
supervisor.listen(process.stdin, process.stdout);
