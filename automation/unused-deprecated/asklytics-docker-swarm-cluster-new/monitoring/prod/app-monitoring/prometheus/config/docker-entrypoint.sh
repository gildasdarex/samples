#!/bin/sh -e

cat /etc/prometheus/prometheus.yml > /tmp/prometheus.yml
cat /etc/prometheus/weave-cortex.yml | \
    sed "s@#password: <token>#@password: '$WEAVE_TOKEN'@g" > /tmp/weave-cortex.yml

sed "s/manager_ip/$PROD_MANAGER_IP_NODE/" /etc/prometheus/python-app-targets.json > /etc/prometheus/python-app-temp-targets.json
mv /etc/prometheus/python-app-temp-targets.json /etc/prometheus/python-app-targets.json

sed "s/manager_ip/$PROD_MANAGER_IP_NODE/" /etc/prometheus/spring-app-targets.json > /etc/prometheus/spring-app-temp-targets.json
mv /etc/prometheus/spring-app-temp-targets.json /etc/prometheus/spring-app-targets.json

sed "s/manager_ip/$PROD_MANAGER_IP_NODE/" /etc/prometheus/ui-app-targets.json > /etc/prometheus/ui-app-temp-targets.json
mv /etc/prometheus/ui-app-temp-targets.json /etc/prometheus/ui-app-targets.json

sed "s/ext_proxy/$EXT_PROXY_IP_NODE/" /etc/prometheus/nginx-targets.json > /etc/prometheus/nginx-temp-1-targets.json
mv /etc/prometheus/nginx-temp-1-targets.json /etc/prometheus/nginx-targets.json

sed "s/int_proxy/$INT_PROXY_IP_NODE/" /etc/prometheus/nginx-targets.json > /etc/prometheus/nginx-temp-2-targets.json
mv /etc/prometheus/nginx-temp-2-targets.json /etc/prometheus/nginx-targets.json



#JOBS=mongo-exporter:9111 redis-exporter:9112

if [ ${JOBS+x} ]; then

for job in $JOBS
do
echo "adding job $job"

SERVICE=$(echo "$job" | cut -d":" -f1)
PORT=$(echo "$job" | cut -d":" -f2)

cat >>/tmp/prometheus.yml <<EOF
  - job_name: '${SERVICE}'
    dns_sd_configs:
    - names:
      - 'tasks.${SERVICE}'
      type: 'A'
      port: ${PORT}
EOF

cat >>/tmp/weave-cortex.yml <<EOF
  - job_name: '${SERVICE}'
    dns_sd_configs:
    - names:
      - 'tasks.${SERVICE}'
      type: 'A'
      port: ${PORT}
EOF

done

fi

mv /tmp/prometheus.yml /etc/prometheus/prometheus.yml
mv /tmp/weave-cortex.yml /etc/prometheus/weave-cortex.yml

set -- /bin/prometheus "$@"

exec "$@"
