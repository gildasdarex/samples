#!/bin/bash

source prod-env.cnf
docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always mon
