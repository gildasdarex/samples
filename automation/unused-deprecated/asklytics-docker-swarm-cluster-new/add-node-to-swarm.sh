#!/usr/bin/env bash

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
        name)
          name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        docker.manager)
            docker_manager="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
        node.type)
            node_type="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
        node.labels)
          node_labels="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
    esac
done

export worker_token=$(docker-machine ssh $docker_manager "docker swarm \
    join-token worker -q")

export manager_token=$(docker-machine ssh $docker_manager "docker swarm \
    join-token manager -q")

if [ $node_type == "manager" ]
then
    node_token=$manager_token
else
    node_token=$worker_token
fi

docker-machine ssh "$name" "docker swarm join \
        --token=${node_token} \
            --listen-addr $(docker-machine ip ${name}) \
                --advertise-addr $(docker-machine ip ${name}) \
                    $(docker-machine ip ${docker_manager})"


if [[ -n "$node_labels" ]]; then
  IFS=',' read -ra LABELS <<< "$node_labels"
  for label in "${LABELS[@]}"
    do
        docker-machine ssh "$docker_manager" "docker node update \
        --label-add ${label}=true  ${name}"
    done
fi
