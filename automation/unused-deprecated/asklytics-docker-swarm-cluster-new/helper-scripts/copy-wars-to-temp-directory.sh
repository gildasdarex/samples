  #!/bin/bash

  optspec=":-:"
  while getopts "$optspec" optchar; do
      case "${OPTARG}" in
        maven.repo.dir)
          LOCAL_MAVEN_REPO_PATH="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        destination.dir)
          GENERATED_FOLDER_NAME="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      esac
  done


  ## Store common commands in to variables
  delete_previous_generated_folder="rm -Rf $GENERATED_FOLDER_NAME"
  make_generated_folder="mkdir -p $GENERATED_FOLDER_NAME"

  # Process start

  move_to_maven_repo="cd $LOCAL_MAVEN_REPO_PATH"
  $move_to_maven_repo

  #Delete previously generated folder
  if [ -d "$GENERATED_FOLDER_NAME" ]; then $delete_previous_generated_folder; fi
  $make_generated_folder

  #Move all wars to generated folder
  find . -maxdepth 10 -mindepth 1 -type d -exec sh -c '(echo {} && cd {} && cp -p *.war '$GENERATED_FOLDER_NAME' && echo)' \;
