#!/bin/bash

. ../registry.config.sh

optspec=":-:"
  while getopts "$optspec" optchar; do
      case "${OPTARG}" in
       	version)
          VERSION="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        build)
          BUILD="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      esac
  done

REGISTRY_URL=$registry VERSION=$VERSION BUILD=$BUILD docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always app-layer
