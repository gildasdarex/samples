#!/bin/bash

. ../registry.config.sh

REGISTRY_URL=$registry docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always app-layer