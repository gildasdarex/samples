#!/bin/bash

echo "Using Docker Registry 157.230.155.170:5000 ..."
export REGISTRY_URL=157.230.155.170:5000

echo "Deploying Backend Apps on Swarm ..." 
echo "  "
echo "  "

docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always app-layer
echo "  "
echo "  "
echo "Finished Deploying Backend Apps on Swarm!"

echo "Waiting  10 seconds before listing (sometimes it take a bit for services to get running) ... "
echo "If service shows as down, try listing services again using 'docker service ls'"
sleep 10
echo "  "
echo "  "
echo "Listing the docker services ...."
echo " "
docker service ls

