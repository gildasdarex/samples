echo "Existing ...since it  -n overwrite has not been tested .. and dest/ backup needs to be made before a copy.."

#echo "Copying properties for AskLytics App configuration (will NOT overwrite existing)...."
#cp -a -r -n ./deployment/demo-setup/apps/properties/config/sentry/. /mnt/demo_vol_configs/configs/apps/config/app/sentry/
#cp -a -r -n ./deployment/demo-setup/apps/properties/config/log4j2/. /mnt/demo_vol_configs/configs/apps/config/app/log4j2/
#cp -a -r -n ./deployment/demo-setup/apps/properties/config/spring/. /mnt/demo_vol_configs/configs/apps/config/app/spring/
#cp -a -r -n ./deployment/demo-setup/apps/properties/config/app-metadata/. /mnt/demo_vol_configs/configs/apps/config/app/app-properties/
#cp -a -r -n ./deployment/demo-setup/apps/properties/config/priceModel /mnt/demo_vol_configs/configs/apps/config/app/app-properties/
#cp -a -r -n ./deployment/demo-setup/apps/properties/config/logstash /mnt/demo_vol_configs/configs/apps/config/app/app-properties/
#cp -a -r -n ./deployment/demo-setup/ui-apps/properties/* /mnt/demo_vol_configs/configs/apps/config/app/ui-properties/

#echo "Finished copying properties for AskLytics App configuration (did NOT overwrite existing files)"
#echo " "
#echo " "
#echo " Listing all properties files ... "

#find /mnt/demo_vol_configs/configs/apps/config/app -type f -follow -print|xargs ls -l

#echo " "
