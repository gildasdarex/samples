echo "Creating directories for Config of AskLytics Apps  ...."

mkdir -p /mnt/demo_vol_configs/configs/apps/config/app/sentry/
mkdir -p /mnt/demo_vol_configs/configs/apps/config/app/log4j2/
mkdir -p /mnt/demo_vol_configs/configs/apps/config/app/spring/
mkdir -p /mnt/demo_vol_configs/configs/apps/config/app/app-properties/
mkdir -p /mnt/demo_vol_configs/configs/apps/config/app/ui-properties/

echo "Finished creating directories for Config of AskLytic appsg"
echo " "
echo " "
echo " Listing exiting and new directories for apps configuration ... "
ls -R /mnt/demo_vol_configs/configs/apps | awk '
/:$/&&f{s=$0;f=0}
/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}
NF&&f{ print s"/"$0 }'
echo " "
