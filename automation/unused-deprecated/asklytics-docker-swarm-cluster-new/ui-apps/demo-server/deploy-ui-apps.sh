#!/bin/bash

echo "Deploying  UI Applications Docker Service..."

. ../registry.config.sh

REGISTRY_URL=$registry docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always ui-layer

echo ""
echo ""
REGISTRY_URL=$registry docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always db-layer
echo ""
echo "Done Deploying UI Applications Docker Service"
echo " "
echo "Listing Docker Services (waiting 10 seconds before listing; sometimes it takes time to comeup; use docker service ls to verify)... "
docker service ls
