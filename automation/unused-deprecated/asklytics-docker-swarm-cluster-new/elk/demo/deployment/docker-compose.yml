version: '3.7'

services:
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.3.0
    logging:
      driver: "json-file"
      options:
        max-file: ${DOCKER_LOG_MAX_FILE:?Docker log max number(DOCKER_LOG_MAX_FILE) is unset or empty}
        max-size: ${DOCKER_LOG_MAX_SIZE:?Docker log fmax size per file(DOCKER_LOG_MAX_SIZE) is unset or empty}
    environment:
      - "ES_JAVA_OPTS=-Xms1g -Xmx1g"
      - "discovery.zen.minimum_master_nodes=2"
      - "node.master=false"
      - "node.data=false"
      - "node.ingest=false"
      - node.name=es
      - cluster.name=demo-asklytics-elk-cluster
      - http.cors.enabled=true
      - http.cors.allow-origin=*
    volumes:
      - /asklytics/storage/mon/storage/mon/db/elk/elasticsearch/es:/usr/share/elasticsearch/data
    networks:
      - elk
    ports:
      - "9200:9200"
      - "9300:9300"
    deploy:
      mode: global
      placement:
        constraints: [node.labels.demo-elk==true]

  master-01:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.3.0
    logging:
      driver: "json-file"
      options:
        max-file: ${DOCKER_LOG_MAX_FILE:?Docker log max number(DOCKER_LOG_MAX_FILE) is unset or empty}
        max-size: ${DOCKER_LOG_MAX_SIZE:?Docker log fmax size per file(DOCKER_LOG_MAX_SIZE) is unset or empty}
    environment:
      - "ES_JAVA_OPTS=-Xms1g -Xmx1g"
      - "discovery.zen.minimum_master_nodes=2"
      - "discovery.zen.ping.unicast.hosts=elasticsearch"
      - "node.master=true"
      - "node.data=false"
      - "node.ingest=false"
      - node.name=master-01
      - cluster.name=demo-asklytics-elk-cluster
      - http.cors.enabled=true
      - http.cors.allow-origin=*
    volumes:
      - /asklytics/storage/mon/storage/mon/db/elk/elasticsearch/master-01:/usr/share/elasticsearch/data
    networks:
      - elk
    depends_on:
      - elasticsearch
    deploy:
      mode: global
      placement:
        constraints: [node.labels.demo-elk==true]

  master-02:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.3.0
    logging:
      driver: "json-file"
      options:
        max-file: ${DOCKER_LOG_MAX_FILE:?Docker log max number(DOCKER_LOG_MAX_FILE) is unset or empty}
        max-size: ${DOCKER_LOG_MAX_SIZE:?Docker log fmax size per file(DOCKER_LOG_MAX_SIZE) is unset or empty}
    environment:
      - "ES_JAVA_OPTS=-Xms1g -Xmx1g"
      - "discovery.zen.minimum_master_nodes=2"
      - "discovery.zen.ping.unicast.hosts=elasticsearch"
      - "node.master=true"
      - "node.data=false"
      - "node.ingest=false"
      - node.name=master-02
      - cluster.name=demo-asklytics-elk-cluster
      - http.cors.enabled=true
      - http.cors.allow-origin=*
    volumes:
      - /asklytics/storage/mon/storage/mon/db/elk/elasticsearch/master-02:/usr/share/elasticsearch/data
    networks:
      - elk
    depends_on:
      - elasticsearch
    deploy:
      mode: global
      placement:
        constraints: [node.labels.demo-elk==true]

  master-03:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.3.0
    logging:
      driver: "json-file"
      options:
        max-file: ${DOCKER_LOG_MAX_FILE:?Docker log max number(DOCKER_LOG_MAX_FILE) is unset or empty}
        max-size: ${DOCKER_LOG_MAX_SIZE:?Docker log fmax size per file(DOCKER_LOG_MAX_SIZE) is unset or empty}
    environment:
      - "ES_JAVA_OPTS=-Xms1g -Xmx1g"
      - "discovery.zen.minimum_master_nodes=2"
      - "discovery.zen.ping.unicast.hosts=elasticsearch"
      - "node.master=true"
      - "node.data=false"
      - "node.ingest=false"
      - node.name=master-03
      - cluster.name=demo-asklytics-elk-cluster
      - http.cors.enabled=true
      - http.cors.allow-origin=*
    volumes:
      - /asklytics/storage/mon/storage/mon/db/elk/elasticsearch/master-03:/usr/share/elasticsearch/data
    networks:
      - elk
    depends_on:
      - elasticsearch
    deploy:
      mode: global
      placement:
        constraints: [node.labels.demo-elk==true]

  data:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.3.0
    logging:
      driver: "json-file"
      options:
        max-file: ${DOCKER_LOG_MAX_FILE:?Docker log max number(DOCKER_LOG_MAX_FILE) is unset or empty}
        max-size: ${DOCKER_LOG_MAX_SIZE:?Docker log fmax size per file(DOCKER_LOG_MAX_SIZE) is unset or empty}
    environment:
      - "ES_JAVA_OPTS=-Xms1g -Xmx1g"
      - "discovery.zen.minimum_master_nodes=2"
      - "discovery.zen.ping.unicast.hosts=elasticsearch"
      - "node.master=false"
      - "node.data=true"
      - "node.ingest=false"
      - node.name=data
      - cluster.name=demo-asklytics-elk-cluster
      - http.cors.enabled=true
      - http.cors.allow-origin=*
    volumes:
      - /asklytics/storage/mon/storage/mon/db/elk/elasticsearch/data-node:/usr/share/elasticsearch/data
    networks:
      - elk
    depends_on:
      - elasticsearch
    deploy:
      mode: global
      placement:
        constraints: [node.labels.demo-elk==true]

  logstash:
    image: docker.elastic.co/logstash/logstash-oss:6.2.3
    logging:
      driver: "json-file"
      options:
        max-file: ${DOCKER_LOG_MAX_FILE:?Docker log max number(DOCKER_LOG_MAX_FILE) is unset or empty}
        max-size: ${DOCKER_LOG_MAX_SIZE:?Docker log fmax size per file(DOCKER_LOG_MAX_SIZE) is unset or empty}
    volumes:
      - /asklytics/monitoring/config/app/elk/logstash/config/:/usr/share/logstash/config/:ro
      - /asklytics/monitoring/config/app/elk/logstash/pipelines:/home/pipelines/
      - /asklytics/logs:/asklytics/logs
    ports:
      - "5000:5000/tcp"
      - "5000:5000/udp"
    environment:
      - "LS_JAVA_OPTS=-Xmx256m -Xms256m"
    networks:
      - elk
    depends_on:
      - elasticsearch
    deploy:
      mode: global
      placement:
        constraints: [node.labels.demo-elk==true]

  kibana:
    image: docker.elastic.co/kibana/kibana-oss:6.2.3
    logging:
      driver: "json-file"
      options:
        max-file: ${DOCKER_LOG_MAX_FILE:?Docker log max number(DOCKER_LOG_MAX_FILE) is unset or empty}
        max-size: ${DOCKER_LOG_MAX_SIZE:?Docker log fmax size per file(DOCKER_LOG_MAX_SIZE) is unset or empty}
    volumes:
      - /asklytics/monitoring/config/app/elk/kibana/:/usr/share/kibana/config:ro
    ports:
      - "5601:5601"
    networks:
      - elk
    depends_on:
      - elasticsearch
    deploy:
      mode: global
      placement:
        constraints: [node.labels.demo-elk==true]

  elastichq:
    image: elastichq/elasticsearch-hq:release-v3.5.0
    logging:
      driver: "json-file"
      options:
        max-file: ${DOCKER_LOG_MAX_FILE:?Docker log max number(DOCKER_LOG_MAX_FILE) is unset or empty}
        max-size: ${DOCKER_LOG_MAX_SIZE:?Docker log fmax size per file(DOCKER_LOG_MAX_SIZE) is unset or empty}
    ports:
      - "5001:5000"
    networks:
      - elk
    depends_on:
      - elasticsearch
    deploy:
      mode: global
      placement:
        constraints: [node.labels.demo-elk==true]

  curator:
    image: ${REGISTRY_URL:-157.230.155.170:5000}/asklytics-curator
    logging:
      driver: "json-file"
      options:
        max-file: ${DOCKER_LOG_MAX_FILE:?Docker log max number(DOCKER_LOG_MAX_FILE) is unset or empty}
        max-size: ${DOCKER_LOG_MAX_SIZE:?Docker log fmax size per file(DOCKER_LOG_MAX_SIZE) is unset or empty}
    volumes:
      - /asklytics/monitoring/config/app/elk/curator/config/:/config
      - /asklytics/logs/curator:/usr/share/curator/config
    networks:
      - elk
    environment:
      - ES_HOST=${ES_HOST:?Elasticsearch host(ES_HOST) is unset or empty}
    depends_on:
      - elasticsearch
    deploy:
      mode: global
      placement:
        constraints: [node.labels.demo-elk==true]

networks:
  elk:
    driver: overlay