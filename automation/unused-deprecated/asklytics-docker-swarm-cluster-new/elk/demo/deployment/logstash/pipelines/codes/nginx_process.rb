require 'rubygems'
require 'json'
# the value of `params` is the value of the hash passed to `script_params`
# in the logstash configuration
def register(params)
  @server_file = params["server_file"]
end

# the filter method receives an event and must return a list of events.
# Dropping an event means not including it in the return array,
# while creating new ones only requires you to add a new instance of
# LogStash::Event to the returned array
def filter(event)
  nginx_servers = File.read(@server_file)
  nginx_server_objects = JSON.parse(nginx_servers)
  nginx_server_objects.each do |nginx_server_object|
    if log_path.include? nginx_server_object["name"]
      event.set("hostname", nginx_server_object["name"])
      event.set("ip", nginx_server_object[:ip])
      break
    end
  end
  return [event]
end


# testing!!
# test "nginx test" do
#   parameters { { "server_file" => "/development/asklytics/dev/repos/asklytics/automation/asklytics-docker-swarm-cluster/elk/deployment/logstash/pipelines/nginx_servers_sample.json" } }
#   in_event { { "path" => "/development/asklytics/dev/repos/asklytics/automation/asklytics-docker-swarm-cluster/elk/deployment/logstash/pipelines/codes/ui_access.log" } }
#   expect("the size is events") {|events| events.first.get("hostname") == "ui" }
# end