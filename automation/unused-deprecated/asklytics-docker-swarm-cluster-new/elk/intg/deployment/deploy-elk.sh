#!/bin/bash


source intg-env.cnf
REGISTRY_URL=$registry docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always elk
