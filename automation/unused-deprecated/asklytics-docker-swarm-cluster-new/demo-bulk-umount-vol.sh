echo "unmounting monitoring on demo-dsclm-01-app-layer..."
docker-machine ssh demo-dsclm-01-app-layer df
docker-machine ssh demo-dsclm-01-app-layer umount /asklytics/monitoring
docker-machine ssh demo-dsclm-01-app-layer df
echo " "
echo " "
echo "unmounting monitoring on demo-dsclw-01-app-layer..."
docker-machine ssh demo-dsclw-01-app-layer df
docker-machine ssh demo-dsclw-01-app-layer umount /asklytics/monitoring
docker-machine ssh demo-dsclw-01-app-layer df
echo " "
echo " "
echo "unmounting monitoring on demo-dsclw-02-app-layer..."
docker-machine ssh demo-dsclw-02-app-layer df
docker-machine ssh demo-dsclw-02-app-layer umount /asklytics/monitoring
docker-machine ssh demo-dsclw-02-app-layer df
echo " "
echo " "
echo "unmounting monitoring on demo-dsclw-03-app-layer..."
docker-machine ssh demo-dsclw-03-app-layer df
docker-machine ssh demo-dsclw-03-app-layer umount /asklytics/monitoring
docker-machine ssh demo-dsclw-03-app-layer df

