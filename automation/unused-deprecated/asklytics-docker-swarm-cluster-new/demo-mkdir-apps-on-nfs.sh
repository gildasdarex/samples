echo "Creating dirs and subdirs for AskLytics Apps .... "
mkdir -p /asklytics/app-data/fs/collection/mlaas
mkdir -p /asklytics/app-data/fs/mlaas
mkdir -p /asklytics/app-data/properties/config/log4j2
mkdir -p /asklytics/app-data/properties/sentry/infra-service
mkdir -p /asklytics/app-data/properties/sentry/camel-analytics
mkdir -p /asklytics/app-data/properties/sentry/analytics-service
mkdir -p /asklytics/app-data/properties/sentry/ml-api
mkdir -p /asklytics/app-data/properties/sentry/ui-data-service
mkdir -p /asklytics/app-data/properties/sentry/camel-prov
mkdir -p /asklytics/app-data/properties/sentry/infra-prov-service
mkdir -p /asklytics/app-data/properties/spring
mkdir -p /asklytics/app-data/properties/logstash
mkdir -p /asklytics/app-data/pylytics
mkdir -p /asklytics/app-data/fs/tenant-asset
mkdir -p /asklytics/logs/spring
mkdir -p /asklytics/logs/python
mkdir -p /asklytics/logs/nginx
echo "Finished creating dirs and AskLytics Apps! "
echo ""
