echo " "
echo "Mounting Pylytics data Vol ..."
cp ./properties/demo-nfs-properties-pylytics-data-mount.sh ./properties/demo-nfs-properties.sh 
demo-mount-nfs-directory-on-nodes.sh
echo " "
echo " "
echo "Mounting App Data Vol ..."
cp ./properties/demo-nfs-properties-app-data-mount.sh ./properties/demo-nfs-properties.sh 
demo-mount-nfs-directory-on-nodes.sh
echo " "
echo " "
echo "Mounting Logs Vol ..."
cp ./properties/demo-nfs-properties-logs-mount.sh ./properties/demo-nfs-properties.sh 
demo-mount-nfs-directory-on-nodes.sh
echo " "
echo " "
echo "Mounting Backup Vol ..."
cp ./properties/demo-nfs-properties-backup-mount.sh ./properties/demo-nfs-properties.sh 
demo-mount-nfs-directory-on-nodes.sh
echo " "
echo " "

echo "Mounting apps db and monitoring config Vol ..."
cp ./properties/demo-nfs-properties-app-db-config-mount.sh ./properties/demo-nfs-properties.sh 
demo-mount-nfs-directory-on-nodes.sh
echo " "
echo " "
echo "Mounting apps App config Vol ..."
cp ./properties/demo-nfs-properties-apps-app-config-mount.sh ./properties/demo-nfs-properties.sh 
demo-mount-nfs-directory-on-nodes.sh

echo " "
echo " "
echo "Done !!"

