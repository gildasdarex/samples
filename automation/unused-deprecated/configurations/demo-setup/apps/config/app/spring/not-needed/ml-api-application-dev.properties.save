

db.config.location=/asklytics/apps/config/app/app-properties
rest.apps.url.file.path=/asklytics/apps/config/app/app-properties

#logstash feature
logs.data.parser.config.file.path=/asklytics/apps/config/app/app-properties/logstash

sentry.properties.file=/asklytics/apps/config/app/sentry/ml-api
rest.apps.service.client.file.path=/asklytics/apps/config/app/app-properties
ml.api.controller.meta.data=/asklytics/apps/config/app/app-properties
# Server Properties
# -----------------
server.port=80
server.contextPath=/
spring.jackson.serialization.INDENT_OUTPUT=true

#max file properties
spring.http.multipart.max-file-size=100MB
spring.http.multipart.max-request-size=100MB

jmeter.data.collector.metrics.units.metadata.file.path =/asklytics/apps/config/app/app-properties
jmeter.data.collector.normalize.metadata.file.path =/asklytics/apps/config/app/app-properties
jmeter.data.collector.transformer.metadata.file.path =/asklytics/apps/config/app/app-properties
jmeter.data.perfmon.metrics.metadata.file.path =/asklytics/apps/config/app/app-properties
jmeter.report.transformer.metadata.file.path =/asklytics/apps/config/app/app-properties
micro.analysis.analytics.service.def.file.path=/asklytics/apps/config/app/app-properties

#Management properties
#https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html
#Find the url that is specific to the spring boot version that we use
#https://docs.spring.io/spring-boot/docs/1.5.7.RELEASE/reference/htmlsingle/#production-ready-endpoints
endpoints.sensitive=true
endpoints.info.sensitive=false
endpoints.enabled=false
endpoints.info.enabled=true
endpoints.health.enabled=true
endpoints.health.sensitive=false
management.security.enabled=false
management.context-path=/manage
endpoints.prometheus.enabled=true
endpoints.prometheus.id=alprometheus
management.metrics.export.prometheus.enabled=true
management.metrics.distribution.percentiles-histogram[http.server.requests]=true
management.metrics.distribution.percentiles[http.server.requests]=0.5, 0.75, 0.95, 0.98, 0.99, 0.999, 1.0
management.metrics.distribution.sla[http.server.requests]=10ms, 100ms

# app security properties
app.security.loginUrl=/api/login/**
app.api.version=1.0
app.cookie.path=/
app.cookie.httpOnly=false
app.cookie.maxAge=-1
app.cookie.secure=true
app.cookie.domain=asklytics.io
app.security.login.header=x-asklytics-api-key
#exposedHeaders below iss a list separated by , default is *
app.security.cors.exposedHeaders=Access-Control-Allow-Origin,Access-Control-Allow-Credentials
#origin below is a list separated by , default is *
#app.security.cors.origins=https://104.236.84.54,https://app.asklytics.xyz,http://138.197.28.120:8080,http://138.197.114.106:8080,https://localhost:4200,http://localhost:4200,http://138.197.114.106:4200,http://app.asklytics.xyz,https://app-int.asklytics.xyz,https://ipw.asklytics.xyz,https://ipw-int.asklytics.xyz,http://localhost:4201,ws-ml-api.asklytics.xyz,ws-ml-api-int.asklytics.xyz,ws-ui-data-service.asklytics.xyz,ws-ui-data-service-int.asklytics.xyz,http://ws-ml-api.asklytics.xyz,http://ws-ml-api-int.asklytics.xyz,http://ws-ui-data-service.asklytics.xyz,http://ws-ui-data-service-int.asklytics.xyz,https://ws-ml-api.asklytics.xyz,https://ws-ml-api-int.asklytics.xyz,https://ws-ui-data-service.asklytics.xyz,https://ws-ui-data-service-int.asklytics.xyz
#app.security.cors.origins=*
app.security.cors.origins=https://demo-ui-data-service.asklytics.io,https://demo-ml-api.asklytics.io,https://demo-app1.asklytics.io,https://demo-app.asklytics.io,http://demo-ui-data-service.asklytics.io,http://demo-ml-api.asklytics.io,http://demo-app1.asklytics.io,http://demo-app.asklytics.io,wss://demo-ws-ui-data-service.asklytics.io,https://demo-ws-ui-data-service.asklytics.io,http://demo-ws-ui-data-service.asklytics.io,wss://demo-ws-ml-api.asklytics.io,https://demo-ws-ml-api.asklytics.io,http://demo-ws-ml-api.asklytics.io,wss://demo-ws-ui-data-service-int.asklytics.io,https://demo-ws-ui-data-service-int.asklytics.io,http://demo-ws-ui-data-service-int.asklytics.io,wss://demo-ws-ml-api-int.asklytics.io,https://demo-ws-ml-api-int.asklytics.io,http://demo-ws-ml-api-int.asklytics.io,demo-ws-ml-api.asklytics.io,demo-ws-ml-api-int.asklytics.io,demo-ws-ui-data-service.asklytics.io,demo-ws-ui-data-service-int.asklytics.io,http://demo-ui-data-service-int.asklytics.io,http://demo-ml-api-int.asklytics.io,http://demo-app1-int.asklytics.io,http://demo-app-int.asklytics.io
#headers below iss a list separated by , default is *
app.security.cors.headers=*
#methos below iss a list separated by , default is *
app.security.cors.methods=*
app.security.cors.allowedCredentials=true

#GEN-1551 https://asklytics.atlassian.net/browse/GEN-1551
#Jwt expiry time clock skew and not before in seconds
app.security.exp.time.clock.skew.sec=30
app.security.nbf.time.limit.sec=0

#jwt expiry time in minutes
app.security.exp.time.limit.min=15

#Auth0 Configuration
app.security.auth0.clientId=6s0iGz2UYW8rozBqkyjxXcDLSiPMA576
app.security.auth0.clientSecret=RDTackpLaTpbjeXdOsKjSi56qpa-5z27XT-dUi_iYPaYOJ-XYrKydyDweOc3KtH-
app.security.auth0.domain=asklytics-demo.auth0.com
app.security.auth0.dbConnection=Username-Password-Authentication
app.security.auth0.dbConnectionId=con_vLBAyM9VTWaMW3sD
app.security.auth0.audience=https://asklytics-demo.auth0.com/api/v2/
app.security.auth0.resultUrl=https://asklytics-demo.auth0.com/api/v2/
app.security.auth0.link.expiry.second=86400

spring.jpa.show-sql=true
logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type=TRACE

ml.api.commit.retry=5
app.file.min.data.points=20
