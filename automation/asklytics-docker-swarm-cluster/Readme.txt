 I- Add do_token as environment variable
    - sudo echo "do_token=abed3e64bd4f1818998a5692870857cfda44b8d9dd90633f9c1147dd127e482e" >> /etc/environment
    - sudo source /etc/environment

 I- Create digital ocean block storage:
   - Go to asklytics-docker-swarm-cluster-new/create-update-cluster directory
   - update demo/cluster-data/volumes_test.json file
   - Run chmod +x create-volume-for-docker-swarm-cluster.sh
   - Run ./create-volume-for-docker-swarm-cluster.sh --volumes.file demo/cluster-data/volumes.json --cluster.name demo


 II- Create nfs servers
   - Go to asklytics-docker-swarm-cluster-new/create-update-nfs-server directory
   - update demo/cluster-data/nfs_server_nodes.json file
   - Run chmod +x create-nfs-server.sh
   - Run ./create-nfs-server.sh --nodes.file demo/cluster-data/nfs_server_nodes.json --do.users.data demo/cluster-data/cluster-nodes-userdata.yml --cluster.name demo



 III- Create docker swarm cluster
    - Go to asklytics-docker-swarm-cluster-new/create-update-cluster directory
    - update demo/cluster-data/deployment_test_nodes.json file
    - Run chmod +x create-docker-swarm-cluster.sh
    - Run ./create-docker-swarm-cluster.sh --nodes.file demo/cluster-data/deployment_test_nodes.json --do.users.data demo/cluster-data/cluster-nodes-userdata.yml --cluster.name demo


 IV- Mount directory between nfs server and docker swarm cluster nodes
    - Go to asklytics-docker-swarm-cluster-new/create-update-nfs-server directory
    - update demo/directories-managment/data_mount_mapping_file.json file
    - Run chmod +x mount-nfs-server-and-client-directory.sh
    - Run ./mount-nfs-server-and-client-directory.sh --data.mount.mapping.file demo/directories-managment/data_mount_mapping_file.json --cluster.name demo


 V- Copy spring boot, db , monitoring and other apps configs file into nfs servers
     - Go to asklytics-docker-swarm-cluster-new/create-update-nfs-server directory
     - update demo/directories-managment/configs_and_properties.json file
     - Run chmod +x cp-deployment-directories.sh
     - Run ./cp-deployment-directories.sh --config.properties.json demo/directories-managment/configs_and_properties.json --cluster.name demo


 VI- Deploy apps
      - Go to asklytics-docker-swarm-cluster-new/service-deployment-scripts directory
      - update demo/directories-managment/configs_and_properties.json file
      - Run chmod +x cp-deployment-directories.sh
      - Run ./cp-deployment-directories.sh --config.properties.json demo/directories-managment/configs_and_properties.json --cluster.name demo
