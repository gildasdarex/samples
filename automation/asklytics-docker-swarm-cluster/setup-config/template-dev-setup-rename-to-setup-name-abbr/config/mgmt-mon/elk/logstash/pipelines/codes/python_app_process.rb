require 'rubygems'
require 'json'
# the value of `params` is the value of the hash passed to `script_params`
# in the logstash configuration
def register(params)

end

# the filter method receives an event and must return a list of events.
# Dropping an event means not including it in the return array,
# while creating new ones only requires you to add a new instance of
# LogStash::Event to the returned array
def filter(event)
  log_path = event.get("path")
  if log_path.include? "pylytics"
    event.set("app_name", "pylytics")
  elsif log_path.include? "blulytics"
    event.set("app_name", "blulytics")
  else
    event.set("app_name", "pylytics")
  end
  return [event]
end


# testing!!
# test "spring app test" do
#   parameters { { "server_file" => "/development/asklytics/dev/repos/asklytics/automation/asklytics-docker-swarm-cluster/elk/deployment/logstash/pipelines/nginx_servers_sample.json" } }
#   in_event { { "path" => "/development/asklytics/dev/repos/asklytics/automation/asklytics-docker-swarm-cluster/elk/deployment/logstash/pipelines/codes/ui_access.log" } }
#   expect("the size is events") {|events| events.first.get("hostname") == "ui" }
# end