# Replace Texts with Appropriate Values

[domain-name-underscore-extension]
e.g. asklytics-test_xyz
this is used for in dir and file names

[domain-name-dot-extension]
e.g/ asklytics-test.xyz
This is used in domain names

[setup-name-abbr]
the abbreviation for the setup name. This is used in files
This is also used as "cluster name" in the input param of infra creation/deployment scripts


# Refactor File Names and Directory Names

setup-name-abbr
the abbreviation for the setup name. This is used in dir names and file names
e.g. intg1
This is also used as "cluster name" in the input param of infra creation/deployment scripts

# SSL Certificate for EXT Nginx Proxy Server
## Setup for Asklytics.io Domain
(e.g. prod)
If its a setup that uses asklytics.io domain, use existing prod ssl certificate 

## Setup for Development Domains 
If its for a development setup (e.g. development, QA, integration), create a ssl certificate 

place it in setup-config/[setup-name-abbr]/config/infra/config/proxy/external/certs/etc_ssl_[domain_name_underscore_extension]/latest
name the files 
ssl-[domain_name_underscore_extension]-expires-[yyyy-mm-dd].crt
ssl-[domain_name_underscore_extension]-expires-[yyyy-mm-dd].key
ssl-params.conf


Replace markers for swarm node managers and proxy servers with their IPs   
    
    [swarm-manager-1-leader]
    [swarm-manager-2]
    [swarm-manager-3]
    [ext-proxy-server]
    [int-proxy-server]

Replace API keys used by the apps

    [send-grid-api-key-monitoring]
    [slack-api-key-url]
    
# ASKLYTICS APPLICATION PROPERTIES
## Backend-apps properties
Copy the properties of the backend apps for the release that needs to be deployed
setup-config/[setup-name-abbr]/config/app/backend-apps/
    - app-properties/
        - logstash/ (this related to dataset management upload to elasticsearch)
        - priceModel/
        - *.json
    - spring
    
## UI app properties
setup-config/[setup-name-abbr]/config/app/frontend-apps/ui-properties/
 