#!/bin/bash

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       docker.stack)
          stack="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

source data.cnf
docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always "$stack"
