#Command to get the Page 1 of  list of PRIVATE images 
(there is a &private=true at the of the API call)
        
    curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer [DIGITALOCEAN-API-KEY]" "https://api.digitalocean.com/v2/images?page=1&per_page=99&private=true"

#Command to get the Page 1 of  list of ALL images 
    
    curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer [DIGITALOCEAN-API-KEY]" "https://api.digitalocean.com/v2/images?page=1&per_page=99&private=true"
