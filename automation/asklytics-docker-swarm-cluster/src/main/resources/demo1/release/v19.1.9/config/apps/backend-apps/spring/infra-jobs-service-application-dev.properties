# Server Properties
# -----------------
server.port=80
server.contextPath=/
spring.jackson.serialization.INDENT_OUTPUT=true
spring.jpa.show-sql=true
logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type=TRACE

#Management properties
#https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html
#Find the url that is specific to the spring boot version that we use
#https://docs.spring.io/spring-boot/docs/1.5.7.RELEASE/reference/htmlsingle/#production-ready-endpoints
endpoints.sensitive=true
endpoints.info.sensitive=false
endpoints.enabled=false
endpoints.info.enabled=true
endpoints.health.enabled=true
endpoints.health.sensitive=false
management.security.enabled=false
management.context-path=/manage

endpoints.prometheus.enabled=true
endpoints.prometheus.id=alprometheus

management.metrics.export.prometheus.enabled=true
management.metrics.distribution.percentiles-histogram[http.server.requests]=true
management.metrics.distribution.percentiles[http.server.requests]=0.5, 0.75, 0.95, 0.98, 0.99, 0.999, 1.0
management.metrics.distribution.sla[http.server.requests]=10ms, 100ms

#AskLytics app properties
sentry.properties.file=/asklytics/apps/config/app/sentry/infra-jobs-service
asklytics.data.access.db.config.location=/asklytics/apps/config/app/app-properties
asklytics.rest.client.url.file.path=/asklytics/apps/config/app/app-properties
asklytics.rest.client.service.client.file.path=/asklytics/apps/config/app/app-properties

log.path=/var/log/infra-jobs-service

asklytics.infra.influxdb.shard.duration.days=1040w
asklytics.infra.reload.time.duration.sec=1
# token for al-bot-demo-alerter bot user
asklytics.notifications.framework.slack.token=xoxp-43672642294-43680702103-875468512228-47c04267ecbf1826de904d3983742fe8
asklytics.notifications.slack.url=http://slack.com

# 19.1.5
# infra prov corn job to restart any failed provisioning processes (default false)
# NOTE: Changes to properties requires service restart
asklytics.infra-service.job.retry-provisioning.scheduler.enable=true
# frequency of infra prov cron job (default - run every 5 minutes: 0 */5 * ? * *)
asklytics.infra-service.job.retry-provisioning.scheduler.expression=0 */5 * ? * *
# Frequency of logging status of successful runs (default once in 15 MINUTES)
asklytics.infra-service.job.retry-provisioning.scheduler.logging.frequency.minutes=15

# CRON JOB for processing analysis (default false) (backup in case there are issues with ActiveMQ)
# NOTE: Changes to properties requires service restart
asklytics.infra-service.job.process-analysis.scheduler.enable=false
# frequency of analysis processing job (default - run every 15 seconds: 0/15 * * ? * *)
asklytics.infra-service.job.process-analysis.scheduler.expression=0/15 * * ? * *
# Frequency of logging status of successful runs (default once in 15 SECONDS)
asklytics.infra-service.job.process-analysis.scheduler.logging.frequency.seconds=15

# CRON JOB for enforcing Data Retention - SOFT delete (default false)
# NOTE: Changes to properties requires service restart
asklytics.infra-service.job.process-data-retention-soft-delete.scheduler.enable=true
# frequency of SOFT delete data retention job (default - run once a day at 2AM: 0 0 2 ? * * *)
asklytics.infra-service.job.process-data-retention-soft-delete.scheduler.expression=0 0 2 ? * * *
# Frequency of logging status of successful runs (default -1 always log)
asklytics.infra-service.job.process-data-retention-soft-delete.scheduler.logging.frequency.minutes=-1

# CRON JOB for enforcing Data Retention - HARD delete (default false)
# NOTE: Changes to properties requires service restart
asklytics.infra-service.job.process-data-retention-hard-delete.scheduler.enable=true
# frequency of HARD delete data retention job (default - run once a day at 3AM: 0 0 3 ? * * *)
asklytics.infra-service.job.process-data-retention-hard-delete.scheduler.expression=0 0 3 ? * * *
asklytics.infra-service.job.process-data-retention-hard-delete.scheduler.logging.frequency.minutes=-1

# CRON JOB to process billing of ad-hoc usage of product by the user
asklytics.infra-service.job.billing-ad-hoc.scheduler.enable=true
# frequency of Ad-Hoc Billing (default //At 00:01:00am, on the 1st day, every month 0 1 0 1 * ? *)
asklytics.infra-service.job.billing-ad-hoc.scheduler.expression=0 1 0 1 * ? *
# Frequency of logging status of successful runs (default -1 always log)
asklytics.infra-service.job.billing-ad-hoc.scheduler.logging.frequency.minutes=-1

# CRON JOB to processing the conversion of trial subscription to paid subscription
asklytics.infra-service.job.subscription-convert-trial.scheduler.enable=true
# frequency of converting trail subscription (default Run once a day at 1AM 0 0 1 ? * * *)
asklytics.infra-service.job.subscription-convert-trial.scheduler.expression=0 0 1 ? * * *
# Frequency of logging status of successful runs (default -1 always log)
asklytics.infra-service.job.subscription-convert-trial.scheduler.logging.frequency.minutes=-1

# v19.1.6
# used by infra-jobs-service but since the war is the same, properties for invoiced.com needs to be declared -- START
asklytics.infra-service.invoiced.billing.api-key=UQpDhE1fyrPs01Z7WnOBbHEAoOI2pbGq
asklytics.infra-service.invoiced.billing.sandbox=true
asklytics.infra-service.invoiced.billing.plan-name=al-tmp-base-product-plan
asklytics.infra-service.security.invoiced.sso.key=ee3869694a719332fce7a6d01b95e7718e39a62cc9a2065f03d6009198f77d8
asklytics.infra-service.invoiced.billing.testMode=true
# used by infra-jobs-service but since the war is the same, properties for invoiced.com needs to be declared -- END

# v19.1.7
# asklytics.infra-service.job.billing.invoiced.vendor.enable needs to be true only for
# infra-jobs-service but since the war is the same, it needs to set to false in infra-service service
# as it does not have provide any invoicing functionality
asklytics.infra-service.job.billing.invoiced.vendor.enable=true
