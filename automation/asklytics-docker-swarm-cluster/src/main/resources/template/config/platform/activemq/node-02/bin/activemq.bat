@echo off
set ACTIVEMQ_HOME="/root/apache-activemq-5.15.11"
set ACTIVEMQ_BASE="/opt/activemq"

set PARAM=%1
:getParam
shift
if "%1"=="" goto end
set PARAM=%PARAM% %1
goto getParam
:end

%ACTIVEMQ_HOME%/bin/activemq %PARAM%