# Configuring Nginx Sites

## EXTernal Proxy
site config files that are loaded are defined in /etc/nginx/nginx.conf using an include statement

WARNING!!!! Either 
a) copy intg1-external-proxy-nginx-default to /etc/ngnix/sites-enabled/
or
b) copy it to /etc/ngnix/sites-available/
and then make sure there is a link to this file from /etc/nginx/sites-enabled !!!
sudo ln -s /etc/nginx/sites-available/intg1-external-proxy-nginx-default /etc/nginx/sites-enabled/

Note: server for public subdomain uses a local root of /webapp/public
so ensure it has access 
sudo chmod -R 755 /webapp/public
 
## INTernal Proxy
site config files that are loaded are defined in /etc/nginx/nginx.conf using an include statement

WARNING!!!! Either 
a) copy intg1-internal-proxy-nginx-default to /etc/ngnix/sites-enabled/
or 
b) copy it to /etc/ngnix/sites-available/
 and then make sure there is a link to this file from /etc/nginx/sites-enabled !!!
 sudo ln -s /etc/nginx/sites-available/intg1-external-proxy-nginx-default /etc/nginx/sites-enabled/



