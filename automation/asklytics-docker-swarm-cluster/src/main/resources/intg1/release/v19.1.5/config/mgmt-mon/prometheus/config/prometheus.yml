global:
  scrape_interval:     15s
  evaluation_interval: 15s

  external_labels:
    monitor: 'promswarm'

rule_files:
  - "swarm_node.rules.yml"
  - "swarm_task.rules.yml"

alerting:
  alertmanagers:
  - scheme: http
    path_prefix: /alertmanager
    static_configs:
      - targets: ['alertmanager:9093']

scrape_configs:
  - job_name: 'prometheus'
    metrics_path: '/prometheus/metrics'
    static_configs:
      - targets: ['10.138.156.238:9090']

  - job_name: 'dockerd-exporter'
    dns_sd_configs:
    - names:
      - 'tasks.dockerd-exporter'
      type: 'A'
      port: 9323

  - job_name: 'cadvisor'
    dns_sd_configs:
    - names:
      - 'tasks.cadvisor'
      type: 'A'
      port: 8080

  - job_name: 'node-exporter'
    dns_sd_configs:
    - names:
      - 'tasks.node-exporter'
      type: 'A'
      port: 9100

  - job_name: elasticsearch
    scrape_interval: 60s
    scrape_timeout:  30s
    metrics_path: "/metrics"
    static_configs:
    - targets:
      - 10.138.156.238:9108
      labels:
        service: elasticsearch
    relabel_configs:
    - source_labels: [__address__]
      regex: '(.*)\:9108'
      target_label:  'instance'
      replacement:   '$1'
    - source_labels: [__address__]
      target_label:  'environment'
      replacement:   '$1'

  - job_name: 'infra-prov-service-exporter'
    metrics_path: '/manage/alprometheus'
    static_configs:
      - targets: ['10.138.156.238:8080']

  - job_name: 'infra-service-exporter'
    metrics_path: '/manage/alprometheus'
    static_configs:
      - targets: ['10.138.156.238:8002']

  - job_name: 'ml-api-exporter'
    metrics_path: '/manage/alprometheus'
    static_configs:
      - targets: ['10.138.156.238:8084']

  - job_name: 'ui-data-service-exporter'
    metrics_path: '/manage/alprometheus'
    static_configs:
      - targets: ['10.138.156.238:8085']

  - job_name: 'analytics-service-exporter'
    metrics_path: '/manage/alprometheus'
    static_configs:
      - targets: ['10.138.156.238:8001']

  - job_name: 'camel-analytics-exporter'
    metrics_path: '/manage/alprometheus'
    static_configs:
      - targets: ['10.138.156.238:8081']

  - job_name: 'camel-prov-exporter'
    metrics_path: '/manage/alprometheus'
    static_configs:
      - targets: ['10.138.156.238:8082']

  - job_name: 'activemq-node-01'
      metrics_path: '/metrics'
      static_configs:
        - targets: ['10.138.156.238:8092']

  - job_name: 'activemq-node-02'
    metrics_path: '/metrics'
    static_configs:
      - targets: ['10.138.156.238:8093']

  - job_name: 'nginx-ui-exporter'
    metrics_path: '/metrics'
    static_configs:
      - targets: ['mon_nginx_ui_vts_exporter:9913']

  - job_name: 'spring-app-blackbox'
    metrics_path: /probe
    params:
      module: [spring_app_http_2xx]
    file_sd_configs:
      - files:
        - /etc/prometheus/spring-app-targets.json
    relabel_configs:
      - source_labels: [__address__]
        regex: (.*)(:80)?
        target_label: __param_target
      - source_labels: [__param_target]
        regex: (.*)
        target_label: instance
        replacement: ${1}
      - source_labels: []
        regex: .*
        target_label: __address__
        replacement: mon_blackbox_exporter:9115

  - job_name: 'python-app-blackbox'
    metrics_path: /probe
    params:
      module: [python_app_http_2xx]
    file_sd_configs:
      - files:
        - /etc/prometheus/python-app-targets.json
    relabel_configs:
      - source_labels: [__address__]
        regex: (.*)(:80)?
        target_label: __param_target
      - source_labels: [__param_target]
        regex: (.*)
        target_label: instance
        replacement: ${1}
      - source_labels: []
        regex: .*
        target_label: __address__
        replacement: mon_blackbox_exporter:9115

  - job_name: 'ui-app-blackbox'
    metrics_path: /probe
    params:
      module: [ui_app_http_2xx]
    file_sd_configs:
      - files:
        - /etc/prometheus/ui-app-targets.json
    relabel_configs:
      - source_labels: [__address__]
        regex: (.*)(:80)?
        target_label: __param_target
      - source_labels: [__param_target]
        regex: (.*)
        target_label: instance
        replacement: ${1}
      - source_labels: []
        regex: .*
        target_label: __address__
        replacement: mon_blackbox_exporter:9115

  - job_name: 'influxdb-blackbox'
    metrics_path: /probe
    params:
      module: [influxdb_app_http_2xx]
    file_sd_configs:
      - files:
        - /etc/prometheus/influxdb-targets.json
    relabel_configs:
      - source_labels: [__address__]
        regex: (.*)(:80)?
        target_label: __param_target
      - source_labels: [__param_target]
        regex: (.*)
        target_label: instance
        replacement: ${1}
      - source_labels: []
        regex: .*
        target_label: __address__
        replacement: mon_blackbox_exporter:9115

  - job_name: 'es-blackbox'
    metrics_path: /probe
    params:
      module: [es_app_http_2xx]
    file_sd_configs:
      - files:
        - /etc/prometheus/es-targets.json
    relabel_configs:
      - source_labels: [__address__]
        regex: (.*)(:80)?
        target_label: __param_target
      - source_labels: [__param_target]
        regex: (.*)
        target_label: instance
        replacement: ${1}
      - source_labels: []
        regex: .*
        target_label: __address__
        replacement: mon_blackbox_exporter:9115

  - job_name: 'nginx-app-blackbox'
    metrics_path: /probe
    params:
      module: [nginx_app_http_3xx]
    file_sd_configs:
      - files:
        - /etc/prometheus/nginx-targets.json
    relabel_configs:
      - source_labels: [__address__]
        regex: (.*)(:80)?
        target_label: __param_target
      - source_labels: [__param_target]
        regex: (.*)
        target_label: instance
        replacement: ${1}
      - source_labels: []
        regex: .*
        target_label: __address__
        replacement: mon_blackbox_exporter:9115