require 'rubygems'
require 'json'
# the value of `params` is the value of the hash passed to `script_params`
# in the logstash configuration
def register(params)

end

# the filter method receives an event and must return a list of events.
# Dropping an event means not including it in the return array,
# while creating new ones only requires you to add a new instance of
# LogStash::Event to the returned array
def filter(event)
  message = event.get("message")
  items = message.split(" | ")
  items.each_with_index do |item, index|
    if index == 0
      event.set("app_time", item)
    end

    if index == 1
      event.set("level", item)
    end

    if index == 2
      event.set("app_message", item)
    end

    if index == 3
      event.set("logger", item)
    end

    if index == 4
      event.set("thread", item)
    end
  end

  return [event]
end


# testing!!
# test "spring app test" do
#   parameters { { "server_file" => "/development/asklytics/dev/repos/asklytics/automation/asklytics-docker-swarm-cluster/elk/deployment/logstash/pipelines/nginx_servers_sample.json" } }
#   in_event { { "path" => "/development/asklytics/dev/repos/asklytics/automation/asklytics-docker-swarm-cluster/elk/deployment/logstash/pipelines/codes/ui_access.log" } }
#   expect("the size is events") {|events| events.first.get("hostname") == "ui" }
# end