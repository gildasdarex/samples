#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       destroy)
          destroy_node="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       node.name)
          node_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done



docker-machine ssh $node_name docker swarm leave

echo "$node_name leave the cluster with success"

if [[ $destroy_node == "true" ]]; then
    docker-machine rm $node_name docker -force
    echo "$node_name is destroyed"
fi

