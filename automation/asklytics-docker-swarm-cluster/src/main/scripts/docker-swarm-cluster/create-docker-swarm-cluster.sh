#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""
select_main_manager="false"
select_main_manager_name=""

usage () {
    echo ""
    echo " Use this script to create docker swarm cluster in digitalocean . To use this script, please run this command "
    echo " ./create-docker-swarm-cluster.sh --nodes.file absolute_path_to_node_json_file --do.users.data absolute_path_to_cloud_init_file --cluster.name name_of_your_cluster  "
    echo ""
    echo " --nodes.file value should be the absolute path of the json file that contains the informations about the nodes of the docker swarm to create "
    echo " --do.users.data is the absolute path to cloud init file that could be used to setup the server. It is not required  "
    echo " --cluster.name name of the cluster   "
}

print_option_values() {
    echo ""
    echo " Options values sent to script are : "
    echo " nodes.file = $nodes_file "
    echo " do.users.data = $do_users_data_file "
    echo " cluster.name = $cluster_name "
}

check_required_options_and_env_variable() {
    echo ""
    echo " Check required options "

    if [[ -z ${nodes_file+x} ]]; then
     echo "nodes.file is missing "
     exit
    fi

    if [[ -z ${cluster_name+x} ]]; then
     echo "cluster.name is missing "
     exit
    fi

    if [[ -z "${do_token}" ]]; then
      echo "do_token env variable  is undefined"
      exit 1
    fi
}

create_node() {
    name=`echo ${1} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    region=`echo ${2} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    snapshot_id=`echo ${3} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    size=`echo ${4} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    tags=`echo ${5} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    do_users_data_file=`echo ${6} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`

    echo "       STARTING TO CREATE THE CLUSTER NODE NAMED  $name            "
    cmd=""
    if [[ -z ${do_users_data_file+x} ]]; then
      cmd=`echo docker-machine create --driver digitalocean --digitalocean-ipv6 --digitalocean-monitoring --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-tags "$tags" "$name"`
      echo ""
      echo "  RUN COMMAND $cmd "
    else
      cmd=`echo docker-machine create --driver digitalocean --digitalocean-ipv6 --digitalocean-monitoring --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-userdata "$do_users_data_file" --digitalocean-tags "$tags" "$name"`
      echo ""
      echo "  RUN COMMAND $cmd "
    fi
    eval $cmd
    echo "        FINISHING TO CREATE NODE NAMED  $name   "
}

update_servers () {
        echo "   ADD CLUSTER NODE $1 TO SERVERS JSON FILE $asklytics_servers_file_path IN PROGRESS     "


    droplet_name=`echo ${1} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`

    check_droplet=`cat $asklytics_servers_file_path | jq --arg droplet_name "$droplet_name" '.servers | .[] | select(.droplet_name == $droplet_name) | .droplet_name'`
    check_droplet=`echo ${check_droplet} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`

    echo "        CHECKING IF NODE $1 IS ALREADY IN OUTPUT FILE  $asklytics_servers_file_path         "


    if [[ $check_droplet == $droplet_name ]]
    then
      echo "        NODE $1 IS ALREADY IN OUTPUT FILE  $asklytics_servers_file_path              "
      return 1
    fi

    # get droplet id - docker-machine inspect droplet_name returns json response that contains droplet id
    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    #get droplet private ip
    droplet_id=`echo ${droplet_id} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    # get droplet private ip - use digital ocean api to get private ip of the droplet
    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`
    #droplet_name=`echo ${1} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    private_ip=`echo ${private_ip} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    droplet_type=`echo ${2} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`

    # Read $asklytics_servers_file_path and add new droplet
    if [[ -z "$3" ]]
    then
         servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
    else
         servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type, "main_manager": "true"}]'`
    fi

    > $asklytics_servers_file_path
    echo $servers >> $asklytics_servers_file_path

    echo " NODE $1 IS ADDED IN OUTPUT FILE  $asklytics_servers_file_path WITH SUCCESS      "

}


add_server_to_swarm () {
    echo -e "\n  Adding Node ${1} to Docker Swarm Cluster ${cluster_name} ..."

    node_type=`echo ${1} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    name=`echo ${2} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    main_manager_name=`echo ${3} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    manager_token=`echo ${4} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    worker_token=`echo ${5} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    manager_node_ip=`echo ${6} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`

    manager_node_ip=`docker-machine ip $main_manager_name`
    token=""

    if [[ $node_type == "manager" ]]
    then
       token=$manager_token
    elif [[  $node_type == "worker" ]]
    then
       token=$worker_token
    else
       exit
    fi

    for i in $(seq 1 5); do
        echo "    Attempting to add node to swarm - Try  ${i} of 5 ..."
        echo "    Leaving swarm in case  previous add failed or droplet image used to create droplet has swarm info .."
        cmd="docker-machine ssh $name docker swarm leave --force </dev/null"
        echo "    Executing CMD: " ${cmd}
        eval ${cmd}
        echo "    Joining swarm ..."
        # ssh causes while loop to stop / hack is to redirect ssh's stand input to /dev/null
        # https://unix.stackexchange.com/questions/66154/ssh-causes-while-loop-to-stop
        cmd="docker-machine ssh $name docker swarm join --token=$token $manager_node_ip:2377 </dev/null"
        echo "    Executing CMD: " ${cmd}
        eval ${cmd}
        echo "    Checking swarm status of node ..."
        # ssh causes while loop to stop / hack is to redirect ssh's stand input to /dev/null
        # https://unix.stackexchange.com/questions/66154/ssh-causes-while-loop-to-stop
        # but using  </dev/null here does NOT get the status
        cmd="docker-machine ssh ${name} docker info | grep Swarm | sed 's/Swarm: //g' | sed 's/^ *//;s/ *$//'"
        echo "    Executing CMD: " ${cmd}
        status=$(eval ${cmd})
        echo "    Docker status for ${name} - Expecting:'active' Found:'${status}'"

        if [[ ${status} != "active" ]]; then
            echo "    docker status for ${name} is not yet active. Going to sleep. Will try again..."
            sleep 30
            if (( $i == 5 )); then
                echo "    Adding Node to Swarm - FAILED. Cluster: ${cluster_name} Node type: ${1} Node Name: ${name} is not yet active. Giving up after 5 attempts"
                return 1
            else
               continue
            fi
        else
            echo "  Adding Node to Swarm - successful. Cluster: ${cluster_name} Node type: ${1} Node Name: ${name}"
            break;
        fi
    done
}

function isSwarmNode(){
    is_swarm=`docker-machine ssh $1 "docker info | grep Swarm | sed 's/Swarm: //g' | sed 's/^ *//;s/ *$//'" </dev/null`
    if [[ $is_swarm == "active" ]]; then
        echo true;
    else
        echo false;
    fi
}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       # json config file that contains the list of droplets to add
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        # yaml file that contains digital ocean metadata to use to initialize the droplet
       do.users.data)
          do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        # name of the cluster
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       help)
          usage
          ;;
    esac
done


echo "                 THE SCRIPT TO CREATE DOCKER SWARM CLUSTER   STARTED            "

print_option_values
check_required_options_and_env_variable

# file where all droplets informations will be saved
asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


if [[ ! -f ${asklytics_servers_file_path} ]]
then
    mkdir -p /asklytics/cluster/${cluster_name}/output/
    touch /asklytics/cluster/${cluster_name}/output/servers.json
    echo {\"servers\": []} >> ${asklytics_servers_file_path}
else
    cp ${asklytics_servers_file_path} ${asklytics_servers_file_path}-pre-create-docker-swarm-cluster-$(date +%s).json
fi


# Loop inside each element in the json config file
jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    snapshot_id=`echo ${i} | jq '.snapshot_id'`
    tags=`echo ${i} | jq '.tags'`

    node_type=`echo ${node_type} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    name=`echo ${name} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`


    echo "             DETECT IF MACHINE NAMED  $name  ALREADY EXISTS                  "
    docker_machine_exists=`docker-machine ls -q | grep "$name"`

    if test -z "$docker_machine_exists"  ; then
        echo "             MACHINE NAMED  $name  DOESN'T EXIST                         "
        create_node $name $region $snapshot_id $size $tags $do_users_data_file
    else
        echo "             MACHINE NAMED  $name  ALREADY EXIST                  "
    fi



    echo "       CHECKING IF NODE $name IS RUNNING OR NOT           "

    for i in $(seq 1 5); do
      status=`docker-machine status $name`
      if [[ $status == "Running" ]]
      then
         echo "    $droplet_name  IS RUNNING                         "
         break;
      else
         echo "       $droplet_name IS NOT RUNNING . CURRENT STATUS IS $status         "
         echo "       WAIT 1 MIN TO CHECK NEW STATUS OF DROPLET $droplet_name          "
         sleep 1m
         continue
      fi
    done


    if [[ $node_type == "manager" ]]; then
        if [[ $select_main_manager == "false" ]]; then
          update_servers $name $node_type "main_manager"
          select_main_manager="true"
          select_main_manager_name=$name
        else
            update_servers $name $node_type
        fi;
     else
        update_servers $name $node_type
    fi;
done

echo "==========================================================="
echo "STAGE 2: Creating Docker Swarm. starting..."
echo "==========================================================="
echo -e "\nStep 1/4 Initializing Docker Swarm..."
main_manager_name=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`
main_manager_name=`echo ${main_manager_name} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
echo "  Found Docker Swarm Leader from ${asklytics_servers_file_path} Leader Name:" ${main_manager_name}
manager_node_ip=`docker-machine ip $main_manager_name`

echo ""
echo "  Checking if docker swarm is already initialized... using Leader name: ${main_manager_name} ip: ${manager_node_ip}"
is_manager_part_of_swarm=`isSwarmNode "$main_manager_name"`

if [[ $is_manager_part_of_swarm == "false" ]]; then
   echo "  Docker Swarm Initialization in progress... "
   docker-machine ssh $main_manager_name docker swarm init --listen-addr $manager_node_ip --advertise-addr $manager_node_ip
   echo "  Docker Swarm Initialization successful"
else
   echo "  Docker Swarm is Already Initialized"
fi;


worker_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token worker -q")

manager_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token manager -q")

echo -e "\nStep 2/4 Adding Nodes to Docker Swarm..."

jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    name=`echo ${i} | jq '.name'`
    labels=`echo ${i} | jq '.labels'`

    node_type=`echo ${node_type} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    name=`echo ${name} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`
    labels=`echo ${labels} | sed 's/\"//g' | sed 's/^ *//;s/ *$//'`

    echo "  Starting to Add Node $name to  Docker Swarm..."

    if [[ $name != $main_manager_name ]]; then

        is_manager_part_of_swarm=`isSwarmNode "$name"`

        if [[ $is_manager_part_of_swarm == "false" ]]; then
             add_server_to_swarm $node_type $name $main_manager_name $manager_token $worker_token $manager_node_ip
        else
            echo "  Skipped adding Node ${name} to swarm. Already part of swarm"
        fi;
    fi;

    for label in $(echo $labels | sed "s/,/ /g" | sed 's/^ *//;s/ *$//')
    do {
        echo "  Adding label: ${label} to  Node ${name}"
        # ssh causes while loop to stop / hack is to redirect ssh's stand input to /dev/null
        # https://unix.stackexchange.com/questions/66154/ssh-causes-while-loop-to-stop
        docker-machine ssh $main_manager_name "docker node update --label-add ${label}=true  ${name}" </dev/null

    } done
    echo "  Finished Adding Node $name to  Docker Swarm"
    echo -e "\n!!!!!! WARNING ... IF SCRIPT STOPS WITHOUT ADDING ALL NODES TO SWARM, IT IS BECAUSE SSH CAUSES WHILE LOOP TO STOP!!! \
        ... Run script again and again until all nodes are added to swarm !!!!\n"

done

echo -e "\nStep 3/4 Backing up server.json..."
cur_time=`echo $(date +%s)`
cmd="cp ${asklytics_servers_file_path} ${asklytics_servers_file_path}-post-creation-docker-swarm-cluster-${cur_time}"
echo "Executing CMD: " ${cmd}
eval ${cmd}
cmd="ls -l ${asklytics_servers_file_path}-post-creation-docker-swarm-cluster-${cur_time}"
echo "Checking if backup was created. Executing CMD: " ${cmd}
eval ${cmd}

echo -e "\nStep 4/4 Listing Docker Swarm nodes and Docker Labels..."
echo -e "\na) Listing Docker Nodes..."
cmd="docker-machine ssh ${main_manager_name} docker node ls"
echo "Executing CMD: " ${cmd}
eval ${cmd}

echo -e "\nb) Listing Docker Labels..."
cmd="docker-machine ssh ${main_manager_name} \"docker node ls -q | xargs docker node inspect -f '{{ .ID }} [{{ .Description.Hostname }}]: {{ .Spec.Labels }}'\""
echo "Executing CMD: " ${cmd}
eval ${cmd}

echo "==========================================================="
echo "STAGE 2: Creating Docker Swarm. Finished"
echo "==========================================================="


echo "                 THE SCRIPT TO CREATE DOCKER SWARM NODE  ENDED                           "
