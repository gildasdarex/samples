#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [[ -z ${cluster_name+x} ]]; then
 echo "cluster.name is missing "
 exit
fi

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


if [[ ! -f $asklytics_servers_file_path ]]
then
   echo "do_token env variable  is undefined"
   exit 1
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi


main_manager_name=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`
main_manager_name=`echo ${main_manager_name} | sed 's/\"//g'`

echo "############# Main manager node is  $main_manager_name #############"



jq -c '.[]' "$nodes_file" | while read i; do
    name=`echo ${i} | jq '.name'`
    labels=`echo ${i} | jq '.labels'`

    name=`echo ${name} | sed 's/\"//g'`
    labels=`echo ${labels} | sed 's/\"//g'`



    for label in $(echo $labels | sed "s/,/ /g")
    do
        echo "############# Add label $label to node $name #############"
        docker-machine ssh $main_manager_name "docker node update --label-add ${label}=true  ${name}" </dev/null
    done
done