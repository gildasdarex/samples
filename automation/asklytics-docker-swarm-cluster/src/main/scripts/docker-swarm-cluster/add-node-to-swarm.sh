#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""
select_main_manager="false"
select_main_manager_name=""



update_servers () {
    echo "#############  Add droplet $1 to output directory ############# "

    droplet_name=`echo ${1} | sed 's/\"//g'`
    # get droplet id - docker-machine inspect droplet_name returns json response that contains droplet id
    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    #get droplet private ip
    droplet_id=`echo ${droplet_id} | sed 's/\"//g'`
    # get droplet private ip - use digital ocean api to get private ip of the droplet
    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`
    #droplet_name=`echo ${1} | sed 's/\"//g'`
    private_ip=`echo ${private_ip} | sed 's/\"//g'`
    droplet_type=`echo ${2} | sed 's/\"//g'`

    # Read $asklytics_servers_file_path and add new droplet
    if [[ -z "$3" ]]
    then
         servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
    else
         servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type, "main_manager": "true"}]'`
    fi

    > $asklytics_servers_file_path
    echo $servers >> $asklytics_servers_file_path

    echo "#############  droplet $1  added to output directory with success ############# "
}


add_server_to_swarm () {
    echo "############# Add droplet $2 to docker swarm cluster #############"
    node_type=`echo ${1} | sed 's/\"//g'`
    name=`echo ${2} | sed 's/\"//g'`
    main_manager_name=`echo ${3} | sed 's/\"//g'`
    manager_token=`echo ${4} | sed 's/\"//g'`
    worker_token=`echo ${5} | sed 's/\"//g'`
    manager_node_ip=`echo ${6} | sed 's/\"//g'`

    manager_node_ip=`docker-machine ip $main_manager_name`
    token=""

    if [[ $node_type == "manager" ]]
    then
       token=$manager_token
    elif [[  $node_type == "worker" ]]
    then
       token=$worker_token
    else
       exit
    fi

    for i in $(seq 1 5); do
        echo "############# Add droplet $2 to docker swarm cluster  : $i try #############"
        cmd=`echo docker-machine ssh $name docker swarm leave`
        echo $cmd
        eval $cmd
        docker-machine ssh $name docker swarm join --token=$token $manager_node_ip:2377
        status=`docker-machine ssh $name docker info | grep Swarm | sed 's/Swarm: //g'`
        echo "Status for $name node after try $i is $status"
        echo "############# Add droplet $2 to docker swarm cluster  : $i try - Status is $status #############"
        if [[ $status != "active" ]]; then
            continue
        else
            break;
        fi
    done
    echo "############# Droplet $name  added to docker swarm cluster with success #############"
}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       # json config file that contains the list of droplets to add
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        # yaml file that contains digital ocean metadata to use to initialize the droplet
       do.users.data)
          do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        # name of the cluster
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [[ -z ${cluster_name+x} ]]; then
 echo "cluster.name is missing "
 exit
fi

# file where all droplets informations will be saved
asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


if [[ ! -f $asklytics_servers_file_path ]]
then
    mkdir -p /asklytics/cluster/$cluster_name/output/
    touch /asklytics/cluster/$cluster_name/output/servers.json
    echo {\"servers\": []} >> $asklytics_servers_file_path
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi

# Loop inside each element in the json config file
jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    snapshot_id=`echo ${i} | jq '.snapshot_id'`
    tags=`echo ${i} | jq '.tags'`
    #volume_id=`echo ${i} | jq '.volume_id'`

    node_type=`echo ${node_type} | sed 's/\"//g'`
    name=`echo ${name} | sed 's/\"//g'`


    echo "############# Detect if machine $name already exists #############"
    docker_machine_exists=`docker-machine ls -q | grep "$name"`

    if test -z "$docker_machine_exists"  ; then
      echo "Machine $name doesn't exists"
    else
      echo "Machine $name already exists"
      continue
    fi

    echo "############# Starting to create cluster node $name #############"
    cmd=`echo docker-machine create --driver digitalocean --digitalocean-ipv6 --digitalocean-monitoring --digitalocean-private-networking --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-userdata "$do_users_data_file" --digitalocean-tags "$tags" "$name"`
    echo $cmd
    eval $cmd
    echo "############# Finish to create cluster node $name #############"


    echo "############# Check if cluster node $name is running or not #############"

    for i in $(seq 1 5); do
      status=`docker-machine status $name`
      if [[ $status == "Running" ]]
      then
         echo "############# $droplet_name is running #############"
         break;
      else
         echo "############# $droplet_name is not running . Current status is $status #############"
         sleep 1m
         continue
      fi
    done

     echo "#############  Finish to check if cluster node $name is running or not ############# "



    if [[ $node_type == "manager" ]]; then
        if [[ $select_main_manager == "false" ]]; then
          update_servers $name $node_type "main_manager"
          select_main_manager="true"
          select_main_manager_name=$name
        else
            update_servers $name $node_type
        fi;
     else
        update_servers $name $node_type
    fi;
done


main_manager_name=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`
main_manager_name=`echo ${main_manager_name} | sed 's/\"//g'`
manager_node_ip=`docker-machine ip $main_manager_name`

echo "############# Initialize docker swarm cluster . Main manager node is  $main_manager_name #############"
docker-machine ssh $main_manager_name docker swarm init --listen-addr $manager_node_ip --advertise-addr $manager_node_ip
echo "############# Initialize docker swarm cluster with success #############"

worker_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token worker -q")

manager_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token manager -q")



jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    name=`echo ${i} | jq '.name'`
    labels=`echo ${i} | jq '.labels'`

    node_type=`echo ${node_type} | sed 's/\"//g'`
    name=`echo ${name} | sed 's/\"//g'`
    labels=`echo ${labels} | sed 's/\"//g'`

    if [[ $name != $main_manager_name ]]; then
        add_server_to_swarm $node_type $name $main_manager_name $manager_token $worker_token $manager_node_ip
    fi;

    for label in $(echo $labels | sed "s/,/ /g")
    do
        echo "############# Add label $label to node $name #############"
        #https://unix.stackexchange.com/questions/66154/ssh-causes-while-loop-to-stop
        docker-machine ssh $main_manager_name "docker node update --label-add ${label}=true  ${name}" </dev/null
    done

done