#!/bin/bash


usage () {
    echo ""
    echo " Use this script to create nfs server in digitalocean . To use this script, please run this command "
    echo " ./create-swarm-network.sh  --nodes.file absolute_path_to_node_json_file --cluster.name name_of_your_cluster  "
    echo -e "\n  --nodes.file : the json file with the list of nodes that are part of this cluster "
    echo "  --cluster.name name of the cluster   "
}

print_option_values() {
    echo ""
    echo " Options values sent to script are : "
     echo "nodes.json = $nodes_file"
   echo " cluster.name = $cluster_name "
}



check_required_options_and_env_variable() {
    echo ""
    echo " Check required options "

    if [[ -z ${nodes_file+x} ]]; then
     echo "nodes.file is missing "
     usage
     exit 1
    fi

    if [[ -z ${cluster_name+x} ]]; then
     echo "cluster.name is missing "
     exit
    fi

    if [[ ! -f $asklytics_servers_file_path ]]
    then
        echo "file $asklytics_servers_file_path not found"
        exit 1
    fi

}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       # json config file that contains the list of swarm nodes
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

echo "                 THE SCRIPT TO CREATE DOCKER SWARM NETWORK  STARTED            "
asklytics_servers_file_path=$nodes_file

print_option_values
check_required_options_and_env_variable

echo "using servers.json located at "  $asklytics_servers_file_path

main_manager=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`

main_manager=`echo ${main_manager} | sed 's/\"//g'`
echo "Running CMD: docker-machine ssh  $main_manager "docker network create -d overlay $cluster_name-asklytics-net" </dev/null ..."
docker-machine ssh  $main_manager "docker network create -d overlay $cluster_name-asklytics-net" </dev/null

echo "                 LISTING ALL NETWORKS           "
docker-machine ssh  $main_manager "docker network ls" </dev/null



echo "                 THE SCRIPT TO CREATE DOCKER SWARM NETWORK  ENDED           "
