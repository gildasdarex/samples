#!/bin/bash


cp_deployment_dir () {
   main_manager=$1
   deployment_dir=$2
   docker-machine ssh  $main_manager "mkdir -p /asklytics/deployment_dir" </dev/null
   docker-machine scp -r deployment_dir $main_manager:/asklytics/deployment_dir/. </dev/null
}

usage () {
    echo ""
    echo " Use this script to create nfs server in digitalocean . To use this script, please run this command "
    echo " ./deploy-docker-swarm-service.sh --deployment.name deployment_name --deployment.dir deployment_dir --docker.stack stack_name --cluster.name name_of_your_cluster  "
    echo ""
}

print_option_values() {
    echo ""
    echo " Options values sent to script are : "
    echo " deployment.name = $deployment_name "
    echo " deployment.dir = $deployment_dir "
    echo " docker.stack = $stack "
    echo " monitoring = $deployment_name "
    echo " cluster.name = $cluster_name "
}

check_required_options_and_env_variable() {
    echo ""
    echo " Check required options "

    if [[ -z ${deployment_name+x} ]]; then
     echo "deployment.name is missing "
     exit
    fi

    if [[ -z ${cluster_name+x} ]]; then
     echo "cluster.name is missing "
     exit
    fi

    if [[ -z ${deployment_dir+x} ]]; then
     echo "deployment.dir is missing "
     exit
    fi

    if [[ -z ${stack+x} ]]; then
     echo "docker.stack is missing "
     exit
    fi

    if [[ ! -f $asklytics_servers_file_path ]]
    then
        echo "file $asklytics_servers_file_path not found"
        exit 1
    fi
}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       deployment.name)
          deployment_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       deployment.dir)
          deployment_dir="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       docker.stack)
          stack="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       monitoring)
          monitoring="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

echo "                 THE SCRIPT TO CREATE DOCKER SWARM SERVICE  STARTED            "


asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


print_option_values
check_required_options_and_env_variable

# if env variable contains other environment variables, it will not get evaluated recursively when using commands like scp"
# so, get the fully evaluated value
deployment_dir=`eval echo $deployment_dir`

main_manager=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`

main_manager=`echo ${main_manager} | sed 's/\"//g'`

echo "                 CREATE  /asklytics/deployment_dir in MANAGER NODE($main_manager)           "

docker-machine ssh  $main_manager "mkdir -p /asklytics/deployment_dir  " </dev/null


echo "                 COPY  $deployment_dir CONTENT IN  /asklytics/deployment_dir ON  MANAGER NODE($main_manager)           "

docker-machine scp -r $deployment_dir $main_manager:/asklytics/deployment_dir/. </dev/null

main_deployment_dir=`basename "$deployment_dir"`

if [[ -z "${monitoring}" ]]; then
  echo "                 DEPLOY SERVICE FOR STACK $stack           "
  docker-machine ssh  $main_manager "cd /asklytics/deployment_dir/$main_deployment_dir &&  docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always $stack" </dev/null
else
    echo "                 DEPLOY MONITORING OR ELK APPS           "
  #docker-machine ssh  $main_manager "cd /asklytics/deployment_dir/$main_deployment_dir && source data.env && docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always $stack" </dev/null
  docker-machine ssh  $main_manager "cd /asklytics/deployment_dir/$main_deployment_dir && chmod +x deploy-apps.sh && ./deploy-apps.sh --docker.stack $stack" </dev/null
fi


echo "Finished Deploying Services on Swarm!"

echo "Waiting  10 seconds before listing (sometimes it take a bit for services to get running) ... "
echo "If service shows as down, try listing services again using 'docker service ls'"
sleep 10
echo "  "
echo "  "
echo "Listing the docker services ...."
echo " "
docker-machine ssh  $main_manager "docker service ls" </dev/null


echo "                 THE SCRIPT TO CREATE DOCKER SWARM SERVICES FOR STACK $stack  ENDED            "
