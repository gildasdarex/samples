#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_volumes_file_path=""
select_main_manager="false"


usage () {
    echo -e "\nUse this script to create volume in digitalocean. To use this script, please run this command "
    echo -e "\t./create-volume.sh --volumes.file absolute_path_to_volume_json_file --cluster.name name_of_your_cluster"
    echo -e "\n\t--volumes.file value absolute path of json file containing info about the volume to create"
    echo -e "\t\t--cluster.name name of the docker swarm cluster. Volumes are not part of the swarm. But its used to create output dir path"
}

print_option_values() {
    echo -e "\nOptions values sent to script are : "
    echo -e "\tvolumes.file=${volumes_file} "
    echo -e "\tcluster.name=${cluster_name} "
}

check_required_options_and_env_variable() {
    echo -e "\nChecking required options ..."

    if [[ -z ${volumes_file+x} ]]; then
     echo -e "volumes.file is missing"
     usage
     exit 1
    fi

    if [[ -z ${cluster_name+x} ]]; then
     echo -e "cluster.name is missing"
     usage
     exit 1
    fi

    if [[ -z "${do_token}" ]]; then
      echo -e "do_token env variable  is undefined. Set in /etc/environment and run source /etc/environment"
      echo -e "When scripts are run as sudo, the env variable from the shell is not passed to the script."
      echo -e "We need to create a aldeployment_profile file that contains env variables."
      echo -e "Then source aldeployment_profile at beginning of this script."
      exit 1
    fi
}

update_volumes () {
    # $1 parameter is the name of the project to clone
    #get droplet id
    echo -e "\n\tAdding Volume with name: $name  TO ${asklytics_volumes_file_path} ..."
    name=`echo ${1} | sed 's/\"//g'`
    volume_id=`echo ${2} | sed 's/\"//g'`
    size_gigabytes=`echo ${3} | sed 's/\"//g'`
    region=`echo ${4} | sed 's/\"//g'`
    description=`echo ${5} | sed 's/\"//g'`

    volumes=`cat ${asklytics_volumes_file_path} | jq  --arg name "$name" --arg volume_id "$volume_id" --arg size_gigabytes "$size_gigabytes" --arg region "$region"  --arg description "$description" '.volumes += [{"name": $name, "volume_id": $volume_id, "size_gigabytes": $size_gigabytes, "region": $region, "description":$description}]'`
    > ${asklytics_volumes_file_path}
    echo $volumes >> ${asklytics_volumes_file_path}
}

create_output_file() {
    echo -e "\t\tLooking for output file ${asklytics_volumes_file_path} ..."
    if [[ ! -f ${asklytics_volumes_file_path} ]]
    then
    echo -e "\t\tCreating and Initializing output file ${asklytics_volumes_file_path} ..."
        mkdir -p /asklytics/cluster/${cluster_name}/output/
        # initializing output file
        touch ${asklytics_volumes_file_path}
        echo {\"volumes\": []} >> ${asklytics_volumes_file_path}
    else
        echo -e "\t\tFound output file  ${asklytics_volumes_file_path} Making a backup copy..."
        #If file already exists, move it so that the script does not overwrite.
        cp ${asklytics_volumes_file_path} ${asklytics_volumes_file_path}-pre-create-volume-$(date +%s)
    fi

}

    create_volumes() {
echo -e "\t\tCreating volumes specified in config file:${volumes_file} ..."

    # Loop inside each element in the json config file
    jq -c '.[]' "$volumes_file" | while read i; do
        size_gigabytes=`echo ${i} | jq '.size_gigabytes'`
        name=`echo ${i} | jq '.name'`
        description=`echo ${i} | jq '.description'`
        region=`echo ${i} | jq '.region'`


        name=`echo ${name} | sed 's/\"//g'`
        region=`echo ${region} | sed 's/\"//g'`
        description=`echo ${description} | sed 's/\"//g'`

        echo -e "\n\tCreating Volume with Name: $name ..."

         #create a volume by using digital ocean api
        cmd=`echo curl -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"size_gigabytes\": $size_gigabytes, \"name\": \"$name\", \"region\": \"$region\", \"description\": \"$description\"}'" "https://api.digitalocean.com/v2/volumes"`
        echo $cmd
        volume_id=`eval $cmd | jq '.volume.id'`
        if [[ $volume_id == "null" ]]
        then
           echo -e "\tFAILED CREATING VOLUME WITH NAME: $name"
           exit 1
        else
           echo -e "\tFinished Creating Volume Name: $name VOLUME ID:$volume_id "
           update_volumes $name $volume_id $size_gigabytes $region $description
        fi
    done
}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       # json config file that contains the list of volumes to create
       volumes.file)
          volumes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       # name of the cluster
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       help)
          usage
          ;;
    esac
done


#read_options_to_script
print_option_values
check_required_options_and_env_variable

echo -e "                 THE SCRIPT TO CREATE VOLUME STARTED..\n"
# file where all volumes info will be saved
asklytics_volumes_file_path="/asklytics/cluster/${cluster_name}/output/volumes.json"
echo -e "Output will be saved in ${asklytics_volumes_file_path}"

echo -e "\nStep 1/3 Initializing output file ..."
create_output_file
echo -e "\nStep 2/3 Creating Volumes ..."
create_volumes

echo -e "\nStep 3/3 Backing up servers.json ..."
cur_time=`echo $(date +%s)`
cmd="cp ${asklytics_volumes_file_path} ${asklytics_volumes_file_path}-post-create-volume-${cur_time}"
echo -e "Executing CMD: " ${cmd}
eval ${cmd}
cmd="ls -l ${asklytics_volumes_file_path}-post-create-nfs-server-${cur_time}"
echo -e "Listing the backup file (ensure it lists 1 file). Executing CMD:" ${cmd}
eval ${cmd}

echo -e "                 THE SCRIPT TO CREATE VOLUME ENDED\n"
