#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""
select_main_manager="false"


update_servers () {
    # $1 parameter is the name of the project to clone
    #get droplet id
    droplet_id=`docker-machine inspect "$1" | jq '.Driver.DropletID'`
    #get droplet private ip
    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`

    if [ -z "$3" ]
    then
        servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$1" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$2" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
    else
        servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$1" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$2" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type, "main_manager": "true"}]'`
    fi

    > $asklytics_servers_file_path
    echo $servers >> $asklytics_servers_file_path
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       nodes.file)
          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       do.users.data)
          do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z ${cluster_name+x} ]; then
 echo "cluster.name is missing "
 exit
fi

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


if [ -e $asklytics_servers_file_path ]
then
    mkdir -p /asklytics/cluster/$cluster_name/output/
    touch /asklytics/cluster/$cluster_name/output/servers.json
    echo {"servers": []} >> $asklytics_servers_file_path
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
else
  MY_SCRIPT_VARIABLE="${DEPLOY_ENV}"
fi

export do_token=$(cat "$do_token_file")

main_manager_name="not-defined"
WORKDIR=$HOME/$purpose
mkdir -p $WORKDIR
chmod -R 777 $WORKDIR
main_manager_name_file_path=$WORKDIR/main_manager_name.txt
node_list_file_path=$WORKDIR/nodes_list.txt
nodes_list=""

jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    image=`echo ${i} | jq '.image'`
    snapshot_id=`echo ${i} | jq '.snapshot_id'`
    tags=`echo ${i} | jq '.tags'`


    cmd=`echo docker-machine create --driver digitalocean --digitalocean-private-networking true --digitalocean-access-token "$do_token" --digitalocean-image "$snapshot_id" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-userdata "$do_users_data_file" --digitalocean-tags "$tags" --digitalocean-private-networking "$name"`
    eval $cmd

    sleep 2m

    #update_servers $name $node_type

    if [ "$node" == "manager" ]; then
        if [ "$select_main_manager" == "false" ]; then
          update_servers $name $node_type "main_manager"
          select_main_manager="true"
        else
            update_servers $name $node_type
        fi;
     else
        update_servers $name $node_type
    fi;

#    while :
#    do
#      get_machine_command=`echo docker-machine ls --filter name=$name --format "{{.Name}}"`
#      machine=`eval $get_machine_command`
#      if [ "$machine" != "$name" ]; then
#        break
#      fi
#    done
#    node=`echo ${node_type} | sed 's/\"//g'`
#    node_name=`echo ${name} | sed 's/\"//g'`
#    nodes_list="$node_name:$node,$nodes_list"
#
#    touch $node_list_file_path
#    > "$node_list_file_path"
#    echo "$nodes_list" >> "$node_list_file_path"

#    if [ "$node" == "manager" ]; then
#        if [ "$main_manager_name" == "not-defined" ]; then
#          main_manager_name=$name
#          touch $main_manager_name_file_path
#          > "$main_manager_name_file_path"
#          echo "$main_manager_name" >> "$main_manager_name_file_path"
#        fi;
#    fi;
done

#main_manager_name=$(cat "$main_manager_name_file_path")
# Initialize Swarm Manager and tokens

#
# main_manager_name=`echo ${main_manager_name} | sed 's/\"//g'`

main_manager_name=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_name'`
main_manager_name=`echo ${main_manager_name} | sed 's/\"//g'`


docker-machine ssh $main_manager_name "docker swarm init \
        --listen-addr $(docker-machine ip ${main_manager_name}) \
            --advertise-addr $(docker-machine ip ${main_manager_name})"

export worker_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token worker -q")

export manager_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token manager -q")


nodes=`cat $node_list_file_path`
nodes=`echo ${nodes} | sed 's/\"//g'`


for node in $(echo $nodes | sed "s/,/ /g")
do
    echo $node
    nodes_infos=$(echo $node | tr ":" "\n")
    for nodes_info in $nodes_infos
    do
        if [ $nodes_info == "manager" ]
        then
           node_type="manager"
        elif [  $nodes_info == "worker" ]
        then
           node_type="worker"
        else
           name=$nodes_info
        fi
    done

    if [ "$name" == "$main_manager_name" ]; then
      continue
    fi;

    if [ "$node_type" == "worker" ]; then
        token=$worker_token
    else
        token=$manager_token
    fi;

    docker-machine ssh "$name" "docker swarm join \
        --token=${token} \
            --listen-addr $(docker-machine ip ${name}) \
                --advertise-addr $(docker-machine ip ${name}) \
                    $(docker-machine ip ${main_manager_name})"
done