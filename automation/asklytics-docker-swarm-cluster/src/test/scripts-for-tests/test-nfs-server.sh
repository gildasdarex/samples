#!/usr/bin/env bash

#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

#update_servers () {
#    # $1 parameter is the name of the project to clone
#    #get droplet id
#    droplet_id=`docker-machine inspect "$1" | jq '.Driver.DropletID'`
#    #get droplet private ip
#    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`
#
#    servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$1" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$2" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
#    > $asklytics_servers_file_path
#    echo $servers >> $asklytics_servers_file_path
#}

update_servers () {
    # $1 parameter is the name of the project to clone
    #get droplet id
    droplet_name=`echo ${1} | sed 's/\"//g'`
    echo "Add node $droplet_name info to servers.json file"
    droplet_id=`docker-machine inspect $droplet_name | jq '.Driver.DropletID'`
    echo "Node $droplet_name id is : $droplet_id"
    #get droplet private ip
    droplet_id=`echo ${droplet_id} | sed 's/\"//g'`
    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`
    echo "Node $droplet_name private ip is : $private_ip"

    #droplet_name=`echo ${1} | sed 's/\"//g'`
    private_ip=`echo ${private_ip} | sed 's/\"//g'`
    droplet_type=`echo ${2} | sed 's/\"//g'`

    servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$droplet_name" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$droplet_type" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
    > $asklytics_servers_file_path
    echo $servers >> $asklytics_servers_file_path
}

function isSwarmNode(){
    is_swarm=`docker-machine ssh $1 "docker info | grep Swarm | sed 's/Swarm: //g'" </dev/null`
    echo "1 $is_swarm"
    if [ $is_swarm == "inactive" ]; then
        echo false;
    else
        echo true;
    fi
}


function testExit(){
     if [ $1 == "null" ]
    then
      echo "Droplet $droplet_name is already added in output file"
      exit 1
    fi
}

echo $al_deployment_resources
echo $JAVA_HOME
#result=`isSwarmNode "darex"`
#echo "2 $result"
#
#optspec=":-:"
#while getopts "$optspec" optchar; do
#    case "${OPTARG}" in
#       nodes.file)
#          nodes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#          ;;
#       do.users.data)
#          do_users_data_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#          ;;
#       cluster.name)
#          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#          ;;
#    esac
#done
#
#if [ -z ${cluster_name+x} ]; then
# echo "cluster.name is missing "
# exit
#fi

#asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"
#
#if [ ! -f $asklytics_servers_file_path ]
#then
#    mkdir -p /asklytics/cluster/$cluster_name/output/
#    touch /asklytics/cluster/$cluster_name/output/servers.json
#    echo {\"servers\": []} >> $asklytics_servers_file_path
#fi

#if [[ -z "${do_token}" ]]; then
#  echo "do_token env variable  is undefined"
#  exit 1
#fi

#jq -c '.[]' "$nodes_file" | while read i; do
#    node_type=`echo ${i} | jq '.node_type'`
#    region=`echo ${i} | jq '.region'`
#    name=`echo ${i} | jq '.name'`
#    size=`echo ${i} | jq '.size'`
#    snapshot_id=`echo ${i} | jq '.snapshot_id'`
#    tags=`echo ${i} | jq '.tags'`
#    volume_id=`echo ${i} | jq '.volume_id'`
#
#    if [ $volume_id == "null" ]
#    then
#       echo "volume id is not defined "
#    elif [  $volume_id == "" ]
#    then
#       echo "volume id is empty "
#    else
#       echo "volume id is set"
#       echo $volume_id
#    fi

#
#
#
#done



#while true
#do
#  STATUS=$(curl -s -o /dev/null -w '%{http_code}' http://www.example.org)
#  if [ $STATUS -eq 200 ]; then
#    echo "Got 200! All done!"
#    break
#  else
#    echo "Got $STATUS :( Not done yet..."
#  fi
#done

