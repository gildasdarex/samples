#!/usr/bin/env bash

file="/development/asklytics/repos/automation/asklytics-docker-swarm-cluster-new/src/test/sample.txt"
pattern1="/mnt/vol_mariadb"
nfs_server_directory="/mnt/vol_mariadb"
etc_export_line=".1.1.1(rw,sync,no_root_squash,no_subtree_check)"
pattern="10.138.90.120"
replacement="/mnt1/demo_vol_datastore"

pattern1=$(echo "$pattern1" | sed 's/\//\\\//g')
#output=`sed "/$pattern1/ s/$/running/" $file`
#echo "$output" > $file

#output=`sed "/${pattern//\//\\/}/ s/$/running/" $file`
#echo "$output" > $file

found_mount_dir=`grep $pattern1 "$file"`
echo $found_mount_dir
#
if test -z "$found_mount_dir"  ; then
  echo "Some Actions # SomeString was not found"
else
  echo "Some Actions # SomeString was  found"
  #gsed -i "/$simple_pat $found_mount_dir$etc_export_line" $file
  #gsed -i "${found_mount_dir}"'/s/.*/'"${found_mount_dir} $etc_export_line" "${file}"
  #gsed "/${found_mount_dir}/ s/$/$etc_export_line/" ${file}
  nfs_server_directory_esc=$(echo "$nfs_server_directory" | sed 's/\//\\\//g')
  gsed "/${nfs_server_directory_esc}/ s/$/ $etc_export_line/" ${file} | sudo tee ${file}
  #echo $data
  #>  ${file}
  #echo $data >> ${file}
  #sed "/$nfs_server_directory_esc/ s/$/$etc_export_line/\" mount_tmp/$nfs_server_name.txt | xargs sudo echo > /etc/exports
fi


#path="/development/asklytics/repos/automation/asklytics-docker-swarm-cluster-new/create-update-nfs-server/demo/directories-managment/configs_and_properties.json"
#arr=()
#while IFS='' read -r line; do
#   arr+=("$line")
#done < <(jq 'keys[]' "$path")
#
#for i in "${arr[@]}"
#do
#   echo "$i"
#   i=`echo ${i} | sed 's/\"//g'`
#   # or do whatever with individual element of the array
#   key_value=`cat ${path} | jq  ".$i"`
#   echo $key_value
#done


#for y in 1 2 3 4 5
#do
#   for x in 1 2
#      do
#      echo "x is ${x}. "
#      if [ "$x" -eq "2" ]; then
#        exit 1
#      fi
# done
#  echo "We have finished the loop now."
#  echo "As an aside, y is now ${y}."
#done

#path="/development/asklytics/repos/automation/asklytics-docker-swarm-cluster-new/create-update-nfs-server/demo/cluster-data/"
#sed -i 's/#manager_ip/10.11.11.12/g' *
#find $path -type f | xargs sed -i  's/#manager_ip/10.11.11.12/g'
#sed -i 's/#manager_ip/10.11.11.12/g' $(find /root/ -type f)