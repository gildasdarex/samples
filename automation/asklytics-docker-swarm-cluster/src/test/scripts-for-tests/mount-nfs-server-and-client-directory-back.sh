#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_servers_file_path=""

#mount_data_from_file () {
#    # $1 parameter is the path of the file that contains the json info
#    jq -c '.[]' "$1" | while read i; do
#        droplet_names=`echo ${i} | jq '.droplet_names'`
#        nfs_server_directory=`echo ${i} | jq '.nfs_server_directory'`
#        #nfs_client_directory=`echo ${i} | jq '.nfs_client_directory'`
#
#        droplet_names=`echo "$droplet_names" | sed 's/[][]//g'`
#
#        etc_export_line="$nfs_server_directory "
#
#        for droplet_name in $(echo $droplet_names | sed "s/,/ /g")
#        do
#            droplet_name=`echo "$droplet_name" | sed -e 's/^"//' -e 's/"$//'`
#            private_ip=` cat $asklytics_servers_file_path | jq --arg droplet_name "$droplet_name" '.servers | .[] | select(.droplet_name == $droplet_name) | .droplet_private_ip'`
#
#            etc_export_line="$etc_export_line $private_ip(rw,sync,no_subtree_check)"
#        done
#
#        # TODO take a backup of /etc/exports and /etc/fstab before change it
#        docker-machine ssh $nfs_server_name "sudo echo $etc_export_line >> /etc/exports"
#
#    done
#}


mount_on_nfs_server () {
    nfs_client_name=`echo ${1} | sed 's/\"//g'`
    nfs_server_name=`echo ${2} | sed 's/\"//g'`
    nfs_server_directory=`echo ${3} | sed 's/\"//g'`

    nfs_server_name=`echo ${nfs_server_name} | sed 's/\"//g'`
    nfs_server_directory=`echo ${nfs_server_directory} | sed 's/\"//g'`
    nfs_client_name=`echo ${nfs_client_name} | sed 's/\"//g'`

    nfs_server_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_server_name "$nfs_server_name" '.servers | .[] | select(.droplet_name == $nfs_server_name) | .droplet_private_ip'`
    nfs_client_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_client_name "$nfs_client_name" '.servers | .[] | select(.droplet_name == $nfs_client_name) | .droplet_private_ip'`

    nfs_server_private_ip=`echo ${nfs_server_private_ip} | sed 's/\"//g'`
    nfs_client_private_ip=`echo ${nfs_client_private_ip} | sed 's/\"//g'`

    echo ".................. Mount $nfs_server_directory of nfs server $nfs_server_name ($nfs_server_private_ip)  to client $nfs_client_name ($nfs_client_private_ip) ........................."

    found_mount_dir=`docker-machine ssh $nfs_server_name sudo grep $nfs_server_directory /etc/exports`
    echo "............ found mount directory..........."
    echo $found_mount_dir
    etc_export_line="  $nfs_client_private_ip(rw,sync,no_root_squash,no_subtree_check)"

    docker-machine ssh $nfs_server_name "sudo mkdir -p $nfs_server_directory"

    if test -z "$found_mount_dir"  ; then
#      cmd=` echo docker-machine ssh $nfs_server_name "sudo echo \"$nfs_server_directory $etc_export_line\" >> /etc/exports"`
#      echo $cmd
#      eval $cmd
       docker-machine ssh $nfs_server_name "sudo echo \"$nfs_server_directory $etc_export_line\" >> /etc/exports"
    else
      nfs_server_directory_esc=$(echo "$nfs_server_directory" | sed 's/\//\\\//g')
      #output=`docker-machine ssh $nfs_server_name sed "/$nfs_server_directory_esc/ s/$/$etc_export_line/" /etc/exports`
      #output=`docker-machine ssh $nfs_server_name "sed \"/$nfs_server_directory_esc/ s/$/$etc_export_line/\" /etc/exports"`
      docker-machine ssh $nfs_server_name "sed \"/$nfs_server_directory_esc/ s/$/$etc_export_line/\" /etc/exports | xargs sudo echo > /etc/exports"
      #docker-machine ssh $nfs_server_name "sudo echo $output > /etc/exports"
    fi

    docker-machine ssh $nfs_server_name "sudo systemctl restart nfs-kernel-server"
}


mount_on_nfs_client () {
    nfs_client_name=`echo ${1} | sed 's/\"//g'`
    nfs_server_name=`echo ${2} | sed 's/\"//g'`
    nfs_server_directory=`echo ${3} | sed 's/\"//g'`
    nfs_client_directory=`echo ${4} | sed 's/\"//g'`

    nfs_server_name=`echo ${nfs_server_name} | sed 's/\"//g'`
    nfs_server_directory=`echo ${nfs_server_directory} | sed 's/\"//g'`
    nfs_client_name=`echo ${nfs_client_name} | sed 's/\"//g'`
    nfs_client_directory=`echo ${nfs_client_directory} | sed 's/\"//g'`

    nfs_server_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_server_name "$nfs_server_name" '.servers | .[] | select(.droplet_name == $nfs_server_name) | .droplet_private_ip'`
    nfs_client_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_client_name "$nfs_client_name" '.servers | .[] | select(.droplet_name == $nfs_client_name) | .droplet_private_ip'`

    nfs_server_private_ip=`echo ${nfs_server_private_ip} | sed 's/\"//g'`
    nfs_client_private_ip=`echo ${nfs_client_private_ip} | sed 's/\"//g'`

    echo ".................. Mount $nfs_server_directory of nfs server $nfs_server_name ($nfs_server_private_ip)  to client $nfs_client_name ($nfs_client_private_ip)  at $nfs_client_directory........................."

    docker-machine ssh $nfs_client_name "sudo mkdir -p $nfs_client_directory"
    docker-machine ssh $nfs_client_name "sudo mount -o noac $nfs_server_private_ip:$nfs_server_directory $nfs_client_directory"
    docker-machine ssh $nfs_client_name "sudo echo $nfs_server_private_ip:$nfs_server_directory $nfs_client_directory nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0 >> /etc/fstab"
}



optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       data.mount.mapping.file)
          mount_mapping_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z ${cluster_name+x} ]; then
 echo "cluster.name is missing "
 exit
fi

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


if [ ! -f $asklytics_servers_file_path ]
then
    echo "No servers existed"
    exit 1
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi

jq -c '.[]' "$mount_mapping_file" | while read i; do
    droplet_name=`echo ${i} | jq '.droplet_name'`
    nfs_mounts=`echo ${i} | jq '.nfs_mounts'`

    droplet_name=`echo ${droplet_name} | sed 's/\"//g'`

    echo "${nfs_mounts}" | jq -c '.[]' | while read i; do
        echo $i
        nfs_server_name=`echo ${i} | jq '.nfs_server_name'`
        nfs_server_directory=`echo ${i} | jq '.nfs_server_directory'`
        nfs_client_directory=`echo ${i} | jq '.nfs_client_directory'`

        nfs_server_name=`echo ${nfs_server_name} | sed 's/\"//g'`
        nfs_server_directory=`echo ${nfs_server_directory} | sed 's/\"//g'`
        nfs_client_directory=`echo ${nfs_client_directory} | sed 's/\"//g'`

        mount_on_nfs_server $droplet_name $nfs_server_name $nfs_server_directory
        #mount_on_nfs_client $droplet_name $nfs_server_name $nfs_server_directory $nfs_client_directory
    done
done