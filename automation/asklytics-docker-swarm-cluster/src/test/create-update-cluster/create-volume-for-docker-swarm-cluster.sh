#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

asklytics_volumes_file_path=""
select_main_manager="false"


#update_servers () {
#    # $1 parameter is the name of the project to clone
#    #get droplet id
#    droplet_id=`docker-machine inspect "$1" | jq '.Driver.DropletID'`
#    #get droplet private ip
#    private_ip=`curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" "https://api.digitalocean.com/v2/droplets/$droplet_id" | jq '.droplet.networks.v4 | .[] | select(.type == "private") | .ip_address'`
#
#    if [ -z "$3" ]
#    then
#        servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$1" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$2" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type}]'`
#    else
#        servers=`cat $asklytics_servers_file_path | jq  --arg droplet_name "$1" --arg droplet_private_ip "$private_ip" --arg droplet_id "$droplet_id" --arg droplet_type "$2" '.servers += [{"droplet_name": $droplet_name, "droplet_id": $droplet_id, "droplet_private_ip": $droplet_private_ip, "droplet_type": $droplet_type, "main_manager": "true"}]'`
#    fi
#
#    > $asklytics_servers_file_path
#    echo $servers >> $asklytics_servers_file_path
#}

update_volumes () {
    # $1 parameter is the name of the project to clone
    #get droplet id
    name=`echo ${1} | sed 's/\"//g'`
    volume_id=`echo ${2} | sed 's/\"//g'`
    size_gigabytes=`echo ${3} | sed 's/\"//g'`
    region=`echo ${4} | sed 's/\"//g'`
    description=`echo ${5} | sed 's/\"//g'`

    volumes=`cat $asklytics_volumes_file_path | jq  --arg name "$name" --arg volume_id "$volume_id" --arg size_gigabytes "$size_gigabytes" --arg region "$region"  --arg description "$description" '.volumes += [{"name": $name, "volume_id": $volume_id, "size_gigabytes": $size_gigabytes, "region": $region, "description":$description}]'`

    > $asklytics_volumes_file_path
    echo $volumes >> $asklytics_volumes_file_path
}


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       volumes.file)
          volumes_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z ${cluster_name+x} ]; then
 echo "cluster.name is missing "
 exit
fi

asklytics_volumes_file_path="/asklytics/cluster/$cluster_name/output/volumes.json"


if [ ! -f $asklytics_volumes_file_path ]
then
    mkdir -p /asklytics/cluster/$cluster_name/output/
    touch /asklytics/cluster/$cluster_name/output/volumes.json
    echo {\"volumes\": []} >> $asklytics_volumes_file_path
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi

jq -c '.[]' "$volumes_file" | while read i; do
    size_gigabytes=`echo ${i} | jq '.size_gigabytes'`
    name=`echo ${i} | jq '.name'`
    description=`echo ${i} | jq '.description'`
    region=`echo ${i} | jq '.region'`

    #volume_id=`curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" -d "{\"size_gigabytes\": \"$size_gigabytes\", \"name\": \"$name\", \"region\": \"$region\", \"description\": \"$description\"}" "https://api.digitalocean.com/v2/volumes" | jq '.volume.id'`
    #volume_id=`echo ${volume_id} | sed 's/\"//g'`

    #curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $do_token" -d '{"size_gigabytes":10, "name": "example", "description": "Block store for examples", "region": "nyc1"}' "https://api.digitalocean.com/v2/volumes"

    name=`echo ${name} | sed 's/\"//g'`
    region=`echo ${region} | sed 's/\"//g'`
    description=`echo ${description} | sed 's/\"//g'`

    cmd=`echo curl -X POST -H "\"Content-Type: application/json"\" -H "\"Authorization: Bearer $do_token"\" -d "'{\"size_gigabytes\": $size_gigabytes, \"name\": \"$name\", \"region\": \"$region\", \"description\": \"$description\"}'" "https://api.digitalocean.com/v2/volumes"`
    echo $cmd
    volume_id=`eval $cmd | jq '.volume.id'`
    if [ $volume_id == "null" ]
    then
       echo "Failed to create volume $name"
       exit 1
    else
       echo "Volume $name created with success. Id is $volume_id"
       update_volumes $name $volume_id $size_gigabytes $region $description
    fi


done