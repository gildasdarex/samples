#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

cp_directory () {
   dir_or_file_on_boostrap_node=`echo ${1} | sed 's/\"//g'`
   dir_or_file_on_nfs_server=`echo ${2} | sed 's/\"//g'`
   nfs_server_droplet_name=`echo ${3} | sed 's/\"//g'`

   nfs_server_private_ip=` cat $asklytics_servers_file_path | jq --arg nfs_server_name "$nfs_server_droplet_name" '.servers | .[] | select(.droplet_name == $nfs_server_name) | .droplet_private_ip'`
   main_manager_private_ip=`cat $asklytics_servers_file_path | jq '.servers | .[] | select(.main_manager == "true") | .droplet_private_ip'`

   echo "#################### Create temp directory ##################"
   mkdir tmp
   cp -r $dir_or_file_on_boostrap_node tmp
   echo "####################  Search and update main main_manager_private_ip in tmp directory##################"
   sed -i "s/#manager_ip/$main_manager_private_ip/g" $(find tmp/ -type f)

   #cmd=` echo docker-machine scp -r $dir_or_file_on_boostrap_node $nfs_server_droplet_name:$dir_or_file_on_nfs_server/.`</dev/null
   docker-machine scp -r tmp/* $nfs_server_droplet_name:$dir_or_file_on_nfs_server/. </dev/null
   #cmd=` echo docker-machine scp -r tmp/* $nfs_server_droplet_name:$dir_or_file_on_nfs_server/.`

   #echo $cmd
   #eval $cmd

   echo "#################### Delete temp directory ##################"
   rm -r tmp/
}

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       config.properties.json)
          config_and_properties_json="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       cluster.name)
          cluster_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

if [ -z ${cluster_name+x} ]; then
 echo "cluster.name is missing "
 exit
fi

asklytics_servers_file_path="/asklytics/cluster/$cluster_name/output/servers.json"


if [ ! -f $asklytics_servers_file_path ]
then
    echo "No servers existed"
    exit 1
fi

if [[ -z "${do_token}" ]]; then
  echo "do_token env variable  is undefined"
  exit 1
fi

list_keys=()
while IFS='' read -r line; do
   list_keys+=("$line")
done < <(jq 'keys[]' "$config_and_properties_json")


for i in "${list_keys[@]}"
do
   # or do whatever with individual element of the array
   key_value=`cat ${config_and_properties_json} | jq  ".$i"`

   echo "${key_value}" | jq -c '.[]' | while read i; do
        dir_or_file_on_boostrap_node=`echo ${i} | jq '.dir_or_file_on_boostrap_node'`
        dir_or_file_on_nfs_server=`echo ${i} | jq '.dir_or_file_on_nfs_server'`
        nfs_server_droplet_name=`echo ${i} | jq '.nfs_server_droplet_name'`

        nfs_server_droplet_name=`echo ${nfs_server_droplet_name} | sed 's/\"//g'`
        dir_or_file_on_nfs_server=`echo ${dir_or_file_on_nfs_server} | sed 's/\"//g'`
        dir_or_file_on_boostrap_node=`echo ${dir_or_file_on_boostrap_node} | sed 's/\"//g'`

        cp_directory $dir_or_file_on_boostrap_node $dir_or_file_on_nfs_server $nfs_server_droplet_name
   done

done
