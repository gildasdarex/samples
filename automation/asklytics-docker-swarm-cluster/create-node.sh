#!/usr/bin/env bash

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
        do.token.file)
          do_token_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        name)
          name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
        region)
          region="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
        image)
          image="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
        size)
          size="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
    esac
done

export do_token=$(cat "$do_token_file")

cmd=`echo docker-machine create --driver digitalocean --digitalocean-access-token "$do_token" --digitalocean-image "$image" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-private-networking "$name"`
$cmd
