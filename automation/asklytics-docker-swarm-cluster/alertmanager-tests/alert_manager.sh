#!/usr/bin/env bash
alerts1='[
  {
    "labels": {
       "alertname": "DiskRunningFull",
       "dev": "sda4",
       "instance": "example3"
     },
     "annotations": {
        "info": "The disk sda1 is running full",
        "summary": "please check the instance example1"
      }
  }
]'
curl -XPOST -d"$alerts1" http://165.22.157.182:9093/alertmanager/api/v1/alerts