#!/bin/bash

temp_work_dir_path="/home/temp_docker_blulytics_apps"
temp_work_dir_pylytics_path="/home/temp_docker_pylytics_apps"


APPS[0]="blulytics"


typeset -A APPS_NAME_IN_WHEEL_MAP
APPS_NAME_IN_WHEEL_MAP["blulytics"]="asklytics_blulytics"

PYLYTICS_APP_NAME="asklytics_algos"