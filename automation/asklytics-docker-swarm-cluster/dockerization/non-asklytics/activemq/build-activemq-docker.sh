#!/bin/bash

. config.sh
. ../../registry.config.sh

MODE="dev"

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
      version)
        VERSION="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      mode)
        MODE="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
    esac
done
    
if [[ "$MODE" == "prod" ]]; then
    REG_URL=$prod_registry
else
    REG_URL=$intg_registry
fi

docker login $REG_URL
if [[ $? -ne 0 ]]
    then
        echo "Cannot login to registry. Please try again."
        exit 1
    fi

cwd=$(pwd)

rm -rf $temp_work_dir_path
mkdir $temp_work_dir_path
cp -r ./docker-src/* $temp_work_dir_path

cd $temp_work_dir_path

cmd="docker build --no-cache -t asklytics-activemq:v$VERSION -t asklytics-activemq:latest \
--file ./Dockerfile \
."

$cmd

docker tag asklytics-activemq:v$VERSION $REG_URL/asklytics-activemq:v$VERSION
docker push $REG_URL/asklytics-activemq:v$VERSION

docker tag asklytics-activemq:latest $REG_URL/asklytics-activemq:latest
docker push $REG_URL/asklytics-activemq:latest
