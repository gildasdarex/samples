#!/bin/bash

. ./config.sh
. ../registry.config.sh

MODE="dev"

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
      version)
        VERSION="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      mode)
        MODE="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
      repo.path)
        repo_directory="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
        ;;
    esac
done
    
if [[ "$MODE" == "prod" ]]; then
    REG_URL=$prod_registry
else
    REG_URL=$intg_registry
fi

docker login $REG_URL
if [[ $? -ne 0 ]]
    then
        echo "Cannot login to registry. Please try again."
        exit 1
    fi

cwd=$(pwd)

rm -rf $temp_work_dir_path
mkdir $temp_work_dir_path
cp -r ./docker-src/* $temp_work_dir_path
cp -r $repo_directory/* $temp_work_dir_path
cp -r $ipw_repo_directory $temp_work_dir_path

cd $repo_directory
branch=$(git status | grep "On branch" | sed 's/^On branch //')

cd $temp_work_dir_path
find . -name 'node_modules' -type d -prune -exec rm -rf '{}' +

cmd="docker build --no-cache -t analytics-ui:v$VERSION-$MODE -t analytics-ui:latest-$MODE \
--build-arg version=$VERSION \
--build-arg angular_main_app_path=$angular_main_app_rel_path  \
--build-arg angular_pricing_app_path=$angular_pricing_app_rel_path \
--file ./Dockerfile \
--build-arg env=$MODE \
--build-arg branch=$branch ."

$cmd

echo " "
echo "Tagging and Pushing to Docker Registry ..."
echo 'Running : docker tag analytics-ui:v'$VERSION-$MODE $REG_URL'/analytics-ui:v'$VERSION-$MODE
docker tag analytics-ui:v$VERSION-$MODE $REG_URL/analytics-ui:v$VERSION-$MODE
echo '!!! If docker push fails, please run the command below again !!!'
echo 'Running : docker push '$REG_URL'/analytics-ui:v'$VERSION
docker push $REG_URL/analytics-ui:v$VERSION-$MODE
echo ''
echo 'Running : docker tag analytics-ui:latest-'$MODE $REG_URL'/analytics-ui:latest-'$MODE
docker tag analytics-ui:latest-$MODE $REG_URL/analytics-ui:latest-$MODE
echo '!!! If docker push fails, please run the command below again !!!'
echo 'docker push '$REG_URL'/analytics-ui:latest-'$MODE
docker push $REG_URL/analytics-ui:latest-$MODE
