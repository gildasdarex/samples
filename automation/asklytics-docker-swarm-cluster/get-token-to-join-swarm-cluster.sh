#!/usr/bin/env bash
#Deploy RancherOS Virtual Machines
#Switch to latest Docker Engine available
#Switch to Debian console

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       do.token.file)
          do_token_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       node.type)
          node_type="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       manager.node)
          main_manager_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       new.node)
          new_node="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

export do_token=$(cat "$do_token_file")
timestamp=$(date +%s)
WORKDIR=$HOME/$timestamp
mkdir -p $WORKDIR
chmod -R 777 $WORKDIR
node_list_file_path=$WORKDIR/nodes_list.txt
nodes_list=""

jq -c '.[]' "$nodes_file" | while read i; do
    node_type=`echo ${i} | jq '.node_type'`
    region=`echo ${i} | jq '.region'`
    name=`echo ${i} | jq '.name'`
    size=`echo ${i} | jq '.size'`
    image=`echo ${i} | jq '.image'`


    cmd=`echo docker-machine create --driver digitalocean --digitalocean-access-token "$do_token" --digitalocean-image "$image" --digitalocean-region "$region" --digitalocean-size "$size" --digitalocean-private-networking "$name"`
    eval $cmd

    node=`echo ${node_type} | sed 's/\"//g'`
    node_name=`echo ${name} | sed 's/\"//g'`
    nodes_list="$node_name:$node,$nodes_list"

    touch $node_list_file_path
    > "$node_list_file_path"
    echo "$nodes_list" >> "$node_list_file_path"

done


export worker_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token worker -q")

export manager_token=$(docker-machine ssh $main_manager_name "docker swarm \
    join-token manager -q")


nodes=`cat $node_list_file_path`
nodes=`echo ${nodes} | sed 's/\"//g'`


for node in $(echo $nodes | sed "s/,/ /g")
do
    echo $node
    nodes_infos=$(echo $node | tr ":" "\n")
    for nodes_info in $nodes_infos
    do
        if [ $nodes_info == "manager" ]
        then
           node_type="manager"
        elif [  $nodes_info == "worker" ]
        then
           node_type="worker"
        else
           name=$nodes_info
        fi
    done


    if [ "$node_type" == "worker" ]; then
        token=$worker_token
    else
        token=$manager_token
    fi;

    docker-machine ssh "$name" "docker swarm join \
        --token=${token} \
            --listen-addr $(docker-machine ip ${name}) \
                --advertise-addr $(docker-machine ip ${name}) \
                    $(docker-machine ip ${main_manager_name})"
done