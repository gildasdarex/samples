#!/bin/bash

if [ "$1" = "-h" ]
then
  echo "Options: "
  echo "--prod loads ./config/config-prod.sh configurations"
  echo "--int  (DEFAULT) loads ./config/config-intn.sh configurations"
  exit 0
fi

while getopts v:a: option
do
 case "${option}"
 in
 a) APPS_IN=${OPTARG};;
 v) VERSION=${OPTARG};;
 esac
done

config=./config/config-int.sh
if [ "$1" = "--prod" ]
then   
	config=./config/config-prod.sh
fi
##Load config
. $config

#move to local mave where wars folder will be present.
cd $LOCAL_MAVEN_REPO_PATH/$GENERATED_FOLDER_NAME

if [[ -n "$APPS_IN" ]]; then
  echo $APPS_IN
  IFS=',' read -ra APPS_TO_EXEC <<< "$APPS_IN"
else
  APPS_TO_EXEC=("${APPS[@]}")
fi  
echo "Apps to restart : ${APPS_TO_EXEC[*]}"


for i in "${APPS_TO_EXEC[@]}"
do
  app=$i
  ip=${APPS_IP_MAP[$i]}
  port=${APPS_TUNNEL_PORT_MAP[$i]}
  lsof -ti:$port | xargs kill -9
  ssh -f -N -L $port:$ip:22 $TARGET_SSH_SERVER_USERNAME@$TARGET_SSH_SERVER_IP
  if [[ -n "$VERSION" ]]; then
    echo "Updating version to $VERSION and restarting ${APPS_SUPERVISOR_APP_NAME_MAP[$i]}"
    ls
    latest_war=`ls -t1 $app-$VERSION* |  head -n 1`
    v_str="war=$latest_war"
    echo "Restarting with was $latest_war"
    supervisor_cmd="supervisorctl restart ${APPS_SUPERVISOR_APP_NAME_MAP[$i]}"
    ssh -p $port $TARGET_SSH_SERVER_USERNAME@localhost "echo $v_str>/usr/local/bin/config-${APPS_SUPERVISOR_APP_NAME_MAP[$i]}.sh;$supervisor_cmd"
  else
    echo "No version supplied. Using the last version and restarting the app"
    supervisor_cmd="supervisorctl restart ${APPS_SUPERVISOR_APP_NAME_MAP[$i]}"
    ssh -p $port $TARGET_SSH_SERVER_USERNAME@localhost "$supervisor_cmd"
  fi

done

