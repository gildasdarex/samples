#!/bin/bash

if [ "$1" = "-h" ]
then
  echo "Options: "
  echo "--prod loads ./config/config-prod.sh configurations"
  echo "--int  (DEFAULT) loads ./config/config-intn.sh configurations"
  exit 0
fi

config=./config/config-int.sh
if [ "$1" = "--prod" ]
then   
	config=./config/config-prod.sh
fi
##Load config
. $config

## Store common commands in to variables
move_to_wheels_repo="cd $LOCAL_WHEEL_REPO_PATH"
open_ssh_tunnel="ssh -f -N -L $TUNNEL_PORT:$TARGET_SERVER_VPN_IP:22 $TARGET_SSH_SERVER_USERNAME@$TARGET_SSH_SERVER_IP"
scp_through_tunnel="scp -P $TUNNEL_PORT -r ./* $TARGET_SERVER_VPN_USERNAME@127.0.0.1:$TARGET_WHEELS_FOLDER_PATH"


## Process start
$move_to_wheels_repo
echo "The below wars will be transferred"
ls $LOCAL_WHEEL_REPO_PATH

$open_ssh_tunnel
$scp_through_tunnel

