#!/bin/bash

export TARGET_SSH_SERVER_IP="45.55.241.212"
export TARGET_SSH_SERVER_USERNAME="root"

export TARGET_SERVER_VPN_IP="10.8.0.10"
export TARGET_SERVER_VPN_USERNAME="root"
export TUNNEL_PORT="1401"

export TARGET_FOLDER_PATH="/mnt/wars"
export GENERATED_FOLDER_NAME="wars"
export LOCAL_MAVEN_REPO_PATH="/development/repos/asklytics/local-maven-repo/com/asklytics"

export TARGET_WHEELS_FOLDER_PATH="/mnt/wheels"
export LOCAL_WHEEL_REPO_PATH="/development/repos/asklytics/pylytics/local-wheel-repo"

APPS[0]="asklytics-infra"
APPS[1]="asklytics-prov-service"
APPS[2]="asklytics-camel-analytics-service"
APPS[3]="asklytics-camel-provisioning-service"
APPS[4]="asklytics-analytics-service"
APPS[5]="asklytics-ui-data-service"
APPS[6]="asklytics-ml-api-app"

typeset -A APPS_TUNNEL_PORT_MAP
APPS_TUNNEL_PORT_MAP["asklytics-infra"]="1235"
APPS_TUNNEL_PORT_MAP["asklytics-prov-service"]="1236"
APPS_TUNNEL_PORT_MAP["asklytics-camel-analytics-service"]="1237"
APPS_TUNNEL_PORT_MAP["asklytics-camel-provisioning-service"]="1238"
APPS_TUNNEL_PORT_MAP["asklytics-analytics-service"]="1239"
APPS_TUNNEL_PORT_MAP["asklytics-ui-data-service"]="1240"
APPS_TUNNEL_PORT_MAP["asklytics-ml-api-app"]="1241"

typeset -A APPS_IP_MAP
APPS_IP_MAP["asklytics-infra"]="10.8.0.6"
APPS_IP_MAP["asklytics-prov-service"]="10.8.0.6"
APPS_IP_MAP["asklytics-camel-analytics-service"]="10.8.0.6"
APPS_IP_MAP["asklytics-camel-provisioning-service"]="10.8.0.6"
APPS_IP_MAP["asklytics-analytics-service"]="10.8.0.3"
APPS_IP_MAP["asklytics-ui-data-service"]="10.8.0.3"
APPS_IP_MAP["asklytics-ml-api-app"]="10.8.0.4"

typeset -A APPS_SUPERVISOR_APP_NAME_MAP
APPS_SUPERVISOR_APP_NAME_MAP["asklytics-infra"]="infra-service"
APPS_SUPERVISOR_APP_NAME_MAP["asklytics-prov-service"]="infra-prov-service"
APPS_SUPERVISOR_APP_NAME_MAP["asklytics-camel-analytics-service"]="camel-analytics"
APPS_SUPERVISOR_APP_NAME_MAP["asklytics-camel-provisioning-service"]="camel-prov"
APPS_SUPERVISOR_APP_NAME_MAP["asklytics-analytics-service"]="analytics-service"
APPS_SUPERVISOR_APP_NAME_MAP["asklytics-ui-data-service"]="ui-data-service"
APPS_SUPERVISOR_APP_NAME_MAP["asklytics-ml-api-app"]="ml-api"

APPS_WAR_TARGET_FOLDER="/development/wars/"
