#!/bin/bash

if [ "$1" = "-h" ]
then
  echo "Options: "
  echo "--prod loads ./config/config-prod.sh configurations"
  echo "--int  (DEFAULT) loads ./config/config-intn.sh configurations"
  exit 0
fi

config=./config/config-int.sh
if [ "$1" = "--prod" ]
then   
	config=./config/config-prod.sh
fi
##Load config
. $config

## Store common commands in to variables
move_to_maven_repo="cd $LOCAL_MAVEN_REPO_PATH"
delete_previous_generated_folder="rm -Rf $GENERATED_FOLDER_NAME"
make_generated_folder="mkdir $GENERATED_FOLDER_NAME"
##open_ssh_tunnel="ssh -f -N -L $TUNNEL_PORT:$TARGET_SERVER_VPN_IP:22 $TARGET_SSH_SERVER_USERNAME@$TARGET_SSH_SERVER_IP"
#scp_through_tunnel="scp -P $TUNNEL_PORT -r $GENERATED_FOLDER_NAME/* $TARGET_SERVER_VPN_USERNAME@127.0.0.1:$TARGET_FOLDER_PATH"


# Process start

$move_to_maven_repo

#Delete previously generated folder
if [ -d "$GENERATED_FOLDER_NAME" ]; then $delete_previous_generated_folder; fi
$make_generated_folder

#eMove all wars to generated folder
find . -maxdepth 10 -mindepth 1 -type d -exec sh -c '(echo {} && cd {} && cp -p *.war '$LOCAL_MAVEN_REPO_PATH/$GENERATED_FOLDER_NAME' && echo)' \;

echo "The below wars will be transferred"
ls $LOCAL_MAVEN_REPO_PATH/$GENERATED_FOLDER_NAME

###$open_ssh_tunnel
####rsync -azP -e "ssh -p $TUNNEL_PORT" $GENERATED_FOLDER_NAME/ $TARGET_SERVER_VPN_USERNAME@127.0.0.1:$TARGET_FOLDER_PATH


APPS_TO_EXEC=("${APPS[@]}")

for i in "${APPS_TO_EXEC[@]}"
do	
  app=$i
  ip=${APPS_IP_MAP[$i]}
  port=${APPS_TUNNEL_PORT_MAP[$i]}

  echo "Transfering"
  ls $LOCAL_MAVEN_REPO_PATH/$GENERATED_FOLDER_NAME/$app*
  echo "to $ip"

  lsof -ti:$port | xargs kill -9
  ssh -f -N -L $port:$ip:22 $TARGET_SSH_SERVER_USERNAME@$TARGET_SSH_SERVER_IP
  rsync -azP -e "ssh -p $port" $GENERATED_FOLDER_NAME/$app* $TARGET_SERVER_VPN_USERNAME@127.0.0.1:$APPS_WAR_TARGET_FOLDER
done
