#Running 
./publish.sh



#Options: 
--prod loads ./config/config-prod.sh configurations
--int  (DEFAULT) loads ./config/config-intn.sh configurations

example : ./publish --prod



#Configuration

TARGET_SSH_SERVER_IP => SSH server IP
TARGET_SSH_SERVER_USERNAME => SSH server UserName

TARGET_SERVER_VPN_IP => The VPN local IP of the server where the wars needs to be uploaded
TARGET_SERVER_VPN_USERNAME => Username of the server in the VPN

TARGET_FOLDER_PATH => The path in the remote sever where the wars needs to be uploaded.
GENERATED_FOLDER_NAME => Temproary filename which should be used while collecting wars in the build server.
LOCAL_MAVEN_REPO_PATH => Path to local maven repo . NOTE the path should expaned till the the projects folder are present.

