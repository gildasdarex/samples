const props = require('../../config/config-properties')
const login = require('../../components/auth/login').login;
const onboarding = require('../../components/general/onboarding')
const subscription = require('../../components/subscription/subscription')


describe('Test suite : Subscription', function() {
    this.timeout(1000000);
    before(async ()=> {
        await onboarding.onBoard(4,1);  //JMeter Insigts - Community
        await onboarding.checkProvisioning(4);
    });
    afterEach(async ()=> {
    });
    it('Upgrade subscription', async ()=> {
       await subscription.upgradeSubscription(4,2);
    });
    it('Change restrictions in subscription', async ()=> {
        await subscription.changeRestriction(4,2);
     });
     it('Change BillingCycle in subscription', async ()=> {
        await subscription.changeBillingCycle(4,2);
     });
     
    it('Cancel Subscription', async ()=> {
        await subscription.cancelSubscription(4,2);
     });
});

