const props = require('../../config/config-properties')
const logout = require('../../components/auth/login').logout;
const onboarding = require('../../components/general/onboarding')


describe('Test suite : Onboarding', function() {
    this.timeout(1000000);
    before(async ()=> {
    });
    after(async ()=> {
    });
    it('Onboarding - Community Edition', async ()=> {
        await onboarding.onBoard(4,1);  //JMeter Insigts - Community
        await onboarding.checkProvisioning(4);
    });

    it('Onboarding - Standard Edition', async ()=> {
        await onboarding.onBoard(4,2);  //JMeter Insigts - Standard
        await onboarding.checkProvisioning(4);
    });
});

