const userManagement = require('../../components/general/user-management')
const props = require('../../config/config-properties')
const login = require('../../components/auth/login').login;
const logout = require('../../components/auth/login').logout;

describe('Test suite : User Management', function() {
    this.timeout(1000000);
    before(async ()=> {
        await login();
    });
    after(async ()=> {
        await logout();
    });
    it('Create User', async ()=> {
        await userManagement.createUser(props.userManagement.users[0]);
        await userManagement.checkIfUserExistInList(props.userManagement.users[0]);
    });
    it('Update User', async ()=> {
        await userManagement.updateUser(props.userManagement.users[0]);
    });
    it('Deactivate User', async ()=> {
        await userManagement.activaeOrDeactivateUser(props.userManagement.users[0],"deactivate");
    });
    it('Activate User', async ()=> {
        await userManagement.activaeOrDeactivateUser(props.userManagement.users[0],"activate");
    });
});

