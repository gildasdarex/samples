const mlog = require('mocha-logger');
    const login = require('../../components/auth/login').login;
const runAnalysis = require('../../components/analysis/run-analysis')
const analysisStatus = require('../../components/analysis/analysis-result-status');
const analysisChartsCheck = require('../../components/analysis/analysis-charts-check');

const props = require('../../config/config-properties')

describe('Test suite : Running Analysis', function() {
    this.timeout(1000000000);
    before(async ()=> {
        await login()
    })
    beforeEach(async ()=> {
    });
    afterEach(async ()=> {
    });
    it('Running Analysis', async ()=> {
        for(let file of props.files["jmeter-insights"]){
            let analysisName = await runAnalysis.runAnalysisOfType("jmeter-insights",file);
            await analysisStatus.ifWaitForAnalysisStatusSuccess(analysisName);
            await analysisChartsCheck.checkCharts(analysisName)
        }
    });
});
