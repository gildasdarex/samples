var fs = require('fs');
const props = require('../config/config-properties')

if (!fs.existsSync(props.screenShotPath)){
    fs.mkdirSync(props.screenShotPath);
}

exports.saveScreenshot = async function(driver,filename) {
    let data = await driver.takeScreenshot();
    fs.writeFile(props.screenShotPath+"/"+filename+Date.now()+".png", data.replace(/^data:image\/png;base64,/,''), 'base64', function(err) {
        if(err) throw err;
    });
};
