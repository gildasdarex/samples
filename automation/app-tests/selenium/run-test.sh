#! /bin/bash

mocha ./spec/general/onboarding.js
mocha ./spec/general/user-management.js
mocha ./spec/subscription/subscription.js
mocha ./spec/analysis/run-analysis.js