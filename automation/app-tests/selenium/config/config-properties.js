//Below is the singleton generator. Singleton is need to preserve random values same in a testing session
var fs = require('fs');

let properties = (function(){
    function properties() {
        screenShotPath = "../../screenShots-"+Date.now();

        //Create default user manually and login once before running the tets.
        defaultUsername = "test100006@test100006.com"
        defaultPassword = "Test123$"

        onBoarding =  {
            email : "tes"+Date.now()+"@tes"+Date.now()+".com",
            password : defaultPassword,
            title : "Mr",
            firstName : "Alan",
            lastName : "M",
            company : "Alpa"+Date.now(),
            department : "dev"
        }

        //localFileBasePath is the the path to the conatiner folders of all asklytics repo
        const locaFileBasePath = "/home/george/dev/repos/asklytics"
        files = {
            "jmeter-insights" : [
                [ //Array represents/contains the files the needed to be uploaded for this analysis.
                    locaFileBasePath+"/commons/data-collector-transformer/src/test/test-data/jmeter/jmx-metrics/c201701110002-jmeter-log-jmx-metrics.csv",
                    locaFileBasePath+"/commons/data-collector-transformer/src/test/test-data/jmeter/jmx-metrics/c201701110002-jmeter-perfmon-jmx-metrics.csv"
                ]
            ]
        }
        userManagement = {
            users : [
                {
                    email : "t"+Date.now()+"@"+Date.now()+".com",
                    firstName : "Bob",
                    lastName : "M",
                    productAccess : {
                        4 : "Editor"
                    },
                    role : "Admin"
                }
            ]
        }
        return {
            defaultUsername : defaultUsername,
            defaultPassword : defaultPassword,
            onBoarding : onBoarding,
            locaFileBasePath : locaFileBasePath,
            userManagement : userManagement,
            files : files,
            screenShotPath : screenShotPath
        }
    }


    var instance;
    return {
        getInstance: function(){
            if (instance == null) {
                instance = new properties();
                // Hide the constructor so the returned objected can't be new'd...
                instance.constructor = null;
            }
            return instance;
        }
    };
})();

module.exports = properties.getInstance();
