const baseURL = "http://localhost:4200"

exports.baseURL = baseURL
exports.loginURL = baseURL+"/login"
exports.userManagementURL = baseURL+"/dashboard/users-list"
exports.onBoardingURL= baseURL+"/signup/4"
exports.subscriptionsURL = baseURL+"/dashboard/subscriptions"
exports.analysisResultsURL = baseURL+"/dashboard/analysis-results"

exports.createnAnalysisTypesURLS = {
    "jmeter-insights" : baseURL+"/dashboard/jmeter-insights/4/2"
}
