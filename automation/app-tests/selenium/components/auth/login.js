const {Builder, By, Key, until} = require('selenium-webdriver');
const driver = require('../init').main.driver;
const urls = require('../../config/config-urls');
const props = require('../../config/config-properties')
const mlog = require('mocha-logger');

exports.login = async ()=> {
    //Below code will loggin anf click the continue button in home page
    mlog.log("(INFO) Logging in to the App")
    await driver.get(urls.loginURL);
    await driver.wait(until.elementLocated(By.name('email')));
    await driver.wait(until.elementIsVisible(driver.findElement(By.name('email'))));
    driver.findElement(By.name('email')).sendKeys(props.defaultUsername);
    driver.findElement(By.name('password')).sendKeys(props.defaultPassword);
    driver.findElement(By.css("button[type='submit']")).click();
    await driver.wait(until.elementLocated(By.id('homeContinue_btn')));
    //driver.findElement(By.id('homeContinue_btn')).click();
}

exports.logout = async()=>{
    mlog.log("(INFO) Logging out of the App");
    await driver.wait(until.elementLocated(By.id('topbar_right_menu_a')));
    driver.findElement(By.id('topbar_right_menu_a')).click();
    driver.findElement(By.id('logout_a')).click();
}
