const {Builder, By, Key, until} = require('selenium-webdriver');
const driver = require('../init').main.driver;

const mlog = require('mocha-logger');

let dataUpload = async (files) =>{
    await driver.wait(until.elementLocated(By.id('datasetCollapse_0')));
    driver.findElement(By.id('datasetCollapse_0')).click();
    await driver.wait(until.elementLocated(By.id('file_-1')));
    //iterate through all the files of analysis and upload
    files.map((file)=>{
        mlog.log("(INFO) Uploading "+file)
        driver.findElement(By.name('file_-1')).sendKeys(file);
    })
}
exports.dataUpload = dataUpload
