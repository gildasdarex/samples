const {Builder, By, Key, until} = require('selenium-webdriver');
const driver = require('../init').main.driver;
const urls = require('../../config/config-urls');
const props = require('../../config/config-properties')
const mlog = require('mocha-logger');
const assert = require('chai').assert;


exports.cancelSubscription = async (productId,editionId)=> {
    await driver.get(urls.subscriptionsURL);

    await driver.wait(until.elementLocated(By.id(productId+'_upgrade_product_btn')))
   driver.findElement(By.id(productId+'_upgrade_product_btn')).click();

   await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_change_subscription_btn')));
   driver.findElement(By.id(productId+'_'+editionId+'_change_subscription_btn')).click();

   await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_cancelSubscription_btn')));
   driver.findElement(By.id(productId+'_'+editionId+'_cancelSubscription_btn')).click();

   await driver.wait(until.elementLocated(By.id(productId+'_product_subscription_confirm_btn')));
   await driver.sleep(4000);
   driver.findElement(By.id(productId+'_product_subscription_confirm_btn')).click();
   
   await driver.wait(until.elementLocated(By.id("swal2-title")));
   let status = await driver.findElement(By.id('swal2-title')).getText();
   if(status && status.toLowerCase()=='success')
       return true;
   else
       assert.isOk(false,"Cancel subscription failed.");
}

exports.changeBillingCycle = async (productId,editionId)=> {
    await driver.get(urls.subscriptionsURL);

    await driver.wait(until.elementLocated(By.id(productId+'_upgrade_product_btn')))
   driver.findElement(By.id(productId+'_upgrade_product_btn')).click();

   await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_change_subscription_btn')));
    driver.findElement(By.id(productId+'_'+editionId+'_change_subscription_btn')).click();

    await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_changeBillingCycle_btn')));
    driver.findElement(By.id(productId+'_'+editionId+'_changeBillingCycle_btn')).click();

    await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_change_billingCycle_cb')));
    driver.findElement(By.id(productId+'_'+editionId+'_change_billingCycle_cb')).click();
    
    await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_apply_change_subscription_btn')));
    driver.findElement(By.id(productId+'_'+editionId+'_apply_change_subscription_btn')).click();

   await driver.wait(until.elementLocated(By.id(productId+'_product_subscription_confirm_btn')));
   await driver.sleep(4000);
   driver.findElement(By.id(productId+'_product_subscription_confirm_btn')).click();

   await driver.wait(until.elementLocated(By.id("swal2-title")));
   let status = await driver.findElement(By.id('swal2-title')).getText();
   if(status && status.toLowerCase()=='success')
       return true;
   else
       assert.isOk(false,"Change Restriction Failed");
}


exports.changeRestriction = async (productId,editionId)=> {
    await driver.get(urls.subscriptionsURL);

    await driver.wait(until.elementLocated(By.id(productId+'_upgrade_product_btn')))
   driver.findElement(By.id(productId+'_upgrade_product_btn')).click();

   await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_change_subscription_btn')));
    driver.findElement(By.id(productId+'_'+editionId+'_change_subscription_btn')).click();

    await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_updateSubscription_btn')));
    driver.findElement(By.id(productId+'_'+editionId+'_updateSubscription_btn')).click();

    await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_change_restriction++1')));
    driver.findElement(By.id(productId+'_'+editionId+'_change_restriction++1')).click();
    
    await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_apply_change_subscription_btn')));
    driver.findElement(By.id(productId+'_'+editionId+'_apply_change_subscription_btn')).click();

   await driver.wait(until.elementLocated(By.id(productId+'_product_subscription_confirm_btn')));
   await driver.sleep(4000);
   driver.findElement(By.id(productId+'_product_subscription_confirm_btn')).click();

   await driver.wait(until.elementLocated(By.id("swal2-title")));
   let status = await driver.findElement(By.id('swal2-title')).getText();
   if(status && status.toLowerCase()=='success')
       return true;
   else
       assert.isOk(false,"Change Restriction Failed");
}

exports.upgradeSubscription = async (productId,editionId)=> {
    await driver.get(urls.subscriptionsURL);

    await driver.wait(until.elementLocated(By.id(productId+'_upgrade_product_btn')))
    driver.findElement(By.id(productId+'_upgrade_product_btn')).click();

    await driver.wait(until.elementLocated(By.id(productId+'_'+editionId+'_upgrade_subscription_btn')));
    driver.findElement(By.id(productId+'_'+editionId+'_upgrade_subscription_btn')).click();

    await driver.wait(until.elementLocated(By.id(productId+'_product_subscription_confirm_btn')));
    await driver.sleep(500);
    driver.findElement(By.id(productId+'_product_subscription_confirm_btn')).click();

    await driver.wait(until.elementLocated(By.id("swal2-title")));
    let status = await driver.findElement(By.id('swal2-title')).getText();
    if(status && status.toLowerCase()=='success')
        return true;
    else
        assert.isOk(false,"Upgrade Subscription Failed");

}