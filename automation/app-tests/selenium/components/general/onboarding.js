const {Builder, By, Key, until} = require('selenium-webdriver');
const driver = require('../init').main.driver;
const urls = require('../../config/config-urls');
const props = require('../../config/config-properties')
const mlog = require('mocha-logger');
const assert = require('chai').assert;
const saveScreenshot = require('../../utils/saveScreenshot').saveScreenshot;


exports.onBoard = async (productId,editionId)=> {
    await driver.get(urls.onBoardingURL);
    await driver.wait(until.elementLocated(By.id(''+productId+'_'+editionId+'_subscribe_btn')));
    driver.findElement(By.id(''+productId+'_'+editionId+'_subscribe_btn')).click();

    await driver.wait(until.elementLocated(By.name('email')));
    driver.findElement(By.name('email')).sendKeys(''+productId+editionId+props.onBoarding.email);
    driver.findElement(By.name('_action_signUpEmail')).click();

    await driver.wait(until.elementLocated(By.name('title')));
    driver.findElement(By.name('title')).sendKeys(props.onBoarding.title);
    driver.findElement(By.name('firstName')).sendKeys(props.onBoarding.firstName);
    driver.findElement(By.name('lastName')).sendKeys(props.onBoarding.lastName);
    driver.findElement(By.name('company')).sendKeys(''+productId+editionId+props.onBoarding.company);
    driver.findElement(By.name('department')).sendKeys(props.onBoarding.department);  
    driver.findElement(By.name('newPassword')).sendKeys(props.onBoarding.password);
    driver.findElement(By.name('confirmPassword')).sendKeys(props.onBoarding.password);
    driver.findElement(By.css("#country option:nth-child(10)")).click();
    driver.findElement(By.css("#timezone option:nth-child(10)")).click();
    driver.findElement(By.name('create_onboarding_customer_btn')).click();

    await driver.wait(until.elementLocated(By.name('finish_create_onboarding_customer')));
    driver.findElement(By.name('finish_create_onboarding_customer')).click();
}

exports.checkProvisioning = async (productId)=> {
    mlog.log("(INFO) Checking provisioning");
    try {
        await driver.wait(until.elementLocated(By.id(''+productId+'_provisioning_success')),120000);
        return true;
    }catch(e){
        await saveScreenshot(driver,"onBoard_checkProvisioning");
        assert.isOk(false,"Timeout while waiting for provisioning details.");
    }
}