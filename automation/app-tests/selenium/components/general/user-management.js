const {Builder, By, Key, until} = require('selenium-webdriver');
const driver = require('../init').main.driver;
const urls = require('../../config/config-urls');
const props = require('../../config/config-properties')
const mlog = require('mocha-logger');
const assert = require('chai').assert;
const saveScreenshot = require('../../utils/saveScreenshot').saveScreenshot;

exports.createUser = async (user)=> {
    //Below code will loggin anf click the continue button in home page

    await driver.get(urls.userManagementURL);
    await driver.wait(until.elementLocated(By.id('createUser_link')));
    driver.findElement(By.id('createUser_link')).click()

    await driver.wait(until.elementLocated(By.name('email')));
    driver.findElement(By.name('email')).sendKeys(user.email);
    driver.findElement(By.name('firstName')).sendKeys(user.firstName);
    driver.findElement(By.name('lastName')).sendKeys(user.lastName);

    await driver.wait(until.elementLocated(By.css("#accountRole option[txt="+user.role+"]")));
    driver.findElement(By.css("#accountRole option[txt="+user.role+"]")).click();
    driver.findElement(By.id('submit_btn')).click();
}

exports.checkIfUserExistInList = async (user)=> {
    await driver.wait(until.elementLocated(By.id("users_table")));
    try{
        await driver.findElement(By.id(user.email))
    }catch(e){
        await saveScreenshot(driver,"checkIfUserExistInList");
        assert.isOk(false,"User creation failed.");
    }
}

exports.updateUser = async (user)=> {
    try{
      await driver.get(urls.userManagementURL);
      await driver.wait(until.elementLocated(By.id("users_table")));

      await driver.wait(until.elementLocated(By.id(user.email+"_user_management_settings")));
      await driver.wait(until.elementIsVisible(driver.findElement(By.id(user.email+"_user_management_settings"))));
      driver.findElement(By.id(user.email+"_user_management_settings")).click();

      await driver.wait(until.elementLocated(By.id(user.email+"_update_btn")));
      await driver.wait(until.elementIsVisible(driver.findElement(By.id(user.email+"_update_btn"))));
      driver.findElement(By.id(user.email+"_update_btn")).click();
    }
    catch(e){
        assert.isOk(false,e);
    }

    try{
        await driver.wait(until.elementLocated(By.name('email')));
        await driver.wait(until.elementLocated(By.name('firstName')));
        driver.findElement(By.name('firstName')).sendKeys("_updated");

        await driver.wait(until.elementLocated(By.name('lastName')));
        driver.findElement(By.name('lastName')).sendKeys("_updated");

        await driver.wait(until.elementLocated(By.id('submit_btn')));
        driver.findElement(By.id('submit_btn')).click();

        try{
            await driver.wait(until.elementLocated(By.id('users_table')),60000);
            return true
        }catch(e){
            assert.isOk(false,"Timeout while checking for users table");
        }
    }catch(e){
        await saveScreenshot(driver,"updateUser");
        assert.isOk(false,"Cannot find user in the list : "+e.message);
    }
}

exports.activaeOrDeactivateUser = async (user,state)=> {
   //await driver.get(urls.userManagementURL);

   await driver.get(urls.userManagementURL);
   await driver.wait(until.elementLocated(By.id("users_table")));

   await driver.wait(until.elementLocated(By.id(user.email+"_user_management_settings")));
   await driver.wait(until.elementIsVisible(driver.findElement(By.id(user.email+"_user_management_settings"))));
   driver.findElement(By.id(user.email+"_user_management_settings")).click();


    await driver.wait(until.elementLocated(By.id(user.email+"_actdeact_btn")));
    await driver.wait(until.elementIsVisible(driver.findElement(By.id(user.email+"_actdeact_btn"))));
    driver.findElement(By.id(user.email+"_actdeact_btn")).click();

    await driver.wait(until.elementLocated(By.id("confirmModal_button")));
    driver.findElement(By.id("confirmModal_button")).click();

    try{
        if(state=="deactivate")
            await driver.wait(until.elementLocated(By.id(user.email+"_notActive_sp")),40000);
        else if(state == "activate")
            await driver.wait(until.elementLocated(By.id(user.email+"_active_sp")),40000);
    }catch(e){
        await saveScreenshot(driver,"activaeOrDeactivateUser");
        assert.isOk(false,"Timeout while checking if user deactivated/activated");
    }
}
