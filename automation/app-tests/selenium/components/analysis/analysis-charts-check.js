const {Builder, By, Key, until} = require('selenium-webdriver');
const driver = require('../init').main.driver;
const urls = require('../../config/config-urls');
const props = require('../../config/config-properties');
const mlog = require('mocha-logger');
const saveScreenshot = require('../../utils/saveScreenshot').saveScreenshot;
const assert = require('chai').assert;

exports.checkCharts = async (analysisName)=>{ 
    await driver.navigate().to(urls.analysisResultsURL);
    await driver.wait(until.elementLocated(By.id(analysisName+"_tile")));
    await driver.findElement(By.id(analysisName+"_tile")).click()
    try{
        await driver.wait(until.elementsLocated(By.css('a[analysis='+analysisName+']')),180000)
    }catch(e){
        saveScreenshot(driver,analysisName+"Error_Scnearios_Fetch_Failed-");
        assert.isOk(false,"Timeout whlile waiting for scenarios of "+analysisName+" : "+e.stack);
    }
    driver.findElements(By.css('a[analysis='+analysisName+']')).then(function(elems){
        elems[0].click();
    });

    await driver.sleep(180000);
    let elems = await driver.findElements(By.css('.menu_tab_link'));
    for(let elem of elems){
        elem.click();
        driver.sleep(10000);
        await saveScreenshot(driver,"charts"+"-"+analysisName+".png");
    }

}
