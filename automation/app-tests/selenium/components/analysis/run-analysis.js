const {Builder, By, Key, until} = require('selenium-webdriver');
const driver = require('../init').main.driver;
const urls = require('../../config/config-urls');
const props = require('../../config/config-properties');
const mlog = require('mocha-logger');
const dataUpload = require('../data-upload/data-upload').dataUpload;
const assert = require('chai').assert;
const saveScreenshot = require('../../utils/saveScreenshot').saveScreenshot;


exports.runAnalysisOfType = async (analysisType,files)=>{ 
    /** Takes analysis Type and array of files and run the analysis
     *  returns name of the analysis. this returned name can be used  to track the status.
     */
    try{
        //Go to JMeter insight create analysis page and upload data
        await driver.navigate().to(urls.createnAnalysisTypesURLS[analysisType]);
        await dataUpload(files);
        //append _sel with analysis name for identifcation. this name will returned by this method. 
        driver.findElement(By.id('analysisName_input')).sendKeys("_sel");
        let analysisName = driver.findElement(By.id('analysisName_input')).getAttribute('value');
        await driver.wait(until.elementIsVisible(driver.findElement(By.id('validate_div'))));
        await driver.wait(until.elementIsVisible(driver.findElement(By.id('createAnalysis_btn'))));
        driver.findElement(By.id('createAnalysis_btn')).click()

        return analysisName;
    }catch(e){
            saveScreenshot(driver,analysisName+"Error_runAnalysisOfType");    
            throw e;
    }
}
