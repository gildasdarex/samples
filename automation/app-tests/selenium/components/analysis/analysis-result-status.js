const {Builder, By, Key, until} = require('selenium-webdriver');
const driver = require('../init').main.driver;
const urls = require('../../config/config-urls');
const props = require('../../config/config-properties');
const mlog = require('mocha-logger');
const saveScreenshot = require('../../utils/saveScreenshot').saveScreenshot;

exports.analysisStatus = async (analysisName)=>{ 
    try{
        await driver.navigate().to(urls.analysisResultsURL);
        var statusIdInUI = analysisName+"_status"
        mlog.log(statusIdInUI)
        await driver.wait(until.elementLocated(By.id(statusIdInUI)));
        mlog.log("Status of Analysis : "+analysisName+" "+driver.findElement(By.id(statusIdInUI)).getText());
    }catch(e){
             saveScreenshot(driver,analysisName+"Error_Analysis_Status");
            throw e;
    }
}

exports.ifWaitForAnalysisStatusSuccess = async (analysisName)=>{ 
    /**
     * Returns if analysis status turns sucess else , takes screenshot, logs the status and returns false
     */
    try{
        await driver.navigate().to(urls.analysisResultsURL);
        var statusIdInUI = analysisName+"_status" 
        await driver.wait(until.elementLocated(By.id(statusIdInUI)));

        await driver.wait(()=>{
            return driver.findElement(By.id(statusIdInUI)).getAttribute("status").then(function (status) {
                if(status<0 || status==110 || status==120){
                    mlog.error("Analysis "+analysisName+" is Failed. Storing screeshot");
                    saveScreenshot(driver,"analysis_not_sucsess_"+analysisName)
                    return true;
                }
                if(status==100){
                    mlog.success("Analysis "+analysisName+" is successful.");
                    return true;
                }
                return false;
            });
        })
    }catch(e){
            await saveScreenshot(driver,"ifWaitForAnalysisStatusSuccess"+"-"+analysisName);
            throw e;
    }
}

