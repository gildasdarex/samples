const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const firefox = require('selenium-webdriver/firefox');


let driver = new webdriver.Builder()
.forBrowser('chrome')
.withCapabilities({'browserName': 'chrome', acceptSslCerts: true, acceptInsecureCerts: true})
.build();   

exports.main = {
    driver: driver
};