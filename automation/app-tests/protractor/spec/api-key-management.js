const urls = require('../config/config-urls');
const props = require('../config/config-properties');
const auth = require('../components/auth');
const apiKeyManagement = require('../components/api-keys-management');


describe('API Key Management', function () {
    beforeEach(function () {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 180*1000;
    });
    it('should login', function () {
        auth.login();
    });

    /*it('should create api', function () {
        apiKeyManagement.createAPIKey((isSuccess)=>{
            expect(isSuccess).toBe(true);
        });
    })*/

    it('should update api key', function () {
        apiKeyManagement.updateAPIKey((isSuccess)=>{
            expect(isSuccess).toBe(true);
        });
    })

    
});