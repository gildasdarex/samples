const urls = require('../config/config-urls');
const props = require('../config/config-properties');
const auth = require('../components/auth');
const analysis = require('../components/analysis');

let analysisNameCreated = ""
describe('Run Analysis', function () {
    beforeEach(function () {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000000;
    });
    it('should create jmeter intellisense analyses', function () {
        auth.login();
        for (let dataset of props.files["jmeter-insights"]) {
            analysis.createJmeterIntellisenseAnalysis(dataset, "jmeter-insights", (analysisName) => {
                analysisNameCreated = analaysisName
                analysis.checkAnalysisStatus(dataset.datasetName, analysisName);
            });
        }
    });
    it('should create mir analyses', function () {
        auth.login();
        for (let dataset of props.files["mir"]) {
            analysis.createJmeterIntellisenseAnalysis(dataset, "mir", (analysisName) => {
                analysis.checkAnalysisStatus(dataset.datasetName, analysisName);
            });
        }
    });

    it('should tag', () => {
        auth.login();
        tag.createTag(analysisNameCreated)
    });
});