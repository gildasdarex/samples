exports.config = {
  capabilities: {
    'browserName': 'chrome',
    marionette: true,
    acceptInsecureCerts: true
  },
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['./analysis.js']
};