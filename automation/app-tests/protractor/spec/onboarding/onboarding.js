const urls = require('../../config/config-urls');
const props = require('../../config/config-properties');
const auth = require('../../components/auth');
const onboarding = require('../../components/onboarding');

describe('Onboarding', function(){
    beforeEach(function() {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000000; 
    });
    it('should Onboard Community Edition', function() {
        onboarding.onBoard(4,1);  //JMeter Insights - Community
        onboarding.checkProvisioning(4,(bo)=>{});
    });
});