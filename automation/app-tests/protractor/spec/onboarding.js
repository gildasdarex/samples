const urls = require('../config/config-urls');
const props = require('../config/config-properties');
const auth = require('../components/auth');
const onboarding = require('../components/onboarding');


describe('onboarding', function () {
    beforeEach(function () {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 180*1000;
    });
    it('should onboard and subscribe to jemeter insights',()=> {
        onboarding.onBoard(4,1,(isSuccess)=>{
            expect(isSuccess).toBe(true);
        });
    });
})