const urls = require('../config/config-urls');
const props = require('../config/config-properties');
const auth = require('../components/auth');
const userManagement = require('../components/user-management');


describe('User Management', function () {
    beforeEach(function () {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 180*1000;
    });
    it('should login', function () {
        auth.login();
    });

    it('should create user', function () {
        userManagement.createUser(props.userManagement.users[0], (isSuccess) => {
            expect(isSuccess).toBe(true);
        });
    })

    it('should update user', function () {
        userManagement.updateUser(props.userManagement.users[0], (isSuccess) => {
            expect(isSuccess).toBe(true);
        });
    })

    it('deactivate user', function () {
        userManagement.activateOrDeactivateUser(props.userManagement.users[0], "deactivated", (isSuccess) => {
            expect(isSuccess).toBe(true);
        });
    })

    it('activate user', function () {
        userManagement.activateOrDeactivateUser(props.userManagement.users[0], "activated", (isSuccess) => {
            expect(isSuccess).toBe(true);
        });
    })
});