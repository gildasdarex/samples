var fs = require('fs');
var path = require('path');
const props = require('../config/config-properties')

if (!fs.existsSync(props.screenShotPath)) {
    fs.mkdirSync(props.screenShotPath);
}

exports.screenshotUtils = {
    createScreenshotAnalysisDirectory : function(datasetName,analysisName){
        if (!fs.existsSync(path.join(props.screenShotPath,analysisName))) {
            fs.mkdirSync(path.join(props.screenShotPath,analysisName));
        }
    },
    screenShotDirectory: props.screenShotPath,
    writeScreenShot: function (data, filename) {
        var stream = fs.createWriteStream(filename);
        stream.write(new Buffer(data, 'base64'));
        stream.end();
    },

    getSizes: function () {
        return browser.executeScript("return {scrollHeight: document.body.scrollHeight, clientHeight: window.screen.height, scrollTop: window.pageYOffset};");
    },

    scrollToBottom: function (analysisName,tabIdx,height, index) {
        var self = this;
        self.getSizes().then(function (data) {
            // continue only if we are not at the bottom of the page
            if(data.scrollHeight<data.clientHeight){ 
                browser.takeScreenshot().then(function (png) {
                    self.writeScreenShot(png, path.join(props.screenShotPath,analysisName,analysisName+"_"+tabIdx+"_"+ index + ".png"));                
                });
            }

            else if (data.scrollTop + data.clientHeight < data.scrollHeight) {
                browser.executeScript("window.scrollTo(0, arguments[0]);", height).then(function () {
                    browser.takeScreenshot().then(function (png) {
                        self.writeScreenShot(png, path.join(props.screenShotPath,analysisName,analysisName+"_"+tabIdx+"_"+ index + ".png"));
                        self.scrollToBottom(analysisName,tabIdx,height + data.clientHeight, index + 1);
                    });
                });
            }
        });
    }
};
