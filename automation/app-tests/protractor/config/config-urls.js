const baseURL = "http://localhost:4200"

exports.baseURL = baseURL
exports.loginURL = baseURL+"/login"
exports.userManagementURL = baseURL+"/dashboard/users-list"
exports.onBoardingURLJMeterInsights = "http://localhost:4200/signup/4/1?v={%22trackingUser%22:%22customer%22,%22supportLevel%22:%22GA%22,%22subscriptionType%22:%22free%22,%22productId%22:4,%22editionId%22:1,%22addOnLineWSList%22:[{%22id%22:1,%22supportLevel%22:%22GA%22,%22addOnType%22:%22FIXED_LIMIT%22,%22billingPeriod%22:%22MONTHLY%22,%22restrictionLineWSList%22:[{%22id%22:12,%22limit%22:7},{%22id%22:2,%22limit%22:7},{%22id%22:1,%22limit%22:2},{%22id%22:4,%22limit%22:14}]},{%22id%22:2,%22supportLevel%22:%22GA%22,%22addOnType%22:%22FIXED_LIMIT%22,%22billingPeriod%22:%22MONTHLY%22,%22restrictionLineWSList%22:[]}]}"
exports.subscriptionsURL = baseURL+"/dashboard/subscriptions"
exports.analysisResultsURL = baseURL+"/dashboard/analysis-results"
exports.apiKeysURL = baseURL+"/dashboard/api-keys"


exports.createnAnalysisTypesURLS = {
    "jmeter-insights" : baseURL+"/dashboard/create/jmeter_insights/4/1",
    "mir": baseURL+"/dashboard/create/mir/3/1",
    "beqos": baseURL+"/dashboard/create/beqos/3/1",
}