//Below is the singleton generator. Singleton is need to preserve random values same in a testing session
var fs = require('fs');

let properties = (function () {
    function properties() {
        waitTime = 40 * 000;
        screenShotPath = "../../screenShots-" + Date.now();

        //Create default user manually and login once before running the tets.
        defaultUsername = "testg10@testg10.com"
        defaultPassword = "Test123$"

        onBoarding = {
            email: "tes" + Date.now() + "@tes" + Date.now() + ".com",
            password: defaultPassword,
            title: "Mr",
            firstName: "Alan",
            lastName: "M",
            company: "Alpa" + Date.now(),
            department: "dev"
        }

        //localFileBasePath is the the path to the conatiner folders of all asklytics repo
        const locaFileBasePath = "/home/george/asklytics"
        files = {
            "jmeter-insights": [
                {
                    "datasetName": "JMeterLogJMXMetrics",
                    "files": [ //Array represents/contains the files the needed to be uploaded for this analysis.
                        locaFileBasePath + "/commons/data-collector-transformer/src/test/test-data/microanalysis/log-2019-04-25-community-edition-expires-2019-05-09.csv",
                        locaFileBasePath + "/commons/data-collector-transformer/src/test/test-data/microanalysis/perfmon-2019-04-25-community-edition-expires-2019-05-09.csv"
                    ]
                }
            ],
            "mir": [
                {
                    "datasetName": "mir",
                    "files": [ //Array represents/contains the files the needed to be uploaded for this analysis.
                        locaFileBasePath + "/commons/data-collector-transformer/src/test/test-data/microanalysis/log-2019-04-25-community-edition-expires-2019-05-09.csv",
                        locaFileBasePath + "/commons/data-collector-transformer/src/test/test-data/microanalysis/perfmon-2019-04-25-community-edition-expires-2019-05-09.csv"
                    ]
                }
            ],
            "beqos": [
                {
                    "datasetName": "beqos",
                    "files": [ //Array represents/contains the files the needed to be uploaded for this analysis.
                        locaFileBasePath + "/commons/data-collector-transformer/src/test/test-data/microanalysis/log-2019-04-25-community-edition-expires-2019-05-09.csv"
                    ]
                }
            ],
            "tomcat-beqos": [
                {
                    "datasetName": "tomcat-beqos",
                    "files": [ //Array represents/contains the files the needed to be uploaded for this analysis.
                        locaFileBasePath + "/commons/data-collector-transformer/src/test/test-data/beqos_jmeter_raw-2019-04-25-community-edition-expires-2019-05-09.csv"]
                }
            ]
        }
        userManagement = {
            users: [
                {
                    email: "t" + Date.now() + "@" + Date.now() + ".com",
                    firstName: "Bob",
                    lastName: "M",
                    productAccess: {
                        4: "Editor"
                    },
                    role: "Admin"
                }
            ]
        }
        return {
            defaultUsername: defaultUsername,
            defaultPassword: defaultPassword,
            onBoarding: onBoarding,
            locaFileBasePath: locaFileBasePath,
            userManagement: userManagement,
            files: files,
            screenShotPath: screenShotPath
        }
    }


    var instance;
    return {
        getInstance: function () {
            if (instance == null) {
                instance = new properties();
                // Hide the constructor so the returned objected can't be new'd...
                instance.constructor = null;
            }
            return instance;
        }
    };
})();

module.exports = properties.getInstance();
