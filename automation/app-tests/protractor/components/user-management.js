const urls = require('../config/config-urls');
const props = require('../config/config-properties')

exports.createUser = (user,cb) => {
    //Below code will loggin anf click the continue button in home page
    browser.waitForAngularEnabled(true);
    var EC = protractor.ExpectedConditions;
    browser.get(urls.userManagementURL).then(() => {

        var ele_createUserLink = element(by.id('createUser_link'));
        browser.wait(EC.visibilityOf(ele_createUserLink), props.waitTime);
        ele_createUserLink.click();

        var ele_email = element(by.name('email'));
        var ele_firstName = element(by.name('firstName'));
        var ele_lastName = element(by.name('lastName'));

        browser.wait(EC.visibilityOf(ele_email), props.waitTime);
        ele_email.sendKeys(user.email);
        ele_firstName.sendKeys(user.firstName);
        ele_lastName.sendKeys(user.lastName);

        var ele_accountRole = element(by.css("#accountRole option[txt=" + user.role + "]"));
        browser.wait(EC.visibilityOf(ele_accountRole), props.waitTime);
        ele_accountRole.click();

        var ele_submitBtn = element(by.id('submit_btn'));
        browser.wait(EC.visibilityOf(ele_submitBtn), props.waitTime);
        ele_submitBtn.click();

        var ele_usersTable = element(by.id('users_table'));
        browser.wait(EC.visibilityOf(ele_usersTable), props.waitTime);
        cb(true);
    })
}


exports.updateUser = (user,cb) => {

        browser.waitForAngularEnabled(true);
        var EC = protractor.ExpectedConditions;

        browser.get(urls.userManagementURL).then(()=>{
            var ele_usersTable = element(by.id('users_table'));
            browser.wait(EC.visibilityOf(ele_usersTable), props.waitTime);

            var ele_userSettings = element(by.id(user.email + "_user_management_settings"));
            browser.wait(EC.visibilityOf(ele_userSettings), props.waitTime);
            ele_userSettings.click();

            var ele_userUpdate = element(by.id(user.email + "_update_btn"));
            browser.wait(EC.visibilityOf(ele_userUpdate), props.waitTime);
            ele_userUpdate.click();

            browser.driver.sleep(3000);

            var ele_firstName = element(by.name("firstName"));
            browser.wait(EC.visibilityOf(ele_firstName), props.waitTime);

            ele_firstName.sendKeys("_updated");

            var ele_submit = element(by.id("submit_btn"));
            browser.wait(EC.visibilityOf(ele_submit), props.waitTime);

            ele_submit.click();

            var ele_usersTable = element(by.id('users_table'));
            browser.wait(EC.visibilityOf(ele_usersTable), props.waitTime);
            cb(true);
        })
}

exports.activateOrDeactivateUser =  (user,state,cb) => {
    //await driver.get(urls.userManagementURL);

    browser.waitForAngularEnabled(true);
        var EC = protractor.ExpectedConditions;

        browser.get(urls.userManagementURL).then(()=>{
            var ele_usersTable = element(by.id('users_table'));
            browser.wait(EC.visibilityOf(ele_usersTable), props.waitTime);

            var ele_userSettings = element(by.id(user.email + "_user_management_settings"));
            browser.wait(EC.visibilityOf(ele_userSettings), props.waitTime);
            ele_userSettings.click();

            var ele_actdeact = element(by.id(user.email + "_actdeact_btn"));
            browser.wait(EC.visibilityOf(ele_actdeact), props.waitTime);
            ele_actdeact.click();

            var ele_confirm = element(by.css(".swal2-confirm"));
            browser.wait(EC.visibilityOf(ele_confirm), props.waitTime);
            ele_confirm.click();

            if(state.toLowerCase() == "deactivated"){
                var ele_deactivated = element(by.id(user.email + "_notActive_sp"));
                browser.wait(EC.visibilityOf(ele_deactivated), props.waitTime);
                cb(true);
            }
            else if(state.toLowerCase() == "activated"){
                var ele_deactivated = element(by.id(user.email + "_active_sp"));
                browser.wait(EC.visibilityOf(ele_deactivated), props.waitTime);
                cb(true);
            }
        })
}