const urls = require('../config/config-urls');
const props = require('../config/config-properties')

exports.createAPIKey = (cb) => {
    //Below code will loggin anf click the continue button in home page
    browser.waitForAngularEnabled(true);
    var EC = protractor.ExpectedConditions;
    browser.get(urls.apiKeysURL).then(() => {

        var ele_createKey = element(by.name('create-api-key'));
        browser.wait(EC.visibilityOf(ele_createKey), props.waitTime);
        ele_createKey.click();

        var ele_userId = element(by.name('userId'));
        browser.wait(EC.visibilityOf(ele_userId), props.waitTime);
        ele_userId.click();

        var ele_userIdOption = element.all(by.css('.userIdOption_prot')).get(0);
        browser.wait(EC.visibilityOf(ele_userIdOption), props.waitTime);
        ele_userIdOption.click();

        var ele_desc = element(by.name('name'));
        browser.wait(EC.visibilityOf(ele_desc), props.waitTime);
        ele_desc.sendKeys("description");
        
        var ele_expiryOption = element.all(by.css('.expiry_prot')).get(0);
        browser.wait(EC.visibilityOf(ele_expiryOption), props.waitTime);
        ele_expiryOption.click();

        var ele_submit = element(by.id('submit'));
        browser.wait(EC.visibilityOf(ele_submit), props.waitTime);
        ele_submit.click();


        var ele_keys_table = element(by.id('api-keys-table'));
        browser.wait(EC.visibilityOf(ele_keys_table), props.waitTime);
        cb(true);
    })
}



exports.updateAPIKey = (cb) => {
        //Below code will loggin anf click the continue button in home page
        browser.waitForAngularEnabled(true);
        var EC = protractor.ExpectedConditions;
        browser.get(urls.apiKeysURL).then(() => {

        var ele_keys_table = element(by.id('api-keys-table'));
        browser.wait(EC.visibilityOf(ele_keys_table), props.waitTime);

        browser.driver.sleep(5000);
        var ele_userIdOption = element.all(by.css('.apiKeyUpdate_prot')).get(0);
        browser.wait(EC.visibilityOf(ele_userIdOption), props.waitTime);
        ele_userIdOption.click();

        var ele_desc = element(by.name('name'));
        browser.wait(EC.visibilityOf(ele_desc), props.waitTime);
        ele_desc.sendKeys("description_updated");
            
        browser.driver.sleep(2000);

        var ele_submit = element(by.id('update_submit_prot'));
        browser.wait(EC.visibilityOf(ele_submit), props.waitTime);
        ele_submit.click();

        var ele_keys_table = element(by.id('api-keys-table'));
        browser.wait(EC.visibilityOf(ele_keys_table), props.waitTime);
        cb(true);
    })
}
