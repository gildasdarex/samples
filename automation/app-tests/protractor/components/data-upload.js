const urls = require('../config/config-urls');
const props = require('../config/config-properties');

let dataUpload = async (dataset) =>{
    var EC = protractor.ExpectedConditions;
    var until = protractor.ExpectedConditions;

   // var ele_dataCollapse =  element(by.id('datasetCollapse_0'));
    //browser.wait(EC.visibilityOf(ele_dataCollapse), props.waitTime);
    //ele_dataCollapse.click();

    var ele_file =  element(by.name('file_-1'));
    browser.wait(until.presenceOf(ele_file), props.waitTime).then(()=>{
        dataset.files.map((file)=>{
            console.log("(INFO) Uploading "+file)
            ele_file.sendKeys(file);
            browser.sleep(8000);
        })
    })
}
exports.dataUpload = dataUpload