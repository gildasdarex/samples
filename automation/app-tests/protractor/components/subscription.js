const urls = require('../config/config-urls');
const props = require('../config/config-properties');

exports.subscribe = (productId,editionId,cb)=> {
    browser.waitForAngularEnabled(true);
    var EC = protractor.ExpectedConditions;
    browser.ignoreSynchronization = true
    browser.get(urls.subscriptionsURL).then(()=>{

        var ele_upgrade_product_btn = element(by.id(productId+'_subscribe_product_btn'));
        browser.wait(EC.visibilityOf(ele_upgrade_product_btn), props.waitTime);
        ele_upgrade_product_btn.click();

        browser.driver.sleep(3000);

        var ele_upgrade_product_edition_btn = element(by.id(productId+'_'+editionId+'_subscribe_btn'));
        browser.wait(EC.visibilityOf(ele_upgrade_product_edition_btn), props.waitTime);
        ele_upgrade_product_edition_btn.click();
        
        var ele_product_subscription_confirm_btn = element(by.id(productId+'_product_subscription_confirm_btn'));
        browser.wait(EC.visibilityOf(ele_product_subscription_confirm_btn), props.waitTime);
        ele_product_subscription_confirm_btn.click();

        var ele_success = element(by.css(".swal2-success"));
        browser.wait(EC.visibilityOf(ele_success), props.waitTime);
       cb(true)

    })
}

exports.upgrade = (productId,editionId,cb)=> {
    browser.waitForAngularEnabled(true);
    var EC = protractor.ExpectedConditions;
    browser.ignoreSynchronization = true
    browser.get(urls.subscriptionsURL).then(()=>{

        var ele_upgrade_product_btn = element(by.id(productId+'_upgrade_product_btn'));
        browser.wait(EC.visibilityOf(ele_upgrade_product_btn), props.waitTime);
        ele_upgrade_product_btn.click();

        browser.driver.sleep(3000);

        var ele_upgrade_product_edition_btn = element(by.id(productId+'_'+editionId+'_upgrade_subscription_btn'));
        browser.wait(EC.visibilityOf(ele_upgrade_product_edition_btn), props.waitTime);
        ele_upgrade_product_edition_btn.click();
        
        var ele_product_subscription_confirm_btn = element(by.id(productId+'_product_subscription_confirm_btn'));
        browser.wait(EC.visibilityOf(ele_product_subscription_confirm_btn), props.waitTime);
        ele_product_subscription_confirm_btn.click();

        var ele_success = element(by.css(".swal2-success"));
        browser.wait(EC.visibilityOf(ele_success), props.waitTime);
       cb(true)

    })
}

exports.changeRestriction = (productId,editionId,cb)=> {
    browser.manage().window().setSize(1600, 1000);
    browser.waitForAngularEnabled(true);
    var EC = protractor.ExpectedConditions;
    browser.ignoreSynchronization = true
    browser.get(urls.subscriptionsURL).then(()=>{

        var ele_upgrade_product_btn = element(by.id(productId+'_upgrade_product_btn'));
        browser.wait(EC.visibilityOf(ele_upgrade_product_btn), props.waitTime);
        ele_upgrade_product_btn.click();

        browser.driver.sleep(3000);

        var ele_change_restr_btn = element(by.id(productId+'_'+editionId+'_change_subscription_btn'));
        browser.wait(EC.visibilityOf(ele_change_restr_btn), props.waitTime);
        ele_change_restr_btn.click();


        browser.driver.sleep(2000);

        var ele_updateSubscription_btn = element(by.id(productId+'_'+editionId+'_updateSubscription_btn'));
        browser.wait(EC.visibilityOf(ele_updateSubscription_btn), props.waitTime);
        ele_updateSubscription_btn.click();


        browser.driver.sleep(5000);

        var ele_inc_restr_btn = element(by.id(productId+'_'+editionId+'_change_restriction++0'));
        browser.wait(EC.visibilityOf(ele_inc_restr_btn), props.waitTime);
        ele_inc_restr_btn.click();


        var ele_change_product_edition_btn = element(by.id(productId+'_'+editionId+'_apply_change_subscription_btn'));
        browser.wait(EC.visibilityOf(ele_change_product_edition_btn), props.waitTime);
        ele_change_product_edition_btn.click();

        browser.driver.sleep(3000);

        var ele_subscription_confirm_btn = element(by.id(productId+'_product_subscription_confirm_btn'));
        browser.wait(EC.visibilityOf(ele_subscription_confirm_btn), props.waitTime);
        ele_subscription_confirm_btn.click();


        var ele_success = element(by.css(".swal2-success"));
        browser.wait(EC.visibilityOf(ele_success), 25*1000);
        cb(true)

    })
}


