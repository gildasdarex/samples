const urls = require('../config/config-urls');
const props = require('../config/config-properties');
const dataupload = require('./data-upload');
const screenshotUtils = require('../utils/saveScreenshot').screenshotUtils;
var fs = require('fs');
var path = require('path');
const checkMicroAnalysisResults = require('./micro-analysis').checkMicroAnalysisResults;



exports.createJmeterIntellisenseAnalysis = (dataset, analysisName, cb) => {
    //Below code will loggin anf click the continue button in home page

    browser.waitForAngular();
    var EC = protractor.ExpectedConditions;

    browser.get(urls.createnAnalysisTypesURLS[analysisName]);
    dataupload.dataUpload(dataset);

    var ele_validate_div = element(by.id('validate_div'));
    browser.wait(EC.visibilityOf(ele_validate_div), props.waitTime);

    var ele_analysisName = element(by.id('analysisName_input'));

    ele_analysisName.clear().sendKeys(dataset.datasetName + "_prot");

    var ele_create_btn = element(by.id('createAnalysis_btn'));
    browser.wait(EC.visibilityOf(ele_create_btn), props.waitTime);
    ele_create_btn.click();
    ele_analysisName.getAttribute('value').then((value) => {
        console.log("Value", value)
        screenshotUtils.createScreenshotAnalysisDirectory(dataset.datasetName, value);
        for (let file of dataset.files)
            fs.createReadStream(file).pipe(fs.createWriteStream(path.join(props.screenShotPath, value, path.basename(file))));
        cb(value);
    })

    browser.sleep(6000);

}


exports.checkAnalysisStatus = (analysisName) => {
    browser.waitForAngular();
    browser.get("http://localhost:4200/dashboard/analysis-results");
    var EC = protractor.ExpectedConditions;
    var ele_analysisSuccess = element(by.className("prot_analysis_success_" + analysisName));
    var ele_analysisFailed = element(by.className("prot_analysis_failed_" + analysisName));

    var presenceOfSuccess = EC.presenceOf(ele_analysisSuccess)
    var presenceOfFailure = EC.presenceOf(ele_analysisFailed)

    browser.wait(EC.or(presenceOfSuccess, presenceOfFailure), props.waitTime).then((result) => {
        browser.isElementPresent(ele_analysisFailed).then((successRes) => {
            if (successRes) {
                browser.sleep(2000);
                console.log("Analysis has failed");
                takeScreenShot(analysisName);
            }
        })
        browser.isElementPresent(ele_analysisSuccess).then((successRes) => {
            if (successRes) {
                browser.sleep(2000);
                console.log("Analysis is successful");
                var ele_analysisTile = element(by.id(analysisName + "_tile"));
                browser.wait(EC.elementToBeClickable(ele_analysisTile), props.waitTime).then((result) => {

                    screenshotUtils.createScreenshotAnalysisDirectory(datasetName, analysisName);
                    browser.sleep(2000);
                    ele_analysisTile.click();

                    var ele_scenarioTileLink = element(by.id(analysisName + 'scenario_tile'));
                    browser.wait(EC.presenceOf(ele_scenarioTileLink), props.waitTime).then((result) => {
                        ele_scenarioTileLink.click();

                        var ele_summary_tab_content = element(by.id('summary'));
                        browser.wait(EC.presenceOf(ele_summary_tab_content), props.waitTime).then((ele_summary_tab_content_result) => {
                            browser.driver.sleep(5000);
                            element.all(by.css('.nav-link')).then((elems) => {
                                takeScreenShotTab(analysisName, elems, 0);
                                checkMicroAnalysisResults(elems, analysisName);
                            });
                        });


                    });
                });
            }
        })
        browser.isElementPresent(ele_analysisFailed).then((failedRes) => {
            if (datasetName == "mir" || datasetName == "beqos" || datasetName == "tomcat-beqos") {
                browser.wait(EC.elementToBeClickable(ele_analysisTile), props.waitTime).then((result) => {

                    screenshotUtils.createScreenshotAnalysisDirectory(datasetName, analysisName);
                    browser.sleep(2000);
                    ele_analysisTile.click();

                    var ele_scenarioTileLink = element(by.id(analysisName + 'scenario_tile'));
                    browser.wait(EC.presenceOf(ele_scenarioTileLink), props.waitTime).then((result) => {
                        ele_scenarioTileLink.click();

                        browser.sleep(5000);
                        takeScreenShot(analysisName)

                    });
                });
            }
            console.log("Analysis has failed", failedRes);
        })
    })


    takeScreenShotTab = (analysisName, tabs, tabIdx) => {
        let elem = tabs[tabIdx];
        browser.actions().mouseMove(elem).click().perform();

        browser.sleep(30000);

        screenshotUtils.getSizes().then(function (data) {
            screenshotUtils.scrollToBottom(analysisName, tabIdx, 0, 1);
            if (tabIdx < tabs.length - 1) {
                takeScreenShotTab(analysisName, tabs, tabIdx + 1);
            }
        });
    }

    takeScreenShot = (analysisName) => {
        browser.sleep(30000);
        screenshotUtils.getSizes().then(function (data) {
            screenshotUtils.scrollToBottom(analysisName, Date.now(), 0, 1);
        });
    }
}
