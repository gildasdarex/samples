const urls = require('../config/config-urls');
const props = require('../config/config-properties');
const dataupload = require('./data-upload');
const screenshotUtils = require('../utils/saveScreenshot').screenshotUtils;
var fs = require('fs');
var path = require('path');
const checkMicroAnalysisResults = require('./micro-analysis').checkMicroAnalysisResults;


exports.createTag = (analysisName) => {
    browser.waitForAngular();
    var EC = protractor.ExpectedConditions;
    browser.get("http://localhost:4200/dashboard/analysis-results");
    var ele_CreateTagBtn = element(by.id("edit_tag_" + analysisName));
    browser.wait(EC.presenceOf(ele_CreateTagBtn)).then((result) => {

        ele_CreateTagBtn.click()
        browser.sleep(10000)

        var ele_nTagContainer = element(by.className("ng2-tag-input__text-input"));
        browser.wait(EC.presenceOf(ele_nTagContainer)).then((result) => {
            ele_nTagContainer.click()
            browser.sleep(5000)
            var tag = '' + Date.now()
            ele_nTagContainer.sendKeys(tag);

            ele_nTagContainer.sendKeys(protractor.Key.ENTER)
            browser.sleep(5000)

            var ele_close = element(by.id("tag-modal-close"));
            browser.wait(EC.presenceOf(ele_close)).then((r) => {
                ele_close.click()
                browser.sleep(10000)
                var ele_tag = element(by.id("tag_" + tag));
                browser.wait(EC.presenceOf(ele_tag)).then((r) => {
                    console.log("Tag creation is successful")
                })
            })

        })
    })
}
