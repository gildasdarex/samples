const urls = require('../config/config-urls');
const props = require('../config/config-properties')

exports.login = () =>{
    browser.waitForAngularEnabled(false);
    browser.get(urls.loginURL);
    var EC = protractor.ExpectedConditions;
    var ele_email =  element(by.name('email'));
    var ele_password = element(by.name('password'));
    var ele_submit = element(by.css('.auth0-lock-submit'));

    browser.wait(EC.visibilityOf(ele_email), props.waitTime);
    browser.wait(EC.visibilityOf(ele_password), props.waitTime);

    ele_email.sendKeys(props.defaultUsername);
    ele_password.sendKeys(props.defaultPassword);

    ele_submit.click();

    browser.driver.sleep(20000);
}