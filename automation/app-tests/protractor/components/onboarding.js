const urls = require('../config/config-urls');
const props = require('../config/config-properties');

exports.onBoard = (productId,editionId,cb)=> {

    browser.waitForAngularEnabled(true);
     var EC = protractor.ExpectedConditions;
     browser.ignoreSynchronization = true

    browser.get(urls.onBoardingURLJMeterInsights).then(()=>{
    var email_input = element(by.name('email'));
    browser.wait(EC.visibilityOf(email_input), props.waitTime);
    email_input.sendKeys(''+productId+editionId+props.onBoarding.email);
    var action_input =  element(by.name('_action_signUpEmail'));
    action_input.click();

    var submit_btn = element(by.name('create_onboarding_customer'));
    browser.wait(EC.visibilityOf(submit_btn), props.waitTime);

    var title_input = element(by.name('title'));
    title_input.sendKeys(props.onBoarding.title);

    element(by.name('firstName')).sendKeys(props.onBoarding.firstName);
    element(by.name('lastName')).sendKeys(props.onBoarding.lastName);
    element(by.name('company')).sendKeys(''+productId+editionId+props.onBoarding.company);
    element(by.name('department')).sendKeys(props.onBoarding.department);  
    element(by.name('newPassword')).sendKeys(props.onBoarding.password);
    element(by.name('confirmPassword')).sendKeys(props.onBoarding.password);
    element(by.css("#country option:nth-child(10)")).click();
    element(by.css("#timezone option:nth-child(10)")).click();
    element(by.name('create_onboarding_customer')).click();

    

    var create_customer_button  = element(by.name('finish_create_onboarding_customer'));
    browser.wait(EC.visibilityOf(create_customer_button), 30*1000);

    create_customer_button.click();


    var subscriptionExists = element(by.id("subscribed_"+productId));
    browser.wait(EC.visibilityOf(subscriptionExists), 50*1000).then(()=>{
        cb(true);
    })

    })
}
