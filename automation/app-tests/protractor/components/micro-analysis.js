const urls = require('../config/config-urls');
const props = require('../config/config-properties');
const dataupload = require('./data-upload');
const screenshotUtils = require('../utils/saveScreenshot').screenshotUtils;
var fs = require('fs');
var path = require('path');

exports.checkMicroAnalysisResults = (tabs,analysisName) => {
    // Precusor  : The page should be on the scenraio result details page.
    clickInsightTab(tabs,0,analysisName);
}

clickInsightTab = (tabs,i,analysisName) =>{
    tabs[i].getAttribute('is_insights_tab').then((isInsight)=>{
        if(!isInsight)
            clickInsightTab(tabs,i+1,analysisName);
        else{
            tabs[i].click();
            browser.sleep(30000);
            element.all(by.className("ma_result_link")).then((linksEle)=>{
                var links = [];
                getLinks(links,linksEle,0,analysisName);
            })
        }
    })
}

getLinks= (links,linksEle,idx,analysisName)=>{
    linksEle[idx].getAttribute("ma_result_link").then((lnk)=>{
        links.push(lnk)
        if (idx < linksEle.length - 1) {
            getLinks(links,linksEle,idx+1,analysisName);
        }
        if(idx == linksEle.length - 1){
            openLink(links,0,analysisName);
        }
    })
}

openLink= (links,idx,analysisName)=>{
    browser.get(urls.baseURL+links[idx]);
    browser.sleep(30000);
    takeScreenShot(analysisName);
    if (idx < links.length - 1) {
        openLink(links,idx+1,analysisName);
    }
}

takeScreenShot= (analysisName) => {
    browser.sleep(30000);
    screenshotUtils.getSizes().then(function (data) {
        screenshotUtils.scrollToBottom(analysisName,Date.now(),0, 1);
    });
}