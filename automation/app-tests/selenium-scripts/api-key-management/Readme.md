## Selenium Scripts for api key Management

### Dependency scripts
1. Onboarding - 
	Update "rand" in propertie.js file with test suffix# of the user to gerate admin and non admin user name to be used in the script.
	If the user is already created, there is no need of running on boarding flow , otherwise it is.
2. Login Flow -
	Login with the user with login script. admin / non admin.


### Description


#### Test Suite #1 - For Admin, Complete flow of creation,updation and deletion of the API key of his/her own
#### File : admin-api-keys-actions-self
Create API Key  - Admin
Update Description of the API Key - Admin
Change the expiry of the api key - Admin
Delete API Key - Admin

#### Test Suite #2 - For Admin, Complete flow of creation,updation and deletion of the API key of other users
#### File : admin-api-keys-actions-other-users
Create API Key  - Admin
Update Description of the API Key - Admin
Change the expiry of the api key - Admin
Delete API Key - Admin

#### Test Suite #3 - For User, Complete flow of creation,updation and deletion of the API key. 
#### File : non-admin-user-api-keys
Create API Key - User
Update API Key - User
Change the expiry of the api key - User
Delete API Key - User
 
