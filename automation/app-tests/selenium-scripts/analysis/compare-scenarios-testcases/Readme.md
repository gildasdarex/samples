## Selenium Scripts for api key Management

### Dependency scripts
1. Analysis creation with two scenarios.
2. Login Flow -
	Login with the user with login script. admin / non admin.

### Description

#### 1. Run the analysis creationt test with to scenarios.
#### File : * Jmeter Insights - create-jemter-insights-analysis-with-two-scenarios
	analysis.twoScenarios.name from properties file will be used as the analysis name.
	This name will be used futher by compare-analysis script to choose the analysis.
#### 2. Run compare scenarios test case.
#### File : compare-scenario

*NOTE : The testcases have to be run separately because analysis creation takes time. #2 from above should be run only after analysis is successful from the step #1* 

