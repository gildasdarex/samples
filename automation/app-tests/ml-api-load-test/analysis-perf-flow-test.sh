#!/usr/bin/env bash

if [ ! -f testng_jar_files.zip ]; then
    wget -O testng_jar_files.zip https://jar-download.com/cache_jars/org.testng/testng/6.9.10/jar_files.zip
    unzip testng_jar_files.zip
fi

testng_suite_file_name="analysis-perf-flow-test-suite.xml"
optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
       ml.api.project.path)
          ml_api_project_path="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
       testng.suite.file.name)
          testng_suite_file_name="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done


java -cp "testng-6.9.10.jar:jcommander-1.48.jar:bsh-2.0b4.jar:$ml_api_project_path/build/classes/test" org.testng.TestNG $ml_api_project_path/src/test/resources/$testng_suite_file_name