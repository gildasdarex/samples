# HTTP multipart form submission
#
# This script uses the HTTPClient.Codecs class to post itself to the
# server as a multi-part form. Thanks to Marc Gemis.

from net.grinder.script.Grinder import grinder
from net.grinder.script import Test
from net.grinder.plugin.http import HTTPRequest, HTTPPluginControl
from HTTPClient import Cookie, CookieModule, CookiePolicyHandler, NVPair
from jarray import zeros

log = grinder.logger.info

HOST_DOMAIN = '138.197.126.46:8080/ml-api'
HOST_URL = 'http://%s' % HOST_DOMAIN

def create_request(test, headers=None):
    request = HTTPRequest()
    if headers:
        request.headers = headers
    test.record(request)
    return request


def get_csrf_token(thread_context):
    cookies = CookieModule.listAllCookies(thread_context)
    csrftoken = ''
    for cookie in cookies:
        if cookie.getName() == 'al-access-token':
            csrftoken = cookie.getValue()
    log("get token: %s" % csrftoken)
    return csrftoken


class TestRunner:
    def __call__(self):

        thread_context = HTTPPluginControl.getThreadHTTPClientContext()

        create_request(Test(1100, 'Connect to host')).GET(HOST_URL + '/')

        login_payload = JSONObject({})

        # Log a user in using username, password and CSRF token read from cookie
        create_request(Test(1300, 'Log in user')).POST(HOST_URL + '/api/login', (
            NVPair('username', 'test1010001@massive.co.zz'),
            NVPair('password', 'test1010001')
        ))

        print get_csrf_token(thread_context)
