##!/usr/bin/env bash
#
#
# this script is used to send a message to asklytics slack workspace
# The script takes 2 parameters:
# 1- the channel where the message need to be send
# 2- the content of the message
#
#!/bin/bash

channel=$1
if [[ $channel == "" ]]
then
        echo "No channel specified"
        exit 1
fi

shift

text=$*

if [[ $text == "" ]]
then
        echo "No text specified"
        exit 1
fi

escapedText=$(echo $text | sed 's/"/\"/g' | sed "s/'/\'/g" )
json="{\"channel\": \"#$channel\", \"text\": \"$escapedText\"}"


curl -s -d "payload=$json" "https://hooks.slack.com/services/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"
