#!/usr/bin/env bash

# usage /replace-properties.sh sample-variables.txt sample-template.properties sample.properties
# -i do not works on mac alone so if you want to use -i you need to provide an extension for your backups.
#The command (note the lack of space between -i and '' and the -e to make it work on new versions of Mac and on GNU):

echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for app-metadata folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to app-metadata folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

declare array
declare -a keys=()
declare -a vals=()

while IFS='=' read -r key val; do
        [[ $key = '#'* ]] && continue
        array["$key"]="$val"            #bash4
        keys+=("$key"); vals+=("$val")  #bash3
done < "$1"

echo "Number of entries: ${#keys[@]} or ${#array[@]}"

cp "$2" "$3"
# two separate array [bash3]
for ((ia = 0; ia < ${#keys[@]}; ia++)); do
        printf '[%s]=[%s]\n' "${keys[ia]}" "${vals[ia]}"
        sed -i'.original' -e 's/'"${keys[ia]}"'/'"${vals[ia]}"'/g' "$3"
done

rm "$3.original"