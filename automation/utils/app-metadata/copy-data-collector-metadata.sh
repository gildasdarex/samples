#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for dev-utils folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to dev-utils folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

if [ "$1" == "" ]; then
    echo "please enter source path."
    exit 0
fi

if [ "$2" == "" ]; then
    echo "please enter destination path."
    exit 0
fi

cp $1 "/*" $2 "/*"

exit 0