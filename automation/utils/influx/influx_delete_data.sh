#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for influx folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to influx folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

read -p 'Username: ' uservar
read -sp 'Password: ' passvar
echo
read -p 'Host: ' host
read -p 'Port: ' port
read -p 'databaseName: ' databaseName
read -p 'analysisId: ' analysisId

function influxfb_local_url {
  echo -e "http://${host}:${port}"
}

function influx_drop_series {
    # echo "$(curl "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}" --data-binary "{\'db\':\'${databaseName}\', \'q\': \'select * from cpu\'}")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM disks where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM cpu where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM jmx where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM memory where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM network where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM tcp where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM metrics_units where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM swap where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM jmeter_log where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM jmeter_perfmon where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM jmeter_log_derived_tps where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM jmeter_log_derived_tps_label where analysis_id='${analysisId}'")"
    echo "$(curl -i -XPOST "$(influxfb_local_url)/query?u=${uservar}&p=${passvar}&pretty=true" --data-urlencode "db=${databaseName}" --data-urlencode "q=drop series FROM jmeter_log_derived_tps_success_or_error where analysis_id='${analysisId}'")"
}
function setup_influxdb {
  influx_drop_series
}

function success {
  echo "$(tput setaf 2)""$*""$(tput sgr0)"
}

function info {
  echo "$(tput setaf 3)""$*""$(tput sgr0)"
}

function error {
  echo "$(tput setaf 1)""$*""$(tput sgr0)" 1>&2
}

function usage {
  echo "Usage: ${0} database_name"
  exit 1
}

setup_influxdb