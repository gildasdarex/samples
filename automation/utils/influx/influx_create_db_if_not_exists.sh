#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for influx folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to influx folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

read -p 'Username: ' uservar
read -sp 'Password: ' passvar
echo
read -p 'Host: ' host
read -p 'Port: ' port
read -p 'databaseName: ' databaseName
read -p 'use default retention (y or n): ' retention

function influx_db_local_url {
  echo -e "http://${host}:${port}"
}

function create_db {
    echo "$(curl -i -XPOST "$(influx_db_local_url)/query?u=${uservar}&p=${passvar}&pretty=true"  --data-urlencode "q=create database "${databaseName}"")"
}

function create_retention {
    echo "$(curl -i -XPOST "$(influx_db_local_url)/query?u=${uservar}&p=${passvar}&pretty=true"  --data-urlencode "q=create retention policy ${name} on ${databaseName} duration ${duration} replication ${replication} shard duration ${shardDuration} default")"
}

if [[ "${retention}" == "y" ]]; then
    create_db
else
    read -p 'retentionPolicyName: ' name
    read -p 'retentionDuration: (check syntax https://docs.influxdata.com/influxdb/v1.6/query_language/spec/#durations) ' duration
    read -p 'replication: ' replication
    read -p 'shardDuration: (check syntax https://docs.influxdata.com/influxdb/v1.6/query_language/spec/#durations) ' shardDuration
    create_db
    create_retention
fi
