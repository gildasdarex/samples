#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for influx folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to influx folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

echo "did you already install jq tool ? "
read -p "yes or no " jq
if [[ "${jq}" == "no" ]]
 then
  echo "please choose OS "
  read -p "mac or linux : " oS
  if [[ "${oS}" == "mac" ]]
  then
    $(eval "brew install jq")
  else
    $(eval "sudo apt-get install jq")
  fi
else

    read -p 'Username: ' uservar
    read -sp 'Password: ' passvar
    echo
    read -p 'Host: ' host
    read -p 'Port: ' port

    function influx_db_local_url {
      echo -e "http://${host}:${port}"
    }

    function update_retention_on_db {

        databases=$(curl -s -XPOST "$(influx_db_local_url)/query?u=${uservar}&p=${passvar}&pretty=true"  --data-urlencode "q=show databases" | jq -r '.results[].series[].values[][]')
        echo $databases
        databasesArray=($databases)
        for databaseName in ${databasesArray[@]}
        do
            echo "$(curl -i -XPOST "$(influx_db_local_url)/query?u=${uservar}&p=${passvar}&pretty=true"  --data-urlencode "q=drop Database ${databaseName}")"
            echo "$(curl -i -XPOST "$(influx_db_local_url)/query?u=${uservar}&p=${passvar}&pretty=true"  --data-urlencode "q=create Database ${databaseName}")"
            IFS='_'
            arrIN=($databaseName); unset IFS;
            echo "$(curl -i -XPOST "$(influx_db_local_url)/query?u=${uservar}&p=${passvar}&pretty=true"  --data-urlencode "q=CREATE USER tenant_user WITH PASSWORD 'Welcome1!${arrIN#?}'")"
            echo "$(curl -i -XPOST "$(influx_db_local_url)/query?u=${uservar}&p=${passvar}&pretty=true"  --data-urlencode "q=GRANT ALL ON ${databaseName} TO tenant_user ")"
            echo "creating retention policy of "$databaseName
            echo "$(curl -i -XPOST "$(influx_db_local_url)/query?u=${uservar}&p=${passvar}&pretty=true"  --data-urlencode "q=Alter retention policy autogen on ${databaseName} duration 1020w replication 1 default")"
        done

    }

    update_retention_on_db
fi