#!/usr/bin/env bash

if [ ! -f "/var/lib/collectd/collectd-server-start-date" ]
then
    touch /var/lib/collectd/collectd-server-start-date
fi
CURRENTEPOCTIMESTART=(date +"%s")
echo ${CURRENTEPOCTIMESTART} > /var/lib/collectd/collectd-server
service collectd start