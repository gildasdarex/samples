#!/usr/bin/env bash

cd /var/lib/collectd/

if [ ! -d "/var/lib/collectd/history" ]
then
    mkdir history
fi

CURRENTEPOCTIMESTART=$(cat /var/lib/collectd/collectd-server-start-date)
CURRENTEPOCTIMEEND=$(date +"%s")
service collectd stop

cp -r /var/lib/collectd/csv /var/lib/collectd/history/${CURRENTEPOCTIMESTART}-${CURRENTEPOCTIMEEND}/.
rm -r /var/lib/collectd/csv