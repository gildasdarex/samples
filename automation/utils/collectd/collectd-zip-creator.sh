#!/usr/bin/env bash

ip=$1
CURRENTEPOCTIMEEND=$(date +"%s")
CURRENTEPOCTIMESTART=$(cat /var/lib/collectd/collectd-server-start-date)
cp -r "/var/lib/collectd/csv" "/var/lib/collectd/${ip}-collectd-${CURRENTEPOCTIMESTART}-${CURRENTEPOCTIMEEND}-csv"

dir="/var/lib/collectd/${ip}-collectd-${CURRENTEPOCTIMESTART}-${CURRENTEPOCTIMEEND}-csv/"


rename_file () {
metric="${dir}/$1"
for f in $(ls $metric)
do
     mv "${metric}/$f" "${metric}/$ip-${f}.csv"
done
}


for file in $(ls $dir)
do
     rename_file $file
done


zip -r "/var/lib/collectd/${ip}-collectd-${CURRENTEPOCTIMESTART}-${CURRENTEPOCTIMEEND}-csv.zip" $dir
echo "zip file generated in /var/lib/collectd/${ip}-collectd-${CURRENTEPOCTIMESTART}-${CURRENTEPOCTIMEEND}-csv.zip"

