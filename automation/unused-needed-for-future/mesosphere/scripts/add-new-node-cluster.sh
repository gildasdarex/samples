#!/bin/bash


optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
      dcos.installer.link)
          dcos_installer_link="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      node.type)
          node_type="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done

#install docker
sudo yum check-update
curl -fsSL https://get.docker.com/ | sh
sudo systemctl start docker
sudo systemctl enable docker

#install dcos requirements
sudo yum install -y tar xz unzip curl ipset

#Add nogroup
sudo sed -i s/SELINUX=enforcing/SELINUX=permissive/g /etc/selinux/config
sudo groupadd nogroup
#sudo reboot

#insatll the node
sudo mkdir -p /opt/dcos_install_tmp
curl -0 "$dcos_installer_link"
sudo tar xf dcos-install.tar -C /opt/dcos_install_tmp

if [[ "${node_type}" == "public" ]]; then
    sudo bash /opt/dcos_install_tmp/dcos_install.sh slave_public
else
    sudo bash /opt/dcos_install_tmp/dcos_install.sh slave
fi