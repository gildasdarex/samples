#!/usr/bin/env bash


echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for influx folder"
echo "please ensure that you installed wget in your laptop "
echo "##############################################################"


echo "did you already give the permissions to elasticsearch folder and installed wget ? "

read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

read -p 'elasticUrl: ' elasticUrl
if [ "${elasticUrl}" == "" ]; then
    echo "please enter elastic search server url, format: http://{user}:{password}@{host}:{port}."
    exit 0
fi
read -p 'indexTemplateName: ' templateName
if [ "${templateName}" == "" ]; then
    echo "please enter template name..."
    exit 0
fi

read -p 'templateJsonFilePath: ' templatePath

if [ -f "${templatePath}" ]; then
        echo "picking file from ${templatePath}"
    else
        templatePath="./templates/index/sample/createTemplate.json"
        echo "picking default file from default path: ${templatePath}"
fi

function elasticsearch_url {
  echo -e "${elasticUrl}"
}

function createIndexTemplate {
    HTTP_RESPONSE="$(curl --silent --write-out "HTTPSTATUS:%{http_code}" -XPOST \
     -H 'Content-Type: application/json' -d @"${templatePath}" \
     "$(elasticsearch_url)/_template/${templateName}")"

    HTTP_BODY=$(echo $HTTP_RESPONSE | sed -e 's/HTTPSTATUS\:.*//g')

    # extract the status
    HTTP_STATUS=$(echo $HTTP_RESPONSE | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')

    # print the body
    echo "$HTTP_BODY" | python -mjson.tool
}

function isIndexTemplateExists {
    response="$(curl --write-out %{http_code} --silent --output /dev/null -I -XHEAD "$(elasticsearch_url)/_template/${templateName}")"
    if [ "${response}" == 200 ];
    then
        echo "Template already exists please use update template"
        exit 0
    else
        echo 0
    fi
}

function startProcessing {
    isExists="$(isIndexTemplateExists)"
    if [ "${isExists}" == 0 ]; then
        echo "Template not exists creating new one..."
        echo ">>>>>>>>server_response<<<<<<<<<<"
        echo "$(createIndexTemplate)"
    fi
}

startProcessing