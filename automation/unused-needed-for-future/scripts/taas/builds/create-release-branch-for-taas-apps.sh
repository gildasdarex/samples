#!/usr/bin/env bash
echo "##############################################################"
echo "please ensure that you have give the right permissions(755 or 777) for release-builds folder"
echo "please ensure that you installed wget in your laptop /n"
echo "##############################################################"


echo "did you already give the permissions to release-builds folder and installed wget ? "
#ask to user if he would like to generate release
read -p "yes or no " setupY
if [[ "${setupY}" == "no" ]]
 then
  exit
fi

#init all parameters

scriptDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
parentdir="$(dirname "$scriptDir")"
scriptdir="$(dirname "$parentdir")"
releaseHelperDir="$parentdir/release-helpers"
propertiesFilesDir="$scriptdir/properties-files"


projects_properties="$propertiesFilesDir/project-set-release-analytics.properties"
projects_dependencies_name_properties="$propertiesFilesDir/projects-dependencies-name.properties"
projects_publish_name_properties="$propertiesFilesDir/projects-publish-names.properties"
branch="dev"

optspec=":-:"
while getopts "$optspec" optchar; do
    case "${OPTARG}" in
      local.repo)
          repository_dir="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      work.branch)
          branch="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      projects.properties)
          projects_properties="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      git.username)
          username="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      git.password)
          password="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      projects.dependencies.name)
          projects_dependencies_name_properties="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
      projects.publish.name)
          projects_publish_name_properties="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
          ;;
    esac
done


update_build_number_in_projects_properties () {
    # $1 parameter is the name of the project type
    # $2 is the value of the build number
    key="$1-last_build_num"
    sed -i '' "s/$key=.*/$key=$2/g" "$projects_properties"
}



projectTypeListAsArray=(analytics,taas)

# Step 5: build all apps
for projectType in ${projectTypeListAsArray[@]}
do
   buildNumber=$(${releaseHelperDir}/_get-project-type-build-number.sh $projectType $projects_properties)
   releaseNumber=$(${releaseHelperDir}/_get-project-type-release-number.sh $projectType $projects_properties)
   apps=$(${releaseHelperDir}/_get-project-type-apps.sh $projectType $projects_properties)
   appsArray=($apps)
   for app in ${appsArray[@]}
   do
      cd "$repository_dir/$app"

      releaseBranch="release_$releaseNumber"
      git checkout $branch
      #refresh all tags
      git fetch --tags --force
      # create the release branch from previous tags
      git checkout -b $releaseBranch "b$buildNumber"
      #git tag "b$buildNumber" $releaseBranch
      git push  origin $releaseBranch
   done
done