#!/usr/bin/env bash
project_dir=$1
buildNumber=$2
dependencyName=$3


update_version_in_gradle_propeties () {
  gradlePropertiesFilePath=$1
  echo " update $dependencyName build number with $buildNumber in $gradlePropertiesFilePath "
  sed -i "s/${dependencyName}=.*/${dependencyName}=${buildNumber}/g" "$gradlePropertiesFilePath"
}


# 1- scan all asklytics project under repository_dir folderand get gradle.properties file path
# 2- send gradle.properties file path to update_version_in_gradle_propeties function
get_all_gradle_properties_file_path () {
  ls  "$1" | while read -r file; do
      if [ "$file" == "gradle.properties" ]; then
         update_version_in_gradle_propeties "$1/$file"
      elif [[ -d "$1/$file" ]]; then
         get_all_gradle_properties_file_path "$1/${file}"
      fi
  done
}

get_all_gradle_properties_file_path $project_dir
