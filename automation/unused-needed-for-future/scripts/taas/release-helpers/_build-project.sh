#!/usr/bin/env bash
#########################################
# This script return the list of the dependencies name that need to be update for specific project.
#  data is read from projects-dependencies-name.properties file
# by example alCamelAwsVer alCamelAnalyticsVer alCamelInfraProvVer is return for infra-camel-service project

##########################################
repository_dir=$1
project=$2
projects_publish_name_properties=$3
#project_set_properties=$4
releaseHelperDir=$4

projectType=$(${releaseHelperDir}/_get-app-type.sh $project "$releaseHelperDir/app-type.properties")

check_if_project_successful_build () {
    # $1 parameter is the name of the project#
    #project is successuf build if we find his directory under local-maven-repo
    echo "------------------------------check if $1 build is sucessful----------------------------------------"
    appPublishNanes=$(${releaseHelperDir}/_get-project-module-publish-name.sh $project $projects_publish_name_properties)
    appPublishNanesArray=($appPublishNanes)

    for appPublishName in ${appPublishNanesArray[@]}
    do
      publishNameRepoPath="$repository_dir/local-maven-repo/com/asklytics/$appPublishName"
      if [ ! -d $publishNameRepoPath ]; then
       echo "build failed for project $1"
       echo "script will end due of this error"
       exit
      fi
    done
}

case "$projectType" in
   "spring")
      cd "$repository_dir/$project"
      echo "------------------------------ clean  $projectType project : $project ----------------------------------------"
      gradle clean
      echo "------------------------------ build  $projectType project: $project----------------------------------------"
      gradle build
      echo "------------------------------ publish  $projectType project: $project----------------------------------------"
      gradle publish
   ;;
   "spring-boot")
      cd "$repository_dir/$project"
      echo "------------------------------ clean  $projectType project: $project ----------------------------------------"
      gradle clean
      echo "------------------------------ build  $projectType project: $project ----------------------------------------"
      gradle build
      echo "------------------------------ publish $projectType project: $project ----------------------------------------"
      gradle publish
   ;;
   "grails")
      cd "$repository_dir/$project"
      echo "------------------------------ clean  $projectType project: $project ----------------------------------------"
      gradle clean
      echo "------------------------------ build  $projectType project: $project ----------------------------------------"
      gradle build
      echo "------------------------------ publish $projectType project: $project ----------------------------------------"
      gradle publish
   ;;
esac

check_if_project_successful_build $project