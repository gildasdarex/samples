#!/usr/bin/env bash
project=$1
version_json_files=$2

version=""

case "$project" in
   "commons") version=$(jq ".alCommonsVer" "$version_json_files")
   ;;
   "data-access") build=$(jq ".alDataAccessVer" "$build_json_files")
   ;;
   "entity-model") build=$(jq ".alEntityModelVer" "$build_json_files")
   ;;
   "data-collector-transformer") build=$(jq ".alDataCollectorTransformerVer" "$build_json_files")
   ;;
   "commons-ws") build=$(jq ".alCommonsWsVer" "$build_json_files")
   ;;
   "infra-security") version=$(jq ".alCommonsVer" "$version_json_files")
   ;;
   "auth-service") version=$(jq ".alCommonsVer" "$version_json_files")
   ;;
   "infra-rest-client") version=$(jq ".alRestClientVer" "$version_json_files")
   ;;
   "infra-service") version=$(jq ".alInfraVer" "$version_json_files")
   ;;
   "ui-data-service") version=$(jq ".alUiDataServiceAppVer" "$version_json_files")
   ;;
   "email-send-service") version=$(jq ".alMailerVer" "$version_json_files")
   ;;
   "analytics-service") version=$(jq ".alAnalyticsServiceVer" "$version_json_files")
   ;;
   "modeler") version=$(jq ".alAwsModelVer" "$version_json_files")
   ;;
   "aws-model") version=$(jq ".alAwsModelVer" "$version_json_files")
   ;;
   "topology-modeler") build=$(jq ".alTopologyModelerVer" "$build_json_files")
   ;;
   "extract-aws") version=$(jq ".alAwscloudwatchVer" "$version_json_files")
   ;;
   "metadata") build=$(jq ".alAwsmetadataVer" "$build_json_files")
   ;;
   "cloudwatch") build=$(jq ".alAwscloudwatchVer" "$build_json_files")
   ;;
   "infra-prov-service") version=$(jq ".alProvServiceVer" "$version_json_files")
   ;;
   "ml-api-app") version=$(jq ".alMlApiAppVer" "$version_json_files")
   ;;
   "infra-camel-service") version=$(jq ".alCamelInfraProvVer" "$version_json_files")
   ;;
   "camel-analytics") build=$(jq ".alCamelAnalyticsVer" "$build_json_files")
   ;;
   "camel-aws-extraction") build=$(jq ".alCamelAwsVer" "$build_json_files")
   ;;
   "camel-infra-prov") build=$(jq ".alCamelInfraProvVer" "$build_json_files")
   ;;
   "analytics-app") version=$(jq ".alAnalyticsAppVer" "$version_json_files")
   ;;
   "offer-survey-data-access") version=$(jq ".alOffersDataAccessVer" "$version_json_files")
   ;;
   "offer-survey-app") version=$(jq ".alOfferSurveyAppVer" "$version_json_files")
   ;;
esac

echo -e $version
