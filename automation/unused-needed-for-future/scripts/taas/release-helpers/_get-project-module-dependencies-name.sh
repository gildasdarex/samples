#!/usr/bin/env bash
#########################################
# This script return the list of the dependencies name that need to be update for specific project.
#  data is read from projects-dependencies-name.properties file
# by example alCamelAwsVer alCamelAnalyticsVer alCamelInfraProvVer is return for infra-camel-service project

##########################################
project=$1
projects_dependencies_name_properties=$2

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ "$line" == $project* ]]
    then
      dependencyNames=$(cut -d "=" -f 2 <<< "$line")
      echo -e $dependencyNames
      exit
    fi
done < "$projects_dependencies_name_properties"