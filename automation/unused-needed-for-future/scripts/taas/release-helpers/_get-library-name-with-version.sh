#!/usr/bin/env bash
#########################################
# This script return the list of the dependencies name that need to be update for specific project.
#  data is read from app-type.properties file
# by grails is return for ml-api-app project

##########################################
library=$1
libNameWithVersion=$(echo $library | awk 'BEGIN {FS=".jar" } ; { print $1 }')
echo $libNameWithVersion