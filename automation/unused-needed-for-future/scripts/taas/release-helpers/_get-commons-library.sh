#!/usr/bin/env bash
#########################################
# This script return the list of the dependencies name that need to be update for specific project.
#  data is read from app-type.properties file
# by grails is return for ml-api-app project

##########################################
libraryToSearch=$1
tomcatLibPath="$2/lib"
tomcatLibraryArray=$(ls $tomcatLibPath)

for lib in $tomcatLibraryArray
do
    if [[ "$lib" == $libraryToSearch* ]]
    then
      result="$result $lib"
    fi
done

echo -e $result