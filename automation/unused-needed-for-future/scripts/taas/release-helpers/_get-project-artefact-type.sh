#!/usr/bin/env bash
#########################################
# This script return the list of the aaps according to projects type.
#  data is read from projects.properties file
# by example ml-api-app analytics-app analytics-service is return for grails_ui type

##########################################
project=$1
project_artefact_type_properties=$2


while IFS='' read -r line || [[ -n "$line" ]]; do
    projectName=$(cut -d "=" -f 1 <<< "$line")
    if [[ "$project" == $projectName ]]
    then
      artefactType=$(cut -d "=" -f 2 <<< "$line")
      echo -e $artefactType
      exit
    fi
done < "$project_artefact_type_properties"