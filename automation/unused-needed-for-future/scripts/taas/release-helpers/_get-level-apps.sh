#!/usr/bin/env bash
#########################################
# This script return the list of the dependencies name that need to be update for specific project.
#  data is read from app-type.properties file
# by grails is return for ml-api-app project

##########################################
level=$1
deployment_level_properties=$2

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ "$line" == *$level* ]]
    then
      appLevel=$(cut -d "=" -f 2 <<< "$line")
      echo -e $appLevel
      exit
    fi
done < "$deployment_level_properties"