#!/bin/bash

/root/.sdkman/candidates/java/current/jre/bin/java -jar -Xms64m -Xmx512m -Dlogging.config=/development/properties/config/log4j2/ml-api.xml -Dsentry.properties.file=/development/properties/sentry/ml-api/sentry.properties -Dlog.path=/var/log/ml-api /development/wars/asklytics-ml-api-app-1.0.8.war --spring.config.location=/development/properties/spring/ --spring.config.name=ml-api-application
