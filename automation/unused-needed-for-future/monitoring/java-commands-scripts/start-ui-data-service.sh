#!/bin/bash

/root/.sdkman/candidates/java/current/jre/bin/java  -jar -Dlogging.config=/development/properties/config/log4j2/ui-data-service.xml -Dsentry.properties.file=/development/properties/sentry/ui-data-service/sentry.properties -Dlog.path=/var/log/data-service /development/wars/asklytics-ui-data-service-1.0.8.war --spring.config.location=/development/properties/spring/ --spring.config.name=data-service-application

