#!/bin/bash

/root/.sdkman/candidates/java/current/jre/bin/java -jar -Xms64m -Xmx512m -Dlogging.config=/development/properties/config/log4j2/analytics-service.xml -Dsentry.properties.file=/development/properties/sentry/analytics-service/sentry.properties -Dlog.path=/var/log/analytics-service /development/wars/asklytics-analytics-service-1.0.8.war --spring.config.location=/development/properties/spring/ --spring.config.name=analytics-service-application

