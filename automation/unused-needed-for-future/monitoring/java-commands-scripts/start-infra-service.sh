#!/bin/bash


/root/.sdkman/candidates/java/current/jre/bin/java -jar -Dlogging.config=/development/properties/config/log4j2/infra-service.xml -Dsentry.properties.file=/development/properties/sentry/infra-service/sentry.properties -Dlog.path=/var/log/infra-service /development/wars/asklytics-infra-1.0.8.war --spring.config.location=/development/properties/spring/ --spring.config.name=infra-service-application
autostart=true
