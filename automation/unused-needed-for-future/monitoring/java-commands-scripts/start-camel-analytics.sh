#!/bin/bash

/root/.sdkman/candidates/java/current/jre/bin/java -jar  -Dlogging.config=/development/properties/config/log4j2/camel-analytics.xml -Dsentry.properties.file=/development/properties/sentry/camel-analytics/sentry.properties -Dlog.path=/var/log/camel-analytics /development/wars/asklytics-camel-analytics-service-1.0.8.war --spring.config.location=/development/properties/spring/ --spring.config.name=came-analytics-application
