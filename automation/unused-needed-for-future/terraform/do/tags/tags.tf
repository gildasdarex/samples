provider "digitalocean" {
  token = "${var.digitalocean_token}"
}

resource "digitalocean_tag" "dsc_master_tag" {
  name = "master"
}

resource "digitalocean_tag" "dsc_worker_tag" {
  name = "worker"
}