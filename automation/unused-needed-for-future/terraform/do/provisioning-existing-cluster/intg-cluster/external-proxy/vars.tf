variable "do_token" {
  type        = "string"
  description = "Digital ocean api token"
}

variable "provision_user" {
  type        = "string"
}

variable "connection_timeout" {
  type        = "string"
}


variable "nginx_default_file" {
  type        = "string"
}

variable "nginx_config_file" {
  type        = "string"
}

variable "blockips_file" {
  type        = "string"
}

variable "provision_ssh_key" {
  type        = "string"
}

variable "provision_user_password" {
  type        = "string"
}

