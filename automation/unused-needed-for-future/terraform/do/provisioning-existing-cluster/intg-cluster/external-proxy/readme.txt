1- in deployment-files directory :
  - update blockips.conf file with the list of ip to deny
  - update external-proxy-nginx-default.template file the right domain name for each application(Check TODO list)
  - update nginx.conf

2- change terraform.tfvars.sample to terraform.tfvars
3- Update the value of properties:
  - do_token (digital ocean api token)
  - provision_user (digital ocean server user- dont change it)
  - provision_user_password (root password)
  - provision_ssh_key (ssh key to use to connect to server)
  - nginx_default_file (full path to external-proxy-nginx-default.template)
  - nginx_config_file (full path to nginx.conf)
  - blockips_file (full path to blockips.conf)

4- Run:
  - terraform init
  - terraform plan
  - terraform apply

