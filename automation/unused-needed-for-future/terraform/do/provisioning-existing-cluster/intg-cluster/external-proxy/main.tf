
data "digitalocean_droplet" "external_proxy" {
  name = "external-proxy"
}


resource "null_resource" "internal_proxy_deployment" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", data.digitalocean_droplet.external_proxy.*.id)}"
  }

  # Bootstrap script can run on any instance of the cluster
  # So we just choose the first in this case
  connection {
    host = "${data.digitalocean_droplet.external_proxy.ipv4_address}"
    type        = "ssh"
    user        = "${var.provision_user}"
    private_key = "${file("${var.provision_ssh_key}")}"
    #password = "${var.provision_user_password}"
    timeout     = "${var.connection_timeout}"
  }



  provisioner "remote-exec" {
    inline = [
      "cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak_$(date +%s)"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "cp /etc/nginx/blockips.conf /etc/nginx/blockips.conf.bak_$(date +%s)"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak_$(date +%s)"
    ]
  }


  provisioner "file" {
    source      = "${var.nginx_config_file}"
    destination = "/etc/nginx/nginx.conf"
  }

  provisioner "file" {
    source      = "${var.blockips_file}"
    destination = "/etc/nginx/blockips.conf"
  }

  provisioner "file" {
    source      = "${var.nginx_default_file}"
    destination = "/etc/nginx/sites-available/default"
  }


  provisioner "remote-exec" {
    inline = [
      "service nginx restart"
    ]
  }


}