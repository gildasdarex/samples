variable "do_token" {
  type        = "string"
  description = "Digital ocean api token"
}

variable "provision_user" {
  type        = "string"
}

variable "connection_timeout" {
  type        = "string"
}

variable "provision_ssh_key" {
  type        = "string"

}

variable "provision_user_password" {
  type        = "string"
}


variable "elk_compose_files_source_dir" {
  type        = "string"
  default     = "./deployment-files"
}

variable "elk_compose_files_dest_dir" {
  type        = "string"
  default     = "/workdir/elk/deployment-files"
}



variable "nfs_directories" {
  type = "map"

  default = {
    elasticsearch_log = ""
    elasticsearch_db = "/asklytics/elk/db/elasticsearch/es"
    elasticsearch_master_01_db = "/asklytics/elk/db/elasticsearch/master-01"
    elasticsearch_master_02_db = "/asklytics/elk/db/elasticsearch/master-02"
    elasticsearch_master_03_db = "/asklytics/elk/db/elasticsearch/master-03"
    elasticsearch_data_db = "/asklytics/elk/db/elasticsearch/data-node"
    logstash_config = "/asklytics/elk/config/logstash"
    logstash_pipeline = "/asklytics/elk/config/pipelines"
    kibana_config = "/asklytics/elk/config/kibana"
    curator_config = "/asklytics/elk/config/curator"
    curator_logs = "/asklytics/logs/curator"
  }


}