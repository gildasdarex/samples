#!/bin/bash

source intg-env.cnf
docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always elk
