data "digitalocean_droplet" "dsclw_manager_01" {
  name = "dsclm-01-app-layer"
}

data "digitalocean_droplet" "nfs_storage" {
  name = "nfs-server-nyc1"
}


resource "null_resource" "main_manager_elk_deployment" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", data.digitalocean_droplet.dsclw_manager_01.*.id)}"
  }

  # Bootstrap script can run on any instance of the cluster
  # So we just choose the first in this case
  /*connection {
    host = "${data.digitalocean_droplet.dsclw_manager_01.ipv4_address}"
    type        = "ssh"
    user        = "${var.provision_user}"
    private_key = "${file("${var.provision_ssh_key}")}"
    timeout     = "${var.connection_timeout}"
  }*/

  provisioner "local-exec" {
    command = "scripts/set_env.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.elk_compose_files_source_dir}/intg-env.cnf"
  }

}

resource "null_resource" "nfs_server_step_1" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", data.digitalocean_droplet.nfs_storage.*.id)}"
  }

  depends_on = [ "null_resource.main_manager_elk_deployment" ]

  # Bootstrap script can run on any instance of the cluster
  # So we just choose the first in this case
  connection {
    host = "${data.digitalocean_droplet.nfs_storage.ipv4_address}"
    type        = "ssh"
    user        = "${var.provision_user}"
    password = "${var.provision_user_password}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /workdir/scripts/elk",
      "chmod -R 777 /workdir/scripts/elk/"
    ]
  }

  provisioner "file" {
    source     = "scripts/create_directories.sh"
    destination = "/workdir/scripts/elk/create-directories.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /workdir/scripts/elk/create-directories.sh",
      #"/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["elasticsearch_log"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["elasticsearch_db"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["elasticsearch_master_01_db"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["elasticsearch_master_02_db"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["elasticsearch_master_03_db"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["elasticsearch_data_node_db"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["logstash_config"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["logstash_pipeline"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["kibana_config"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["curator_config"]}",
      "/workdir/scripts/elk/create-directories.sh ${var.nfs_directories["curator_logs"]}"
    ]
  }


  provisioner "file" {
    source      = "${var.elk_compose_files_source_dir}/logstash/config/"
    destination = "${var.nfs_directories["logstash_config"]}"
  }

  provisioner "file" {
    source      = "${var.elk_compose_files_source_dir}/logstash/pipelines/"
    destination = "${var.nfs_directories["logstash_pipeline"]}/"
  }

  provisioner "file" {
    source      = "${var.elk_compose_files_source_dir}/kibana/config/"
    destination = "${var.nfs_directories["kibana_config"]}/"
  }

  provisioner "file" {
    source      = "${var.elk_compose_files_source_dir}/curator/config/"
    destination = "${var.nfs_directories["curator_config"]}"
  }

}


resource "null_resource" "deployment" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", data.digitalocean_droplet.dsclw_manager_01.*.id)}"
  }

  depends_on = [ "null_resource.nfs_server_step_1" ]

  # Bootstrap script can run on any instance of the cluster
  # So we just choose the first in this case
  connection {
    host = "${data.digitalocean_droplet.dsclw_manager_01.ipv4_address}"
    type        = "ssh"
    user        = "${var.provision_user}"
    private_key = "${file("${var.provision_ssh_key}")}"
    timeout     = "${var.connection_timeout}"
  }


  provisioner "remote-exec" {
    inline = [
      "rm -R ${var.elk_compose_files_dest_dir}",
      "mkdir -p ${var.elk_compose_files_dest_dir}",
      "chmod -R 777 ${var.elk_compose_files_dest_dir}"
    ]
  }

  provisioner "file" {
    source      = "${var.elk_compose_files_source_dir}/"
    destination = "${var.elk_compose_files_dest_dir}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x ${var.elk_compose_files_dest_dir}/deploy-elk.sh",
      "cd ${var.elk_compose_files_dest_dir}",
      "${var.elk_compose_files_dest_dir}/deploy-elk.sh"
    ]
  }

}
