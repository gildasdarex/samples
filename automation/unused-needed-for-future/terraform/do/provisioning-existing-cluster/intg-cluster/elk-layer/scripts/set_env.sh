#!/usr/bin/env bash
manager_node_public_ip=$1
manager_node_private_ip=$2
file_path=$3

sed -i "" "s/manager_node_public_ip/$manager_node_public_ip/g" $file_path
sed -i "" "s/manager_node_private_ip/$manager_node_private_ip/g" $file_path


#sed -i "s/node_private_ip/$node_ip/g" $file_path
#sed -i ""  's/influxdb_node_private_ip/10.02.01.09/g' influxdb-targets.json