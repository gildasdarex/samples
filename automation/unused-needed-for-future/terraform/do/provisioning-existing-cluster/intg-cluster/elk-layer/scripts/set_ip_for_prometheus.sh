#!/usr/bin/env bash
node_ip=$1
file_path=$2
app_type=$3

case $app_type in
   "spring")   sed -i "s/spring_manager_node_private_ip/$node_ip/g" $file_path;;
   "python")   sed -i "s/python_manager_node_private_ip/$node_ip/g" $file_path;;
   "influxdb") sed -i "s/influxdb_manager_node_private_ip/$node_ip/g" $file_path;;
   "ui")       sed -i "s/ui_manager_node_private_ip/$node_ip/g" $file_path;;
   "es")       sed -i "s/es_manager_node_private_ip/$node_ip/g" $file_path;;
   "ext_proxy")    sed -i "s/ext_proxy_private_ip/$node_ip/g" $file_path;;
   "int_proxy")    sed -i "s/int_proxy_private_ip/$node_ip/g" $file_path;;
   *) echo "Sorry, I can not get a $rental rental  for you!";;
esac

#sed -i "s/node_private_ip/$node_ip/g" $file_path
#sed -i ""  's/influxdb_node_private_ip/10.02.01.09/g' influxdb-targets.json