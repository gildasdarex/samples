1- in deployment-files directory :
  - update intg-env.cnf (Check TODO list)

2- change terraform.tfvars.sample to terraform.tfvars

3- Update the value of properties:
  - do_token (digital ocean api token)
  - provision_user (digital ocean server user- dont change it)
  - provision_user_password (root password)
  - provision_ssh_key (ssh key to use to connect to server)
  - monitoring_compose_files_source_dir (full path to deployment-files directory)
  - nfs_directories (mount directory in nfs server for config and db for each app. dont update those values  )


4- Run:
  - terraform init
  - terraform plan
  - terraform apply

