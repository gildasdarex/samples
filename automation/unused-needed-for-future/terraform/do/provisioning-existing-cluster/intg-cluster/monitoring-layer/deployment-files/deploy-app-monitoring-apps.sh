#!/bin/bash

echo "Using Docker Registry 157.230.155.170:5000 ..."
export REGISTRY_URL=157.230.155.170:5000
source intg-env.cnf
docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always mon
