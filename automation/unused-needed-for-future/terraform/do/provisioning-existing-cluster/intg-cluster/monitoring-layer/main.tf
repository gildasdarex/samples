data "digitalocean_droplet" "dsclw_manager_01" {
  name = "dsclm-01-app-layer"
}

data "digitalocean_droplet" "nfs_storage" {
  name = "nfs-server-nyc1"
}


resource "null_resource" "main_manager_monitoring_deployment" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", data.digitalocean_droplet.dsclw_manager_01.*.id)}"
  }

  # Bootstrap script can run on any instance of the cluster
  # So we just choose the first in this case
  connection {
    host = "${data.digitalocean_droplet.dsclw_manager_01.ipv4_address}"
    type        = "ssh"
    user        = "${var.provision_user}"
    private_key = "${file("${var.provision_ssh_key}")}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "local-exec" {
    command = "scripts/set_ip_for_blackbox.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/prometheus/config/spring-app-targets.json spring"
  }

  provisioner "local-exec" {
    command = "scripts/set_ip_for_blackbox.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/prometheus/config/es-targets.json es"
  }

  provisioner "local-exec" {
    command = "scripts/set_ip_for_blackbox.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/prometheus/config/influxdb-targets.json influxdb"
  }

  provisioner "local-exec" {
    command = "scripts/set_ip_for_blackbox.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/prometheus/config/nginx-targets.json ext_proxy"
  }

  provisioner "local-exec" {
    command = "scripts/set_ip_for_blackbox.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/prometheus/config/nginx-targets.json int_proxy"
  }

  provisioner "local-exec" {
    command = "scripts/set_ip_for_prometheus.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/prometheus/config/python-app-targets.json python"
  }

  provisioner "local-exec" {
    command = "scripts/set_ip_for_blackbox.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/prometheus/config/ui-app-targets.json python"
  }

  provisioner "local-exec" {
    command = "scripts/set_ip_for_blackbox.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/prometheus/config/prometheus.yml python"
  }

  provisioner "local-exec" {
    command = "scripts/set_ip_for_prometheus.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/prometheus/config/prometheus.yml spring"
  }

  provisioner "local-exec" {
    command = "scripts/set_env.sh ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${data.digitalocean_droplet.dsclw_manager_01.ipv4_address_private} ${var.monitoring_compose_files_source_dir}/intg-env.cnf"
  }


  /*provisioner "file" {
    source      = "${var.monitoring_compose_files_source_dir}/prometheus/config"
    destination = "${var.monitoring_compose_files_dest_dirs["prometheus"]}/"
  }

  provisioner "file" {
    source      = "${var.monitoring_compose_files_source_dir}/blackbox-config"
    destination = "${var.monitoring_compose_files_dest_dirs["blackbox"]}/"
  }

  provisioner "file" {
    source      = "${var.monitoring_compose_files_source_dir}/alertmanager/conf"
    destination = "${var.monitoring_compose_files_dest_dirs["alertmanager"]}/"
  }*/

}

resource "null_resource" "nfs_server_step_1" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", data.digitalocean_droplet.nfs_storage.*.id)}"
  }

  depends_on = [ "null_resource.main_manager_monitoring_deployment" ]

  # Bootstrap script can run on any instance of the cluster
  # So we just choose the first in this case
  connection {
    host = "${data.digitalocean_droplet.nfs_storage.ipv4_address}"
    type        = "ssh"
    user        = "${var.provision_user}"
    password = "${var.provision_user_password}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "remote-exec" {
    inline = [
      "rm -R /workdir",
      "mkdir -p /workdir/scripts/",
      "chmod -R 777 /workdir/scripts/"
    ]
  }

  provisioner "file" {
    source     = "scripts/create_directories.sh"
    destination = "/workdir/scripts/create-directories.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /workdir/scripts/create-directories.sh",
      "/workdir/scripts/create-directories.sh ${var.nfs_directories["prometheus_db"]}",
      "/workdir/scripts/create-directories.sh ${var.nfs_directories["prometheus_config"]}",
      "/workdir/scripts/create-directories.sh ${var.nfs_directories["grafana_db"]}",
      "/workdir/scripts/create-directories.sh ${var.nfs_directories["alertmanager_db"]}",
      "/workdir/scripts/create-directories.sh ${var.nfs_directories["alertmanager_config"]}",
      "/workdir/scripts/create-directories.sh ${var.nfs_directories["blackbox_config"]}",
      "/workdir/scripts/create-directories.sh ${var.nfs_directories["dockerd_config"]}",
      "/workdir/scripts/create-directories.sh ${var.nfs_directories["rules"]}"
    ]
  }


  provisioner "file" {
    source      = "${var.monitoring_compose_files_source_dir}/prometheus/config/"
    destination = "${var.nfs_directories["prometheus_config"]}"
  }

  provisioner "file" {
    source      = "${var.monitoring_compose_files_source_dir}/blackbox-config/"
    destination = "${var.nfs_directories["blackbox_config"]}/"
  }

  provisioner "file" {
    source      = "${var.monitoring_compose_files_source_dir}/alertmanager/conf/"
    destination = "${var.nfs_directories["alertmanager_config"]}/"
  }

  provisioner "file" {
    source      = "${var.monitoring_compose_files_source_dir}/dockerd-exporter/Caddyfile"
    destination = "${var.nfs_directories["dockerd_config"]}/Caddyfile"
  }

  provisioner "file" {
    source      = "${var.monitoring_compose_files_source_dir}/prometheus/rules/"
    destination = "${var.nfs_directories["rules"]}"
  }

}


resource "null_resource" "deployment" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", data.digitalocean_droplet.dsclw_manager_01.*.id)}"
  }

  depends_on = [ "null_resource.main_manager_monitoring_deployment", "null_resource.nfs_server_step_1" ]

  # Bootstrap script can run on any instance of the cluster
  # So we just choose the first in this case
  connection {
    host = "${data.digitalocean_droplet.dsclw_manager_01.ipv4_address}"
    type        = "ssh"
    user        = "${var.provision_user}"
    private_key = "${file("${var.provision_ssh_key}")}"
    timeout     = "${var.connection_timeout}"
  }


  provisioner "remote-exec" {
    inline = [
      "rm -R ${var.monitoring_compose_files_dest_dir}",
      "mkdir -p ${var.monitoring_compose_files_dest_dir}"
    ]
  }

  provisioner "file" {
    source      = "${var.monitoring_compose_files_source_dir}/"
    destination = "${var.monitoring_compose_files_dest_dir}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod -R 777 ${var.monitoring_compose_files_dest_dir}",
      "chmod +x ${var.monitoring_compose_files_dest_dir}/alertmanager/conf/docker-entrypoint.sh",
      "chmod +x ${var.monitoring_compose_files_dest_dir}/prometheus/config/docker-entrypoint.sh"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x ${var.monitoring_compose_files_dest_dir}/deploy-app-monitoring-apps.sh",
      "cd ${var.monitoring_compose_files_dest_dir}/",
      "${var.monitoring_compose_files_dest_dir}/deploy-app-monitoring-apps.sh"
      #"export REGISTRY_URL=157.230.155.170:5000",
      #"source intg-env.cnf",
      #"docker stack deploy --with-registry-auth --compose-file docker-compose.yml --resolve-image always mon"
    ]
  }

}
