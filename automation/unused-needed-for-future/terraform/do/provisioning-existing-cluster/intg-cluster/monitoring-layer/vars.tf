variable "do_token" {
  type        = "string"
  description = "Digital ocean api token"
}

variable "provision_user" {
  type        = "string"
}

variable "provision_user_password" {
  type        = "string"
}

variable "provision_ssh_key" {
  type        = "string"

}

variable "connection_timeout" {
  type        = "string"
}

variable "monitoring_compose_files_source_dir" {
  type        = "string"
  default     = "./deployment-files"
}

variable "monitoring_compose_files_dest_dir" {
  type        = "string"
  default     = "/workdir/monitoring/deployment-files"
}



variable "nfs_directories" {
  type = "map"

  default = {
    prometheus_log = ""
    prometheus_db = "/asklytics/storage/mon/storage/mon/db/prometheus"
    prometheus_config = "/asklytics/monitoring/config/app/prometheus/config"
    grafana_db = "/asklytics/storage/mon/storage/mon/db/grafana"
    alertmanager_log = ""
    alertmanager_db = "/asklytics/storage/mon/storage/mon/db/alertmanager"
    alertmanager_config = "/asklytics/monitoring/config/app/alertmanager"
    blackbox_config = "/asklytics/monitoring/config/app/blackbox"
    dockerd_config = "/asklytics/monitoring/config/app/dockerd-exporter/"
    rules = "/asklytics/monitoring/config/app/prometheus/rules"
  }

}

variable "monitoring_compose_files_dest_dirs" {
  type = "map"

  default = {
    prometheus = ""
    alertmanager = ""
    blackbox = ""
    grafana = ""
  }

}