variable "connection_timeout" {
  description = "Timeout for connection to servers"
  default     = "2m"
}

variable "domain" {
  description = "Domain name used in droplet hostnames, e.g example.com"
}

variable "ssh_keys" {
  type        = "list"
  description = "A list of SSH IDs or fingerprints to enable in the format [12345, 123456] that are added to manager nodes"
}

variable "provision_ssh_key" {
  default     = "~/.ssh/id_rsa"
  description = "File path to SSH private key used to access the provisioned nodes. Ensure this key is listed in the manager and work ssh keys list"
}

variable "provision_user" {
  default     = "root"
  description = "User used to log in to the droplets via ssh for issueing Docker commands"
}

variable "region" {
  description = "Datacenter region in which the cluster will be created"
  default     = "ams3"
}


variable "image" {
  description = "Droplet image used for the manager nodes"
  default     = "docker-18-04"
}

variable "size" {
  description = "Droplet size of manager nodes"
  default     = "s-1vcpu-1gb"
}

variable "name" {
  description = "nfs server name"
  default     = "nfs"
}


variable "user_data" {
  description = "User data content for nfs node"

  default = <<EOF
  #!/bin/sh
EOF
}

variable "tags" {
  description = "List of DigitalOcean tag ids"
  default     = []
  type        = "list"
}

variable "mounted_directories" {
  description = "List of directories to create and mount"
  default     = []
  type        = "list"
}

variable "nfs_clients_private_ips" {
  description = "List of private ip of nfs client"
  default     = []
  type        = "list"
}

variable "nfs_clients_public_ips" {
  description = "List of public ip of nfs client"
  default     = []
  type        = "list"
}


variable "backups" {
  description = "Enable DigitalOcean droplet backups"
  default     = false
}

variable "mounted_directories_client" {
  type = "list"
  default = []
}

variable "is_new" {
  default = "true"
}