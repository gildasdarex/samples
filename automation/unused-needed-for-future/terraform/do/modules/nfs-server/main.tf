
resource "digitalocean_droplet" "nfs_server" {
  ssh_keys           = "${var.ssh_keys}"
  image              = "${var.image}"
  region             = "${var.region}"
  size               = "${var.size}"
  private_networking = true
  backups            = "${var.backups}"
  ipv6               = false
  tags               = ["${var.tags}"]
  user_data          = "${var.user_data}"
  name               = "${var.name}"

  count = "${var.is_new == "true"  ? 1 : 0}"

  connection {
    type        = "ssh"
    user        = "${var.provision_user}"
    private_key = "${file("${var.provision_ssh_key}")}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "file" {
    source     = "${path.module}/scripts/create-directories.sh"
    destination = "/workdir/scripts/create-directories.sh"
  }

  provisioner "file" {
    source     = "${path.module}/scripts/fill-export-file.sh"
    destination = "/workdir/scripts/fill-export-file.sh"
  }


  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install nfs-kernel-server",
    ]
  }

}


resource "null_resource" "nfs_server_setup" {
  count = "${length(var.nfs_clients_public_ips)}"

  triggers {
    nfs_server_setup = "${uuid()}"
  }

  connection {
    host        = "${digitalocean_droplet.nfs_server.0.ipv4_address}"
    type        = "ssh"
    user        = "${var.provision_user}"
    private_key = "${file("${var.provision_ssh_key}")}"
    timeout     = "${var.connection_timeout}"
  }


  provisioner "remote-exec" {
    inline = [
      "chmod +x /workdir/scripts/create-directories.sh",
      "/workdir/scripts/create-directories.sh ${join(",", var.mounted_directories)}"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /workdir/scripts/fill-export-file.sh",
      "/workdir/scripts/fill-export-file.sh ${join(",", var.mounted_directories)} ${join(",", var.nfs_clients_private_ips)}"
    ]
  }
}

