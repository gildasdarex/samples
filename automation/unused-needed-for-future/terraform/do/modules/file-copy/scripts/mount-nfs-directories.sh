#!/usr/bin/env bash

nfs_server_ip=$1
directories=$2
directories_client=$3

IFS=', ' read -r -a directories_array <<< "$directories"
IFS=', ' read -r -a directories_client_array <<< "$directories_client"

for index in "${!directories_array[@]}"
do
    sudo mount $nfs_server_ip:${directories_array[index]} ${directories_client_array[index]}
done
