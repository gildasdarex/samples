#!/usr/bin/env bash

directories=$1
nfs_clients_private_ips=$2

for private_ip in $(echo $nfs_clients_private_ips | sed "s/,/ /g")
do
    for dir in $(echo $directories | sed "s/,/ /g")
    do
       sudo echo "$dir  $private_ip(rw,sync,no_subtree_check)" >> /etc/exports
    done

    sudo ufw allow from $private_ip to any port nfs
    sudo echo "" >> /etc/exports
done

sudo systemctl restart nfs-kernel-server