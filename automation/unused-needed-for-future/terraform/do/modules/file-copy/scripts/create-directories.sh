#!/usr/bin/env bash

directories=$1

for i in $(echo $directories | sed "s/,/ /g")
do
    # call your procedure/other scripts here below
    mkdir -p "$i"
    sudo chown nobody:nogroup "$i"
done