variable "connection_timeout" {
  description = "Timeout for connection to servers"
  default     = "2m"
}

variable "ssh_keys" {
  type        = "list"
  description = "A list of SSH IDs or fingerprints to enable in the format [12345, 123456] that are added to manager nodes"
}

variable "provision_ssh_key" {
  default     = "~/.ssh/id_rsa"
  description = "File path to SSH private key used to access the provisioned nodes. Ensure this key is listed in the manager and work ssh keys list"
}

variable "provision_user" {
  default     = "root"
  description = "User used to log in to the droplets via ssh for issueing Docker commands"
}


variable "dsc_nodes_private_ips" {
  description = "List of private ip of nfs client"
  default     = []
  type        = "list"
}

variable "dsc_nodes_public_ips" {
  description = "List of public ip of nfs client"
  default     = []
  type        = "list"
}

variable "dsc_name" {
  description = "Name of your cluster. Alpha-numeric and hyphens only, please."
  default     = "digitalocean-dcs"
}


variable "dirs" {
  type = "list"
  default = [
    {
      source = ""
      destination = ""
    }
  ]
}