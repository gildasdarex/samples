
resource "null_resource" "rerun" {
  count = "${length(var.dsc_nodes_private_ips)}"

  triggers {
    rerun = "${uuid()}"
  }

  connection {
    host        = "${element(var.dsc_nodes_private_ips, count.index)}"
    type        = "ssh"
    user        = "${var.provision_user}"
    private_key = "${file("${var.provision_ssh_key}")}"
    timeout     = "${var.connection_timeout}"
  }


  provisioner "file" {
    source      = "${path.module}/scripts/mount-nfs-directories.sh"
    destination = "/workdir/scripts/mount-nfs-directories.sh"
  }


}

