
resource "null_resource" "nfs_client" {
  count = "${length(var.nfs_clients_public_ips)}"

  triggers {
    nfs_client = "${uuid()}"
  }

  connection {
    host        = "${element(var.nfs_clients_public_ips, count.index)}"
    type        = "ssh"
    user        = "${var.provision_user}"
    private_key = "${file("${var.provision_ssh_key}")}"
    timeout     = "${var.connection_timeout}"
  }


  provisioner "file" {
    source      = "${path.module}/scripts/mount-nfs-directories.sh"
    destination = "/workdir/scripts/mount-nfs-directories.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /workdir/scripts/mount-nfs-directories.sh",
      "/workdir/scripts/mount-nfs-directories.sh ${var.nfs_server_private_ip} ${join(",", var.nfs_server_directories)} ${join(",", var.nfs_client_directories)} ",
    ]
  }
}

