variable "connection_timeout" {
  description = "Timeout for connection to servers"
  default     = "2m"
}

variable "provision_ssh_key" {
  default     = "~/.ssh/id_rsa"
  description = "File path to SSH private key used to access the provisioned nodes. Ensure this key is listed in the manager and work ssh keys list"
}

variable "provision_user" {
  default     = "root"
  description = "User used to log in to the droplets via ssh for issueing Docker commands"
}

variable "nfs_client_directories" {
  description = "List of directories to create and mount in  nfs client"
  default     = []
  type        = "list"
}

variable "nfs_server_private_ip" {
  description = "nfs server private ip"
  default     = []
  type        = "list"
}

variable "nfs_server_public_ip" {
  description = "nfs server public ip"
  default     = []
  type        = "list"
}

variable "nfs_clients_private_ips" {
  description = "List of private ip of nfs client"
  default     = []
  type        = "list"
}

variable "nfs_clients_public_ips" {
  description = "List of public ip of nfs client"
  default     = []
  type        = "list"
}

variable "nfs_server_directories" {
  description = "List of directories to create and mount in  nfs server"
  default     = []
  type        = "list"
}


variable "dirs" {
  type = "list"
  default = [
    {
      nfs_server_dir = ""
      nfs_client_dir = ""
    }
  ]
}